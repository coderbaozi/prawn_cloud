package com.prawn.ecommerce.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.prawn.ecommerce.pojo.Address;
/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface AddressDao extends JpaRepository<Address,String>,JpaSpecificationExecutor<Address>{
	
}
