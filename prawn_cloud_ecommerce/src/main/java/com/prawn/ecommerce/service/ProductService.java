package com.prawn.ecommerce.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import util.IdWorker;

import com.prawn.ecommerce.dao.ProductDao;
import com.prawn.ecommerce.pojo.Product;

/**
 * 服务层
 * 
 * @author Administrator
 *
 */
@Service
public class ProductService {

	@Autowired
	private ProductDao productDao;
	
	@Autowired
	private IdWorker idWorker;

	/**
	 * 查询全部列表
	 * @return
	 */
	public List<Product> findAll() {
		return productDao.findAll();
	}

	
	/**
	 * 条件查询+分页
	 * @param whereMap
	 * @param page
	 * @param size
	 * @return
	 */
	public Page<Product> findSearch(Map whereMap, int page, int size) {
		Specification<Product> specification = createSpecification(whereMap);
		PageRequest pageRequest =  PageRequest.of(page-1, size);
		return productDao.findAll(specification, pageRequest);
	}

	
	/**
	 * 条件查询
	 * @param whereMap
	 * @return
	 */
	public List<Product> findSearch(Map whereMap) {
		Specification<Product> specification = createSpecification(whereMap);
		return productDao.findAll(specification);
	}

	/**
	 * 根据ID查询实体
	 * @param id
	 * @return
	 */
	public Product findById(String id) {
		return productDao.findById(id).get();
	}

	/**
	 * 增加
	 * @param product
	 */
	public void add(Product product) {
		product.setProductId( idWorker.nextId()+"" );
		productDao.save(product);
	}

	/**
	 * 修改
	 * @param product
	 */
	public void update(Product product) {
		productDao.save(product);
	}

	/**
	 * 删除
	 * @param id
	 */
	public void deleteById(String id) {
		productDao.deleteById(id);
	}

	/**
	 * 动态条件构建
	 * @param searchMap
	 * @return
	 */
	private Specification<Product> createSpecification(Map searchMap) {

		return new Specification<Product>() {

			@Override
			public Predicate toPredicate(Root<Product> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<Predicate>();
                // 商品id
                if (searchMap.get("productId")!=null && !"".equals(searchMap.get("productId"))) {
                	predicateList.add(cb.like(root.get("productId").as(String.class), "%"+(String)searchMap.get("productId")+"%"));
                }
                // 商品名称
                if (searchMap.get("productName")!=null && !"".equals(searchMap.get("productName"))) {
                	predicateList.add(cb.like(root.get("productName").as(String.class), "%"+(String)searchMap.get("productName")+"%"));
                }
                // 商品标题
                if (searchMap.get("productTitle")!=null && !"".equals(searchMap.get("productTitle"))) {
                	predicateList.add(cb.like(root.get("productTitle").as(String.class), "%"+(String)searchMap.get("productTitle")+"%"));
                }
                // 单位
                if (searchMap.get("productUnit")!=null && !"".equals(searchMap.get("productUnit"))) {
                	predicateList.add(cb.like(root.get("productUnit").as(String.class), "%"+(String)searchMap.get("productUnit")+"%"));
                }
                // 商品条形码
                if (searchMap.get("productBarcode")!=null && !"".equals(searchMap.get("productBarcode"))) {
                	predicateList.add(cb.like(root.get("productBarcode").as(String.class), "%"+(String)searchMap.get("productBarcode")+"%"));
                }
                // 商品图片
                if (searchMap.get("productImages")!=null && !"".equals(searchMap.get("productImages"))) {
                	predicateList.add(cb.like(root.get("productImages").as(String.class), "%"+(String)searchMap.get("productImages")+"%"));
                }
                // 商品类别
                if (searchMap.get("categoryId")!=null && !"".equals(searchMap.get("categoryId"))) {
                	predicateList.add(cb.like(root.get("categoryId").as(String.class), "%"+(String)searchMap.get("categoryId")+"%"));
                }
                // 商品产地
                if (searchMap.get("productArea")!=null && !"".equals(searchMap.get("productArea"))) {
                	predicateList.add(cb.like(root.get("productArea").as(String.class), "%"+(String)searchMap.get("productArea")+"%"));
                }
                // 商铺id
                if (searchMap.get("merchantId")!=null && !"".equals(searchMap.get("merchantId"))) {
                	predicateList.add(cb.like(root.get("merchantId").as(String.class), "%"+(String)searchMap.get("merchantId")+"%"));
                }
				
				return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

			}
		};

	}

}
