package com.prawn.ecommerce.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import util.IdWorker;

import com.prawn.ecommerce.dao.MerchantDao;
import com.prawn.ecommerce.pojo.Merchant;

/**
 * 服务层
 * 
 * @author Administrator
 *
 */
@Service
public class MerchantService {

	@Autowired
	private MerchantDao merchantDao;
	
	@Autowired
	private IdWorker idWorker;

	/**
	 * 查询全部列表
	 * @return
	 */
	public List<Merchant> findAll() {
		return merchantDao.findAll();
	}

	
	/**
	 * 条件查询+分页
	 * @param whereMap
	 * @param page
	 * @param size
	 * @return
	 */
	public Page<Merchant> findSearch(Map whereMap, int page, int size) {
		Specification<Merchant> specification = createSpecification(whereMap);
		PageRequest pageRequest =  PageRequest.of(page-1, size);
		return merchantDao.findAll(specification, pageRequest);
	}

	
	/**
	 * 条件查询
	 * @param whereMap
	 * @return
	 */
	public List<Merchant> findSearch(Map whereMap) {
		Specification<Merchant> specification = createSpecification(whereMap);
		return merchantDao.findAll(specification);
	}

	/**
	 * 根据ID查询实体
	 * @param id
	 * @return
	 */
	public Merchant findById(String id) {
		return merchantDao.findById(id).get();
	}

	/**
	 * 增加
	 * @param merchant
	 */
	public void add(Merchant merchant) {
		merchant.setMerchantId( idWorker.nextId()+"" );
		merchantDao.save(merchant);
	}

	/**
	 * 修改
	 * @param merchant
	 */
	public void update(Merchant merchant) {
		merchantDao.save(merchant);
	}

	/**
	 * 删除
	 * @param id
	 */
	public void deleteById(String id) {
		merchantDao.deleteById(id);
	}

	/**
	 * 动态条件构建
	 * @param searchMap
	 * @return
	 */
	private Specification<Merchant> createSpecification(Map searchMap) {

		return new Specification<Merchant>() {

			@Override
			public Predicate toPredicate(Root<Merchant> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<Predicate>();
                // 商铺id
                if (searchMap.get("merchantId")!=null && !"".equals(searchMap.get("merchantId"))) {
                	predicateList.add(cb.like(root.get("merchantId").as(String.class), "%"+(String)searchMap.get("merchantId")+"%"));
                }
                // 商铺名称
                if (searchMap.get("merchantName")!=null && !"".equals(searchMap.get("merchantName"))) {
                	predicateList.add(cb.like(root.get("merchantName").as(String.class), "%"+(String)searchMap.get("merchantName")+"%"));
                }
                // 联系电话
                if (searchMap.get("merchantPhone")!=null && !"".equals(searchMap.get("merchantPhone"))) {
                	predicateList.add(cb.like(root.get("merchantPhone").as(String.class), "%"+(String)searchMap.get("merchantPhone")+"%"));
                }
                // 商铺logo
                if (searchMap.get("merchantLogo")!=null && !"".equals(searchMap.get("merchantLogo"))) {
                	predicateList.add(cb.like(root.get("merchantLogo").as(String.class), "%"+(String)searchMap.get("merchantLogo")+"%"));
                }
                // 商铺二维码
                if (searchMap.get("merchantQrcode")!=null && !"".equals(searchMap.get("merchantQrcode"))) {
                	predicateList.add(cb.like(root.get("merchantQrcode").as(String.class), "%"+(String)searchMap.get("merchantQrcode")+"%"));
                }
                // 省份
                if (searchMap.get("merchantProvince")!=null && !"".equals(searchMap.get("merchantProvince"))) {
                	predicateList.add(cb.like(root.get("merchantProvince").as(String.class), "%"+(String)searchMap.get("merchantProvince")+"%"));
                }
                // 市
                if (searchMap.get("merchantCity")!=null && !"".equals(searchMap.get("merchantCity"))) {
                	predicateList.add(cb.like(root.get("merchantCity").as(String.class), "%"+(String)searchMap.get("merchantCity")+"%"));
                }
                // 区/县
                if (searchMap.get("merchantTown")!=null && !"".equals(searchMap.get("merchantTown"))) {
                	predicateList.add(cb.like(root.get("merchantTown").as(String.class), "%"+(String)searchMap.get("merchantTown")+"%"));
                }
                // 详细地址
                if (searchMap.get("merchantAddress")!=null && !"".equals(searchMap.get("merchantAddress"))) {
                	predicateList.add(cb.like(root.get("merchantAddress").as(String.class), "%"+(String)searchMap.get("merchantAddress")+"%"));
                }
                // 商家介绍
                if (searchMap.get("merchantDesc")!=null && !"".equals(searchMap.get("merchantDesc"))) {
                	predicateList.add(cb.like(root.get("merchantDesc").as(String.class), "%"+(String)searchMap.get("merchantDesc")+"%"));
                }
                // 商家备注
                if (searchMap.get("merchanRemarks")!=null && !"".equals(searchMap.get("merchanRemarks"))) {
                	predicateList.add(cb.like(root.get("merchanRemarks").as(String.class), "%"+(String)searchMap.get("merchanRemarks")+"%"));
                }
                // 创建者
                if (searchMap.get("createBy")!=null && !"".equals(searchMap.get("createBy"))) {
                	predicateList.add(cb.like(root.get("createBy").as(String.class), "%"+(String)searchMap.get("createBy")+"%"));
                }
				
				return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

			}
		};

	}

}
