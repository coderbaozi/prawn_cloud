package com.prawn.ecommerce.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import util.IdWorker;

import com.prawn.ecommerce.dao.AddressDao;
import com.prawn.ecommerce.pojo.Address;

/**
 * 服务层
 * 
 * @author Administrator
 *
 */
@Service
public class AddressService {

	@Autowired
	private AddressDao addressDao;
	
	@Autowired
	private IdWorker idWorker;

	/**
	 * 查询全部列表
	 * @return
	 */
	public List<Address> findAll() {
		return addressDao.findAll();
	}

	
	/**
	 * 条件查询+分页
	 * @param whereMap
	 * @param page
	 * @param size
	 * @return
	 */
	public Page<Address> findSearch(Map whereMap, int page, int size) {
		Specification<Address> specification = createSpecification(whereMap);
		PageRequest pageRequest =  PageRequest.of(page-1, size);
		return addressDao.findAll(specification, pageRequest);
	}

	
	/**
	 * 条件查询
	 * @param whereMap
	 * @return
	 */
	public List<Address> findSearch(Map whereMap) {
		Specification<Address> specification = createSpecification(whereMap);
		return addressDao.findAll(specification);
	}

	/**
	 * 根据ID查询实体
	 * @param id
	 * @return
	 */
	public Address findById(String id) {
		return addressDao.findById(id).get();
	}

	/**
	 * 增加
	 * @param address
	 */
	public void add(Address address) {
		address.setAddressId( idWorker.nextId()+"" );
		addressDao.save(address);
	}

	/**
	 * 修改
	 * @param address
	 */
	public void update(Address address) {
		addressDao.save(address);
	}

	/**
	 * 删除
	 * @param id
	 */
	public void deleteById(String id) {
		addressDao.deleteById(id);
	}

	/**
	 * 动态条件构建
	 * @param searchMap
	 * @return
	 */
	private Specification<Address> createSpecification(Map searchMap) {

		return new Specification<Address>() {

			@Override
			public Predicate toPredicate(Root<Address> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<Predicate>();
                // 收货地址id
                if (searchMap.get("addressId")!=null && !"".equals(searchMap.get("addressId"))) {
                	predicateList.add(cb.like(root.get("addressId").as(String.class), "%"+(String)searchMap.get("addressId")+"%"));
                }
                // 用户id
                if (searchMap.get("userId")!=null && !"".equals(searchMap.get("userId"))) {
                	predicateList.add(cb.like(root.get("userId").as(String.class), "%"+(String)searchMap.get("userId")+"%"));
                }
                // 收货人姓名
                if (searchMap.get("receiverName")!=null && !"".equals(searchMap.get("receiverName"))) {
                	predicateList.add(cb.like(root.get("receiverName").as(String.class), "%"+(String)searchMap.get("receiverName")+"%"));
                }
                // 联系电话
                if (searchMap.get("receiverPhone")!=null && !"".equals(searchMap.get("receiverPhone"))) {
                	predicateList.add(cb.like(root.get("receiverPhone").as(String.class), "%"+(String)searchMap.get("receiverPhone")+"%"));
                }
                // 省份
                if (searchMap.get("receiverProvince")!=null && !"".equals(searchMap.get("receiverProvince"))) {
                	predicateList.add(cb.like(root.get("receiverProvince").as(String.class), "%"+(String)searchMap.get("receiverProvince")+"%"));
                }
                // 城市
                if (searchMap.get("receiverCity")!=null && !"".equals(searchMap.get("receiverCity"))) {
                	predicateList.add(cb.like(root.get("receiverCity").as(String.class), "%"+(String)searchMap.get("receiverCity")+"%"));
                }
                // 县区
                if (searchMap.get("receiverTown")!=null && !"".equals(searchMap.get("receiverTown"))) {
                	predicateList.add(cb.like(root.get("receiverTown").as(String.class), "%"+(String)searchMap.get("receiverTown")+"%"));
                }
                // 详细地址
                if (searchMap.get("receiverAddress")!=null && !"".equals(searchMap.get("receiverAddress"))) {
                	predicateList.add(cb.like(root.get("receiverAddress").as(String.class), "%"+(String)searchMap.get("receiverAddress")+"%"));
                }
                // 邮政编码
                if (searchMap.get("receiverCode")!=null && !"".equals(searchMap.get("receiverCode"))) {
                	predicateList.add(cb.like(root.get("receiverCode").as(String.class), "%"+(String)searchMap.get("receiverCode")+"%"));
                }
				
				return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

			}
		};

	}

}
