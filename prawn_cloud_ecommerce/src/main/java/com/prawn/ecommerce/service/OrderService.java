package com.prawn.ecommerce.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import util.IdWorker;

import com.prawn.ecommerce.dao.OrderDao;
import com.prawn.ecommerce.pojo.Order;

/**
 * 服务层
 * 
 * @author Administrator
 *
 */
@Service
public class OrderService {

	@Autowired
	private OrderDao orderDao;
	
	@Autowired
	private IdWorker idWorker;

	/**
	 * 查询全部列表
	 * @return
	 */
	public List<Order> findAll() {
		return orderDao.findAll();
	}

	
	/**
	 * 条件查询+分页
	 * @param whereMap
	 * @param page
	 * @param size
	 * @return
	 */
	public Page<Order> findSearch(Map whereMap, int page, int size) {
		Specification<Order> specification = createSpecification(whereMap);
		PageRequest pageRequest =  PageRequest.of(page-1, size);
		return orderDao.findAll(specification, pageRequest);
	}

	
	/**
	 * 条件查询
	 * @param whereMap
	 * @return
	 */
	public List<Order> findSearch(Map whereMap) {
		Specification<Order> specification = createSpecification(whereMap);
		return orderDao.findAll(specification);
	}

	/**
	 * 根据ID查询实体
	 * @param id
	 * @return
	 */
	public Order findById(String id) {
		return orderDao.findById(id).get();
	}

	/**
	 * 增加
	 * @param order
	 */
	public void add(Order order) {
		order.setOrderId( idWorker.nextId()+"" );
		orderDao.save(order);
	}

	/**
	 * 修改
	 * @param order
	 */
	public void update(Order order) {
		orderDao.save(order);
	}

	/**
	 * 删除
	 * @param id
	 */
	public void deleteById(String id) {
		orderDao.deleteById(id);
	}

	/**
	 * 动态条件构建
	 * @param searchMap
	 * @return
	 */
	private Specification<Order> createSpecification(Map searchMap) {

		return new Specification<Order>() {

			@Override
			public Predicate toPredicate(Root<Order> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<Predicate>();
                // 订单id
                if (searchMap.get("orderId")!=null && !"".equals(searchMap.get("orderId"))) {
                	predicateList.add(cb.like(root.get("orderId").as(String.class), "%"+(String)searchMap.get("orderId")+"%"));
                }
                // 用户id
                if (searchMap.get("userId")!=null && !"".equals(searchMap.get("userId"))) {
                	predicateList.add(cb.like(root.get("userId").as(String.class), "%"+(String)searchMap.get("userId")+"%"));
                }
                // 商品id
                if (searchMap.get("productId")!=null && !"".equals(searchMap.get("productId"))) {
                	predicateList.add(cb.like(root.get("productId").as(String.class), "%"+(String)searchMap.get("productId")+"%"));
                }
                // 收货地址id
                if (searchMap.get("addressId")!=null && !"".equals(searchMap.get("addressId"))) {
                	predicateList.add(cb.like(root.get("addressId").as(String.class), "%"+(String)searchMap.get("addressId")+"%"));
                }
                // 支付类型
                if (searchMap.get("orderPaymentType")!=null && !"".equals(searchMap.get("orderPaymentType"))) {
                	predicateList.add(cb.like(root.get("orderPaymentType").as(String.class), "%"+(String)searchMap.get("orderPaymentType")+"%"));
                }
                // 物流名称
                if (searchMap.get("orderLogisticsName")!=null && !"".equals(searchMap.get("orderLogisticsName"))) {
                	predicateList.add(cb.like(root.get("orderLogisticsName").as(String.class), "%"+(String)searchMap.get("orderLogisticsName")+"%"));
                }
                // 物流单号
                if (searchMap.get("orderLogisticsNumber")!=null && !"".equals(searchMap.get("orderLogisticsNumber"))) {
                	predicateList.add(cb.like(root.get("orderLogisticsNumber").as(String.class), "%"+(String)searchMap.get("orderLogisticsNumber")+"%"));
                }
				
				return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

			}
		};

	}

}
