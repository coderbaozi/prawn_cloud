package com.prawn.ecommerce.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import util.IdWorker;

import com.prawn.ecommerce.dao.OmmentDao;
import com.prawn.ecommerce.pojo.Omment;

/**
 * 服务层
 * 
 * @author Administrator
 *
 */
@Service
public class OmmentService {

	@Autowired
	private OmmentDao ommentDao;
	
	@Autowired
	private IdWorker idWorker;

	/**
	 * 查询全部列表
	 * @return
	 */
	public List<Omment> findAll() {
		return ommentDao.findAll();
	}

	
	/**
	 * 条件查询+分页
	 * @param whereMap
	 * @param page
	 * @param size
	 * @return
	 */
	public Page<Omment> findSearch(Map whereMap, int page, int size) {
		Specification<Omment> specification = createSpecification(whereMap);
		PageRequest pageRequest =  PageRequest.of(page-1, size);
		return ommentDao.findAll(specification, pageRequest);
	}

	
	/**
	 * 条件查询
	 * @param whereMap
	 * @return
	 */
	public List<Omment> findSearch(Map whereMap) {
		Specification<Omment> specification = createSpecification(whereMap);
		return ommentDao.findAll(specification);
	}

	/**
	 * 根据ID查询实体
	 * @param id
	 * @return
	 */
	public Omment findById(String id) {
		return ommentDao.findById(id).get();
	}

	/**
	 * 增加
	 * @param omment
	 */
	public void add(Omment omment) {
		omment.setCommentId( idWorker.nextId()+"" );
		ommentDao.save(omment);
	}

	/**
	 * 修改
	 * @param omment
	 */
	public void update(Omment omment) {
		ommentDao.save(omment);
	}

	/**
	 * 删除
	 * @param id
	 */
	public void deleteById(String id) {
		ommentDao.deleteById(id);
	}

	/**
	 * 动态条件构建
	 * @param searchMap
	 * @return
	 */
	private Specification<Omment> createSpecification(Map searchMap) {

		return new Specification<Omment>() {

			@Override
			public Predicate toPredicate(Root<Omment> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<Predicate>();
                // 评论id
                if (searchMap.get("commentId")!=null && !"".equals(searchMap.get("commentId"))) {
                	predicateList.add(cb.like(root.get("commentId").as(String.class), "%"+(String)searchMap.get("commentId")+"%"));
                }
                // 用户id
                if (searchMap.get("userId")!=null && !"".equals(searchMap.get("userId"))) {
                	predicateList.add(cb.like(root.get("userId").as(String.class), "%"+(String)searchMap.get("userId")+"%"));
                }
                // 商品id
                if (searchMap.get("productId")!=null && !"".equals(searchMap.get("productId"))) {
                	predicateList.add(cb.like(root.get("productId").as(String.class), "%"+(String)searchMap.get("productId")+"%"));
                }
                // 评论内容
                if (searchMap.get("commentContent")!=null && !"".equals(searchMap.get("commentContent"))) {
                	predicateList.add(cb.like(root.get("commentContent").as(String.class), "%"+(String)searchMap.get("commentContent")+"%"));
                }
                // 图片
                if (searchMap.get("commentPic")!=null && !"".equals(searchMap.get("commentPic"))) {
                	predicateList.add(cb.like(root.get("commentPic").as(String.class), "%"+(String)searchMap.get("commentPic")+"%"));
                }
                // 评论状态1、待审核2、已审核可见3、不可查看
                if (searchMap.get("commentStatus")!=null && !"".equals(searchMap.get("commentStatus"))) {
                	predicateList.add(cb.like(root.get("commentStatus").as(String.class), "%"+(String)searchMap.get("commentStatus")+"%"));
                }
				
				return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

			}
		};

	}

}
