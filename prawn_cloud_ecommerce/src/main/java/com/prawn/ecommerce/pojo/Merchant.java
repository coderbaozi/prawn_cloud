package com.prawn.ecommerce.pojo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="ec_merchant")
public class Merchant implements Serializable{

	@Id
	private String merchantId;//商铺id


	
	private String merchantName;//商铺名称
	private String merchantPhone;//联系电话
	private String merchantLogo;//商铺logo
	private String merchantQrcode;//商铺二维码
	private String merchantProvince;//省份
	private String merchantCity;//市
	private String merchantTown;//区/县
	private String merchantAddress;//详细地址
	private String merchantDesc;//商家介绍
	private String merchanRemarks;//商家备注
	private String createBy;//创建者
	private java.util.Date created;//创建时间
	private java.util.Date updated;//更新时间

	
	public String getMerchantId() {		
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getMerchantName() {		
		return merchantName;
	}
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getMerchantPhone() {		
		return merchantPhone;
	}
	public void setMerchantPhone(String merchantPhone) {
		this.merchantPhone = merchantPhone;
	}

	public String getMerchantLogo() {		
		return merchantLogo;
	}
	public void setMerchantLogo(String merchantLogo) {
		this.merchantLogo = merchantLogo;
	}

	public String getMerchantQrcode() {		
		return merchantQrcode;
	}
	public void setMerchantQrcode(String merchantQrcode) {
		this.merchantQrcode = merchantQrcode;
	}

	public String getMerchantProvince() {		
		return merchantProvince;
	}
	public void setMerchantProvince(String merchantProvince) {
		this.merchantProvince = merchantProvince;
	}

	public String getMerchantCity() {		
		return merchantCity;
	}
	public void setMerchantCity(String merchantCity) {
		this.merchantCity = merchantCity;
	}

	public String getMerchantTown() {		
		return merchantTown;
	}
	public void setMerchantTown(String merchantTown) {
		this.merchantTown = merchantTown;
	}

	public String getMerchantAddress() {		
		return merchantAddress;
	}
	public void setMerchantAddress(String merchantAddress) {
		this.merchantAddress = merchantAddress;
	}

	public String getMerchantDesc() {		
		return merchantDesc;
	}
	public void setMerchantDesc(String merchantDesc) {
		this.merchantDesc = merchantDesc;
	}

	public String getMerchanRemarks() {		
		return merchanRemarks;
	}
	public void setMerchanRemarks(String merchanRemarks) {
		this.merchanRemarks = merchanRemarks;
	}

	public String getCreateBy() {		
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public java.util.Date getCreated() {		
		return created;
	}
	public void setCreated(java.util.Date created) {
		this.created = created;
	}

	public java.util.Date getUpdated() {		
		return updated;
	}
	public void setUpdated(java.util.Date updated) {
		this.updated = updated;
	}


	
}
