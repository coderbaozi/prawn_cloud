package com.prawn.ecommerce.pojo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="ec_product")
public class Product implements Serializable{

	@Id
	private String productId;//商品id


	
	private String productName;//商品名称
	private String productTitle;//商品标题
	private Double productPrice;//商品单价
	private String productUnit;//单位
	private Integer productNum;//商品库存
	private String productBarcode;//商品条形码
	private String productImages;//商品图片
	private String categoryId;//商品类别
	private Integer productStars;//商品点赞数
	private String productArea;//商品产地
	private Integer productStatus;//商品状态: 1-正常
	private String merchantId;//商铺id
	private java.util.Date productCreated;//商品创建时间
	private java.util.Date productUpdated;//商品更新时间

	
	public String getProductId() {		
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {		
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductTitle() {		
		return productTitle;
	}
	public void setProductTitle(String productTitle) {
		this.productTitle = productTitle;
	}

	public Double getProductPrice() {		
		return productPrice;
	}
	public void setProductPrice(Double productPrice) {
		this.productPrice = productPrice;
	}

	public String getProductUnit() {		
		return productUnit;
	}
	public void setProductUnit(String productUnit) {
		this.productUnit = productUnit;
	}

	public Integer getProductNum() {		
		return productNum;
	}
	public void setProductNum(Integer productNum) {
		this.productNum = productNum;
	}

	public String getProductBarcode() {		
		return productBarcode;
	}
	public void setProductBarcode(String productBarcode) {
		this.productBarcode = productBarcode;
	}

	public String getProductImages() {		
		return productImages;
	}
	public void setProductImages(String productImages) {
		this.productImages = productImages;
	}

	public String getCategoryId() {		
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public Integer getProductStars() {		
		return productStars;
	}
	public void setProductStars(Integer productStars) {
		this.productStars = productStars;
	}

	public String getProductArea() {		
		return productArea;
	}
	public void setProductArea(String productArea) {
		this.productArea = productArea;
	}

	public Integer getProductStatus() {		
		return productStatus;
	}
	public void setProductStatus(Integer productStatus) {
		this.productStatus = productStatus;
	}

	public String getMerchantId() {		
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public java.util.Date getProductCreated() {		
		return productCreated;
	}
	public void setProductCreated(java.util.Date productCreated) {
		this.productCreated = productCreated;
	}

	public java.util.Date getProductUpdated() {		
		return productUpdated;
	}
	public void setProductUpdated(java.util.Date productUpdated) {
		this.productUpdated = productUpdated;
	}


	
}
