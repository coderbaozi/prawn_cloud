package com.prawn.ecommerce.pojo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="ec_address")
public class Address implements Serializable{

	@Id
	private String addressId;//收货地址id


	
	private String userId;//用户id
	private String receiverName;//收货人姓名
	private String receiverPhone;//联系电话
	private String receiverProvince;//省份
	private String receiverCity;//城市
	private String receiverTown;//县区
	private String receiverAddress;//详细地址
	private String receiverCode;//邮政编码
	private java.util.Date receiverCreated;//创建时间
	private java.util.Date receiverUpdate;//更新时间

	
	public String getAddressId() {		
		return addressId;
	}
	public void setAddressId(String addressId) {
		this.addressId = addressId;
	}

	public String getUserId() {		
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getReceiverName() {		
		return receiverName;
	}
	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public String getReceiverPhone() {		
		return receiverPhone;
	}
	public void setReceiverPhone(String receiverPhone) {
		this.receiverPhone = receiverPhone;
	}

	public String getReceiverProvince() {		
		return receiverProvince;
	}
	public void setReceiverProvince(String receiverProvince) {
		this.receiverProvince = receiverProvince;
	}

	public String getReceiverCity() {		
		return receiverCity;
	}
	public void setReceiverCity(String receiverCity) {
		this.receiverCity = receiverCity;
	}

	public String getReceiverTown() {		
		return receiverTown;
	}
	public void setReceiverTown(String receiverTown) {
		this.receiverTown = receiverTown;
	}

	public String getReceiverAddress() {		
		return receiverAddress;
	}
	public void setReceiverAddress(String receiverAddress) {
		this.receiverAddress = receiverAddress;
	}

	public String getReceiverCode() {		
		return receiverCode;
	}
	public void setReceiverCode(String receiverCode) {
		this.receiverCode = receiverCode;
	}

	public java.util.Date getReceiverCreated() {		
		return receiverCreated;
	}
	public void setReceiverCreated(java.util.Date receiverCreated) {
		this.receiverCreated = receiverCreated;
	}

	public java.util.Date getReceiverUpdate() {		
		return receiverUpdate;
	}
	public void setReceiverUpdate(java.util.Date receiverUpdate) {
		this.receiverUpdate = receiverUpdate;
	}


	
}
