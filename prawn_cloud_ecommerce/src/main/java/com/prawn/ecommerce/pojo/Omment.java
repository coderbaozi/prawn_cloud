package com.prawn.ecommerce.pojo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="ec_omment")
public class Omment implements Serializable{

	@Id
	private String commentId;//评论id


	
	private String userId;//用户id
	private String productId;//商品id
	private String commentContent;//评论内容
	private String commentPic;//图片
	private String commentStatus;//评论状态1、待审核2、已审核可见3、不可查看
	private java.util.Date commentTime;//评论时间

	
	public String getCommentId() {		
		return commentId;
	}
	public void setCommentId(String commentId) {
		this.commentId = commentId;
	}

	public String getUserId() {		
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getProductId() {		
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getCommentContent() {		
		return commentContent;
	}
	public void setCommentContent(String commentContent) {
		this.commentContent = commentContent;
	}

	public String getCommentPic() {		
		return commentPic;
	}
	public void setCommentPic(String commentPic) {
		this.commentPic = commentPic;
	}

	public String getCommentStatus() {		
		return commentStatus;
	}
	public void setCommentStatus(String commentStatus) {
		this.commentStatus = commentStatus;
	}

	public java.util.Date getCommentTime() {		
		return commentTime;
	}
	public void setCommentTime(java.util.Date commentTime) {
		this.commentTime = commentTime;
	}


	
}
