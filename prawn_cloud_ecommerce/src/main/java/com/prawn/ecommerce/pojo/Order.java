package com.prawn.ecommerce.pojo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="ec_order")
public class Order implements Serializable{

	@Id
	private String orderId;//订单id


	
	private String userId;//用户id
	private String productId;//商品id
	private Integer num;//购买数量
	private String addressId;//收货地址id
	private Double postage;//邮费
	private Double orderPayment;//实付金额
	private String orderPaymentType;//支付类型
	private Integer orderStatus;//1-未付款2-已付款3-未发货4-已发货
	private java.util.Date orderCreated;//订单创建时间
	private java.util.Date orderUpdated;//订单更新时间
	private java.util.Date orderPaymentTime;//付款时间
	private java.util.Date orderDeliveryTime;//发货时间
	private String orderLogisticsName;//物流名称
	private String orderLogisticsNumber;//物流单号

	
	public String getOrderId() {		
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getUserId() {		
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getProductId() {		
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}

	public Integer getNum() {		
		return num;
	}
	public void setNum(Integer num) {
		this.num = num;
	}

	public String getAddressId() {		
		return addressId;
	}
	public void setAddressId(String addressId) {
		this.addressId = addressId;
	}

	public Double getPostage() {		
		return postage;
	}
	public void setPostage(Double postage) {
		this.postage = postage;
	}

	public Double getOrderPayment() {		
		return orderPayment;
	}
	public void setOrderPayment(Double orderPayment) {
		this.orderPayment = orderPayment;
	}

	public String getOrderPaymentType() {		
		return orderPaymentType;
	}
	public void setOrderPaymentType(String orderPaymentType) {
		this.orderPaymentType = orderPaymentType;
	}

	public Integer getOrderStatus() {		
		return orderStatus;
	}
	public void setOrderStatus(Integer orderStatus) {
		this.orderStatus = orderStatus;
	}

	public java.util.Date getOrderCreated() {		
		return orderCreated;
	}
	public void setOrderCreated(java.util.Date orderCreated) {
		this.orderCreated = orderCreated;
	}

	public java.util.Date getOrderUpdated() {		
		return orderUpdated;
	}
	public void setOrderUpdated(java.util.Date orderUpdated) {
		this.orderUpdated = orderUpdated;
	}

	public java.util.Date getOrderPaymentTime() {		
		return orderPaymentTime;
	}
	public void setOrderPaymentTime(java.util.Date orderPaymentTime) {
		this.orderPaymentTime = orderPaymentTime;
	}

	public java.util.Date getOrderDeliveryTime() {		
		return orderDeliveryTime;
	}
	public void setOrderDeliveryTime(java.util.Date orderDeliveryTime) {
		this.orderDeliveryTime = orderDeliveryTime;
	}

	public String getOrderLogisticsName() {		
		return orderLogisticsName;
	}
	public void setOrderLogisticsName(String orderLogisticsName) {
		this.orderLogisticsName = orderLogisticsName;
	}

	public String getOrderLogisticsNumber() {		
		return orderLogisticsNumber;
	}
	public void setOrderLogisticsNumber(String orderLogisticsNumber) {
		this.orderLogisticsNumber = orderLogisticsNumber;
	}


	
}
