package com.prawn.search.dao;

import com.prawn.search.pojo.Details;
import com.prawn.search.pojo.Experts;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface ExpertsDao extends ElasticsearchRepository<Experts, String> {
}
