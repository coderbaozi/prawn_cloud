package com.prawn.search.dao;


import com.prawn.search.pojo.IndustryInformation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface IndustryInformationDao extends ElasticsearchRepository<IndustryInformation, String> {
    public Page<IndustryInformation> findByTitleOrContentAndState(String title, String content, int state, Pageable pageable);

}
