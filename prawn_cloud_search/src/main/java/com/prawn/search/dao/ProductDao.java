package com.prawn.search.dao;

import com.prawn.search.pojo.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface ProductDao extends ElasticsearchRepository<Product, String> {
    public Page<Product> findByProductNameOrProductTitleAndProductStatus( String productName, String productTitle, int productStatus,Pageable pageable);
}
