package com.prawn.search.dao;

import com.prawn.search.pojo.EducationInformation;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface EducationInformationDao extends ElasticsearchRepository<EducationInformation, String> {
//    public Page<EducationInformation> findByTitleOrDetailAndState(String title, String detail, int status, Pageable pageable);
}
