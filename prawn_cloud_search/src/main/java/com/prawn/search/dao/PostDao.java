package com.prawn.search.dao;

import com.prawn.search.pojo.IntellectualPropertyRights;
import com.prawn.search.pojo.Post;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface PostDao extends ElasticsearchRepository<Post, String> {
}
