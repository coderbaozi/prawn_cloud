package com.prawn.search.dao;

import com.prawn.search.pojo.Experts;
import com.prawn.search.pojo.IntellectualPropertyRights;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface IntellectualPropertyRightsDao extends ElasticsearchRepository<IntellectualPropertyRights, String> {
}
