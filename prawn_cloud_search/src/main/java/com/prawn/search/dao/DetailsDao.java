package com.prawn.search.dao;

import com.prawn.search.pojo.Details;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface DetailsDao extends ElasticsearchRepository<Details, String> {
}
