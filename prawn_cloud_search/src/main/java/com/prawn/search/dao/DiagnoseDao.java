package com.prawn.search.dao;

import com.prawn.search.pojo.Diagnose;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DiagnoseDao extends ElasticsearchRepository<Diagnose, String> {
//    public List<Diagnose> getByDiseasenameOrSymptomAndStatus(String diseaseName, String symptom, String status);
    public Page<Diagnose> findAll(Pageable pageable);
}
