package com.prawn.search.controller;

import com.prawn.search.pojo.Details;
import com.prawn.search.pojo.IntellectualPropertyRights;
import com.prawn.search.service.IntellectualPropertyRightsServise;
import entity.PageResult;
import entity.Result;
import entity.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/rights/search")
public class IntellectualPropertyRightsController {
    @Autowired
    private IntellectualPropertyRightsServise intellectualPropertyRightsServise;

    /**
     * 根据时间倒序分页搜索
     * @param key
     * @param page
     * @param size
     * @return
     */
    @RequestMapping(value = "/time/{page}/{size}", method = RequestMethod.GET)
    public Result timeQuery(@RequestParam("key") String key, @PathVariable int page, @PathVariable int size) {
        Page<IntellectualPropertyRights> rights = intellectualPropertyRightsServise.timeQuery(key, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<>(rights.getTotalElements(), rights.getContent()));
    }
}
