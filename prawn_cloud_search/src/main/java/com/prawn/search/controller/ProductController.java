package com.prawn.search.controller;

import com.prawn.search.pojo.Product;
import com.prawn.search.service.ProductService;
import entity.PageResult;
import entity.Result;
import entity.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/product/search")
@CrossOrigin
public class ProductController {
    @Autowired
    private ProductService productService;

    @RequestMapping(value = "/{page}/{size}", method = RequestMethod.GET)
    public Result findByKey(@RequestParam(value = "key") String key, @PathVariable int page, @PathVariable int size) {
        int status = 1;
        Page<Product> pageData = productService.findByKey(status, key, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Product>(pageData.getTotalElements(), pageData.getContent()));
    }
}
