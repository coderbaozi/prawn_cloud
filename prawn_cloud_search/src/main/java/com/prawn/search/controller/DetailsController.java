package com.prawn.search.controller;

import com.prawn.search.pojo.Details;
import com.prawn.search.service.DetailsService;
import entity.PageResult;
import entity.Result;
import entity.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/details/search")
public class DetailsController {

	@Autowired
	private DetailsService detailsService;

	/**
	 * 按点击量倒序排序分页搜索
	 *
	 * @param key
	 * @param page
	 * @param size
	 * @return
	 */
	@RequestMapping(value = "/praiseNum/{page}/{size}", method = RequestMethod.GET)
	public Result praiseNumQuery(@RequestParam("key") String key, @PathVariable int page, @PathVariable int size) {
		Page<Details> details = detailsService.praiseNumQuery(key, page, size);
		return new Result(true, StatusCode.OK, "查询成功", new PageResult<>(details.getTotalElements(), details.getContent()));
	}

	/**
	 * 按回复时间倒序排序分页搜索
	 *
	 * @param key
	 * @param page
	 * @param size
	 * @return
	 */
	@RequestMapping(value = "/time/{page}/{size}", method = RequestMethod.GET)
	public Result timeQuery(@RequestParam("key") String key, @PathVariable int page, @PathVariable int size) {
		Page<Details> details = detailsService.timeQuery(key, page, size);
		return new Result(true, StatusCode.OK, "查询成功", new PageResult<>(details.getTotalElements(), details.getContent()));
	}
}
