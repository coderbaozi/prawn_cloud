package com.prawn.search.controller;

import com.prawn.search.pojo.Diagnose;
import com.prawn.search.service.DiagnoseService;
import entity.PageResult;
import entity.Result;
import entity.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/diagnose/search")
@CrossOrigin
public class DiagnoseController {

    @Autowired
    private DiagnoseService diagnoseService;

    @RequestMapping(value = "/{page}/{size}", method = RequestMethod.GET)
    public Result findByKey(@PathVariable int page, @PathVariable int size, @RequestParam(value = "key") String key) {
        Page<Diagnose> diagnoseList = diagnoseService.findByKey(key, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<>(diagnoseList.getTotalElements(), diagnoseList.getContent()));
    }

    @RequestMapping(value = "/all/{page}/{size}", method = RequestMethod.GET)
    public Result findAll(@PathVariable int page, @PathVariable int size) {
        Page<Diagnose> all = diagnoseService.findAll(page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<>(all.getTotalElements(), all.getContent()));
    }

    @RequestMapping(value = "/accurate/{page}/{size}", method = RequestMethod.GET)
    public Result accurateQuery(@PathVariable int page, @PathVariable int size, @RequestParam(value = "key") String key) {
        Page<Diagnose> diagnoses = diagnoseService.accurateQuery(key, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<>(diagnoses.getTotalElements(), diagnoses.getContent()));
    }
}
