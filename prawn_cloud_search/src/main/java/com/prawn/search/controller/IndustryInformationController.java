package com.prawn.search.controller;

import com.prawn.search.pojo.IndustryInformation;
import com.prawn.search.service.IndustryInformationService;
import entity.PageResult;
import entity.Result;
import entity.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;


@RestController
@RequestMapping("/industry/search")
@CrossOrigin
public class IndustryInformationController {
    @Autowired
    private IndustryInformationService industryInformationService;

    /**
     * 按时间倒序排序分页搜索
     *
     * @param key
     * @param page
     * @param size
     * @return
     */
    @RequestMapping(value = "/time/{page}/{size}/{module}", method = RequestMethod.GET)
    public Result findByKey(@PathVariable(value = "module") int module, @RequestParam("key") String key, @PathVariable int page, @PathVariable int size) {
        int status = 1;
        Page<IndustryInformation> pageData = industryInformationService.findByKey(module, status, key, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<IndustryInformation>(pageData.getTotalElements(), pageData.getContent()));
    }

    /**
     * 按点击量倒序排序分页搜索
     *
     * @param key
     * @param page
     * @param size
     * @return
     */
    @RequestMapping(value = "/clickNum/{page}/{size}/{module}", method = RequestMethod.GET)
    public Result clickNumQuery(@PathVariable(value = "module") int module, @RequestParam("key") String key, @PathVariable int page, @PathVariable int size) {
        int status = 1;
        Page<IndustryInformation> pageData = industryInformationService.clickNumQuery(module, status, key, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<IndustryInformation>(pageData.getTotalElements(), pageData.getContent()));
    }
}
