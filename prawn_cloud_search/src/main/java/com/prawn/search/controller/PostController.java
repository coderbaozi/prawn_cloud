package com.prawn.search.controller;

import com.prawn.search.pojo.Details;
import com.prawn.search.pojo.Post;
import com.prawn.search.service.PostService;
import entity.PageResult;
import entity.Result;
import entity.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/post/search")
public class PostController {
    @Autowired
    private PostService postService;

    /**
     * 按点击量倒序排序分页搜索
     * @param key
     * @param page
     * @param size
     * @return
     */
    @RequestMapping(value = "/clickNum/{page}/{size}", method = RequestMethod.GET)
    public Result praiseNumQuery(@RequestParam("key") String key, @PathVariable int page, @PathVariable int size) {
        Page<Post> posts = postService.clickNumQuery(key, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<>(posts.getTotalElements(), posts.getContent()));
    }

    /**
     * 按创建时间倒序排序分页搜索
     * @param key
     * @param page
     * @param size
     * @return
     */
    @RequestMapping(value = "/time/{page}/{size}", method = RequestMethod.GET)
    public Result timeQuery(@RequestParam("key") String key, @PathVariable int page, @PathVariable int size) {
        Page<Post> posts = postService.timeQuery(key,page,size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<>(posts.getTotalElements(), posts.getContent()));
    }
}
