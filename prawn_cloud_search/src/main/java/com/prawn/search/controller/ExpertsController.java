package com.prawn.search.controller;

import com.prawn.search.pojo.Experts;
import com.prawn.search.service.ExpertsService;
import entity.PageResult;
import entity.Result;
import entity.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/experts/search")
@CrossOrigin
public class ExpertsController {
    @Autowired
    private ExpertsService expertsService;

    /**
     * 按点击量倒序排序分页搜索
     *
     * @param key
     * @param page
     * @param size
     * @return
     */
    @RequestMapping(value = "/praiseNum/{page}/{size}", method = RequestMethod.GET)
    public Result praiseNumQuery(@RequestParam("key") String key, @PathVariable int page, @PathVariable int size) {
        Page<Experts> experts = expertsService.praiseNumQuery(key, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<>(experts.getTotalElements(), experts.getContent()));
    }

    /**
     * 按回复量倒序分页搜索
     *
     * @param key
     * @param page
     * @param size
     * @return
     */
    @RequestMapping(value = "/replies/{page}/{size}", method = RequestMethod.GET)
    public Result repliesQuery(@RequestParam("key") String key, @PathVariable int page, @PathVariable int size) {
        Page<Experts> replies = expertsService.repliesQuery(key, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<>(replies.getTotalElements(), replies.getContent()));
    }
}
