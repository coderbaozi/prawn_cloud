package com.prawn.search.controller;

import com.prawn.search.pojo.EducationInformation;
import com.prawn.search.service.EducationInformationService;
import entity.PageResult;
import entity.Result;
import entity.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/education/search")
@CrossOrigin
public class EducationInformationController {
    @Autowired
    private EducationInformationService educationInformationService;

    /**
     * 按时间排序倒序搜索
     *
     * @param module
     * @param key
     * @param page
     * @param size
     * @return
     */
    @RequestMapping(value = "/time/{page}/{size}/{module}", method = RequestMethod.GET)
    public Result findByKey(@PathVariable("module") int module, @RequestParam("key") String key, @PathVariable int page, @PathVariable int size) {
        int status = 1;
        Page<EducationInformation> pageData = educationInformationService.findByKey(status, module, key, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<>(pageData.getTotalElements(), pageData.getContent()));
    }

    /**
     * 按点击量排序倒序搜索
     *
     * @param module
     * @param key
     * @param page
     * @param size
     * @return
     */
    @RequestMapping(value = "/clickNum/{page}/{size}/{module}", method = RequestMethod.GET)
    public Result clickNumQuery(@PathVariable("module") int module, @RequestParam("key") String key, @PathVariable int page, @PathVariable int size) {
        int status = 1;
        Page<EducationInformation> pageData = educationInformationService.clickNumQuery(status, module, key, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<>(pageData.getTotalElements(), pageData.getContent()));
    }
}
