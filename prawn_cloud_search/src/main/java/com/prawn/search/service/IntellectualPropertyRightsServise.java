package com.prawn.search.service;

import com.prawn.search.pojo.IntellectualPropertyRights;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Service;

@Service
public class IntellectualPropertyRightsServise {
    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    /**
     * 根据时间倒序分页搜索
     *
     * @param key
     * @param page
     * @param size
     * @return
     */
    public Page<IntellectualPropertyRights> timeQuery(String key, int page, int size) {
        Pageable pageable = PageRequest.of(page - 1, size);
        int status = 1;
        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.boolQuery()
                        .must(QueryBuilders.termQuery("state", status))
                        .must(QueryBuilders.multiMatchQuery(key, "title", "editor", "source")))
                .withPageable(pageable).withSort(SortBuilders.fieldSort("createDate").order(SortOrder.DESC))
                .build();

        return elasticsearchTemplate.queryForPage(searchQuery, IntellectualPropertyRights.class);
    }
}
