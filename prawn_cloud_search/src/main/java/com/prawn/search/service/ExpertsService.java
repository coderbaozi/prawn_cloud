package com.prawn.search.service;

import com.prawn.search.pojo.Experts;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Service;

@Service
public class ExpertsService {
    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    /**
     * 按点赞量倒序排序分页搜索
     * @param key
     * @param page
     * @param size
     * @return
     */
    public Page<Experts> praiseNumQuery(String key, int page, int size) {
        int status = 1;
        Pageable pageable = PageRequest.of(page-1,size);
        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.boolQuery()
                        .must(QueryBuilders.multiMatchQuery(key,"name","goodAt"))
                        .must(QueryBuilders.termQuery("state", status))
                ).withPageable(pageable)
                .withSort(SortBuilders.fieldSort("praiseNum").order(SortOrder.DESC))
                .build();

        return elasticsearchTemplate.queryForPage(searchQuery, Experts.class);
    }

    /**
     * 按回复量倒序分页搜索
     * @param key
     * @param page
     * @param size
     * @return
     */
    public Page<Experts> repliesQuery(String key, int page, int size) {
        int status = 1;
        Pageable pageable = PageRequest.of(page-1,size);
        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.boolQuery()
                        .must(QueryBuilders.multiMatchQuery(key,"name","goodAt"))
                        .must(QueryBuilders.termQuery("state", status))
                ).withPageable(pageable)
                .withSort(SortBuilders.fieldSort("repliesNum").order(SortOrder.DESC))
                .build();

        return elasticsearchTemplate.queryForPage(searchQuery, Experts.class);
    }
}
