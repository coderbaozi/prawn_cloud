package com.prawn.search.service;

import com.prawn.search.pojo.Details;
import com.prawn.search.pojo.Experts;
import com.prawn.search.pojo.IndustryInformation;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Service;

@Service
public class DetailsService {
    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    /**
     * 按点击量倒序排序分页搜索
     *
     * @param key
     * @param page
     * @param size
     * @return
     */
    public Page<Details> praiseNumQuery(String key, int page, int size) {
        int status = 1;
        Pageable pageable = PageRequest.of(page - 1, size);
        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.boolQuery()
                        .must(QueryBuilders.matchQuery(key, "reply"))
                        .must(QueryBuilders.termQuery("state", status))
                ).withPageable(pageable)
                .withSort(SortBuilders.fieldSort("praiseNum").order(SortOrder.DESC))
                .build();

        return elasticsearchTemplate.queryForPage(searchQuery, Details.class);
    }

    /**
     * 按回复时间倒序排序分页搜索
     * @param key
     * @param page
     * @param size
     * @return
     */
    public Page<Details> timeQuery(String key, int page, int size) {
        int status = 1;
        Pageable pageable = PageRequest.of(page - 1, size);

        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.boolQuery()
                        .must(QueryBuilders.termQuery("state", status))
                        .must(QueryBuilders.matchQuery(key, "reply")))
                .withPageable(pageable).withSort(SortBuilders.fieldSort("creationTime").order(SortOrder.DESC))
                .build();

        return elasticsearchTemplate.queryForPage(searchQuery, Details.class);
    }
}
