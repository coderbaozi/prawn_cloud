package com.prawn.search.service;


import com.prawn.search.dao.IndustryInformationDao;
import com.prawn.search.pojo.IndustryInformation;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;

import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Service;

@Service
public class IndustryInformationService {
    @Autowired
    private IndustryInformationDao industryInformationDao;

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    /**
     * 按时间倒序排序分页搜索
     *
     * @param status
     * @param key
     * @param page
     * @param size
     * @return
     */
    public Page<IndustryInformation> findByKey(int module, int status, String key, int page, int size) {
        Pageable pageable = PageRequest.of(page - 1, size);

        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.boolQuery()
                        .must(QueryBuilders.termQuery("state", status))
                        .must(QueryBuilders.termQuery("module", module))
                        .must(QueryBuilders.multiMatchQuery(key, "title", "summary", "editor")))
                .withPageable(pageable).withSort(SortBuilders.fieldSort("creationTime").order(SortOrder.DESC))
                .build();

        return elasticsearchTemplate.queryForPage(searchQuery, IndustryInformation.class);
    }

    /**
     * 按点击量倒序排序分页搜索
     *
     * @param status
     * @param key
     * @param page
     * @param size
     * @return
     */
    public Page<IndustryInformation> clickNumQuery(int module, int status, String key, int page, int size) {
        Pageable pageable = PageRequest.of(page - 1, size);

        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.boolQuery()
                        .must(QueryBuilders.termQuery("state", status))
                        .must(QueryBuilders.termQuery("module", module))
                        .must(QueryBuilders.multiMatchQuery(key, "title", "summary", "editor")))
                .withPageable(pageable).withSort(SortBuilders.fieldSort("clickNum").order(SortOrder.DESC))
                .build();

        return elasticsearchTemplate.queryForPage(searchQuery, IndustryInformation.class);
    }
}
