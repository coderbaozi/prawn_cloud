package com.prawn.search.pojo;


import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;


import javax.persistence.Id;
import java.io.Serializable;

/**
 * 实体类
 *
 * @author Administrator
 */
@Document(indexName = "experts", type = "experts")
public class Experts implements Serializable {

    @Id
    private String id;//专家id

    @Field(type = FieldType.text, analyzer = "ik_max_word", searchAnalyzer = "ik_max_word")
    private String name;//名称

    private String picture;//头像

    private String address;//地址
    @Field(type = FieldType.text, analyzer = "ik_max_word", searchAnalyzer = "ik_max_word")
    private String goodAt;//擅长

    @Field(type = FieldType.Integer)
    private Integer repliesNum;//回复量
    @Field(type = FieldType.Integer)
    private Integer praiseNum;//点赞量


    private Integer consultingNum;//咨询量

    private int state;//状态；0审核中，1审核通过，2审核不通过

    private String onlineTime;//在线时间

    private String typeId;//类别id


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }


    public Integer getRepliesNum() {
        return repliesNum;
    }

    public void setRepliesNum(Integer repliesNum) {
        this.repliesNum = repliesNum;
    }

    public Integer getPraiseNum() {
        return praiseNum;
    }

    public void setPraiseNum(Integer praiseNum) {
        this.praiseNum = praiseNum;
    }


    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGoodAt() {
        return goodAt;
    }

    public void setGoodAt(String goodAt) {
        this.goodAt = goodAt;
    }

    public Integer getConsultingNum() {
        return consultingNum;
    }

    public void setConsultingNum(Integer consultingNum) {
        this.consultingNum = consultingNum;
    }

    public String getOnlineTime() {
        return onlineTime;
    }

    public void setOnlineTime(String onlineTime) {
        this.onlineTime = onlineTime;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }
}
