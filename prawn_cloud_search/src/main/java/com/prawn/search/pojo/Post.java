package com.prawn.search.pojo;


import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * 实体类
 *
 * @author Administrator
 */
@Document(indexName = "post", type = "post")
public class Post implements Serializable {
    @Id
    private String id;//帖子id
    @Field(type = FieldType.text, analyzer = "ik_max_word", searchAnalyzer = "ik_max_word")
    private String title;//标题
    @Field(type = FieldType.Date)
    private java.util.Date creationTime;//创建时间


    private String userName;//提问者名称
    private int state;//状态；0审核中；1审核通过；2审核不通过，3删除
    private String expertsId;//专家id
    private String typeId;//类型id.
    @Field(type = FieldType.Integer)
    private int clickNum;//点击量


    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public int getClickNum() {
        return clickNum;
    }

    public void setClickNum(int clickNum) {
        this.clickNum = clickNum;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public java.util.Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(java.util.Date creationTime) {
        this.creationTime = creationTime;
    }


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getExpertsId() {
        return expertsId;
    }

    public void setExpertsId(String expertsId) {
        this.expertsId = expertsId;
    }


}
