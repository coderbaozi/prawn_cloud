package com.prawn.search.pojo;

import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;


import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

/**
 * 实体类
 *
 * @author Administrator
 */
@Document(indexName = "prawn_cloud_information", type = "information")
public class IndustryInformation implements Serializable {

    @Id
    private String id;//资讯id


    @Field(type = FieldType.text, analyzer = "ik_max_word", searchAnalyzer = "ik_max_word")
    private String title;//标题

    @Field(type = FieldType.text, analyzer = "ik_max_word", searchAnalyzer = "ik_max_word")
    private String summary;//概要


    private String content;//正文

    private String typeId;//资讯类型id
    private String picture;//图片
    private Integer state; //删除状态 （0）:删除 （1）:可用
    private Integer module;//所属模块（1行情2政策）
    @Field(type = FieldType.Integer)
    private Integer clickNum;//点击量
    @Field(type = FieldType.text, analyzer = "ik_max_word", searchAnalyzer = "ik_max_word")
    private String editor;//作者
    @Field(type = FieldType.Date)
    private Date creationTime;//创建时间

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public Integer getModule() {
        return module;
    }

    public void setModule(Integer module) {
        this.module = module;
    }

    public Integer getClickNum() {
        return clickNum;
    }

    public void setClickNum(Integer clickNum) {
        this.clickNum = clickNum;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getEditor() {
        return editor;
    }

    public void setEditor(String editor) {
        this.editor = editor;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
