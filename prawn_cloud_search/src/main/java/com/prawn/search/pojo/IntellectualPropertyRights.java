package com.prawn.search.pojo;


import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;


import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

/**
 * 实体类
 * @author Administrator
 *
 */
@Document(indexName = "rights", type = "rights")
public class IntellectualPropertyRights implements Serializable{

	@Id
	private String id;//id

	@Field(type = FieldType.text, analyzer = "ik_max_word", searchAnalyzer = "ik_max_word")
	private String title;//标题
	@Field(type = FieldType.text, analyzer = "ik_max_word", searchAnalyzer = "ik_max_word")
	private String editor;//作者
	@Field(type = FieldType.text, analyzer = "ik_max_word", searchAnalyzer = "ik_max_word")
	private String source;//来源

	private String file;//文件url
	@Field(type = FieldType.Date)
	private Date createDate;//发表时间
	private int state;//状态

	public int isState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getEditor() {
		return editor;
	}

	public void setEditor(String editor) {
		this.editor = editor;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
}
