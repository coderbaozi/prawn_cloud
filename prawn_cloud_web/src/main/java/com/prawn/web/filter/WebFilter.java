package com.prawn.web.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class WebFilter extends ZuulFilter {
    /**
     * 过滤器类型：
     * pre: 在请求被路由之前调用
     * route: 在路由请求时被调用
     * post: 表示在route和error过滤器之后被调用
     * error: 处理请求发生错误是被调用
     */
    @Override
    public String filterType() {
        return "post";
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        return false;
    }

    @Override
    public Object run() throws ZuulException {
//        //得到request上下文
//        RequestContext currentContext = RequestContext.getCurrentContext();
//        //得到request域
//        HttpServletRequest request = currentContext.getRequest();
//        //得到头信息
//        String header = request.getHeader("Authorization");
//        //判断是否有头信息
//        if(header!=null && !"".equals(header)){
//            //把头信息继续向下传
//            currentContext.addZuulRequestHeader("Authorization", header);
//        }
        RequestContext ctx = RequestContext.getCurrentContext();

        HttpServletResponse response = ctx.getResponse();
        // 设置哪个源可以访问我
        response.setHeader("Access-Control-Allow-Origin", "*");
        // 允许哪个方法(也就是哪种类型的请求)访问我
        response.setHeader("Access-Control-Allow-Methods", "PUT,GET,POST,OPTIONS");
        // 允许携带哪个头访问我
        response.setHeader("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, X-File-Name,token");
        //文本大小，可以不设置（下载文件的时候注意下，发现没加的话，文件大小变了）
        if (ctx.getOriginContentLength() != null && ctx.getOriginContentLength() > 0) {
            response.setHeader("Content-Length", ctx.getOriginContentLength().toString());
        }
        //允许携带cookie
        response.setHeader("Access-Control-Allow-Credentials", "true");

        ctx.setResponse(response);

        return null;
    }
}
