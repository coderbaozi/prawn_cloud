package com.prawn.monitor.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.prawn.monitor.pojo.Warning;
/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface WarningDao extends JpaRepository<Warning,String>,JpaSpecificationExecutor<Warning>{
	
}
