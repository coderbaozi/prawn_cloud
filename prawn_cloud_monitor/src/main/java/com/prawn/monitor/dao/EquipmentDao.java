package com.prawn.monitor.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.prawn.monitor.pojo.Equipment;
/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface EquipmentDao extends JpaRepository<Equipment,String>,JpaSpecificationExecutor<Equipment>{

}
