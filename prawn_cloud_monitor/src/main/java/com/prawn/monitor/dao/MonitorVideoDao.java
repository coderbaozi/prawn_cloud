package com.prawn.monitor.dao;

import com.prawn.monitor.pojo.MonitorVideo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MonitorVideoDao extends JpaRepository<MonitorVideo,String>, JpaSpecificationExecutor<MonitorVideo> {

    @Query(value = "select * from monitor_base where baseId=?",nativeQuery = true)
    Page<MonitorVideo> findByBaseId(String baseId,Pageable pageRequest);

    @Query(value = "select * from monitor_base where baseId=?",nativeQuery = true)
    List<MonitorVideo> findByBaseId(String baseId);

    @Modifying
    @Query(value = "UPDATE monitor_base SET statusCode=?2 WHERE id=?1",nativeQuery = true)
    void modifyStatus(String id, String status);

    public List<MonitorVideo> findByBaseIdAndStatusCode(String baseId,String statusCode);

    @Modifying
    @Query(value = "update monitor_base set vedioUrl=?2 where deviceSerial=?1",nativeQuery = true)
    void upData(String ds, String url1);
}
