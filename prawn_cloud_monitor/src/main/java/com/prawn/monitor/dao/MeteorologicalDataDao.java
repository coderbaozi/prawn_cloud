package com.prawn.monitor.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.prawn.monitor.pojo.MeteorologicalData;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface MeteorologicalDataDao extends JpaRepository<MeteorologicalData,String>,JpaSpecificationExecutor<MeteorologicalData>{

    @Query(value = "SELECT  ?1 FROM `monitor_meteorological_data`  where equipmentId=?2 and acquisitionTime > ?3 and acquisitionTime < ?4 ",nativeQuery = true)
    Page<Double> querySource(String checkItemName, String equipmentId, String startTime, String endTime, Pageable pageable);

}
