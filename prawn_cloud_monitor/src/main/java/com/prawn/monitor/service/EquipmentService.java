package com.prawn.monitor.service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import util.IdWorker;

import com.prawn.monitor.dao.EquipmentDao;
import com.prawn.monitor.pojo.Equipment;

/**
 * 服务层
 *
 * @author Administrator
 *
 */
@Service
public class EquipmentService {

    @Autowired
    private EquipmentDao equipmentDao;

    @Autowired
    private IdWorker idWorker;

    /**
     * 根据基地id和typeId查询所有设备,分页
     */
    public Page<Equipment> findAllByBaseId(String baseId,String typeId,int page,int size){
        PageRequest pageRequest =  PageRequest.of(page-1, size);

        Map whereMap = new HashMap();
        whereMap.put("baseId",baseId);
        whereMap.put("typeId",typeId);

        Specification<Equipment> specification = createSpecification(whereMap);
        return equipmentDao.findAll(specification,pageRequest);
    }
    /**
     * 查询全部列表
     * @return
     */
    public List<Equipment> findAll() {
        return equipmentDao.findAll();
    }


    /**
     * 条件查询+分页
     * @param whereMap
     * @param page
     * @param size
     * @return
     */
    public Page<Equipment> findSearch(Map whereMap, int page, int size) {
        Specification<Equipment> specification = createSpecification(whereMap);
        PageRequest pageRequest =  PageRequest.of(page-1, size);
        return equipmentDao.findAll(specification, pageRequest);
    }


    /**
     * 条件查询
     * @param whereMap
     * @return
     */
    public List<Equipment> findSearch(Map whereMap) {
        Specification<Equipment> specification = createSpecification(whereMap);
        return equipmentDao.findAll(specification);
    }

    /**
     * 根据ID查询实体
     * @param id
     * @return
     */
    public Equipment findById(String id) {
        return equipmentDao.findById(id).get();
    }

    /**
     * 增加
     * @param equipment
     */
    public void add(Equipment equipment) {
        equipment.setId( idWorker.nextId()+"" );
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        equipment.setUpdateTime(timestamp);
        equipmentDao.save(equipment);
    }


    /**
     * 修改
     * @param equipment
     */
    public void update(Equipment equipment) {
        equipmentDao.save(equipment);
    }

    /**
     * 删除
     * @param id
     */
    public void deleteById(String id) {
        equipmentDao.deleteById(id);
    }

    /**
     * 动态条件构建
     * @param searchMap
     * @return
     */
    private Specification<Equipment> createSpecification(Map searchMap) {

        return new Specification<Equipment>() {

            @Override
            public Predicate toPredicate(Root<Equipment> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                List<Predicate> predicateList = new ArrayList<Predicate>();
                // id
                if (searchMap.get("id")!=null && !"".equals(searchMap.get("id"))) {
                    predicateList.add(cb.like(root.get("id").as(String.class), "%"+(String)searchMap.get("id")+"%"));
                }
                // 登录用户id
                if (searchMap.get("userId")!=null && !"".equals(searchMap.get("userId"))) {
                    predicateList.add(cb.like(root.get("userId").as(String.class), "%"+(String)searchMap.get("userId")+"%"));
                }
                // 设备名称
                if (searchMap.get("equipmentName")!=null && !"".equals(searchMap.get("equipmentName"))) {
                    predicateList.add(cb.like(root.get("equipmentName").as(String.class), "%"+(String)searchMap.get("equipmentName")+"%"));
                }
                // 类型id
                if (searchMap.get("typeId")!=null && !"".equals(searchMap.get("typeId"))) {
                    predicateList.add(cb.like(root.get("typeId").as(String.class), "%"+(String)searchMap.get("typeId")+"%"));
                }
                // 基地id
                //这里的 % % 被我去掉了，因为不能模糊查询
                if (searchMap.get("baseId")!=null && !"".equals(searchMap.get("baseId"))) {
                    predicateList.add(cb.like(root.get("baseId").as(String.class), (String)searchMap.get("baseId")));
                }
                // 接口所需用户名
                if (searchMap.get("username")!=null && !"".equals(searchMap.get("username"))) {
                    predicateList.add(cb.like(root.get("username").as(String.class), "%"+(String)searchMap.get("username")+"%"));
                }
                // 接口所需密码
                if (searchMap.get("password")!=null && !"".equals(searchMap.get("password"))) {
                    predicateList.add(cb.like(root.get("password").as(String.class), "%"+(String)searchMap.get("password")+"%"));
                }
                // 接口所需设备编号
                if (searchMap.get("equipmentId")!=null && !"".equals(searchMap.get("equipmentId"))) {
                    predicateList.add(cb.like(root.get("equipmentId").as(String.class), "%"+(String)searchMap.get("equipmentId")+"%"));
                }
                // 接口所需id
                if (searchMap.get("custid")!=null && !"".equals(searchMap.get("custid"))) {
                    predicateList.add(cb.like(root.get("custid").as(String.class), "%"+(String)searchMap.get("custid")+"%"));
                }
                // 接口所需,但基本不用
                if (searchMap.get("sessionkey")!=null && !"".equals(searchMap.get("sessionkey"))) {
                    predicateList.add(cb.like(root.get("sessionkey").as(String.class), "%"+(String)searchMap.get("sessionkey")+"%"));
                }
                // 标记删除
                if (searchMap.get("deleted")!=null && !"".equals(searchMap.get("deleted"))) {
                    predicateList.add(cb.like(root.get("deleted").as(String.class), "%"+(String)searchMap.get("deleted")+"%"));
                }

                return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

            }
        };

    }

}
