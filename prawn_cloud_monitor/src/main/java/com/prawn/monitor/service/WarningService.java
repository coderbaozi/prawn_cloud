package com.prawn.monitor.service;

import java.sql.Timestamp;
import java.util.*;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import util.IdWorker;

import com.prawn.monitor.dao.WarningDao;
import com.prawn.monitor.pojo.Warning;

/**
 * 服务层
 * 
 * @author Administrator
 *
 */
@Service
public class WarningService {

	@Autowired
	private WarningDao warningDao;
	
	@Autowired
	private IdWorker idWorker;

	/**
	 * 根据设备id查询
	 */
	public List<Warning> findByEquipmentId(String equipmentId){
		Map whereMap = new HashMap();
		whereMap.put("equipmentId",equipmentId);
		Specification<Warning> specification = createSpecification(whereMap);
		return warningDao.findAll(specification);
	}
	/**
	 * 查询全部列表
	 * @return
	 */
	public List<Warning> findAll() {
		return warningDao.findAll();
	}

	
	/**
	 * 条件查询+分页
	 * @param whereMap
	 * @param page
	 * @param size
	 * @return
	 */
	public Page<Warning> findSearch(Map whereMap, int page, int size) {
		Specification<Warning> specification = createSpecification(whereMap);
		PageRequest pageRequest =  PageRequest.of(page-1, size);
		return warningDao.findAll(specification, pageRequest);
	}

	
	/**
	 * 条件查询
	 * @param whereMap
	 * @return
	 */
	public List<Warning> findSearch(Map whereMap) {
		Specification<Warning> specification = createSpecification(whereMap);
		return warningDao.findAll(specification);
	}

	/**
	 * 根据ID查询实体
	 * @param id
	 * @return
	 */
	public Warning findById(String id) {
		return warningDao.findById(id).get();
	}

	/**
	 * 增加
	 * @param warning
	 */
	public void add(Warning warning) {
		warning.setId( idWorker.nextId()+"" );
		Date date = new Date();
		Timestamp timestamp = new Timestamp(date.getTime());
		warning.setUpdateDate(timestamp);
		warningDao.save(warning);
	}

	/**
	 * 修改
	 * @param warning
	 */
	public void update(Warning warning) {
		warningDao.save(warning);
	}

	/**
	 * 删除
	 * @param id
	 */
	public void deleteById(String id) {
		warningDao.deleteById(id);
	}

	/**
	 * 动态条件构建
	 * @param searchMap
	 * @return
	 */
	private Specification<Warning> createSpecification(Map searchMap) {

		return new Specification<Warning>() {

			@Override
			public Predicate toPredicate(Root<Warning> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<Predicate>();
                // id
                if (searchMap.get("id")!=null && !"".equals(searchMap.get("id"))) {
                	predicateList.add(cb.equal(root.get("id").as(String.class), (String)searchMap.get("id")));
                }
                // 设备id
				//这里的 % % 被我去掉了，因为不能模糊查询
                if (searchMap.get("equipmentId")!=null && !"".equals(searchMap.get("equipmentId"))) {
                	predicateList.add(cb.equal(root.get("equipmentId").as(String.class), (String)searchMap.get("equipmentId")));
                }
                // 通道的名称
                if (searchMap.get("channelName")!=null && !"".equals(searchMap.get("channelName"))) {
                	predicateList.add(cb.equal(root.get("channelName").as(String.class), (String)searchMap.get("channelName")));
                }
                // 单位
                if (searchMap.get("unit")!=null && !"".equals(searchMap.get("unit"))) {
                	predicateList.add(cb.equal(root.get("unit").as(String.class), (String)searchMap.get("unit")));
                }
                // 是否启用，0关，1开
                if (searchMap.get("isUse")!=null && !"".equals(searchMap.get("isUse"))) {
                	predicateList.add(cb.equal(root.get("isUse").as(String.class), (String)searchMap.get("isUse")));
                }
				
				return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

			}
		};

	}

}
