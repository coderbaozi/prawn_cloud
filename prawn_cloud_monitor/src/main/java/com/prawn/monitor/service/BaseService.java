package com.prawn.monitor.service;

import java.sql.Timestamp;
import java.util.*;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import util.IdWorker;

import com.prawn.monitor.dao.BaseDao;
import com.prawn.monitor.pojo.Base;

/**
 * 服务层
 * 
 * @author Administrator
 *
 */
@Service
public class BaseService {

	@Autowired
	private BaseDao baseDao;
	
	@Autowired
	private IdWorker idWorker;

	/**
	 * 查询全部列表
	 * @return
	 */
	public List<Base> findAll() {
		return baseDao.findAll();
	}

	
	/**
	 * 条件查询+分页
	 * @param whereMap
	 * @param page
	 * @param size
	 * @return
	 */
	public Page<Base> findSearch(Map whereMap, int page, int size) {
		Specification<Base> specification = createSpecification(whereMap);
		PageRequest pageRequest =  PageRequest.of(page-1, size);
		return baseDao.findAll(specification, pageRequest);
	}

	
	/**
	 * 条件查询
	 * @param whereMap
	 * @return
	 */
	public List<Base> findSearch(Map whereMap) {
		Specification<Base> specification = createSpecification(whereMap);
		return baseDao.findAll(specification);
	}

	/**
	 * 根据ID查询实体
	 * @param id
	 * @return
	 */
	public Base findById(String id) {
		return baseDao.findById(id).get();
	}

	/**
	 * 增加
	 * @param base
	 */
	public void add(Base base) {
		base.setId( idWorker.nextId()+"" );
		Date date = new Date();
		Timestamp timestamp = new Timestamp(date.getTime());
		base.setCreateDate(timestamp);
		baseDao.save(base);
	}

	/**
	 * 修改
	 * @param base
	 */
	public void update(Base base) {
		baseDao.save(base);
	}

	/**
	 * 删除
	 * @param id
	 */
	public void deleteById(String id) {
		baseDao.deleteById(id);
	}

	/**
	 * 动态条件构建
	 * @param searchMap
	 * @return
	 */
	private Specification<Base> createSpecification(Map searchMap) {

		return new Specification<Base>() {

			@Override
			public Predicate toPredicate(Root<Base> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<Predicate>();
                // id
                if (searchMap.get("id")!=null && !"".equals(searchMap.get("id"))) {
                	predicateList.add(cb.equal(root.get("id").as(String.class), (String)searchMap.get("id")));
                }
                // 基地编号
                if (searchMap.get("baseId")!=null && !"".equals(searchMap.get("baseId"))) {
                	predicateList.add(cb.equal(root.get("baseId").as(String.class), (String)searchMap.get("baseId")));
                }
                // 视频名称
                if (searchMap.get("videoName")!=null && !"".equals(searchMap.get("videoName"))) {
                	predicateList.add(cb.equal(root.get("videoName").as(String.class), (String)searchMap.get("videoName")));
                }
                // 摄像头监控位置
                if (searchMap.get("monitoringLocation")!=null && !"".equals(searchMap.get("monitoringLocation"))) {
                	predicateList.add(cb.equal(root.get("monitoringLocation").as(String.class), (String)searchMap.get("monitoringLocation")));
                }
                // 视频路径
                if (searchMap.get("vedioUrl")!=null && !"".equals(searchMap.get("vedioUrl"))) {
                	predicateList.add(cb.equal(root.get("vedioUrl").as(String.class), (String)searchMap.get("vedioUrl")));
                }
                // 创建者
                if (searchMap.get("createBy")!=null && !"".equals(searchMap.get("createBy"))) {
                	predicateList.add(cb.equal(root.get("createBy").as(String.class), (String)searchMap.get("createBy")));
                }
				
				return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

			}
		};

	}

}
