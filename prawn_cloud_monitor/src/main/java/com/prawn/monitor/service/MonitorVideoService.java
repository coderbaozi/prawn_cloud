package com.prawn.monitor.service;

import com.alibaba.fastjson.JSONObject;
import com.prawn.monitor.dao.MonitorVideoDao;
import com.prawn.monitor.pojo.MonitorVideo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import util.IdWorker;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Transactional
@Service
public class MonitorVideoService {
    @Autowired
    private MonitorVideoDao monitorVideoDao;
    @Autowired
    private IdWorker idWorker;
//    public List<MonitorVideo>findAll(String baseId,int page,int size){
//        List<MonitorVideo> monitorVideos = monitorVideoDao.findByBaseId(baseId,pageRequest);
//
//        return monitorVideoDao.findAll();
//    }
    public List<MonitorVideo> findByBaseId(String baseId){
        return monitorVideoDao.findByBaseId(baseId);
    }
    public Page<MonitorVideo> findByBaseId(String baseId,int page,int size){
        Pageable pageRequest =  PageRequest.of(page-1, size);
        return monitorVideoDao.findByBaseId(baseId,pageRequest);
    }
    public void add(MonitorVideo monitorVideo){
        monitorVideo.setId(idWorker.nextId()+"");
        Date date = new Date();
        monitorVideo.setCreateDate(date);
        String url="https://open.ys7.com/api/lapp/token/get";
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/x-www-form-urlencoded");
        MultiValueMap<String, Object> postParameters = new LinkedMultiValueMap<>();
        postParameters.add("appKey", "c6fe0bfab1714f9cb05efaa3c52e5e07");
        postParameters.add("appSecret", "30baac8733d378059e4c37b37898bc94");
        HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<>(postParameters, headers);
        RestTemplate client = new RestTemplate();
        JSONObject s = client.postForObject(url, httpEntity, JSONObject.class);
        assert s != null;
        JSONObject data = s.getJSONObject("data");
        String accessToken = data.getString("accessToken");
        monitorVideo.setAccessToken(accessToken);
        String url1="https://open.ys7.com/ezopen/h5/iframe_se?url=ezopen://"+monitorVideo.getValidateCode()+"@open.ys7.com/"+monitorVideo.getDeviceSerial()+"/1.live&autoplay=0&audio=1&accessToken="+monitorVideo.getAccessToken()+"&templete=2";
        monitorVideo.setVedioUrl(url1);
        monitorVideoDao.save(monitorVideo);
    }
    public void deleteById(String id){
        monitorVideoDao.deleteById(id);
    }

    public MonitorVideo findId(String id) {
        return monitorVideoDao.findById(id).get();
    }

    public Page<MonitorVideo> findAll(int page, int size) {
        PageRequest pageRequest =  PageRequest.of(page-1, size);
        return monitorVideoDao.findAll(pageRequest);
    }

    public void modifyStatus(String id, String status) {
        monitorVideoDao.modifyStatus(id,status);
    }

    public void upData(MonitorVideo monitorVideo) {
        String url = "https://open.ys7.com/api/lapp/token/get";
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/x-www-form-urlencoded");
        MultiValueMap<String, Object> postParameters = new LinkedMultiValueMap<>();
        postParameters.add("appKey", "c6fe0bfab1714f9cb05efaa3c52e5e07");
        postParameters.add("appSecret", "30baac8733d378059e4c37b37898bc94");
        HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<>(postParameters, headers);
        RestTemplate client = new RestTemplate();
        JSONObject s = client.postForObject(url, httpEntity, JSONObject.class);
        assert s != null;
        JSONObject data = s.getJSONObject("data");
        String accessToken = data.getString("accessToken");
        monitorVideo.setAccessToken(accessToken);
        String url1 = "https://open.ys7.com/ezopen/h5/iframe_se?url=ezopen://" + monitorVideo.getValidateCode() + "@open.ys7.com/" + monitorVideo.getDeviceSerial() + "/1.live&autoplay=0&audio=1&accessToken=" + monitorVideo.getAccessToken() + "&templete=2";
        String ds = monitorVideo.getDeviceSerial();
        monitorVideoDao.upData(ds, url1);
    }

    /**
     * 溯源模块 用户查看基地监控
     * @param baseId
     * @return
     */
    public List<MonitorVideo> traceMonitor(String baseId){
        return monitorVideoDao.findByBaseIdAndStatusCode(baseId,"1");
    }
}
