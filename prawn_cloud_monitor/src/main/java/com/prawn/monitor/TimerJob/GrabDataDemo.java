package com.prawn.monitor.TimerJob;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.prawn.monitor.pojo.MeteorologicalData;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import util.IdWorker;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class GrabDataDemo {

    @Autowired
    private IdWorker idWorker;

//    //方法入口
//    public static void main(String[] args) throws Exception {
//        String addr="60817411";
//        String startTime="2019-10-08%2009:00:00";
//        String endTime="2019-10-08%2011:00:00";
//        String channelNumber="";
//        List<MeteorologicalData> ls = getHistoryData(addr, startTime, endTime, channelNumber);
//
//        for (MeteorologicalData md: ls
//             ) {
//            System.out.println(md);
//
//        }
//
//
//    }

    public List<MeteorologicalData> getHistoryData(String addr, String startTime, String endTime, String channelNumber) throws ParseException {
        StringBuilder bd = new StringBuilder("http://139.159.250.217:89/qfls/api?method=getDeviceSensorDataFromTime&");
//        checkParam();
        bd.append("addr=" + addr);
        bd.append("&startTime=" + startTime);
        bd.append("&endTime=" + endTime);
        String response = pickData(bd.toString());
        JSONObject object = (JSONObject) JSONObject.parse(response);
        String data = object.get("dataarr").toString();


        MeteorologicalData meteorologicalData;
        List<MeteorologicalData> list = new ArrayList<>(20);
        //JSONArray相当于list
        JSONArray jsonArray = JSONArray.parseArray(data);
        //JSONObject相当于map
        for (int i = 0; i < jsonArray.size(); i++) {
            meteorologicalData = new MeteorologicalData();
            meteorologicalData.setId(idWorker.nextId() + "");
            meteorologicalData.setEquipmentId(addr);


            JSONObject job = jsonArray.getJSONObject(i);
            String time = job.get("gatherTime").toString();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date date = simpleDateFormat.parse(time);
            meteorologicalData.setAcquisitionTime(date);
            meteorologicalData.buildElectricEnergy(Double.parseDouble(job.get("column_0").toString())).
                    buildIllumination(Double.parseDouble(job.get("column_1").toString())).
                    buildwindSpeed(Double.parseDouble(job.get("column_2").toString())).
                    buildwindDirect(Double.parseDouble(job.get("column_3").toString())).
                    buildairTemperature(Double.parseDouble(job.get("column_4").toString())).
                    buildhumidity(Double.parseDouble(job.get("column_5").toString())).
                    buildrain(Double.parseDouble(job.get("column_6").toString())).
                    buildsoilTemperature(Double.parseDouble(job.get("column_8").toString())).
                    buildsoilMoisture(Double.parseDouble(job.get("column_9").toString()));
            list.add(meteorologicalData);
        }


        return list;

    }


//    private static void checkParam(String param) {
//
//
//    }


    /*
     * 爬取网页信息
     */
    private static String pickData(String url) {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        try {
            HttpGet httpget = new HttpGet(url);
            CloseableHttpResponse response = httpclient.execute(httpget);
            try {
                // 获取响应实体
                HttpEntity entity = response.getEntity();
                // 打印响应状态
                if (entity != null) {
                    return EntityUtils.toString(entity);
                }
            } finally {
                response.close();
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // 关闭连接,释放资源
            try {
                httpclient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }


}
