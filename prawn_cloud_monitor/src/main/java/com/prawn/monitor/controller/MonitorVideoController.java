package com.prawn.monitor.controller;

import com.prawn.monitor.pojo.MonitorVideo;
import com.prawn.monitor.service.MonitorVideoService;
import entity.PageResult;
import entity.Result;
import entity.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/monitor")
public class MonitorVideoController {
    @Autowired
    private MonitorVideoService videoService;

    /**
     * 溯源模块调用接口:用户查看该基地监控
     *
     * @return
     */
    @RequestMapping(value = "/trace", method = RequestMethod.GET)
    public Result traceMonitor(@RequestParam("baseId") String baseId) {
        List<MonitorVideo> monitorVideoList = videoService.traceMonitor(baseId);
        if (monitorVideoList.size() == 0) {
            return new Result(false, StatusCode.ACCESSERROR, "基地未开放监控");
        }
        return new Result(true, StatusCode.OK, "调用成功", monitorVideoList);
    }

    @RequestMapping(value = "/{page}/{size}", method = RequestMethod.GET)
    public Result findAll(@PathVariable int page, @PathVariable int size) {
        Page<MonitorVideo> mv = videoService.findAll(page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<MonitorVideo>(mv.getTotalElements(), mv.getContent()));
    }

    @RequestMapping(value = "/{baseId}", method = RequestMethod.POST)
    public Result findByBaseId(@PathVariable String baseId) {
        return new Result(true, StatusCode.OK, "查询成功", videoService.findByBaseId(baseId));
    }

    @RequestMapping(value = "/{baseId}/{page}/{size}", method = RequestMethod.POST)
    public Result findByBaseId(@PathVariable String baseId, @PathVariable int page, @PathVariable int size) {
        Page<MonitorVideo> mv = videoService.findByBaseId(baseId, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<MonitorVideo>(mv.getTotalElements(), mv.getContent()));
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result add(@RequestBody MonitorVideo monitorVideo) {
        videoService.add(monitorVideo);
        return new Result(true, StatusCode.OK, "添加成功");
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public Result delete(@PathVariable String id) {
        videoService.deleteById(id);
        return new Result(true, StatusCode.OK, "删除成功");
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Result findByid(@PathVariable String id) {
        return new Result(true, StatusCode.OK, "查询成功", videoService.findId(id));
    }

    @RequestMapping(value = "/modify/{id}/{status}", method = RequestMethod.GET)
    public Result modify(@PathVariable String id, @PathVariable String status) {
        videoService.modifyStatus(id, status);
        return new Result(true, StatusCode.OK, "修改成功");
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public Result upData(@RequestBody MonitorVideo monitorVideo) {
        videoService.upData(monitorVideo);
        MonitorVideo vs = videoService.findId(monitorVideo.getId());
        return new Result(true, StatusCode.OK, "刷新成功", vs);
    }
}
