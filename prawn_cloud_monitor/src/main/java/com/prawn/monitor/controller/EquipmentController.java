package com.prawn.monitor.controller;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.prawn.monitor.pojo.Equipment;
import com.prawn.monitor.service.EquipmentService;

import entity.PageResult;
import entity.Result;
import entity.StatusCode;
/**
 * 控制器层
 * @author Administrator
 *
 */
@RestController
@CrossOrigin
@RequestMapping("/equipment")
public class EquipmentController {

    @Autowired
    private EquipmentService equipmentService;


    /**
     * 根据基地id和typeId查询所有设备,分页
     */
    @RequestMapping(value="/findAllByBaseId/{baseId}/{typeId}/{page}/{size}",method= RequestMethod.GET)
    public Result findAllByBaseId(@PathVariable String baseId, @PathVariable String typeId,@PathVariable int page, @PathVariable int size){
        return new Result(true,StatusCode.OK,"查询成功", equipmentService.findAllByBaseId(baseId, typeId, page, size));
    }

    //根据基地id查询池塘调用溯源模块的接口就好了

    /**
     * 查询全部数据
     * @return
     */
    @RequestMapping(method= RequestMethod.GET)
    public Result findAll(){
        return new Result(true,StatusCode.OK,"查询成功", equipmentService.findAll());
    }

    /**
     * 根据ID查询
     * @param id ID
     * @return
     */
    @RequestMapping(value="/{id}",method= RequestMethod.GET)
    public Result findById(@PathVariable String id){
        return new Result(true,StatusCode.OK,"查询成功", equipmentService.findById(id));
    }


    /**
     * 分页+多条件查询
     * @param searchMap 查询条件封装
     * @param page 页码
     * @param size 页大小
     * @return 分页结果
     */
    @RequestMapping(value="/search/{page}/{size}",method=RequestMethod.POST)
    public Result findSearch(@RequestBody Map searchMap , @PathVariable int page, @PathVariable int size){
        Page<Equipment> pageList = equipmentService.findSearch(searchMap, page, size);
        return  new Result(true,StatusCode.OK,"查询成功",  new PageResult<Equipment>(pageList.getTotalElements(), pageList.getContent()) );
    }

    /**
     * 根据条件查询
     * @param searchMap
     * @return
     */
    @RequestMapping(value="/search",method = RequestMethod.POST)
    public Result findSearch( @RequestBody Map searchMap){
        return new Result(true,StatusCode.OK,"查询成功", equipmentService.findSearch(searchMap));
    }

    /**
     * 增加
     * @param equipment
     */
    @RequestMapping(method=RequestMethod.POST)
    public Result add(@RequestBody Equipment equipment){
        equipmentService.add(equipment);
        return new Result(true,StatusCode.OK,"增加成功");
    }

    /**
     * 修改
     * @param equipment
     */
    @RequestMapping(value="/{id}",method= RequestMethod.PUT)
    public Result update(@RequestBody Equipment equipment, @PathVariable String id ){
        equipment.setId(id);
        equipmentService.update(equipment);
        return new Result(true,StatusCode.OK,"修改成功");
    }

    /**
     * 删除
     * @param id
     */
    @RequestMapping(value="/{id}",method= RequestMethod.DELETE)
    public Result delete(@PathVariable String id ){
        equipmentService.deleteById(id);
        return new Result(true,StatusCode.OK,"删除成功");
    }

}
