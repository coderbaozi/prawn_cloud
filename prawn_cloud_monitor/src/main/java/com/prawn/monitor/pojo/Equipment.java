package com.prawn.monitor.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="monitor_equipment")
public class Equipment implements Serializable{

    @Id
    private String id;//id

    private String userId;//登录用户id
    private String equipmentName;//设备名称
    private String username;//接口所需用户名
    private String password;//接口所需密码

    private String poodId;//池塘id

    private String equipmentId;//接口所需设备编号
    private String typeId;//设备类型（0为气象，1为水质）
    private String custid;//接口所需id
    private String baseId;//基地id
    private String baseName;//基地名称
    private String manufactorId;//设备厂商
    private String sessionkey;//接口所需,但基本不用
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GM+8")
    private java.util.Date updateTime;//添加时间
    private String deleted;//标记删除
    private String remarks;//备注


    public String getPoodId() {
        return poodId;
    }

    public void setPoodId(String poodId) {
        this.poodId = poodId;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getManufactorId() {
        return manufactorId;
    }

    public void setManufactorId(String manufactorId) {
        this.manufactorId = manufactorId;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getBaseName() {
        return baseName;
    }

    public void setBaseName(String baseName) {
        this.baseName = baseName;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEquipmentName() {
        return equipmentName;
    }
    public void setEquipmentName(String equipmentName) {
        this.equipmentName = equipmentName;
    }

    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public String getEquipmentId() {
        return equipmentId;
    }
    public void setEquipmentId(String equipmentId) {
        this.equipmentId = equipmentId;
    }

    public String getCustid() {
        return custid;
    }
    public void setCustid(String custid) {
        this.custid = custid;
    }

    public String getBaseId() {
        return baseId;
    }
    public void setBaseId(String baseId) {
        this.baseId = baseId;
    }

    public String getSessionkey() {
        return sessionkey;
    }
    public void setSessionkey(String sessionkey) {
        this.sessionkey = sessionkey;
    }

    public java.util.Date getUpdateTime() {
        return updateTime;
    }
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getDeleted() {
        return deleted;
    }
    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }



}
