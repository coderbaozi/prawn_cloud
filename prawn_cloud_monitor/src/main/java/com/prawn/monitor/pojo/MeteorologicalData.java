package com.prawn.monitor.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="monitor_meteorological_data")
public class MeteorologicalData implements Serializable{

	@Id
	private String id;//id
	
	private String equipmentId;//所属设备编号
	private String baseId;//基地id

	private Double electricEnergy;//电能
	private String electricEnergyUnit="mV";
	private Double illumination;//光照
	private String illuminationUnit="Lux";
	private Double windSpeed;//风速
	private String windSpeedUnit="m/s";
	private Double windDirect;//风向
	private String windDirectUnit="度";
	private Double airTemperature;//气温
	private String airTemperatureUnit="℃";
	private Double humidity;//湿度
	private String humidityUnit="%";
	private Double rain;//雨量
	private String rainUnit="mm";
	private Double soilTemperature;//土温
	private String soilTemperatureUnit="℃";
	private Double soilMoisture;//土湿


	private String soiMoistureUnit="%";
	private Double cam;//
	private String CAMUnit;//
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GM+8")
	private java.util.Date acquisitionTime;//采集时间
	private String deleted;//标记删除
	private String equipmentWorking;//气象设备工作状态0（关闭）1（工作中）2（故障）



	public Double getCheckItem(String checkName){
		if(checkName.equals("electricEnergy"))
			return this.electricEnergy;
		else if(checkName.equals("illumination"))
			return this.illumination;
		else if(checkName.equals("windSpeed"))
			return this.windSpeed;
		else if(checkName.equals("windDirect"))
			return this.windDirect;
		else if(checkName.equals("airTemperature"))
			return this.airTemperature;
		else if(checkName.equals("humidity"))
			return this.humidity;
		else if(checkName.equals("rain"))
			return this.rain;
		else if(checkName.equals("soilTemperature"))
			return this.soilTemperature;
		else if(checkName.equals("soilMoisture"))
			return this.soilMoisture;
		else if(checkName.equals("cam"))
			return this.cam;
		else
			return -1.0;


	}

	public String getBaseId() {
		return baseId;
	}

	public void setBaseId(String baseId) {
		this.baseId = baseId;
	}

	public MeteorologicalData buildElectricEnergy(Double electricEnergy){
		this.electricEnergy=electricEnergy;
		return this;
	}
	public MeteorologicalData buildIllumination(Double illumination){
		this.illumination=illumination;
		return this;
	}public MeteorologicalData buildwindSpeed(Double windSpeed){
		this.windSpeed=windSpeed;
		return this;
	}public MeteorologicalData buildwindDirect(Double windDirect){
		this.windDirect=windDirect;
		return this;
	}public MeteorologicalData buildairTemperature(Double airTemperature){
		this.airTemperature=airTemperature;
		return this;
	}public MeteorologicalData buildhumidity(Double humidity){
		this.humidity=humidity;
		return this;
	}public MeteorologicalData buildrain(Double rain){
		this.rain=rain;
		return this;
	}
	public MeteorologicalData buildsoilTemperature(Double soilTemperature){
		this.soilTemperature=soilTemperature;
		return this;
	}
	public MeteorologicalData buildsoilMoisture(Double soilMoisture){
		this.soilMoisture=soilMoisture;
		return this;
	}


	public Double getCam() {
		return cam;
	}

	public void setCam(Double cam) {
		this.cam = cam;
	}

	public String getCAMUnit() {
		return CAMUnit;
	}

	public void setCAMUnit(String CAMUnit) {
		this.CAMUnit = CAMUnit;
	}



	public String getId() {		
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getEquipmentId() {		
		return equipmentId;
	}
	public void setEquipmentId(String equipmentId) {
		this.equipmentId = equipmentId;
	}

	public Double getElectricEnergy() {		
		return electricEnergy;
	}
	public void setElectricEnergy(Double electricEnergy) {
		this.electricEnergy = electricEnergy;
	}

	public String getElectricEnergyUnit() {		
		return electricEnergyUnit;
	}
	public void setElectricEnergyUnit(String electricEnergyUnit) {
		this.electricEnergyUnit = electricEnergyUnit;
	}

	public Double getIllumination() {		
		return illumination;
	}
	public void setIllumination(Double illumination) {
		this.illumination = illumination;
	}

	public String getIlluminationUnit() {		
		return illuminationUnit;
	}
	public void setIlluminationUnit(String illuminationUnit) {
		this.illuminationUnit = illuminationUnit;
	}

	public Double getWindSpeed() {		
		return windSpeed;
	}
	public void setWindSpeed(Double windSpeed) {
		this.windSpeed = windSpeed;
	}

	public String getWindSpeedUnit() {		
		return windSpeedUnit;
	}
	public void setWindSpeedUnit(String windSpeedUnit) {
		this.windSpeedUnit = windSpeedUnit;
	}

	public Double getWindDirect() {		
		return windDirect;
	}
	public void setWindDirect(Double windDirect) {
		this.windDirect = windDirect;
	}

	public String getWindDirectUnit() {		
		return windDirectUnit;
	}
	public void setWindDirectUnit(String windDirectUnit) {
		this.windDirectUnit = windDirectUnit;
	}

	public Double getAirTemperature() {		
		return airTemperature;
	}
	public void setAirTemperature(Double airTemperature) {
		this.airTemperature = airTemperature;
	}

	public String getAirTemperatureUnit() {		
		return airTemperatureUnit;
	}
	public void setAirTemperatureUnit(String airTemperatureUnit) {
		this.airTemperatureUnit = airTemperatureUnit;
	}

	public Double getHumidity() {		
		return humidity;
	}
	public void setHumidity(Double humidity) {
		this.humidity = humidity;
	}

	public String getHumidityUnit() {		
		return humidityUnit;
	}
	public void setHumidityUnit(String humidityUnit) {
		this.humidityUnit = humidityUnit;
	}

	public Double getRain() {		
		return rain;
	}
	public void setRain(Double rain) {
		this.rain = rain;
	}

	public String getRainUnit() {		
		return rainUnit;
	}
	public void setRainUnit(String rainUnit) {
		this.rainUnit = rainUnit;
	}

	public Double getSoilTemperature() {		
		return soilTemperature;
	}
	public void setSoilTemperature(Double soilTemperature) {
		this.soilTemperature = soilTemperature;
	}

	public String getSoilTemperatureUnit() {		
		return soilTemperatureUnit;
	}
	public void setSoilTemperatureUnit(String soilTemperatureUnit) {
		this.soilTemperatureUnit = soilTemperatureUnit;
	}

	public Double getSoilMoisture() {		
		return soilMoisture;
	}
	public void setSoilMoisture(Double soilMoisture) {
		this.soilMoisture = soilMoisture;
	}

	public String getSoiMoistureUnit() {		
		return soiMoistureUnit;
	}
	public void setSoiMoistureUnit(String soiMoistureUnit) {
		this.soiMoistureUnit = soiMoistureUnit;
	}



	public java.util.Date getAcquisitionTime() {		
		return acquisitionTime;
	}
	public void setAcquisitionTime(java.util.Date acquisitionTime) {
		this.acquisitionTime = acquisitionTime;
	}

	public String getDeleted() {		
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}

	public String getEquipmentWorking() {		
		return equipmentWorking;
	}
	public void setEquipmentWorking(String equipmentWorking) {
		this.equipmentWorking = equipmentWorking;
	}


	
}
