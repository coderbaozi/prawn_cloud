package com.prawn.monitor.pojo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="monitor_base")
public class Base implements Serializable{

	@Id
	private String id;//id


	
	private String baseId;//基地编号

	public String getBaseName() {
		return baseName;
	}

	public void setBaseName(String baseName) {
		this.baseName = baseName;
	}

	private String baseName;//基地名称
	private String videoName;//视频名称
	private String monitoringLocation;//摄像头监控位置
	private String vedioUrl;//视频路径
	private String createBy;//创建者
	private java.util.Date createDate;//创建时间

	
	public String getId() {		
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getBaseId() {		
		return baseId;
	}
	public void setBaseId(String baseId) {
		this.baseId = baseId;
	}

	public String getVideoName() {		
		return videoName;
	}
	public void setVideoName(String videoName) {
		this.videoName = videoName;
	}

	public String getMonitoringLocation() {		
		return monitoringLocation;
	}
	public void setMonitoringLocation(String monitoringLocation) {
		this.monitoringLocation = monitoringLocation;
	}

	public String getVedioUrl() {		
		return vedioUrl;
	}
	public void setVedioUrl(String vedioUrl) {
		this.vedioUrl = vedioUrl;
	}

	public String getCreateBy() {		
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public java.util.Date getCreateDate() {		
		return createDate;
	}
	public void setCreateDate(java.util.Date createDate) {
		this.createDate = createDate;
	}


	
}
