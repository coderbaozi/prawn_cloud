package com.prawn.monitor.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="monitor_water_data")
public class WaterData implements Serializable{

	@Id
	private String id;//id

	private String equipmentId;//水质设备编号
	private String baseId;//基地id
	private Double dissolvedOxygen;//溶解氧
	private Double waterTemperature;//水温
	private Double phValue;//PH值
	private Double ammoniaNitrogen;//氨氮
	private Double waterLevel;//水位

	private Double conductivity;//电导率
	private Double turbidity;//浊度
	private Double permanganateIndex;//高猛酸盐指数
	private Double phosphorus;//总磷
	private Double nitrogen;//总氮
	private Double chlorophyll;//叶绿素α
	private Double algalDensity;//藻密度



	private String pressureSensorStatus;//压力传感器状态（0）：关（1）：开
	private String floatValveStatus;//浮球阀状态（0）：关（1）：开
	private String waterQualityWorking;//水质传感器工作状态0（关闭）1（工作中）2（故障）
	private String spare1;//备用字段1
	private String spare2;//备用字段2
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GM+8")
	private java.util.Date updateTime;//上报时间
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GM+8")
	private java.util.Date acquisitionTime;//采集时间


	public Double getCheckItem(String checkName){
		if(checkName.equals("dissolvedOxygen"))
			return this.dissolvedOxygen;
		if(checkName.equals("waterTemperature"))
			return this.waterTemperature;
		if(checkName.equals("phValue"))
			return this.phValue;
		if(checkName.equals("ammoniaNitrogen"))
			return this.ammoniaNitrogen;
		if(checkName.equals("waterLevel"))
			return this.waterLevel;
		if(checkName.equals("conductivity"))
			return this.conductivity;
		if(checkName.equals("turbidity"))
			return this.turbidity;
		if(checkName.equals("permanganateIndex"))
			return this.permanganateIndex;
		if(checkName.equals("phosphorus"))
			return this.phosphorus;
		if(checkName.equals("nitrogen"))
			return this.nitrogen;
		if(checkName.equals("chlorophyll"))
			return this.chlorophyll;
		if(checkName.equals("algalDensity"))
			return this.algalDensity;

		return 0.0;

	}

	public String getBaseId() {
		return baseId;
	}

	public void setBaseId(String baseId) {
		this.baseId = baseId;
	}

	public String getPressureSensorStatus() {
		return pressureSensorStatus;
	}

	public void setPressureSensorStatus(String pressureSensorStatus) {
		this.pressureSensorStatus = pressureSensorStatus;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Date getAcquisitionTime() {
		return acquisitionTime;
	}

	public void setAcquisitionTime(Date acquisitionTime) {
		this.acquisitionTime = acquisitionTime;
	}

	private String dissolvedOxygenUnit="%";//溶解氧
	private String waterTemperatureUnit="℃";//水温
	private String phValueUnit="PH";//PH值
	private String ammoniaNitrogenUnit="%";//氨氮

	private String conductivityUnit="μS/cm";
	private String turbidityUnit="NTU";
	private String permanganateIndexUnit="mg/L";
	private String phosphorusUnit="mg/L";
	private String nitrogenUnit="mg/L";
	private String chlorophyllUnit="mg/L";
	private String algalDensityUnit="cells/L";




	private String waterLevelUnit="m";//水位






	public String getId() {		
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getEquipmentId() {		
		return equipmentId;
	}
	public void setEquipmentId(String equipmentId) {
		this.equipmentId = equipmentId;
	}

	public Double getDissolvedOxygen() {		
		return dissolvedOxygen;
	}
	public void setDissolvedOxygen(Double dissolvedOxygen) {
		this.dissolvedOxygen = dissolvedOxygen;
	}

	public Double getWaterTemperature() {
		return waterTemperature;
	}
	public void setWaterTemperature(Double water_temperature) {
		this.waterTemperature = water_temperature;
	}

	public Double getPhValue() {		
		return phValue;
	}
	public void setPhValue(Double phValue) {
		this.phValue = phValue;
	}

	public Double getAmmoniaNitrogen() {		
		return ammoniaNitrogen;
	}
	public void setAmmoniaNitrogen(Double ammoniaNitrogen) {
		this.ammoniaNitrogen = ammoniaNitrogen;
	}

	public Double getWaterLevel() {		
		return waterLevel;
	}
	public void setWaterLevel(Double waterLevel) {
		this.waterLevel = waterLevel;
	}



	public String getFloatValveStatus() {		
		return floatValveStatus;
	}
	public void setFloatValveStatus(String floatValveStatus) {
		this.floatValveStatus = floatValveStatus;
	}

	public String getWaterQualityWorking() {		
		return waterQualityWorking;
	}
	public void setWaterQualityWorking(String waterQualityWorking) {
		this.waterQualityWorking = waterQualityWorking;
	}

	public String getSpare1() {		
		return spare1;
	}
	public void setSpare1(String spare1) {
		this.spare1 = spare1;
	}

	public String getSpare2() {		
		return spare2;
	}
	public void setSpare2(String spare2) {
		this.spare2 = spare2;
	}

	public Double getConductivity() {
		return conductivity;
	}

	public void setConductivity(Double conductivity) {
		this.conductivity = conductivity;
	}

	public Double getTurbidity() {
		return turbidity;
	}

	public void setTurbidity(Double turbidity) {
		this.turbidity = turbidity;
	}

	public Double getPermanganateIndex() {
		return permanganateIndex;
	}

	public void setPermanganateIndex(Double permanganateIndex) {
		this.permanganateIndex = permanganateIndex;
	}

	public Double getPhosphorus() {
		return phosphorus;
	}

	public void setPhosphorus(Double phosphorus) {
		this.phosphorus = phosphorus;
	}

	public Double getNitrogen() {
		return nitrogen;
	}

	public void setNitrogen(Double nitrogen) {
		this.nitrogen = nitrogen;
	}

	public Double getChlorophyll() {
		return chlorophyll;
	}

	public void setChlorophyll(Double chlorophyll) {
		this.chlorophyll = chlorophyll;
	}

	public Double getAlgalDensity() {
		return algalDensity;
	}

	public void setAlgalDensity(Double algalDensity) {
		this.algalDensity = algalDensity;
	}

	public String getDissolvedOxygenUnit() {
		return dissolvedOxygenUnit;
	}

	public void setDissolvedOxygenUnit(String dissolvedOxygenUnit) {
		this.dissolvedOxygenUnit = dissolvedOxygenUnit;
	}

	public String getWaterTemperatureUnit() {
		return waterTemperatureUnit;
	}

	public void setWaterTemperatureUnit(String water_temperatureUnit) {
		this.waterTemperatureUnit = water_temperatureUnit;
	}

	public String getPhValueUnit() {
		return phValueUnit;
	}

	public void setPhValueUnit(String phValueUnit) {
		this.phValueUnit = phValueUnit;
	}

	public String getAmmoniaNitrogenUnit() {
		return ammoniaNitrogenUnit;
	}

	public void setAmmoniaNitrogenUnit(String ammoniaNitrogenUnit) {
		this.ammoniaNitrogenUnit = ammoniaNitrogenUnit;
	}

	public String getConductivityUnit() {
		return conductivityUnit;
	}

	public void setConductivityUnit(String conductivityUnit) {
		this.conductivityUnit = conductivityUnit;
	}

	public String getTurbidityUnit() {
		return turbidityUnit;
	}

	public void setTurbidityUnit(String turbidityUnit) {
		this.turbidityUnit = turbidityUnit;
	}

	public String getPermanganateIndexUnit() {
		return permanganateIndexUnit;
	}

	public void setPermanganateIndexUnit(String permanganateIndexUnit) {
		this.permanganateIndexUnit = permanganateIndexUnit;
	}

	public String getPhosphorusUnit() {
		return phosphorusUnit;
	}

	public void setPhosphorusUnit(String phosphorusUnit) {
		this.phosphorusUnit = phosphorusUnit;
	}

	public String getNitrogenUnit() {
		return nitrogenUnit;
	}

	public void setNitrogenUnit(String nitrogenUnit) {
		this.nitrogenUnit = nitrogenUnit;
	}

	public String getChlorophyllUnit() {
		return chlorophyllUnit;
	}

	public void setChlorophyllUnit(String chlorophyllUnit) {
		this.chlorophyllUnit = chlorophyllUnit;
	}

	public String getAlgalDensityUnit() {
		return algalDensityUnit;
	}

	public void setAlgalDensityUnit(String algalDensityUnit) {
		this.algalDensityUnit = algalDensityUnit;
	}

	public String getWaterLevelUnit() {
		return waterLevelUnit;
	}

	public void setWaterLevelUnit(String waterLevelUnit) {
		this.waterLevelUnit = waterLevelUnit;
	}
}
