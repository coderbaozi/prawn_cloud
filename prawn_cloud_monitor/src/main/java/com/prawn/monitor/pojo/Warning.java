package com.prawn.monitor.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="monitor_warning")
public class Warning implements Serializable{

	@Id
	private String id;//id

	private String equipmentId;//设备id
	private String channelName;//通道的名称
	private String unit;//单位
	private Double maxValues;//最大值
	private Double minValues;//最小值
	private String isUse;//是否启用，0关，1开
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GM+8")
	private java.util.Date updateDate;//更新时间

	
	public String getId() {		
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getEquipmentId() {		
		return equipmentId;
	}
	public void setEquipmentId(String equipmentId) {
		this.equipmentId = equipmentId;
	}

	public String getChannelName() {		
		return channelName;
	}
	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public String getUnit() {		
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Double getMaxValue() {		
		return maxValues;
	}
	public void setMaxValue(Double maxValue) {
		this.maxValues = maxValue;
	}

	public Double getMinValue() {		
		return minValues;
	}
	public void setMinValue(Double minValue) {
		this.minValues = minValue;
	}

	public String getIsUse() {		
		return isUse;
	}
	public void setIsUse(String isUse) {
		this.isUse = isUse;
	}

	public java.util.Date getUpdateDate() {		
		return updateDate;
	}
	public void setUpdateDate(java.util.Date updateDate) {
		this.updateDate = updateDate;
	}

	@Override
	public String toString() {
		return "Warning{" +
				"id='" + id + '\'' +
				", equipmentId='" + equipmentId + '\'' +
				", channelName='" + channelName + '\'' +
				", unit='" + unit + '\'' +
				", maxValue=" + maxValues +
				", minValue=" + minValues +
				", isUse='" + isUse + '\'' +
				", updateDate=" + updateDate +
				'}';
	}
}
