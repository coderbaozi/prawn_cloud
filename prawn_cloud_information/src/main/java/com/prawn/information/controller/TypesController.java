package com.prawn.information.controller;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.prawn.information.pojo.Information;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.prawn.information.pojo.Types;
import com.prawn.information.service.TypesService;

import entity.PageResult;
import entity.Result;
import entity.StatusCode;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * 控制器层
 *
 * @author Administrator
 */
@RestController
@CrossOrigin
@RequestMapping("/informationTypes")
@Api(value = "文章类型类公共接口", tags = "文章类型类公共接口")
public class TypesController {

    @Autowired
    private TypesService typesService;

    public TypesController() {
    }

//    /**
//     * 删除
//     *
//     * @param id
//     */
//    @ApiOperation(value = "删除类型")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "id",value = "资讯类型id",dataType = "String", paramType = "path")
//    })
//    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
//    public Result delete(@PathVariable String id) {
//        typesService.deleteById(id);
//        return new Result(true, StatusCode.OK, "删除成功");
//    }

    /**
     * 按类型编号搜索类型
     * @param id
     * @return
     */
    @ApiOperation(value = "按类型编号搜索类型")
    @ApiImplicitParam(name = "id", value = "类型编号", paramType = "path", required = true)
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Result findById(@PathVariable String id) {
        Types educationTypes = typesService.findById(id);
        return new Result(true, StatusCode.OK, "查询成功", educationTypes);
    }

}
