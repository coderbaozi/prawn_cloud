package com.prawn.information.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.prawn.information.util.FastDFSUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.omg.PortableInterceptor.INACTIVE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.prawn.information.pojo.Experts;
import com.prawn.information.service.ExpertsService;

import entity.PageResult;
import entity.Result;
import entity.StatusCode;

/**
 * 控制器层
 *
 * @author Administrator
 */
@RestController
@CrossOrigin
@RequestMapping("/experts")
@Api(value = "专家接口", tags = "专家接口")
public class ExpertsController {

    @Autowired
    private ExpertsService expertsService;

    @Autowired
    private FastDFSUtil fastDFSUtil;

    @Value("${fdfs.web-server-url}")
    private String fastdfsUrl;

    /**
     * 判断用户是否为专家
     *
     * @return
     */
    @ApiOperation(value = "判断用户是否为专家")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", paramType = "path", required = true)
    })
    @RequestMapping(value = "/isExperts/{userId}", method = RequestMethod.GET)
    public Result isExperts(@PathVariable String userId) {
//        Iterable<Experts> expertsList = expertsService.findAll();
//        for (Experts experts : expertsList) {
//            if (userId.equals(experts.getUserId())) {
//                return new Result(true, StatusCode.OK, "该用户是专家");
//            }
//        }
//        return new Result(false, StatusCode.ERROR, "该用户不是专家");
        try {
            Experts experts = expertsService.findByUserId(userId, 1);
            return new Result(true, StatusCode.OK, "该用户是专家");
        } catch (Exception e) {
            return new Result(false, StatusCode.ERROR, "该用户不是专家");
        }
    }


    /**
     * 查询全部专家
     *
     * @return
     */
    @ApiOperation(value = "查询全部专家")
    @RequestMapping(value = "/findAll", method = RequestMethod.GET)
    public Result findAll() {
        return new Result(true, StatusCode.OK, "查询成功", expertsService.findAll());
    }

    /**
     * 查询全部专家-分页
     *
     * @return
     */
    @ApiOperation(value = "查询全部专家-分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findAll/{page}/{size}", method = RequestMethod.GET)
    public Result findAll(@PathVariable int page, @PathVariable int size) {
        Page<Experts> pageList = expertsService.findAll(1, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Experts>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 根据专家ID查询专家
     *
     * @param id ID
     * @return
     */
    @ApiOperation(value = "根据专家ID查询专家")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "专家id", paramType = "path", dataType = "String", required = true)
    })
    @RequestMapping(value = "/findById/{id}", method = RequestMethod.GET)
    public Result findById(@PathVariable String id) {
        return new Result(true, StatusCode.OK, "查询成功", expertsService.findById(id));
    }

    /**
     * 根据用户ID查询专家
     *
     * @param userId ID
     * @return
     */
    @ApiOperation(value = "根据用户ID查询专家")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findByUser/{userId}", method = RequestMethod.GET)
    public Result findByUser(@PathVariable String userId) {
        List<Integer> status = new ArrayList<>();
        status.add(0);
        status.add(1);
        status.add(2);
        return new Result(true, StatusCode.OK, "查询成功", expertsService.findByUserId(userId, status));
    }

    /**
     * 判断用户是否申请专家
     *
     * @param userId ID
     * @return
     */
    @ApiOperation(value = "判断用户是否申请专家")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", paramType = "path", required = true)
    })
    @RequestMapping(value = "/isApply/{userId}", method = RequestMethod.GET)
    public Result isApply(@PathVariable String userId) {
        try {
            Experts experts = expertsService.findByUserId(userId, 0);
            return new Result(true, StatusCode.OK, "待审核");
        } catch (Exception e) {
            return new Result(false, StatusCode.ERROR, "不在待审核列表中");
        }
    }

    /**
     * 判断用户是否申请专家失败
     *
     * @param userId ID
     * @return
     */
    @ApiOperation(value = "判断用户是否申请专家失败")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", paramType = "path", required = true)
    })
    @RequestMapping(value = "/isRefused/{userId}", method = RequestMethod.GET)
    public Result isRefused(@PathVariable String userId) {
        try {
            Experts experts = expertsService.findByUserId(userId, 2);
            return new Result(true, StatusCode.OK, "申请失败");
        } catch (Exception e) {
            return new Result(false, StatusCode.ERROR, "不在申请失败列表中");
        }
    }

    /**
     * 按专家类别搜索专家
     *
     * @param typeId
     * @param page
     * @param size
     * @return
     */
    @ApiOperation(value = "按专家类别搜索专家")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "typeId", value = "类别id", paramType = "path", required = true),
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findByTypeId/{typeId}/{page}/{size}", method = RequestMethod.POST)
    public Result findByTypeId(@PathVariable String typeId, @PathVariable int page, @PathVariable int size) {
        Page<Experts> pageList = expertsService.findByTypeId(typeId, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Experts>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 用户申请成为专家
     *
     * @param experts
     */
    @ApiOperation(value = "用户申请成为专家", notes = "图片转换为链接：产业资讯接口 /information/upload")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "experts", value = "专家对象", dataType = "Experts")
    })
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result add(@RequestBody Experts experts) {
        experts.setConsultingNum(0);
        experts.setPraiseNum(0);
        experts.setRepliesNum(0);
        experts.setState(0);
        expertsService.add(experts);
        return new Result(true, StatusCode.OK, "申请成功");
    }

    /**
     * 管理员同意专家申请
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "管理员同意专家申请")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "专家id", paramType = "path", dataType = "String", required = true)
    })
    @RequestMapping(value = "/agree/{id}", method = RequestMethod.PUT)
    public Result agree(@PathVariable String id) {
        Experts experts = expertsService.findById(0, id);
        experts.setState(1);
        expertsService.update(experts);
        return new Result(true, StatusCode.OK, "通过成功");
    }

    /**
     * 管理员拒绝专家申请
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "管理员拒绝专家申请")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "专家id", paramType = "path", dataType = "String", required = true)
    })
    @RequestMapping(value = "/refuse/{id}", method = RequestMethod.PUT)
    public Result refuse(@PathVariable String id) {
        Experts experts = expertsService.findById(0, id);
        experts.setState(2);
        expertsService.update(experts);
        return new Result(true, StatusCode.OK, "拒绝成功");
    }

    /**
     * 专家申请列表
     *
     * @param page
     * @param size
     * @return
     */
    @ApiOperation(value = "专家申请列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/agreeing/{page}/{size}", method = RequestMethod.GET)
    public Result agreeing(@PathVariable int page, @PathVariable int size) {
        Page<Experts> pageList = expertsService.findAll(0, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Experts>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 专家申请失败列表
     *
     * @param page
     * @param size
     * @return
     */
    @ApiOperation(value = "专家申请失败列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/failed/{page}/{size}", method = RequestMethod.GET)
    public Result failed(@PathVariable int page, @PathVariable int size) {
        Page<Experts> pageList = expertsService.findAll(2, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Experts>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 修改专家申请信息
     *
     * @param experts
     */
    @ApiOperation(value = "修改专家申请信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "experts", value = "专家对象", dataType = "Experts"),
            @ApiImplicitParam(name = "id", value = "专家id", paramType = "path", dataType = "String", required = true)
    })
    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public Result update(@RequestBody Experts experts, @PathVariable String id) {
        List<Integer> status = new ArrayList<>();
        status.add(0);
        status.add(2);
        Experts oldExperts = expertsService.findByIdAndState(id, status);
        try {
            if (!oldExperts.getPicture().equals(experts.getPicture())) {
                int begin = fastdfsUrl.length() + 8 - 1;
                String deleteUrl = oldExperts.getPicture().substring(begin);
                fastDFSUtil.deleteFile(deleteUrl);
            }
        } catch (Exception e) {
        }
        experts.setId(id);
        experts.setRepliesNum(0);
        experts.setConsultingNum(0);
        experts.setPraiseNum(0);
        experts.setState(0);
        expertsService.update(experts);
        return new Result(true, StatusCode.OK, "修改成功，请等待审核。");
    }

    /**
     * 修改专家信息
     *
     * @param experts
     */
    @ApiOperation(value = "修改专家信息 - 使用绕过网关的接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "experts", value = "专家对象", dataType = "Experts"),
            @ApiImplicitParam(name = "id", value = "专家id", paramType = "path", dataType = "String", required = true)
    })
    @RequestMapping(value = "/updateExperts/{id}", method = RequestMethod.PUT)
    public Result updateExperts(@RequestBody Experts experts, @PathVariable String id) {
        List<Integer> status = new ArrayList<>();
        status.add(1);
        Experts oldExperts = expertsService.findByIdAndState(id, status);
        try {
            if (!oldExperts.getPicture().equals(experts.getPicture())) {
                int begin = fastdfsUrl.length() + 8 - 1;
                String deleteUrl = oldExperts.getPicture().substring(begin);
                fastDFSUtil.deleteFile(deleteUrl);
            }
        } catch (Exception e) {
        }
        experts.setId(id);
        experts.setRepliesNum(oldExperts.getRepliesNum());
        experts.setPraiseNum(oldExperts.getPraiseNum());
        experts.setConsultingNum(oldExperts.getConsultingNum());
        experts.setState(1);
        expertsService.update(experts);
        return new Result(true, StatusCode.OK, "修改成功");
    }

    /**
     * 删除专家
     *
     * @param id
     */
    @ApiOperation(value = "删除专家")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "专家id", dataType = "String", paramType = "path")
    })
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public Result delete(@PathVariable String id) {
        Experts experts = expertsService.findById(id);
        experts.setState(3);
        expertsService.update(experts);
        return new Result(true, StatusCode.OK, "删除成功");
    }

    /**
     * 按咨询量查询专家
     * @param page
     * @param size
     * @return
     */
    @ApiOperation(value = "按咨询量查询专家")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findByConsultingNum/{page}/{size}", method = RequestMethod.POST)
    public Result findByConsultingNum(@PathVariable int page, @PathVariable int size) {
        Page<Experts> pageList = expertsService.findByConsultingNum(page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Experts>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 按回复量查询专家
     * @param page
     * @param size
     * @return
     */
    @ApiOperation(value = "按回复量查询专家")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findByRepliesNum/{page}/{size}", method = RequestMethod.POST)
    public Result findByRepliesNum(@PathVariable int page, @PathVariable int size) {
        Page<Experts> pageList = expertsService.findByRepliesNum(page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Experts>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 按有用量查询专家
     * @param page
     * @param size
     * @return
     */
    @ApiOperation(value = "按有用量查询专家")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findByPraiseNum/{page}/{size}", method = RequestMethod.POST)
    public Result findByPraiseNum(@PathVariable int page, @PathVariable int size) {
        Page<Experts> pageList = expertsService.findByPraiseNum(page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Experts>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 按回复率查询专家
     * @param page
     * @param size
     * @return
     */
    @ApiOperation(value = "按回复率查询专家")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findByRepliesPercent/{page}/{size}", method = RequestMethod.POST)
    public Result findByRepliesPercent(@PathVariable int page, @PathVariable int size) {
        Page<Experts> pageList = expertsService.findByRepliesPercent(page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Experts>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 按咨询量+类别搜索专家
     *
     * @param typeId
     * @param page
     * @param size
     * @return
     */
    @ApiOperation(value = "按咨询量+类别搜索专家")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "typeId", value = "类别id", paramType = "path", required = true),
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findByTypeAndConsultingNum/{typeId}/{page}/{size}", method = RequestMethod.POST)
    public Result findByTypeAndConsultingNum(@PathVariable String typeId, @PathVariable int page, @PathVariable int size) {
        Page<Experts> pageList = expertsService.findByTypeAndConsultingNum(typeId, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Experts>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 按回复量+家类别搜索专家
     *
     * @param typeId
     * @param page
     * @param size
     * @return
     */
    @ApiOperation(value = "按回复量+家类别搜索专家")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "typeId", value = "类别id", paramType = "path", required = true),
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findByTypeAndRepliesNum/{typeId}/{page}/{size}", method = RequestMethod.POST)
    public Result findByTypeAndRepliesNum(@PathVariable String typeId, @PathVariable int page, @PathVariable int size) {
        Page<Experts> pageList = expertsService.findByTypeAndRepliesNum(typeId, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Experts>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 按有用量+类别搜索专家
     *
     * @param typeId
     * @param page
     * @param size
     * @return
     */
    @ApiOperation(value = "按有用量+类别搜索专家")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "typeId", value = "类别id", paramType = "path", required = true),
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findByTypeAndPraiseNum/{typeId}/{page}/{size}", method = RequestMethod.POST)
    public Result findByTypeAndPraiseNum(@PathVariable String typeId, @PathVariable int page, @PathVariable int size) {
        Page<Experts> pageList = expertsService.findByTypeAndPraiseNum(typeId, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Experts>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 按咨询率+专家类别搜索专家
     *
     * @param typeId
     * @param page
     * @param size
     * @return
     */
    @ApiOperation(value = "按咨询率+专家类别搜索专家")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "typeId", value = "类别id", paramType = "path", required = true),
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findByTypeAndRepliesPercent/{typeId}/{page}/{size}", method = RequestMethod.POST)
    public Result findByTypeAndRepliesPercent(@PathVariable String typeId, @PathVariable int page, @PathVariable int size) {
        Page<Experts> pageList = expertsService.findByTypeAndRepliesPercent(typeId, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Experts>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 查询已删除专家-分页
     *
     * @return
     */
    @ApiOperation(value = "查询已删除专家-分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findDelete/{page}/{size}", method = RequestMethod.GET)
    public Result findDelete(@PathVariable int page, @PathVariable int size) {
        Page<Experts> pageList = expertsService.findAll(3, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Experts>(pageList.getTotalElements(), pageList.getContent()));
    }
}
