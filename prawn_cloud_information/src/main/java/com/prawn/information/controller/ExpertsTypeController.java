package com.prawn.information.controller;

import com.prawn.information.pojo.ExpertsType;
import com.prawn.information.pojo.Types;
import com.prawn.information.service.ExpertsTypeService;
import com.prawn.information.service.TypesService;
import entity.PageResult;
import entity.Result;
import entity.StatusCode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * 控制器层
 *
 * @author Administrator
 */
@RestController
@CrossOrigin
@RequestMapping("/expertsType")
@Api(value = "专家类别接口", tags = "专家类别接口")
public class ExpertsTypeController {

    @Autowired
    private ExpertsTypeService expertsTypeService;

    public ExpertsTypeController() {
    }

    /**
     * 增加
     * @param expertsType
     * @return
     */
    @ApiOperation(value = "增加专家类别类型")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "expertsType",value = "专家类别对象",dataType = "ExpertsType")
    })
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result add(@RequestBody ExpertsType expertsType) {
        expertsTypeService.add(expertsType);
        return new Result(true, StatusCode.OK, "增加成功");
    }

    /**
     * 修改
     * @param expertsType
     * @param id
     * @return
     */
    @ApiOperation(value = "修改专家类别")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "expertsType",value = "专家类别对象",dataType = "ExpertsType"),
            @ApiImplicitParam(name = "id",value = "专家类别id",dataType = "String", paramType = "path")
    })
    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public Result update(@RequestBody ExpertsType expertsType, @PathVariable String id) {
        expertsType.setId(id);
        expertsTypeService.update(expertsType);
        return new Result(true, StatusCode.OK, "修改成功");
    }

//    /**
//     * 删除
//     *
//     * @param id
//     */
//    @ApiOperation(value = "删除专家类别")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "id",value = "专家类别id",dataType = "String", paramType = "path")
//    })
//    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
//    public Result delete(@PathVariable String id) {
//        expertsTypeService.deleteById(id);
//        return new Result(true, StatusCode.OK, "删除成功");
//    }


    @ApiOperation(value = "查询所有专家类别")
    @RequestMapping(method = RequestMethod.GET)
    public Result findAll() {
        List<ExpertsType> typesList = expertsTypeService.findAll();
        return new Result(true, StatusCode.OK, "查询成功", typesList);
    }

    /**
     * 查询全部数据-分页
     * @param page 页码
     * @param size 页大小
     * @return 分页结果
     */
    @ApiOperation(value = "查询全部数据-分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value="/{page}/{size}",method=RequestMethod.POST)
    public Result findAll( @PathVariable int page, @PathVariable int size){
        Page<ExpertsType> pageList = expertsTypeService.findAll(page, size);
        return  new Result(true,StatusCode.OK,"查询成功",  new PageResult<ExpertsType>(pageList.getTotalElements(), pageList.getContent()) );
    }

    @ApiOperation(value = "按类别编号搜索类别")
    @ApiImplicitParam(name = "id", value = "专家类别id", paramType = "path", required = true)
    @RequestMapping(value = "/findById/{id}", method = RequestMethod.GET)
    public Result findById(@PathVariable String id) {
        Optional<ExpertsType> expertsType = expertsTypeService.findById(id);
        return new Result(true, StatusCode.OK, "查询成功", expertsType);
    }

}
