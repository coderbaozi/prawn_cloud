package com.prawn.information.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.prawn.information.pojo.*;
import com.prawn.information.service.DetailsService;
import com.prawn.information.service.ExpertsService;
import com.prawn.information.util.FastDFSUtil;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.prawn.information.service.PostService;

import entity.PageResult;
import entity.Result;
import entity.StatusCode;

/**
 * 控制器层
 *
 * @author Administrator
 */
@RestController
@CrossOrigin
@RequestMapping("/post")
@Api(value = "帖子接口", tags = "帖子接口")
public class PostController {

    @Autowired
    private PostService postService;

    @Autowired
    private DetailsService detailsService;

    @Autowired
    private ExpertsService expertsService;

    @Autowired
    private FastDFSUtil fastDFSUtil;

    @Value("${fdfs.web-server-url}")
    private String fastdfsUrl;


    /**
     * 查询全部帖子
     *
     * @return
     */
    @ApiOperation(value = "查询全部帖子")
    @RequestMapping(value = "/findAll", method = RequestMethod.GET)
    public Result findAll() {
        return new Result(true, StatusCode.OK, "查询成功", postService.findAll());
    }

    /**
     * 待审核帖子列表
     *
     * @return
     */
    @ApiOperation(value = "待审核帖子列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/agreeing/{page}/{size}", method = RequestMethod.GET)
    public Result agreeing(@PathVariable int page, @PathVariable int size) {
        Page<Post> pageList = postService.findAll(0, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Post>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 审核失败帖子列表
     *
     * @return
     */
    @ApiOperation(value = "审核失败帖子列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/failed/{page}/{size}", method = RequestMethod.GET)
    public Result failed(@PathVariable int page, @PathVariable int size) {
        Page<Post> pageList = postService.findAll(2, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Post>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 查询全部帖子-分页
     *
     * @return
     */
    @ApiOperation(value = "查询全部帖子-分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findAll/{page}/{size}", method = RequestMethod.GET)
    public Result findAll(@PathVariable int page, @PathVariable int size) {
        Page<Post> pageList = postService.findAll(1, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Post>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 根据ID查询帖子
     *
     * @param id ID
     * @return
     */
    @ApiOperation(value = "根据ID查询帖子 - 后台")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "帖子id", paramType = "path", dataType = "String", required = true)
    })
    @RequestMapping(value = "/findById/{id}", method = RequestMethod.GET)
    public Result findById(@PathVariable String id) {
        return new Result(true, StatusCode.OK, "查询成功", postService.findById(id));
    }

    /**
     * 根据ID查询帖子
     *
     * @param id ID
     * @return
     */
    @ApiOperation(value = "根据ID查询帖子 - 前台")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "帖子id", paramType = "path", dataType = "String", required = true),
            @ApiImplicitParam(name = "ip", value = "用户ip", paramType = "path", dataType = "String", required = true)
    })
    @RequestMapping(value = "/findById/{id}/{ip}", method = RequestMethod.GET)
    public Result findById(@PathVariable String id, @PathVariable String ip) {
        return new Result(true, StatusCode.OK, "查询成功", postService.findById(id,ip));
    }

    /**
     * 根据用户ID查询帖子
     *
     * @param userId ID
     * @return
     */
    @ApiOperation(value = "根据用户ID查询帖子")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", paramType = "path", required = true),
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findByUser/{userId}/{page}/{size}", method = RequestMethod.GET)
    public Result findByUser(@PathVariable String userId, @PathVariable int page, @PathVariable int size) {
        List<Integer> status = new ArrayList<>();
        status.add(0);
        status.add(1);
        status.add(2);
        Page<Post> pageList = postService.findByUserId(userId, status, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Post>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 根据专家ID查询帖子
     *
     * @param expertsId ID
     * @return
     */
    @ApiOperation(value = "根据专家ID查询帖子")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "expertsId", value = "专家id", paramType = "path", required = true),
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findByExperts/{expertsId}/{page}/{size}", method = RequestMethod.GET)
    public Result findByExperts(@PathVariable String expertsId, @PathVariable int page, @PathVariable int size) {
        Page<Post> pageList = postService.findByExpertsId(expertsId, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Post>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 用户发帖提问
     *
     * @param post
     */
    @ApiOperation(value = "用户发帖提问")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "post", value = "帖子对象", dataType = "Post")
    })
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result add(@RequestBody Post post) {
        Experts experts = expertsService.findById(post.getExpertsId());
        post.setClickNum(0);
        post.setState(0);
        postService.add(post);
        return new Result(true, StatusCode.OK, "发帖成功，请等待审核。");
    }

    /**
     * 管理员审核帖子通过
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "管理员审核帖子通过")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "帖子id", paramType = "path", dataType = "String", required = true)
    })
    @RequestMapping(value = "/agree/{id}", method = RequestMethod.PUT)
    public Result agree(@PathVariable String id) {
        Post post = postService.findById(0, id);
        Experts experts = expertsService.findById(post.getExpertsId());
        int consultingNum = experts.getConsultingNum() + 1;  //咨询量+1
        experts.setConsultingNum(consultingNum);
        expertsService.update(experts);
        post.setState(1);//审核通过
        postService.update(post);
        return new Result(true, StatusCode.OK, "通过成功");
    }


    /**
     * 管理员审核帖子不通过
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "管理员审核帖子不通过")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "帖子id", paramType = "path", dataType = "String", required = true)
    })
    @RequestMapping(value = "/refuse/{id}", method = RequestMethod.PUT)
    public Result refuse(@PathVariable String id) {
        Post post = postService.findById(0, id);
        post.setState(2);//审核不通过
        postService.update(post);
        return new Result(true, StatusCode.OK, "拒绝成功");
    }

    /**
     * 删除帖子
     *
     * @param id
     */
    @ApiOperation(value = "删除帖子")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "帖子id", paramType = "path", dataType = "String", required = true)
    })
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public Result delete(@PathVariable String id) {
        Post post = postService.findById(id).get();
        try {
            List<Details> detailsList = detailsService.findByPostId(id);
            for (Details details :
                    detailsList) {
                details.setState(3);
                detailsService.update(details);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        post.setState(3);
        postService.update(post);
        return new Result(true, StatusCode.OK, "删除成功");
    }

    /**
     * 修改帖子
     *
     * @param post
     * @param id
     * @return
     */
    @ApiOperation(value = "修改帖子")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "post", value = "帖子对象", dataType = "Post"),
            @ApiImplicitParam(name = "id", value = "帖子id", paramType = "path", dataType = "String", required = true)
    })
    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public Result update(@RequestBody Post post, @PathVariable String id) {
        post.setId(id);
        post.setState(0);
        post.setCreationTime(new Date());
        postService.update(post);
        return new Result(true, StatusCode.OK, "修改成功");
    }

    /**
     * 按分类查询帖子
     *
     * @param typeId
     * @param page
     * @param size
     * @return
     */
    @ApiOperation(value = "按分类查询 - 分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "typeId", value = "类型Id", paramType = "path", required = true),
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findByType/{typeId}/{page}/{size}", method = RequestMethod.GET)
    public Result findByType(@PathVariable String typeId, @PathVariable int page, @PathVariable int size) {
        Page<Post> pageList = postService.findByType(typeId, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Post>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 按时间查询- 分页
     *
     * @param page
     * @param size
     * @return
     */
    @ApiOperation(value = "按时间查询 - 分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findByTime/{page}/{size}", method = RequestMethod.GET)
    public Result findByTime(@PathVariable int page, @PathVariable int size) {
        Page<Post> pageList = postService.findByTime(page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Post>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 按点击量查询 - 分页
     *
     * @param page
     * @param size
     * @return
     */
    @ApiOperation(value = "按点击量查询 - 分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findByClickNum/{page}/{size}", method = RequestMethod.GET)
    public Result findByClickNum(@PathVariable int page, @PathVariable int size) {
        Page<Post> pageList = postService.findByClickNum(page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Post>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 按分类+时间查询帖子
     *
     * @param typeId
     * @param page
     * @param size
     * @return
     */
    @ApiOperation(value = "按分类+时间查询 - 分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "typeId", value = "类型Id", paramType = "path", required = true),
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findByTypeAndTime/{typeId}/{page}/{size}", method = RequestMethod.GET)
    public Result findByTypeAndTime(@PathVariable String typeId, @PathVariable int page, @PathVariable int size) {
        Page<Post> pageList = postService.findByTypeAndTime(typeId, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Post>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 按分类+点击查询帖子
     *
     * @param typeId
     * @param page
     * @param size
     * @return
     */
    @ApiOperation(value = "按分类+点击查询 - 分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "typeId", value = "类型Id", paramType = "path", required = true),
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findByTypeAndClick/{typeId}/{page}/{size}", method = RequestMethod.GET)
    public Result findByTypeAndClick(@PathVariable String typeId, @PathVariable int page, @PathVariable int size) {
        Page<Post> pageList = postService.findByTypeAndClick(typeId, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Post>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 查询删除历史
     *
     * @return
     */
    @ApiOperation(value = "查询删除历史")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findDelete/{page}/{size}", method = RequestMethod.GET)
    public Result findDelete(@PathVariable int page, @PathVariable int size) {
        Page<Post> pageList = postService.findAll(3, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Post>(pageList.getTotalElements(), pageList.getContent()));
    }

}
