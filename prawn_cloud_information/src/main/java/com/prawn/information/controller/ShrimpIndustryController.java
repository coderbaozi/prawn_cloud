package com.prawn.information.controller;

import com.prawn.information.dto.InformationDTO;
import com.prawn.information.pojo.Information;
import com.prawn.information.service.InformationService;
import com.prawn.information.util.FastDFSUtil;
import entity.PageResult;
import entity.Result;
import entity.StatusCode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

/**
 * 控制器层
 *
 * @author Administrator
 */
@RestController
@CrossOrigin
@RequestMapping("/shrimpIndustry")
@Api(value = "产业资讯接口", tags = "产业资讯接口")
public class ShrimpIndustryController {

    @Autowired
    private InformationService informationService;

    @Autowired
    private FastDFSUtil fastDFSUtil;

    @Value("${fdfs.web-server-url}")
    private String fastdfsUrl;

    /**
     * 增加
     *
     * @param information
     */
    @ApiOperation(value = "增加产业资讯")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "information", value = "产业资讯对象", dataType = "Information")
    })
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result add(@RequestBody Information information) {
        information.setModule(1);
        information.setState(1);
        information.setClickNum(0);
        information.setRecommend(false);
        informationService.add(information);
        return new Result(true, StatusCode.OK, "增加成功");
    }

    /**
     * 修改
     *
     * @param information
     */
    @ApiOperation(value = "修改产业资讯 - 使用绕过网关的路径")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "information", value = "产业资讯对象", dataType = "Information"),
            @ApiImplicitParam(name = "id", value = "资讯id", dataType = "String", paramType = "path")
    })
    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public Result update(@RequestBody Information information, @PathVariable String id) {
        Information oldInformation = informationService.findById(id).get();
        try {
            if (!oldInformation.getPicture().equals(information.getPicture())) {
                fastDFSUtil.deleteFile(oldInformation.getPicture());
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        information.setModule(1);
        information.setId(id);
        information.setState(1);
        information.setCreationTime(oldInformation.getCreationTime());
        informationService.update(information);
        return new Result(true, StatusCode.OK, "修改成功");
    }

    /**
     * 查询全部
     *
     * @return
     */
    @ApiOperation(value = "查询所有产业资讯")
    @RequestMapping(method = RequestMethod.GET)
    public Result findAll() {
        return new Result(true, StatusCode.OK, "查询成功", informationService.findAll(1));
    }

    /**
     * 查询全部产业资讯-分页
     *
     * @param page 页码
     * @param size 页大小
     * @return 分页结果
     */
    @ApiOperation(value = "查询全部产业资讯-分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/{page}/{size}", method = RequestMethod.POST)
    public Result findAll(@PathVariable int page, @PathVariable int size) {
        Page<InformationDTO> pageList = informationService.findAll(1, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<InformationDTO>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 查询产业资讯删除历史-分页
     *
     * @param page 页码
     * @param size 页大小
     * @return 分页结果
     */
    @ApiOperation(value = "查询产业资讯删除历史-分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findDelete/{page}/{size}", method = RequestMethod.POST)
    public Result findDelete(@PathVariable int page, @PathVariable int size) {
        Page<InformationDTO> pageList = informationService.findDelete(1, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<InformationDTO>(pageList.getTotalElements(), pageList.getContent()));
    }


    /**
     * 按类型编号搜索
     *
     * @param typeId
     * @param page
     * @param size
     * @return
     */
    @ApiOperation(value = "按类型编号搜索产业资讯")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "typeId", value = "类型编号", paramType = "path", required = true),
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/search/searchByTypeId/{typeId}/{page}/{size}", method = RequestMethod.POST)
    public Result findSearch(@PathVariable String typeId, @PathVariable int page, @PathVariable int size) {
        Page<InformationDTO> pageList = informationService.findByTypeId(1, typeId, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<InformationDTO>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 按点击量和类型查询 - 分页
     *
     * @param page
     * @param size
     * @return
     */
    @ApiOperation(value = "按点击量和类型查询产业资讯 - 分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "typeId", value = "类型编号", paramType = "path", required = true),
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findByClickAndType/{typeId}/{page}/{size}", method = RequestMethod.GET)
    public Result findByClickAndType(@PathVariable String typeId, @PathVariable int page, @PathVariable int size) {
        Page<InformationDTO> pageList = informationService.findByClickAndType(1, typeId, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<InformationDTO>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 按时间查询产业资讯 - 分页
     *
     * @param page
     * @param size
     * @return
     */
    @ApiOperation(value = "按时间查询产业资讯 - 分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findByTime/{page}/{size}", method = RequestMethod.GET)
    public Result findByTime(@PathVariable int page, @PathVariable int size) {
        Page<InformationDTO> pageList = informationService.findByTime(1, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<InformationDTO>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 查询推荐 - 分页
     *
     * @param page
     * @param size
     * @return
     */
    @ApiOperation(value = "查询推荐产业资讯 - 分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findByRecommend/{page}/{size}", method = RequestMethod.GET)
    public Result findByRecommend(@PathVariable int page, @PathVariable int size) {
        Page<InformationDTO> pageList = informationService.findByRecommend(1, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<InformationDTO>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 按点击量查询 - 分页
     *
     * @param page
     * @param size
     * @return
     */
    @ApiOperation(value = "按点击量查询产业资讯 - 分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findByClickNum/{page}/{size}", method = RequestMethod.GET)
    public Result findByClickNum(@PathVariable int page, @PathVariable int size) {
        Page<InformationDTO> pageList = informationService.findByClickNum(1, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<InformationDTO>(pageList.getTotalElements(), pageList.getContent()));
    }

	/**
     * 按时间查询虾业专题
	 * @return
     */
	@ApiOperation(value = "按时间查询产业资讯")
	@RequestMapping(value = "/findByTime", method = RequestMethod.GET)
	public Result findByTime() {
		return new Result(true, StatusCode.OK, "查询成功", informationService.findByTime(1));
	}

	/**
     * 查询推荐虾业专题
	 * @return
     */
	@ApiOperation(value = "查询推荐产业资讯")
	@RequestMapping(value = "/findByRecommend", method = RequestMethod.GET)
	public Result findByRecommend() {
		return new Result(true, StatusCode.OK, "查询成功", informationService.findByRecommend(1));
	}

	/**
     * 按点击量查询产业行情
	 * @return
     */
	@ApiOperation(value = "按点击量查询产业资讯")
	@RequestMapping(value = "/findByClickNum", method = RequestMethod.GET)
	public Result findByClickNum() {
		return new Result(true, StatusCode.OK, "查询成功", informationService.findByClickNum(1));
	}

    /**
     * 每周精彩资讯
     * @return
     */
    @ApiOperation(value = "每周精彩资讯 - 分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findByClickWeekly/{page}/{size}", method = RequestMethod.GET)
    public Result findByClickWeekly(@PathVariable int page, @PathVariable int size) {
        Page<InformationDTO> pageList = informationService.findByClickWeekly(1, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<InformationDTO>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 每月精彩资讯
     * @return
     */
    @ApiOperation(value = "每月精彩资讯 - 分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findByClickMonthly/{page}/{size}", method = RequestMethod.GET)
    public Result findByClickMonthly(@PathVariable int page, @PathVariable int size) {
        Page<InformationDTO> pageList = informationService.findByClickMonthly(1, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<InformationDTO>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 每周精彩资讯
     * @return
     */
    @ApiOperation(value = "每周精彩资讯")
    @RequestMapping(value = "/findByClickWeekly", method = RequestMethod.GET)
    public Result findByClickWeekly() {
        return new Result(true, StatusCode.OK, "查询成功", informationService.findByClickWeekly(1));
    }

    /**
     * 每月精彩资讯
     * @return
     */
    @ApiOperation(value = "每月精彩资讯")
    @RequestMapping(value = "/findByClickMonthly", method = RequestMethod.GET)
    public Result findByClickMonthly() {
        return new Result(true, StatusCode.OK, "查询成功", informationService.findByClickMonthly(1));
    }

}
