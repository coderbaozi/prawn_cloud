package com.prawn.information.controller;

import com.prawn.information.pojo.Types;
import com.prawn.information.service.TypesService;
import entity.PageResult;
import entity.Result;
import entity.StatusCode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 控制器层
 *
 * @author Administrator
 */
@RestController
@CrossOrigin
@RequestMapping("/policiesTypes")
@Api(value = "政策法规类型接口", tags = "政策法规类型接口")
public class PoliciesTypesController {

    @Autowired
    private TypesService typesService;

    public PoliciesTypesController() {
    }

    /**
     * 增加
     *
     * @param types
     */
    @ApiOperation(value = "增加政策法规类型")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "types",value = "类型对象",dataType = "InformationTypes")
    })
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result add(@RequestBody Types types) {
        types.setModule(2);
        typesService.add(types);
        return new Result(true, StatusCode.OK, "增加成功");
    }

    /**
     * 修改
     *
     * @param types
     */
    @ApiOperation(value = "修改政策法规类型")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "types",value = "类型对象",dataType = "InformationTypes"),
            @ApiImplicitParam(name = "id",value = "类型id",dataType = "String", paramType = "path")
    })
    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public Result update(@RequestBody Types types, @PathVariable String id) {
        types.setModule(2);
        types.setId(id);
        typesService.update(types);
        return new Result(true, StatusCode.OK, "修改成功");
    }

    @ApiOperation(value = "查询所有政策法规类型")
    @RequestMapping(method = RequestMethod.GET)
    public Result findAll() {
        List<Types> typesList = typesService.findAll(2);
        return new Result(true, StatusCode.OK, "查询成功", typesList);
    }

    /**
     * 查询全部数据-分页
     * @param page 页码
     * @param size 页大小
     * @return 分页结果
     */
    @ApiOperation(value = "查询全部数据-分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value="/{page}/{size}",method=RequestMethod.POST)
    public Result findAll( @PathVariable int page, @PathVariable int size){
        Page<Types> pageList = typesService.findAll(2, page, size);
        return  new Result(true,StatusCode.OK,"查询成功",  new PageResult<Types>(pageList.getTotalElements(), pageList.getContent()) );
    }

}
