package com.prawn.information.controller;

import com.prawn.information.dto.InformationDTO;
import com.prawn.information.pojo.ExpertsArticles;
import com.prawn.information.pojo.Information;
import com.prawn.information.service.ExpertsArticlesService;
import com.prawn.information.service.InformationService;
import com.prawn.information.util.FastDFSUtil;
import entity.PageResult;
import entity.Result;
import entity.StatusCode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import util.IdWorker;

import java.util.List;

/**
 * 控制器层
 *
 * @author Administrator
 */
@RestController
@CrossOrigin
@RequestMapping("/diseaseArticles")
@Api(value = "专家技术文章接口", tags = "专家技术文章接口")
public class DiseaseArticlesController {

    @Autowired
    private InformationService informationService;

    @Autowired
    private ExpertsArticlesService expertsArticlesService;

    @Autowired
    private FastDFSUtil fastDFSUtil;

    @Autowired
    private IdWorker idWorker;

    @Value("${fdfs.web-server-url}")
    private String fastdfsUrl;

    /**
     * 专家发表技术文章 - 等待管理员审核
     *
     * @param information
     */
    @ApiOperation(value = "专家发表技术文章" ,notes = " - 等待管理员审核")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "information", value = "产业资讯对象", dataType = "Information", paramType = "body"),
            @ApiImplicitParam(name = "expertsId", value = "专家id", dataType = "String", paramType = "query")
    })
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result add(@RequestBody Information information, @RequestParam String expertsId) {
        information.setModule(3);
        information.setState(2);
        information.setClickNum(0);
        information.setRecommend(false);
        String id = idWorker.nextId() + "";
        information.setId(id);
        informationService.addDiseaseArticles(information);
        ExpertsArticles expertsArticles = new ExpertsArticles();
        expertsArticles.setInformationId(id);
        expertsArticles.setExpertsId(expertsId);
        expertsArticlesService.add(expertsArticles);
        return new Result(true, StatusCode.OK, "增加成功");
    }

    /**
     * 专家修改技术文章 - 等待管理员审核
     *
     * @param information
     */
    @ApiOperation(value = "专家修改技术文章 - 使用绕过网关的路径", notes = " - 等待管理员审核")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "information", value = "产业资讯对象", dataType = "Information"),
            @ApiImplicitParam(name = "id", value = "资讯id", dataType = "String", paramType = "path")
    })
    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public Result update(@RequestBody Information information, @PathVariable String id) {
        Information oldInformation = informationService.findByIdAll(id);
        try {
            if (!oldInformation.getPicture().equals(information.getPicture())) {
                fastDFSUtil.deleteFile(oldInformation.getPicture());
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        information.setModule(3);
        information.setId(id);
        information.setState(2);
        information.setCreationTime(oldInformation.getCreationTime());
        informationService.update(information);
        return new Result(true, StatusCode.OK, "修改成功");
    }

    /**
     * 管理员审核专家技术文章 - 通过
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "管理员审核专家技术文章 - 通过")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "资讯id", paramType = "path", dataType = "String", required = true)
    })
    @RequestMapping(value = "/agree/{id}", method = RequestMethod.PUT)
    public Result agree(@PathVariable String id) {
        Information information = informationService.findById(2, id);
        information.setState(1);
        informationService.update(information);
        return new Result(true, StatusCode.OK, "审核成功");
    }

    /**
     * 管理员审核专家技术文章 - 不通过
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "管理员审核专家技术文章 - 不通过")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "资讯id", paramType = "path", dataType = "String", required = true)
    })
    @RequestMapping(value = "/refuse/{id}", method = RequestMethod.PUT)
    public Result refuse(@PathVariable String id) {
        Information information = informationService.findById(2, id);
        information.setState(3);
        informationService.update(information);
        return new Result(true, StatusCode.OK, "审核成功");
    }

    /**
     * 专家技术文章待审核列表
     *
     * @param page
     * @param size
     * @return
     */
    @ApiOperation(value = "专家技术文章 - 待审核列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/agreeing/{page}/{size}", method = RequestMethod.GET)
    public Result agreeing(@PathVariable int page, @PathVariable int size) {
        Page<InformationDTO> pageList = informationService.findAll(2,3, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<InformationDTO>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 专家技术文章 - 审核不通过列表
     *
     * @param page
     * @param size
     * @return
     */
    @ApiOperation(value = "专家技术文章 - 审核不通过列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/failed/{page}/{size}", method = RequestMethod.GET)
    public Result failed(@PathVariable int page, @PathVariable int size) {
        Page<InformationDTO> pageList = informationService.findAll(3,3, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<InformationDTO>(pageList.getTotalElements(), pageList.getContent()));
    }


    /**
     * 查询全部专家技术文章
     *
     * @return
     */
    @ApiOperation(value = "查询所有专家技术文章")
    @RequestMapping(method = RequestMethod.GET)
    public Result findAll() {
        return new Result(true, StatusCode.OK, "查询成功", informationService.findAll(3));
    }

    /**
     * 查询全部专家技术文章-分页
     *
     * @param page 页码
     * @param size 页大小
     * @return 分页结果
     */
    @ApiOperation(value = "查询全部专家技术文章-分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/{page}/{size}", method = RequestMethod.POST)
    public Result findAll(@PathVariable int page, @PathVariable int size) {
        Page<InformationDTO> pageList = informationService.findAll(3, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<InformationDTO>(pageList.getTotalElements(), pageList.getContent()));
    }


    /**
     * 按类型编号搜索专家技术文章
     *
     * @param typeId
     * @param page
     * @param size
     * @return
     */
    @ApiOperation(value = "按类型编号搜索专家技术文章")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "typeId", value = "类型编号", paramType = "path", required = true),
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/search/searchByTypeId/{typeId}/{page}/{size}", method = RequestMethod.POST)
    public Result findSearch(@PathVariable String typeId, @PathVariable int page, @PathVariable int size) {
        Page<InformationDTO> pageList = informationService.findByTypeId(3, typeId, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<InformationDTO>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 按时间查询专家技术文章 - 分页
     *
     * @param page
     * @param size
     * @return
     */
    @ApiOperation(value = "按时间查询专家技术文章 - 分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findByTime/{page}/{size}", method = RequestMethod.GET)
    public Result findByTime(@PathVariable int page, @PathVariable int size) {
        Page<InformationDTO> pageList = informationService.findByTime(3, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<InformationDTO>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 查询推荐专家技术文章 - 分页
     *
     * @param page
     * @param size
     * @return
     */
    @ApiOperation(value = "查询推荐专家技术文章 - 分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findByRecommend/{page}/{size}", method = RequestMethod.GET)
    public Result findByRecommend(@PathVariable int page, @PathVariable int size) {
        Page<InformationDTO> pageList = informationService.findByRecommend(3, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<InformationDTO>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 按点击量查询专家技术文章 - 分页
     *
     * @param page
     * @param size
     * @return
     */
    @ApiOperation(value = "按点击量查询专家技术文章 - 分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findByClickNum/{page}/{size}", method = RequestMethod.GET)
    public Result findByClickNum(@PathVariable int page, @PathVariable int size) {
        Page<InformationDTO> pageList = informationService.findByClickNum(3, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<InformationDTO>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 按时间查询专家技术文章
     * @return
     */
    @ApiOperation(value = "按时间查询专家技术文章")
    @RequestMapping(value = "/findByTime", method = RequestMethod.GET)
    public Result findByTime() {
        return new Result(true, StatusCode.OK, "查询成功", informationService.findByTime(3));
    }

    /**
     * 查询推荐专家技术文章
     * @return
     */
    @ApiOperation(value = "查询推荐专家技术文章")
    @RequestMapping(value = "/findByRecommend", method = RequestMethod.GET)
    public Result findByRecommend() {
        return new Result(true, StatusCode.OK, "查询成功", informationService.findByRecommend(3));
    }

    /**
     * 按点击量查询专家技术文章
     * @return
     */
    @ApiOperation(value = "按点击量查询专家技术文章")
    @RequestMapping(value = "/findByClickNum", method = RequestMethod.GET)
    public Result findByClickNum() {
        return new Result(true, StatusCode.OK, "查询成功", informationService.findByClickNum(3));
    }

    /**
     * 根据专家ID查询文章
     * @param expertsId
     * @return
     */
    @ApiOperation(value = "根据专家id查询文章")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "expertsId", value = "专家id", paramType = "path", dataType = "String", required = true),
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findByExperts/{expertsId}/{page}/{size}", method = RequestMethod.GET)
    public Result findByExperts(@PathVariable String expertsId, @PathVariable int page, @PathVariable int size) {
        Page<InformationDTO> pageList = informationService.findByExperts(expertsId, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<InformationDTO>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 删除专家技术文章
     *
     * @param id
     */
    @ApiOperation(value = "删除专家技术文章")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "资讯id", dataType = "String", paramType = "path")
    })
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public Result delete(@PathVariable String id) {
        Information information = informationService.findById(id).get();
        informationService.deleteById(id); //逻辑删除
        expertsArticlesService.deleteByInformationId(id);
        return new Result(true, StatusCode.OK, "删除成功");
    }

    /**
     * 根据ID查询文章
     *
     * @param id ID
     * @return
     */
    @ApiOperation(value = "id查询文章 - 后台")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "产业资讯id", paramType = "path", dataType = "String", required = true)
    })
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Result findById(@PathVariable String id) {
        return new Result(true, StatusCode.OK, "查询成功", informationService.findByIdAll(id));
    }

    @ApiOperation(value = "id查询文章 - 前台")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "产业资讯id", paramType = "path", dataType = "String", required = true),
            @ApiImplicitParam(name = "ip", value = "用户ip", paramType = "path", dataType = "String", required = true)
    })
    @RequestMapping(value = "/{id}/{ip}", method = RequestMethod.GET)
    public Result findById(@PathVariable String id, @PathVariable String ip) {
        return new Result(true, StatusCode.OK, "查询成功", informationService.findByIdAll(id, ip));
    }

    /**
     * 专家技术文章 - 已删除列表
     *
     * @param page
     * @param size
     * @return
     */
    @ApiOperation(value = "专家技术文章 - 已删除列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findDelete/{page}/{size}", method = RequestMethod.GET)
    public Result findDelete(@PathVariable int page, @PathVariable int size) {
        Page<InformationDTO> pageList = informationService.findAll(0,3, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<InformationDTO>(pageList.getTotalElements(), pageList.getContent()));
    }
}
