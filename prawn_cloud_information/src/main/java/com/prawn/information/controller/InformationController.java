package com.prawn.information.controller;

import java.io.IOException;
import java.util.List;

import com.prawn.information.util.FastDFSUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.prawn.information.pojo.Information;
import com.prawn.information.service.InformationService;

import entity.Result;
import entity.StatusCode;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * 控制器层
 *
 * @author Administrator
 */
@RestController
@CrossOrigin
@RequestMapping("/information")
@Api(value = "文章类公共接口", tags = "文章类公共接口")
public class InformationController {

    @Autowired
    private InformationService informationService;

    @Autowired
    private FastDFSUtil fastDFSUtil;

    @Value("${fdfs.web-server-url}")
    private String fastdfsUrl;

    /**
     * 图片转换成url
     *
     * @param file
     * @return
     * @throws IOException
     */
    @ApiOperation(value = "上传图片，返回url - 使用绕过网关的路径")
    @RequestMapping(value = "/upload", method = RequestMethod.POST, headers = "content-type=multipart/form-data")
    public Result upload(@RequestParam("file") MultipartFile file) throws IOException {
        String url = fastDFSUtil.uploadFile(file);
        return new Result(true, StatusCode.OK, "上传成功", url);
    }

    /**
     * 删除图片 - 使用绕过网关的路径
     * @param delUrl
     * @return
     */
    @ApiOperation(value = "删除图片 - 使用绕过网关的路径")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "delUrl", value = "图片路径", dataType = "String" , paramType = "query")
    })
    @RequestMapping(value = "/delPic", method = RequestMethod.DELETE)
    public Result delPic(@RequestParam("delUrl") String delUrl) {
        fastDFSUtil.deleteFile(delUrl);
        return new Result(true,StatusCode.OK,"删除成功");
    }

    /**
     * 删除文章
     *
     * @param id
     */
    @ApiOperation(value = "删除文章")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "资讯id", dataType = "String", paramType = "path")
    })
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public Result delete(@PathVariable String id) {
//        Information information = informationService.findById(id).get();
        informationService.deleteById(id); //逻辑删除
        return new Result(true, StatusCode.OK, "删除成功");
    }

    /**
     * 根据ID查询文章
     *
     * @param informationId ID
     * @return
     */
    @ApiOperation(value = "资讯id查询文章 - 后台")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "informationId", value = "产业资讯id", paramType = "path", dataType = "String", required = true)
    })
    @RequestMapping(value = "/{informationId}", method = RequestMethod.GET)
    public Result findById(@PathVariable String informationId) {
        return new Result(true, StatusCode.OK, "查询成功", informationService.findById(informationId));
    }

    /**
     * 根据ID查询文章
     *
     * @param id ID
     * @return
     */
    @ApiOperation(value = "资讯id查询文章 - 前台")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "产业资讯id", paramType = "path", dataType = "String", required = true),
            @ApiImplicitParam(name = "ip", value = "用户id", paramType = "path", dataType = "String", required = true)
    })
    @RequestMapping(value = "/{id}/{ip}", method = RequestMethod.GET)
    public Result findById(@PathVariable String id, @PathVariable String ip) {
        return new Result(true, StatusCode.OK, "查询成功", informationService.findById(id,ip));
    }

    /**
     * 管理员设置推荐
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "管理员设置推荐")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "产业资讯id", paramType = "path", dataType = "String", required = true)
    })
    @RequestMapping(value = "/recommend/{id}", method = RequestMethod.PUT)
    public Result recommend(@PathVariable String id) {
        Information information = informationService.findById(id).get();
        information.setRecommend(true);
        informationService.update(information);
        return new Result(true, StatusCode.OK, "设置成功");
    }

    /**
     * 管理员取消推荐
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "管理员取消推荐")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "产业资讯id", paramType = "path", dataType = "String", required = true)
    })
    @RequestMapping(value = "/cancelRecommend/{id}", method = RequestMethod.PUT)
    public Result cancelRecommend(@PathVariable String id) {
        Information information = informationService.findById(id).get();
        information.setRecommend(false);
        informationService.update(information);
        return new Result(true, StatusCode.OK, "设置成功");
    }

}
