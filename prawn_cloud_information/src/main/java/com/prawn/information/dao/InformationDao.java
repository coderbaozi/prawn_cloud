package com.prawn.information.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.prawn.information.pojo.Information;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface InformationDao extends JpaRepository<Information,String>,JpaSpecificationExecutor<Information>{

    public Page<Information> findByModuleAndState(int module, int state, Pageable pageable);

    public Optional<Information> findByIdAndState(String id, int state);
    public List<Information> findByModuleAndState(int module, int state);

//    public Page<Information> findByModuleAndTypeIdAndState(int module, String typeId, int state, Pageable pageable);
//
//    public Page<Information> findByModuleAndState(int module, int state, Pageable pageable);
//
//    public Page<Information> findByModuleAndStateOrderByCreationTimeDesc(int module, int state, Pageable pageable);
//
//    public Page<Information> findByModuleAndStateAndRecommendTrue(int module, int state, Pageable pageable);
//
//    public Page<Information> findByModuleAndStateOrderByClickNumDesc(int module, int state, Pageable pageable);
//
//    public Page<Information> findByModuleAndStateAndTypeIdOrderByClickNumDesc(int module, int state, String typeId, Pageable pageable);
//
//    public Page<Information> findByModuleAndStateAndTypeIdOrderByCreationTimeDesc(int module, int state, String typeId, Pageable pageable);

    public Page<Information> findByModuleAndStateOrderByCreationTimeDesc(int module, int state, Pageable pageable);

    public Page<Information> findByModuleAndStateAndRecommendTrue(int module, int state, Pageable pageable);

    public Page<Information> findByModuleAndStateOrderByClickNumDesc(int module, int state, Pageable pageable);

    public Page<Information> findByModuleAndTypeIdAndStateOrderByCreationTimeDesc(int module, String typeId, int state, Pageable pageable);

    public Page<Information> findByModuleAndStateAndTypeIdOrderByClickNumDesc(int module, int state, String typeId, Pageable pageable);

    public Page<Information> findByModuleAndStateAndTypeIdOrderByCreationTimeDesc(int module, int state, String typeId, Pageable pageable);

    public List<Information> findByModuleAndStateOrderByCreationTimeDesc(int module, int state);

    public List<Information> findByModuleAndStateAndRecommendTrue(int module, int state);

    public List<Information> findByModuleAndStateOrderByClickNumDesc(int module, int state);



    @Query(nativeQuery = true,
            value = "SELECT *\n" +
                    "FROM industry_information info\n" +
                    "WHERE info.module = (?1)\n" +
                    "AND info.state = (?2)\n" +
                    "AND YEAR (info.`creationTime`) = YEAR (NOW( ))\n" +
                    "ORDER BY info.`creationTime` DESC;")
    List<Information> findByClickWeekly(int module, int state);

    @Query(nativeQuery = true,
            value = "SELECT *\n" +
                    "FROM industry_information info\n" +
                    "WHERE info.module = (?1)\n" +
                    "AND info.state = (?2)\n" +
                    "AND YEAR (info.`creationTime`) = YEAR (NOW( ))\n" +
                    "ORDER BY info.`creationTime` DESC;")
    List<Information> findByClickMonthly(int module, int state);

    @Query(nativeQuery = true,
            value = "SELECT *\n" +
                    "FROM industry_information info\n" +
                    "WHERE info.module = (?1)\n" +
                    "AND info.state = (?2)\n" +
                    "AND YEAR (info.`creationTime`) = YEAR (NOW( ))\n" +
                    "ORDER BY info.`creationTime` DESC;")
    List<Information> findByClickWeekly(int module, int state, int page, int size);

    @Query(nativeQuery = true,
            value = "SELECT *\n" +
                    "FROM industry_information info\n" +
                    "WHERE info.module = (?1)\n" +
                    "AND info.state = (?2)\n" +
                    "AND YEAR (info.`creationTime`) = YEAR (NOW( ))\n" +
                    "ORDER BY info.`creationTime` DESC\n" +
                    " LIMIT ?3,?4 ")
    List<Information> findByClickMonthly(int module, int state, int page, int size);

    @Query(nativeQuery = true,
            value = "select * from industry_information where id in (?1)")
    public List<Information> findByIdList(List<String> id);

    @Transactional
    @Modifying
    @Query(nativeQuery = true, value = "update industry_information set clickNum=? where id = ?")
    public void updateInfo(int clickNum, String id);
}
