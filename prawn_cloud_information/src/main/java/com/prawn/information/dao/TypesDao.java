package com.prawn.information.dao;

import com.prawn.information.pojo.Information;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.prawn.information.pojo.Types;

import java.util.List;
import java.util.Optional;

/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface TypesDao extends JpaRepository<Types,String>,JpaSpecificationExecutor<Types>{

    public Types findByIdAndStateTrue(String id);

    public List<Types> findByModuleAndStateTrue(int module);

    public Page<Types> findByModuleAndStateTrue(int module, Pageable pageable);

}
