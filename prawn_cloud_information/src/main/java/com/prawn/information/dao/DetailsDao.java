package com.prawn.information.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.prawn.information.pojo.Details;

import java.util.List;
import java.util.Optional;

/**
 * 数据访问接口
 *
 * @author Administrator
 */
public interface DetailsDao extends JpaRepository<Details, String>, JpaSpecificationExecutor<Details> {

    public Optional<Details> findByIdAndState(String id, int state);

    public Page<Details> findByPostIdAndState(String postId, int state, Pageable pageable);

    public List<Details> findByPostIdAndState(String postId, int state);

    public Page<Details> findByReplier(String replier, Pageable pageable);

    public Page<Details> findByState(int state, Pageable pageable);

    public Page<Details> findByReplierAndStateIn(String replier, List<Integer> state, Pageable pageable);
}
