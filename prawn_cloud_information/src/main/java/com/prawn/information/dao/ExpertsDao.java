package com.prawn.information.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.prawn.information.pojo.Experts;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

/**
 * 数据访问接口
 *
 * @author Administrator
 */
public interface ExpertsDao extends JpaRepository<Experts, String>, JpaSpecificationExecutor<Experts> {

    public Optional<Experts> findByUserIdAndState(String userId, int state);

    public Optional<Experts> findByUserId(String userId);

    public Iterable<Experts> findByState(int state);

    public Page<Experts> findByState(int state, Pageable pageable);

    public Optional<Experts> findByIdAndState(String id, int state);

    public Page<Experts> findByTypeIdAndState(String typeId, int state, Pageable pageable);

    public Page<Experts> findByStateOrderByConsultingNumDesc(int state, Pageable pageable);

    public Page<Experts> findByStateOrderByRepliesNumDesc(int state, Pageable pageable);

    public Page<Experts> findByStateOrderByPraiseNumDesc(int state, Pageable pageable);

    @Query(nativeQuery = true,
     value = "select * from experts where state = (?1) order by repliesNum/consultingNum desc")
    public List<Experts> findByRepliesPercent(int state);

    public Experts findByUserIdAndStateIn(String userId, List<Integer> state);

    public Experts findByIdAndStateIn(String id, List<Integer> state);

    @Query(nativeQuery = true,
            value = "select * from experts where state = (?1) and typeId = (?2) order by repliesNum/consultingNum desc")
    public List<Experts> findByTypeAndRepliesPercent(int state, String typeId);

    public Page<Experts> findByStateAndTypeIdOrderByConsultingNumDesc(int state, String typeId, Pageable pageable);

    public Page<Experts> findByStateAndTypeIdOrderByRepliesNumDesc(int state, String typeId, Pageable pageable);

    public Page<Experts> findByStateAndTypeIdOrderByPraiseNumDesc(int state, String typeId, Pageable pageable);
}
