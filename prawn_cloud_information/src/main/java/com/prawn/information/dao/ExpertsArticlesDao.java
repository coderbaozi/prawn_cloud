package com.prawn.information.dao;

import com.prawn.information.pojo.ExpertsArticles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface ExpertsArticlesDao extends JpaRepository<ExpertsArticles,String>,JpaSpecificationExecutor<ExpertsArticles>{

    public void deleteByInformationId(String informationId);

    public ExpertsArticles findByInformationId(String informationId);

    @Query(nativeQuery = true,
    value = "select informationId from experts_articles where expertsId = (?1) and state = 1")
    public List<String> findByExpertsId(String expertsId);
}
