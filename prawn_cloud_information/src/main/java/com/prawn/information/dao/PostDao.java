package com.prawn.information.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.prawn.information.pojo.Post;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * 数据访问接口
 *
 * @author Administrator
 */
public interface PostDao extends JpaRepository<Post, String>, JpaSpecificationExecutor<Post> {

    public List<Post> findByState(int state);

    public Page<Post> findByState(int state, Pageable pageable);

    public Optional<Post> findByIdAndState(String id, int state);

    public Page<Post> findByUserId(String userId, Pageable pageable);

    public Page<Post> findByExpertsIdAndState(String expertsId, int state, Pageable pageable);

    public Page<Post> findByTypeIdAndState(String typeId, int state, Pageable pageable);

    public Page<Post> findByStateOrderByCreationTimeDesc(int state, Pageable pageable);

    public Page<Post> findByStateOrderByClickNumDesc(int state, Pageable pageable);

    public Page<Post> findByTypeIdAndStateOrderByCreationTimeDesc(String typeId, int state, Pageable pageable);

    public Page<Post> findByTypeIdAndStateOrderByClickNumDesc(String typeId, int state, Pageable pageable);

    public Page<Post> findByUserIdAndStateIn(String userId, List<Integer> state, Pageable pageable);

    @Transactional
    @Modifying
    @Query(nativeQuery = true, value = "update post set clickNum=? where id = ?")
    public void updatePost(int clickNum, String id);
}
