package com.prawn.information.dao;

import com.prawn.information.pojo.ExpertsType;
import com.prawn.information.pojo.Types;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
import java.util.Optional;

/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface ExpertsTypeDao extends JpaRepository<ExpertsType,String>,JpaSpecificationExecutor<ExpertsType>{

}
