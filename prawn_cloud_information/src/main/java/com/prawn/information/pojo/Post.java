package com.prawn.information.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 实体类
 *
 * @author Administrator
 */
@Entity
@Table(name = "post")
@ApiModel(value = "Post", description = "帖子对象")
public class Post implements Serializable {

    @Id
    @ApiModelProperty(value = "帖子id", hidden = true)
    private String id;//帖子id

    @ApiModelProperty(value = "标题")
    private String title;//标题
    @ApiModelProperty(value = "创建时间", hidden = true)
    private java.util.Date creationTime;//创建时间
    @ApiModelProperty(value = "提问者id")
    private String userId;//提问者id
    @ApiModelProperty(value = "提问者名称")
    private String userName;//提问者名称
    @ApiModelProperty(value = "专家id")
    private String expertsId;//专家id
    @ApiModelProperty(value = "图片")
    private String images;//图片
    @ApiModelProperty(value = "状态", hidden = true)
    private int state;//状态；0审核中；1审核通过；2审核不通过，3删除
    @ApiModelProperty(value = "类型id")
    private String typeId;//类型id
    @ApiModelProperty(value = "点击量", hidden = true)
    private int clickNum;//点击量

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public java.util.Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(java.util.Date creationTime) {
        this.creationTime = creationTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getExpertsId() {
        return expertsId;
    }

    public void setExpertsId(String expertsId) {
        this.expertsId = expertsId;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public int getClickNum() {
        return clickNum;
    }

    public void setClickNum(int clickNum) {
        this.clickNum = clickNum;
    }
}
