package com.prawn.information.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="information_types")
@ApiModel(value="InformationTypes", description = "类型对象")
public class Types implements Serializable{

	@Id
	@ApiModelProperty(value = "类型id", hidden = true)
	private String id;//资讯类型id



	@ApiModelProperty(value = "类型")
	private String name;//资讯类型
	@ApiModelProperty(value = "父级分类（扩展需要时使用）", hidden = true)
	private String parentId;//父级分类（扩展需要时使用）
	@ApiModelProperty(value = "状态（false：删除）",hidden = true)
	private boolean state;//状态（false：删除）
	@ApiModelProperty(value = "所属模块", hidden = true)
	private Integer module;//所属模块（1资讯2政策3技术文章4帖子）

	public Integer getModule() {
		return module;
	}

	public void setModule(Integer module) {
		this.module = module;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getName() {		
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getParentId() {		
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}
}
