package com.prawn.information.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 实体类
 *
 * @author Administrator
 */
@Entity
@Table(name = "experts_type")
@ApiModel(value = "ExpertsType", description = "专家类别对象")
public class ExpertsType implements Serializable {

    @Id
    @ApiModelProperty(value = "专家类别id", hidden = true)
    private String id;//专家类别id


    @ApiModelProperty(value = "专家类别")
    private String name;//专家类别


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
