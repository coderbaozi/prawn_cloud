package com.prawn.information.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 实体类
 *
 * @author Administrator
 */
@Entity
@Table(name = "experts")
@ApiModel(value = "Experts", description = "专家对象")
public class Experts implements Serializable {

    @Id
    @ApiModelProperty(value = "专家id", hidden = true)
    private String id;//专家id

    @ApiModelProperty(value = "名称")
    private String name;//名称
    @ApiModelProperty(value = "头像")
    private String picture;//头像
    @ApiModelProperty(value = "地址")
    private String address;//地址
    @ApiModelProperty(value = "擅长")
    private String goodAt;//擅长
    @ApiModelProperty(value = "简介")
    private String introduction;//简介
    @ApiModelProperty(value = "回复量", hidden = true)
    private Integer repliesNum;//回复量
    @ApiModelProperty(value = "点赞量", hidden = true)
    private Integer praiseNum;//点赞量
    @ApiModelProperty(value = "咨询量", hidden = true)
    private Integer consultingNum;//咨询量
    @ApiModelProperty(value = "用户id")
    private String userId;//用户id
    @ApiModelProperty(value = "状态", hidden = true)
    private int state;//状态；0审核中，1审核通过，2审核不通过，3删除
    @ApiModelProperty(value = "在线时间")
    private String onlineTime;//在线时间
    @ApiModelProperty(value = "类别id")
    private String typeId;//类别id

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public Integer getRepliesNum() {
        return repliesNum;
    }

    public void setRepliesNum(Integer repliesNum) {
        this.repliesNum = repliesNum;
    }

    public Integer getPraiseNum() {
        return praiseNum;
    }

    public void setPraiseNum(Integer praiseNum) {
        this.praiseNum = praiseNum;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGoodAt() {
        return goodAt;
    }

    public void setGoodAt(String goodAt) {
        this.goodAt = goodAt;
    }

    public Integer getConsultingNum() {
        return consultingNum;
    }

    public void setConsultingNum(Integer consultingNum) {
        this.consultingNum = consultingNum;
    }

    public String getOnlineTime() {
        return onlineTime;
    }

    public void setOnlineTime(String onlineTime) {
        this.onlineTime = onlineTime;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }
}
