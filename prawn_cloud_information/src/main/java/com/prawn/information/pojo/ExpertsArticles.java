package com.prawn.information.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="experts_articles")
@ApiModel(value="ExpertsArticles", description = "专家所写技术文章对象")
public class ExpertsArticles implements Serializable{

	@Id
	@ApiModelProperty(value = "id", hidden = true)
	private String id;//点击记录对象

	@ApiModelProperty(value = "专家id")
	private String expertsId;//专家id
	@ApiModelProperty(value = "专家技术文章id")
	private String informationId;//专家技术文章id
	@ApiModelProperty(value = "状态")
	private Integer state; //状态

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getExpertsId() {
		return expertsId;
	}

	public void setExpertsId(String expertsId) {
		this.expertsId = expertsId;
	}

	public String getInformationId() {
		return informationId;
	}

	public void setInformationId(String informationId) {
		this.informationId = informationId;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}
}
