package com.prawn.information.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * 实体类
 *
 * @author Administrator
 */
@Entity
@Table(name = "post_details")
@ApiModel(value = "Details", description = "回帖对象")
public class Details implements Serializable {

    @Id
    @ApiModelProperty(value = "回帖id", hidden = true)
    private String id;//回帖id


    @ApiModelProperty(value = "回复")
    private String reply;//回复
    @ApiModelProperty(value = "回复者", notes = "用户id/专家id")
    private String replier;//回复者
    @ApiModelProperty(value = "回复者名称", notes = "用户id/专家id")
    private String replierName;//回复者名称
    @ApiModelProperty(value = "点赞量", hidden = true)
    private Integer praiseNum;//点赞量
    @ApiModelProperty(value = "帖子id")
    private String postId;//帖子id
    @ApiModelProperty(value = "身份", notes = "false普通用户,true专家")
    private boolean isExperts;//false普通用户,true专家
    @ApiModelProperty(value = "回帖时间", hidden = true)
    private java.util.Date creationTime;//回帖时间
    @ApiModelProperty(value = "图片")
    private String images;//图片
    @ApiModelProperty(value = "状态", hidden = true)
    private int state;//状态；0审核中，1审核通过，2审核不通过，3删除

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }

    public String getReplier() {
        return replier;
    }

    public void setReplier(String replier) {
        this.replier = replier;
    }

    public Integer getPraiseNum() {
        return praiseNum;
    }

    public void setPraiseNum(Integer praiseNum) {
        this.praiseNum = praiseNum;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public boolean isExperts() {
        return isExperts;
    }

    public void setExperts(boolean experts) {
        isExperts = experts;
    }

    public String getReplierName() {
        return replierName;
    }

    public void setReplierName(String replierName) {
        this.replierName = replierName;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

}
