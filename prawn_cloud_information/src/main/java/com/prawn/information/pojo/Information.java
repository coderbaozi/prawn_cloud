package com.prawn.information.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 实体类
 *
 * @author Administrator
 */
@Entity
@Table(name = "industry_information")
@ApiModel(value = "Information", description = "产业资讯对象")
public class Information implements Serializable {

    @Id
    @ApiModelProperty(value = "资讯id", hidden = true)
    private String id;//资讯id


    @ApiModelProperty(value = "标题")
    private String title;//标题
    @ApiModelProperty(value = "作者")
    private String editor;//作者
    @ApiModelProperty(value = "概要")
    private String summary;//概要
    @ApiModelProperty(value = "正文")
    private String content;//正文
    @ApiModelProperty(value = "图片")
    private String picture;//图片
    @ApiModelProperty(value = "资讯类型id")
    private String typeId;//资讯类型id
    @ApiModelProperty(value = "创建时间", hidden = true)
    private java.util.Date creationTime;//创建时间
    @ApiModelProperty(value = "状态（0删除 1存在 2审核中 3审核不通过）", hidden = true)
    private int state;//状态（0删除 1存在 2审核中 3审核不通过）
    @ApiModelProperty(value = "所属模块", hidden = true)
    private Integer module;//所属模块（1行情2政策3专家文章）
    @ApiModelProperty(value = "点击量", hidden = true)
    private Integer clickNum;//点击量
    @ApiModelProperty(value = "推荐", hidden = true)
    private boolean recommend;//推荐

    public Integer getModule() {
        return module;
    }

    public void setModule(Integer module) {
        this.module = module;
    }

    public Integer getClickNum() {
        return clickNum;
    }

    public void setClickNum(Integer clickNum) {
        this.clickNum = clickNum;
    }

    public boolean isRecommend() {
        return recommend;
    }

    public void setRecommend(boolean recommend) {
        this.recommend = recommend;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEditor() {
        return editor;
    }

    public void setEditor(String editor) {
        this.editor = editor;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public java.util.Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(java.util.Date creationTime) {
        this.creationTime = creationTime;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }
}
