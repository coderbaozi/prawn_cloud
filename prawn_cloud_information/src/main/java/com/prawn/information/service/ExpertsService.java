package com.prawn.information.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import com.prawn.information.pojo.Information;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import util.IdWorker;

import com.prawn.information.dao.ExpertsDao;
import com.prawn.information.pojo.Experts;

/**
 * 服务层
 *
 * @author Administrator
 */
@Service
public class ExpertsService {

    @Autowired
    private ExpertsDao expertsDao;

    @Autowired
    private IdWorker idWorker;

    /**
     * 查询全部列表
     *
     * @return
     */
    public Iterable<Experts> findAll() {
        return expertsDao.findByState(1);
    }

    /**
     * 查询全部列表-分页
     *
     * @return
     */
    public Page<Experts> findAll(int state, int page, int size) {
        PageRequest pageRequest = PageRequest.of(page - 1, size);
        return expertsDao.findByState(state, pageRequest);
    }

    /**
     * 根据id查找
     * @param state
     * @param id
     * @return
     */
    public Experts findById(int state, String id) {
        return expertsDao.findByIdAndState(id, state).get();
    }

    public Experts findById(String id) {
        return expertsDao.findById(id).get();
    }

    public Experts findByIdAndState(String id, List<Integer> status) {
        return expertsDao.findByIdAndStateIn(id, status);
    }

    public Page<Experts> findByTypeId(String typeId, int page, int size) {
        PageRequest pageRequest = PageRequest.of(page-1, size);
        return expertsDao.findByTypeIdAndState(typeId, 1, pageRequest);
    }

    /**
     * 增加
     *
     * @param experts
     */
    public void add(Experts experts) {
        experts.setConsultingNum(0);
        experts.setRepliesNum(0);
        experts.setPraiseNum(0);
        experts.setId(idWorker.nextId() + "");
        expertsDao.save(experts);
    }

    /**
     * 修改
     *
     * @param experts
     */
    public void update(Experts experts) {
        expertsDao.save(experts);
    }

    /**
     * 删除
     *
     * @param id
     */
    public void deleteById(String id) {
        expertsDao.deleteById(id);
    }

    /**
     * 根据用户ID查询专家
     *
     * @param userId
     * @return
     */
    public Experts findByUserId(String userId, int state) {
        return expertsDao.findByUserIdAndState(userId, state).get();
    }

    public Experts findByUserId(String userId, List<Integer> status) {
        return expertsDao.findByUserIdAndStateIn(userId, status);
    }

    public Page<Experts> findByConsultingNum(int page, int size) {
        return expertsDao.findByStateOrderByConsultingNumDesc(1, PageRequest.of(page-1, size));
    }

    public Page<Experts> findByRepliesNum(int page, int size) {
        return expertsDao.findByStateOrderByRepliesNumDesc(1, PageRequest.of(page-1, size));
    }

    public Page<Experts> findByPraiseNum(int page, int size) {
        return expertsDao.findByStateOrderByPraiseNumDesc(1, PageRequest.of(page-1, size));
    }

    public Page<Experts> findByRepliesPercent(int page, int size) {
        List<Experts> list = expertsDao.findByRepliesPercent(1);
        Pageable pageable = PageRequest.of(page-1,size);
        int start = (int)pageable.getOffset();
        int end = (start + pageable.getPageSize()) > list.size() ? list.size() : ( start + pageable.getPageSize());
        return new PageImpl<Experts>(list.subList(start, end), pageable, list.size());
    }

    public Page<Experts> findByTypeAndConsultingNum(String typeId, int page, int size) {
        return expertsDao.findByStateAndTypeIdOrderByConsultingNumDesc(1,typeId, PageRequest.of(page-1, size));
    }

    public Page<Experts> findByTypeAndRepliesNum(String typeId, int page, int size) {
        return expertsDao.findByStateAndTypeIdOrderByRepliesNumDesc(1,typeId,  PageRequest.of(page-1, size));
    }

    public Page<Experts> findByTypeAndPraiseNum(String typeId, int page, int size) {
        return expertsDao.findByStateAndTypeIdOrderByPraiseNumDesc(1,typeId,  PageRequest.of(page-1, size));
    }

    public Page<Experts> findByTypeAndRepliesPercent(String typeId, int page, int size) {
        List<Experts> list = expertsDao.findByTypeAndRepliesPercent(1, typeId);
        Pageable pageable = PageRequest.of(page-1,size);
        int start = (int)pageable.getOffset();
        int end = (start + pageable.getPageSize()) > list.size() ? list.size() : ( start + pageable.getPageSize());
        return new PageImpl<Experts>(list.subList(start, end), pageable, list.size());
    }

}
