package com.prawn.information.service;

import java.util.*;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import util.IdWorker;

import com.prawn.information.dao.DetailsDao;
import com.prawn.information.pojo.Details;

/**
 * 服务层
 *
 * @author Administrator
 */
@Service
public class DetailsService {

    @Autowired
    private DetailsDao detailsDao;

    @Autowired
    private IdWorker idWorker;

    /**
     * 根据帖子id查询回复
     *
     * @param postId
     * @param page
     * @param size
     * @return
     */
    public Page<Details> findByPostId(String postId, int page, int size) {
        PageRequest pageRequest = PageRequest.of(page - 1, size);
        return detailsDao.findByPostIdAndState(postId, 1, pageRequest);
    }

    public List<Details> findByPostId(String postId) {
        return detailsDao.findByPostIdAndState(postId, 1);
    }

    /**
     * 根据ID查询实体
     *
     * @param id
     * @return
     */
    public Details findById(int state, String id) {
        return detailsDao.findByIdAndState(id, state).get();
    }

    public Details findById(String id) {
        return detailsDao.findById(id).get();
    }

    /**
     * 增加
     *
     * @param details
     */
    public void add(Details details) {
        details.setId(idWorker.nextId() + "");
        details.setPraiseNum(0);
        details.setCreationTime(new Date());
        detailsDao.save(details);
    }

    /**
     * 修改
     *
     * @param details
     */
    public void update(Details details) {
        detailsDao.save(details);
    }

    /**
     * 删除
     *
     * @param id
     */
    public void deleteById(String id) {
        detailsDao.deleteById(id);
    }

    public void deleteList(List<Details> detailsList) {
        for (Details details : detailsList) {
            detailsDao.deleteById(details.getId());
        }
    }

    /**
     * 查询全部列表-分页
     *
     * @return
     */
    public Page<Details> findAll(int state, int page, int size) {
        PageRequest pageRequest = PageRequest.of(page - 1, size);
        return detailsDao.findByState(state, pageRequest);
    }


    /**
     * 根据专家id查询回复
     *
     * @param replier
     * @param page
     * @param size
     * @return
     */
    public Page<Details> findByReplier(String replier, List<Integer> status, int page, int size) {
        PageRequest pageRequest = PageRequest.of(page - 1, size);
        return detailsDao.findByReplierAndStateIn(replier, status, pageRequest);
    }
}
