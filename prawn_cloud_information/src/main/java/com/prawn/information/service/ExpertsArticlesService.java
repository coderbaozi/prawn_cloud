package com.prawn.information.service;

import com.prawn.information.dao.ExpertsArticlesDao;
import com.prawn.information.pojo.ExpertsArticles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import util.IdWorker;

import java.util.List;

/**
 * @Author: cxy
 * @Description:
 */
@Service
public class ExpertsArticlesService {

    @Autowired
    private ExpertsArticlesDao expertsArticlesDao;

    @Autowired
    private IdWorker idWorker;


    /**
     * 增加
     * @param expertsArticles
     */
    public void add(ExpertsArticles expertsArticles) {
        expertsArticles.setId(idWorker.nextId() + "");
        expertsArticlesDao.save(expertsArticles);
    }

    public void deleteByInformationId(String informationId){
        ExpertsArticles expertsArticles = expertsArticlesDao.findByInformationId(informationId);
        expertsArticles.setState(0);
        expertsArticlesDao.save(expertsArticles);
//        expertsArticlesDao.deleteByInformationId(informationId);
    }

}
