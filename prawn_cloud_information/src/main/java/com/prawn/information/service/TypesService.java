package com.prawn.information.service;

import java.util.*;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import util.IdWorker;

import com.prawn.information.dao.TypesDao;
import com.prawn.information.pojo.Types;

/**
 * 服务层
 *
 * @author Administrator
 */
@Service
public class TypesService {

    @Autowired
    private TypesDao typesDao;

    @Autowired
    private IdWorker idWorker;


    /**
     * 增加
     *
     * @param types
     */
    public void add(Types types) {
        types.setId(idWorker.nextId() + "");
        types.setState(true);
        typesDao.save(types);
    }

    /**
     * 修改
     *
     * @param types
     */
    public void update(Types types) {
        types.setState(true);
        typesDao.save(types);
    }

    /**
     * 删除
     *
     * @param id
     */
    public void deleteById(String id) {
        Optional<Types> types = typesDao.findById(id);
        types.get().setState(false);
        typesDao.save(types.get());
    }



    public List<Types> findAll(int module) {
        return typesDao.findByModuleAndStateTrue(module);
    }

    public Types findById(String id) {
        return typesDao.findByIdAndStateTrue(id);
    }

    public Page<Types> findAll(int module, int page, int size) {
        return typesDao.findByModuleAndStateTrue(module, PageRequest.of(page-1, size));
    }
}
