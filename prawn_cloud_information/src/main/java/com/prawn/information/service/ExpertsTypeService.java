package com.prawn.information.service;

import com.prawn.information.dao.ExpertsTypeDao;
import com.prawn.information.pojo.ExpertsType;
import com.prawn.information.pojo.Types;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import util.IdWorker;

import java.util.List;
import java.util.Optional;

/**
 * 服务层
 *
 * @author Administrator
 */
@Service
public class ExpertsTypeService {

    @Autowired
    private ExpertsTypeDao expertsTypeDao;

    @Autowired
    private IdWorker idWorker;


    /**
     * 增加
     * @param expertsType
     */
    public void add(ExpertsType expertsType) {
        expertsType.setId(idWorker.nextId() + "");
        expertsTypeDao.save(expertsType);
    }

    /**
     * 修改
     * @param expertsType
     */
    public void update(ExpertsType expertsType) {
        expertsTypeDao.save(expertsType);
    }

    /**
     * 删除
     *
     * @param id
     */
    public void deleteById(String id) {
        expertsTypeDao.deleteById(id);
    }

    public List<ExpertsType> findAll() {
        return expertsTypeDao.findAll();
    }

    public Optional<ExpertsType> findById(String id) {
        return expertsTypeDao.findById(id);
    }

    public Page<ExpertsType> findAll(int page, int size) {
        PageRequest pageRequest = PageRequest.of(page-1, size);
        return expertsTypeDao.findAll(pageRequest);
    }
}
