package com.prawn.information.task;

import com.prawn.information.dao.InformationDao;
import com.prawn.information.dao.PostDao;
import com.prawn.information.pojo.Information;
import com.prawn.information.pojo.Post;
import com.prawn.information.service.InformationService;
import com.prawn.information.service.PostService;
import com.prawn.information.util.RedisUtil;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.data.redis.core.Cursor;

import java.util.*;

/**
 * @Author: cxy
 * @Description: 阅读量计数定时任务
 */
public class InfoTask extends QuartzJobBean {

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private PostDao postDao;

    @Autowired
    private InformationDao informationDao;

    /**
     * 对redis中的数据进行计数并写入数据库
     *
     * @param jobExecutionContext
     * @throws JobExecutionException
     */
    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        Map<Object, Object> clickPost = redisUtil.hmget("clickPost");
        Map<Object, Object> clickInfo = redisUtil.hmget("clickInfo");

        for (Map.Entry<Object, Object> entry : clickPost.entrySet()) {
            postDao.updatePost((int) entry.getValue(), (String) entry.getKey());
        }

        for (Map.Entry<Object, Object> entry : clickInfo.entrySet()) {
            informationDao.updateInfo((int) entry.getValue(), (String) entry.getKey());
        }
    }
}
