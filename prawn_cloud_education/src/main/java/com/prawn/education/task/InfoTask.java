package com.prawn.education.task;

import com.prawn.education.dao.InformationDao;
import com.prawn.education.pojo.Information;
import com.prawn.education.service.InformationService;
import com.prawn.education.util.RedisUtil;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * @Author: cxy
 * @Description: 阅读量计数定时任务
 */
public class InfoTask extends QuartzJobBean {

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private InformationDao informationDao;

    /**
     * 对redis中的数据进行计数并写入数据库
     *
     * @param jobExecutionContext
     * @throws JobExecutionException
     */
    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        Map<Object, Object> clickNum = redisUtil.hmget("clickNum");
        for (Map.Entry<Object, Object> entry : clickNum.entrySet()) {
            informationDao.updateClickNum((int) entry.getValue(), (String) entry.getKey());
        }
    }
}
