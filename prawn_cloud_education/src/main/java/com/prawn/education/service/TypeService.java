package com.prawn.education.service;

import java.util.*;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import util.IdWorker;

import com.prawn.education.dao.TypeDao;
import com.prawn.education.pojo.Type;

/**
 * 服务层
 *
 * @author Administrator
 */
@Service
public class TypeService {

    @Autowired
    private TypeDao typeDao;

    @Autowired
    private IdWorker idWorker;

    /**
     * 增加
     *
     * @param type
     */
    public void add(Type type) {
        type.setId(idWorker.nextId() + "");
        type.setState(true);
        typeDao.save(type);
    }

    /**
     * 修改
     *
     * @param type
     */
    public void update(Type type) {
        type.setState(true);
        typeDao.save(type);
    }

    /**
     * 删除
     *
     * @param id
     */
    public void deleteById(String id) {
        Optional<Type> type = typeDao.findById(id);
        type.get().setState(false);
        typeDao.save(type.get());
    }

    public List<Type> findAll(int module) {
        return typeDao.findByModuleAndStateTrue(module);
    }

    public Optional<Type> findById(String id) {
        return typeDao.findByIdAndStateTrue(id);
    }

    public Page<Type> findAll(int module, int page, int size) {
        return typeDao.findByModuleAndStateTrue(module, PageRequest.of(page-1, size));
    }

}
