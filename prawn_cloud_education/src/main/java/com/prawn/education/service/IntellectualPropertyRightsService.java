package com.prawn.education.service;

import com.prawn.education.dao.IntellectualPropertyRightsDao;
import com.prawn.education.pojo.Information;
import com.prawn.education.pojo.IntellectualPropertyRights;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import util.IdWorker;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * 服务层
 *
 * @author Administrator
 */
@Service
public class IntellectualPropertyRightsService {

    @Autowired
    private IntellectualPropertyRightsDao intellectualPropertyRightsDao;

    @Autowired
    private IdWorker idWorker;

    /**
     * 增加
     * @param intellectualPropertyRights
     */
    public void add(IntellectualPropertyRights intellectualPropertyRights) {
        intellectualPropertyRights.setId(idWorker.nextId() + "");
        intellectualPropertyRights.setState(true);
        intellectualPropertyRightsDao.save(intellectualPropertyRights);
    }

    /**
     * 修改
     * @param intellectualPropertyRights
     */
    public void update(IntellectualPropertyRights intellectualPropertyRights) {
        intellectualPropertyRightsDao.save(intellectualPropertyRights);
    }

    /**
     * 删除
     * @param id
     */
    public void deleteById(String id) {
        IntellectualPropertyRights intellectualPropertyRights = intellectualPropertyRightsDao.findById(id).get();
        intellectualPropertyRights.setState(false);
        update(intellectualPropertyRights);
    }

    public IntellectualPropertyRights findById(String id){
        return intellectualPropertyRightsDao.findByIdAndStateTrue(id);
    }

    public IntellectualPropertyRights findByDeleteId(String id){
        return intellectualPropertyRightsDao.findByIdAndStateFalse(id);
    }

    public List<IntellectualPropertyRights> findAll() {
        return intellectualPropertyRightsDao.findByStateTrue();
    }

    public Page<IntellectualPropertyRights> findAll(int page, int size) {
        PageRequest pageRequest = PageRequest.of(page-1, size);
        return intellectualPropertyRightsDao.findByStateTrue(pageRequest);
    }

    public Page<IntellectualPropertyRights> findByDelete(int page, int size) {
        PageRequest pageRequest = PageRequest.of(page-1, size);
        return intellectualPropertyRightsDao.findByStateFalse(pageRequest);
    }

    public Page<IntellectualPropertyRights> findByTypeId(String typeId, int page, int size) {
        return intellectualPropertyRightsDao.findByTypeIdAndStateTrue(typeId, PageRequest.of(page - 1, size));
    }
}
