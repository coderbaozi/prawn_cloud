package com.prawn.education.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import util.IdWorker;

import com.prawn.education.dao.DiagnoseDao;
import com.prawn.education.pojo.Diagnose;

/**
 * 服务层
 * 
 * @author Administrator
 *
 */
@Service
public class DiagnoseService {

	@Autowired
	private DiagnoseDao diagnoseDao;
	
	@Autowired
	private IdWorker idWorker;

	/**
	 * 查询全部列表
	 * @return
	 */
	public List<Diagnose> findAll() {
		return diagnoseDao.findAll();
	}

	
	/**
	 * 条件查询+分页
	 * @param whereMap
	 * @param page
	 * @param size
	 * @return
	 */
	public Page<Diagnose> findSearch(Map whereMap, int page, int size) {
		Specification<Diagnose> specification = createSpecification(whereMap);
		PageRequest pageRequest =  PageRequest.of(page-1, size);
		return diagnoseDao.findAll(specification, pageRequest);
	}

	
	/**
	 * 条件查询
	 * @param whereMap
	 * @return
	 */
	public List<Diagnose> findSearch(Map whereMap) {
		Specification<Diagnose> specification = createSpecification(whereMap);
		return diagnoseDao.findAll(specification);
	}

	/**
	 * 根据ID查询实体
	 * @param id
	 * @return
	 */
	public Diagnose findById(String id) {
		return diagnoseDao.findById(id).get();
	}

	/**
	 * 增加
	 * @param diagnose
	 */
	public void add(Diagnose diagnose) {
		diagnose.setId( idWorker.nextId()+"" );
		diagnoseDao.save(diagnose);
	}

	/**
	 * 修改
	 * @param diagnose
	 */
	public void update(Diagnose diagnose) {
		diagnoseDao.save(diagnose);
	}

	/**
	 * 删除
	 * @param id
	 */
	public void deleteById(String id) {
		diagnoseDao.deleteById(id);
	}

	/**
	 * 动态条件构建
	 * @param searchMap
	 * @return
	 */
	private Specification<Diagnose> createSpecification(Map searchMap) {

		return new Specification<Diagnose>() {

			@Override
			public Predicate toPredicate(Root<Diagnose> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<Predicate>();
                // 
                if (searchMap.get("id")!=null && !"".equals(searchMap.get("id"))) {
                	predicateList.add(cb.like(root.get("id").as(String.class), "%"+(String)searchMap.get("id")+"%"));
                }
                // 
                if (searchMap.get("shrimpKind")!=null && !"".equals(searchMap.get("shrimpKind"))) {
                	predicateList.add(cb.like(root.get("shrimpKind").as(String.class), "%"+(String)searchMap.get("shrimpKind")+"%"));
                }
                // 
                if (searchMap.get("symptom")!=null && !"".equals(searchMap.get("symptom"))) {
                	predicateList.add(cb.like(root.get("symptom").as(String.class), "%"+(String)searchMap.get("symptom")+"%"));
                }
                // 
                if (searchMap.get("solution")!=null && !"".equals(searchMap.get("solution"))) {
                	predicateList.add(cb.like(root.get("solution").as(String.class), "%"+(String)searchMap.get("solution")+"%"));
                }
				
				return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

			}
		};

	}

}
