package com.prawn.education.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import util.IdWorker;

import com.prawn.education.dao.ModelDao;
import com.prawn.education.pojo.Model;

/**
 * 服务层
 * 
 * @author Administrator
 *
 */
@Service
public class ModelService {

	@Autowired
	private ModelDao modelDao;
	
	@Autowired
	private IdWorker idWorker;

	/**
	 * 查询全部列表
	 * @return
	 */
	public List<Model> findAll() {
		return modelDao.findAll();
	}

	
	/**
	 * 条件查询+分页
	 * @param whereMap
	 * @param page
	 * @param size
	 * @return
	 */
	public Page<Model> findSearch(Map whereMap, int page, int size) {
		Specification<Model> specification = createSpecification(whereMap);
		PageRequest pageRequest =  PageRequest.of(page-1, size);
		return modelDao.findAll(specification, pageRequest);
	}

	
	/**
	 * 条件查询
	 * @param whereMap
	 * @return
	 */
	public List<Model> findSearch(Map whereMap) {
		Specification<Model> specification = createSpecification(whereMap);
		return modelDao.findAll(specification);
	}

	/**
	 * 根据ID查询实体
	 * @param id
	 * @return
	 */
	public Model findById(String id) {
		return modelDao.findById(id).get();
	}

	/**
	 * 增加
	 * @param model
	 */
	public void add(Model model) {
		model.setId( idWorker.nextId()+"" );
		modelDao.save(model);
	}

	/**
	 * 修改
	 * @param model
	 */
	public void update(Model model) {
		modelDao.save(model);
	}

	/**
	 * 删除
	 * @param id
	 */
	public void deleteById(String id) {
		modelDao.deleteById(id);
	}

	/**
	 * 动态条件构建
	 * @param searchMap
	 * @return
	 */
	private Specification<Model> createSpecification(Map searchMap) {

		return new Specification<Model>() {

			@Override
			public Predicate toPredicate(Root<Model> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<Predicate>();
                // id
                if (searchMap.get("id")!=null && !"".equals(searchMap.get("id"))) {
                	predicateList.add(cb.like(root.get("id").as(String.class), "%"+(String)searchMap.get("id")+"%"));
                }
                // 口感
                if (searchMap.get("texture")!=null && !"".equals(searchMap.get("texture"))) {
                	predicateList.add(cb.like(root.get("texture").as(String.class), "%"+(String)searchMap.get("texture")+"%"));
                }
                // 颜色
                if (searchMap.get("color")!=null && !"".equals(searchMap.get("color"))) {
                	predicateList.add(cb.like(root.get("color").as(String.class), "%"+(String)searchMap.get("color")+"%"));
                }
                // 外观
                if (searchMap.get("appearance")!=null && !"".equals(searchMap.get("appearance"))) {
                	predicateList.add(cb.like(root.get("appearance").as(String.class), "%"+(String)searchMap.get("appearance")+"%"));
                }
				// 类型(蒸煮时间、速冻时间)
				if (searchMap.get("modeltype")!=null && !"".equals(searchMap.get("modeltype"))) {
					predicateList.add(cb.equal(root.get("modeltype").as(Integer.class),  (Integer) searchMap.get("modeltype")));
				}
				// 时长
				if (searchMap.get("duration")!=null && !"".equals(searchMap.get("duration"))) {
					predicateList.add(cb.equal(root.get("duration").as(Integer.class),  (Integer) searchMap.get("duration")));
				}
				// 温度
				if (searchMap.get("temperature")!=null && !"".equals(searchMap.get("temperature"))) {
					predicateList.add(cb.equal(root.get("temperature").as(Integer.class),  (Integer) searchMap.get("temperature")));
				}

				return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

			}
		};

	}

}
