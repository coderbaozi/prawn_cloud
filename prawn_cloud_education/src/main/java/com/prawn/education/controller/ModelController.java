package com.prawn.education.controller;
import java.util.List;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.prawn.education.pojo.Model;
import com.prawn.education.service.ModelService;

import entity.PageResult;
import entity.Result;
import entity.StatusCode;
/**
 * 控制器层
 * @author Administrator
 *
 */
@RestController
@CrossOrigin
@Api(value = "仿真模型接口", tags = "仿真模型接口")
@RequestMapping("/model")
public class ModelController {

	@Autowired
	private ModelService modelService;
	
	
	/**
	 * 查询全部数据
	 * @return
	 */
    @ApiOperation(value = "查询全部数据")
	@RequestMapping(method= RequestMethod.GET)
	public Result findAll(){
		return new Result(true,StatusCode.OK,"查询成功",modelService.findAll());
	}
	
	/**
	 * 根据ID查询
	 * @param id ID
	 * @return
	 */
    @ApiOperation(value = "根据ID查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "主键", paramType = "path", required = true)
    })
	@RequestMapping(value="/{id}",method= RequestMethod.GET)
	public Result findById(@PathVariable String id){
		return new Result(true,StatusCode.OK,"查询成功",modelService.findById(id));
	}


	/**
	 * 分页+多条件查询
	 * @param searchMap 查询条件封装
	 * @param page 页码
	 * @param size 页大小
	 * @return 分页结果
	 */
	@ApiOperation("分页+多条件查询")
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "body",name = "searchMap", value = "仿真模型对象",dataType = "Model"),
			@ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
			@ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
	})
	@RequestMapping(value="/search/{page}/{size}",method=RequestMethod.POST)
	public Result findSearch(@RequestBody Map searchMap , @PathVariable int page, @PathVariable int size){
		try {
			Thread.sleep(3000);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Page<Model> pageList = modelService.findSearch(searchMap, page, size);
		return  new Result(true,StatusCode.OK,"查询成功",  new PageResult<Model>(pageList.getTotalElements(), pageList.getContent()) );
	}

	/**
     * 根据条件查询
     * @param searchMap
     * @return
     */
    @ApiOperation(value = "根据条件查询（不分页）")
    @ApiImplicitParams({
			@ApiImplicitParam(paramType = "body",name = "searchMap", value = "仿真模型对象",dataType = "Model"),
    })
    @RequestMapping(value="/search",method = RequestMethod.POST)
    public Result findSearch( @RequestBody Map searchMap){
		try {
			Thread.sleep(3000);
		} catch (Exception e) {
			e.printStackTrace();
		}
        return new Result(true,StatusCode.OK,"查询成功",modelService.findSearch(searchMap));
    }
	
	/**
	 * 增加
	 * @param model
	 */
    @ApiOperation(value = "增加")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "model", value = "仿真模型对象", dataType = "Model")
    })
	@RequestMapping(method=RequestMethod.POST)
	public Result add(@RequestBody Model model  ){
		modelService.add(model);
		return new Result(true,StatusCode.OK,"增加成功");
	}
	
	/**
	 * 修改
	 * @param model
	 */
    @ApiOperation(value = "修改")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "model", value = "仿真模型对象", dataType = "Model"),
            @ApiImplicitParam(name = "id", value = "主键", paramType = "path", required = true)
    })
	@RequestMapping(value="/{id}",method= RequestMethod.PUT)
	public Result update(@RequestBody Model model, @PathVariable String id ){
		model.setId(id);
		modelService.update(model);		
		return new Result(true,StatusCode.OK,"修改成功");
	}
	
	/**
	 * 删除
	 * @param id
	 */
    @ApiOperation(value = "删除")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "主键", paramType = "path", required = true)
    })
	@RequestMapping(value="/{id}",method= RequestMethod.DELETE)
	public Result delete(@PathVariable String id ){
		modelService.deleteById(id);
		return new Result(true,StatusCode.OK,"删除成功");
	}
	
}
