package com.prawn.education.controller;

import com.prawn.education.pojo.IntellectualPropertyRightsTypes;
import com.prawn.education.pojo.Type;
import com.prawn.education.service.IntellectualPropertyRightsTypesService;
import com.prawn.education.service.TypeService;
import entity.PageResult;
import entity.Result;
import entity.StatusCode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * 控制器层
 * @author Administrator
 *
 */
@RestController
@CrossOrigin
@RequestMapping("/intellectualPropertyRightsTypes")
@Api(value = "知识产权类型类接口", tags = "知识产权类型类接口")
public class IntellectualPropertyRightsTypesController {


	@Autowired
	private IntellectualPropertyRightsTypesService typeService;

	@ApiOperation(value = "根据id查询")
	@ApiImplicitParam(name = "id", value = "类型id", paramType = "path", required = true)
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Result findById(@PathVariable String id) {
		Optional<IntellectualPropertyRightsTypes> educationTypes = typeService.findById(id);
		return new Result(true, StatusCode.OK, "查询成功", educationTypes);
	}

	/**
	 * 增加
	 * @param type
	 */
	@ApiOperation(value = "增加", notes = "- 暂时只做两级分类，level为1，2，若level传输数据错误，则默认为一级分类")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "type",value = "知识产权类型对象",dataType = "IntellectualPropertyRightsTypes")
	})
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public Result add(@RequestBody IntellectualPropertyRightsTypes type){
		typeService.add(type);
		return new Result(true,StatusCode.OK,"增加成功");
	}

	/**
	 * 修改
	 * @param type
	 */
	@ApiOperation(value = "修改", notes = "- 暂时只做两级分类，level为1，2，若level传输数据错误，则默认为一级分类")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "type",value = "知识产权类型对象",dataType = "IntellectualPropertyRightsTypes"),
			@ApiImplicitParam(name = "id",value = "类型id",dataType = "String", paramType = "path")
	})
	@RequestMapping(value="/update/{id}",method= RequestMethod.PUT)
	public Result update(@RequestBody IntellectualPropertyRightsTypes type, @PathVariable String id ){
		type.setId(id);
		typeService.update(type);
		return new Result(true,StatusCode.OK,"修改成功");
	}

	/**
	 * 查询全部数据-分页
	 * @param page 页码
	 * @param size 页大小
	 * @return 分页结果
	 */
	@ApiOperation(value = "查询全部类型-分页")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
			@ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
	})
	@RequestMapping(value="/{page}/{size}",method=RequestMethod.POST)
	public Result findAll( @PathVariable int page, @PathVariable int size){
		Page<IntellectualPropertyRightsTypes> pageList = typeService.findAll(page, size);
		return  new Result(true,StatusCode.OK,"查询成功",  new PageResult<IntellectualPropertyRightsTypes>(pageList.getTotalElements(), pageList.getContent()) );
	}

	@ApiOperation(value = "按级别查询类型-分页")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "level", value = "级别", paramType = "path", required = true),
			@ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
			@ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
	})
	@RequestMapping(value="findByLevel/{level}/{page}/{size}",method=RequestMethod.POST)
	public Result findByLevel(@PathVariable int level, @PathVariable int page, @PathVariable int size){
		Page<IntellectualPropertyRightsTypes> pageList = typeService.findByLevel(level, page, size);
		return  new Result(true,StatusCode.OK,"查询成功",  new PageResult<IntellectualPropertyRightsTypes>(pageList.getTotalElements(), pageList.getContent()) );
	}

	@ApiOperation(value = "查询某类型的下一级分类-分页")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "parentId", value = "级别", paramType = "path", required = true),
			@ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
			@ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
	})
	@RequestMapping(value="findSon/{parentId}/{page}/{size}",method=RequestMethod.POST)
	public Result findSon(@PathVariable String parentId, @PathVariable int page, @PathVariable int size){
		Page<IntellectualPropertyRightsTypes> pageList = typeService.findByParentId(parentId ,page, size);
		return  new Result(true,StatusCode.OK,"查询成功",  new PageResult<IntellectualPropertyRightsTypes>(pageList.getTotalElements(), pageList.getContent()) );
	}

}
