package com.prawn.education.controller;

import com.prawn.education.pojo.Information;
import com.prawn.education.service.InformationService;
import com.prawn.education.util.FastDFSUtil;
import entity.PageResult;
import entity.Result;
import entity.StatusCode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

/**
 * 控制器层
 *
 * @author Administrator
 */
@RestController
@CrossOrigin
@RequestMapping("/education")
@Api(value = "远程教育接口", tags = "远程教育接口")
public class EducationController {

    @Autowired
    private InformationService informationService;

    @Autowired
    private FastDFSUtil fastDFSUtil;

    @Value("${fdfs.web-server-url}")
    private String fastdfsUrl;

    /**
     * 增加
     *
     * @param information
     */
    @ApiOperation(value = "增加远程教育" , notes = " \"pic\" 和 \"contentUrl\" 的数据: 调用公共类的upload接口上传图片/文件，返回的url填入此字段。")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "information",value = "文件对象",dataType = "Course")
    })
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result add(@RequestBody Information information) {
        information.setModule(1);
        informationService.add(information);
        return new Result(true, StatusCode.OK, "增加成功");
    }

    /**
     * 修改
     *
     * @param information
     */
    @ApiOperation(value = "修改远程教育 - 使用绕过网关的接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "information",value = "文件对象",dataType = "Course"),
            @ApiImplicitParam(name = "id",value = "文件id",dataType = "String", paramType = "path")
    })
    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public Result update(@RequestBody Information information, @PathVariable String id) {
        Information oldInformation = informationService.findById(id).get();
        //更新图片
        try {
            if(!oldInformation.getPic().equals(information.getPic())){
                fastDFSUtil.deleteFile(oldInformation.getPic());
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        //更新视频
        try {
            if(!oldInformation.getContentUrl().equals(information.getContentUrl())){
                fastDFSUtil.deleteFile(oldInformation.getContentUrl());
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        information.setId(id);
        information.setModule(1);
        informationService.update(information);
        return new Result(true, StatusCode.OK, "修改成功");
    }

    /**
     * 查询全部数据
     * @return
     */
    @ApiOperation(value = "查询所有远程教育")
    @RequestMapping(method= RequestMethod.GET)
    public Result findAll(){
        return new Result(true,StatusCode.OK,"查询成功",informationService.findAll(1));
    }

    /**
     * 查询全部远程教育-分页
     *
     * @return
     */
    @ApiOperation(value = "查询全部远程教育-分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findAll/{page}/{size}", method = RequestMethod.GET)
    public Result findAll(@PathVariable int page, @PathVariable int size) {
        Page<Information> pageList = informationService.findAll(1, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Information>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 按类型编号搜索远程教育
     * @param typeId 类型id
     * @param page 页码
     * @param size 页大小
     * @return 分页结果
     */
    @ApiOperation(value = "按类型编号搜索远程教育")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "typeId", value = "类型编号", paramType = "path", required = true),
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value="/search/searchByTypeId/{typeId}/{page}/{size}",method=RequestMethod.GET)
    public Result findSearch(@PathVariable String typeId , @PathVariable int page, @PathVariable int size){
        Page<Information> pageList = informationService.findByTypeId(1, typeId, page, size);
        return  new Result(true,StatusCode.OK,"查询成功",  new PageResult<Information>(pageList.getTotalElements(), pageList.getContent()) );
    }

    /**
     * 按时间查询远程教育 - 分页
     *
     * @param page
     * @param size
     * @return
     */
    @ApiOperation(value = "按时间查询远程教育 - 分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findByTime/{page}/{size}", method = RequestMethod.GET)
    public Result findByTime(@PathVariable int page, @PathVariable int size) {
        Page<Information> pageList = informationService.findByTime(1, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Information>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 查询推荐远程教育 - 分页
     *
     * @param page
     * @param size
     * @return
     */
    @ApiOperation(value = "查询推荐远程教育 - 分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findByRecommend/{page}/{size}", method = RequestMethod.GET)
    public Result findByRecommend(@PathVariable int page, @PathVariable int size) {
        Page<Information> pageList = informationService.findByRecommend(1, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Information>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 按点击量查询远程教育 - 分页
     *
     * @param page
     * @param size
     * @return
     */
    @ApiOperation(value = "按点击量查询远程教育 - 分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findByClickNum/{page}/{size}", method = RequestMethod.GET)
    public Result findByClickNum(@PathVariable int page, @PathVariable int size) {
        Page<Information> pageList = informationService.findByClickNum(1, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Information>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 按时间查询远程教育
     * @return
     */
    @ApiOperation(value = "按时间查询远程教育")
    @RequestMapping(value = "/findByTime", method = RequestMethod.GET)
    public Result findByTime() {
        return new Result(true, StatusCode.OK, "查询成功", informationService.findByTime(1));
    }

    /**
     * 查询推荐远程教育
     * @return
     */
    @ApiOperation(value = "查询推荐远程教育")
    @RequestMapping(value = "/findByRecommend", method = RequestMethod.GET)
    public Result findByRecommend() {
        return new Result(true, StatusCode.OK, "查询成功", informationService.findByRecommend(1));
    }

    /**
     * 按点击量查询远程教育
     * @return
     */
    @ApiOperation(value = "按点击量查询远程教育")
    @RequestMapping(value = "/findByClickNum", method = RequestMethod.GET)
    public Result findByClickNum() {
        return new Result(true, StatusCode.OK, "查询成功", informationService.findByClickNum(1));
    }

    /**
     * 按类型编号搜索远程教育
     * @param typeId 类型id
     * @param page 页码
     * @param size 页大小
     * @return 分页结果
     */
    @ApiOperation(value = "按类型编号和时间搜索远程教育")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "typeId", value = "类型编号", paramType = "path", required = true),
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value="/findByTypeAndTime/{typeId}/{page}/{size}",method=RequestMethod.GET)
    public Result findByTypeAndTime(@PathVariable String typeId , @PathVariable int page, @PathVariable int size){
        Page<Information> pageList = informationService.findByTypeIdAndTime(1, typeId, page, size);
        return  new Result(true,StatusCode.OK,"查询成功",  new PageResult<Information>(pageList.getTotalElements(), pageList.getContent()) );
    }

    /**
     * 按类型编号和点击量搜索远程教育
     * @param typeId 类型id
     * @param page 页码
     * @param size 页大小
     * @return 分页结果
     */
    @ApiOperation(value = "按类型编号和点击量搜索远程教育")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "typeId", value = "类型编号", paramType = "path", required = true),
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value="/findByTypeAndClick/{typeId}/{page}/{size}",method=RequestMethod.GET)
    public Result findByTypeAndClick(@PathVariable String typeId , @PathVariable int page, @PathVariable int size){
        Page<Information> pageList = informationService.findByTypeIdAndClick(1, typeId, page, size);
        return  new Result(true,StatusCode.OK,"查询成功",  new PageResult<Information>(pageList.getTotalElements(), pageList.getContent()) );
    }

    /**
     * 查询已删除远程教育-分页
     *
     * @return
     */
    @ApiOperation(value = "查询已删除远程教育-分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findByDelete/{page}/{size}", method = RequestMethod.GET)
    public Result findByDelete(@PathVariable int page, @PathVariable int size) {
        Page<Information> pageList = informationService.findByDelete(1, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Information>(pageList.getTotalElements(), pageList.getContent()));
    }

}
