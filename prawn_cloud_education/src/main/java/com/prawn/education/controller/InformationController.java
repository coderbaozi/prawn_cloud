package com.prawn.education.controller;

import java.io.IOException;

import com.prawn.education.util.FastDFSUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import com.prawn.education.pojo.Information;
import com.prawn.education.service.InformationService;

import entity.Result;
import entity.StatusCode;
import org.springframework.web.multipart.MultipartFile;

/**
 * 控制器层
 *
 * @author Administrator
 */
@RestController
@CrossOrigin
@RequestMapping("/file")
@Api(value = "文件类公共接口", tags = "文件类公共接口")
public class InformationController {

    @Autowired
    private InformationService informationService;

    @Autowired
    private FastDFSUtil fastDFSUtil;

    @Value("${fdfs.web-server-url}")
    private String fastdfsUrl;

    /**
     * 图片转换成url
     *
     * @param file
     * @return
     * @throws IOException
     */
    @ApiOperation(value = "上传文件，返回url - 使用绕过网关的路径")
    @RequestMapping(value = "/upload", method = RequestMethod.POST, headers = "content-type=multipart/form-data")
    public Result upload(@RequestParam("file") MultipartFile file) throws IOException {
        String url = fastDFSUtil.uploadFile(file);
        return new Result(true, StatusCode.OK, "上传成功", url);
    }

    /**
     * 删除图片 - 使用绕过网关的路径
     * @param delUrl
     * @return
     */
    @ApiOperation(value = "删除文件 - 使用绕过网关的路径")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "delUrl", value = "图片路径", dataType = "String" , paramType = "query")
    })
    @RequestMapping(value = "/delPic", method = RequestMethod.DELETE)
    public Result delPic(@RequestParam("delUrl") String delUrl) {
        fastDFSUtil.deleteFile(delUrl);
        return new Result(true,StatusCode.OK,"删除成功");
    }

    /**
     * 删除
     *
     * @param id
     */
    @ApiOperation(value = "删除文件")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id",value = "文件id",dataType = "String", paramType = "path")
    })
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
    public Result delete(@PathVariable String id) {
        informationService.deleteById(id); //逻辑删除
        return new Result(true, StatusCode.OK, "删除成功");
    }

    /**
     * 根据ID查询
     * @param id ID
     * @return
     */
    @ApiOperation(value = "根据ID查询 - 后台")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "文件id", paramType = "path", dataType = "String", required = true)
    })
    @RequestMapping(value="/{id}",method= RequestMethod.GET)
    public Result findById(@PathVariable String id){
        return new Result(true,StatusCode.OK,"查询成功",informationService.findById(id).get());
    }

    /**
     * 根据ID查询
     * @param id ID
     * @return
     */
    @ApiOperation(value = "根据ID查询 - 前台")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "文件id", paramType = "path", dataType = "String", required = true)
    })
    @RequestMapping(value="/{id}/{ip}",method= RequestMethod.GET)
    public Result findById(@PathVariable String id, @PathVariable String ip){
        return new Result(true,StatusCode.OK,"查询成功",informationService.findById(id,ip).get());
    }

    /**
     * 根据已删除ID查询
     * @param id ID
     * @return
     */
    @ApiOperation(value = "根据已删除ID查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "文件id", paramType = "path", dataType = "String", required = true)
    })
    @RequestMapping(value="/findByDeleteId/{id}",method= RequestMethod.GET)
    public Result findByDeleteId(@PathVariable String id){
        return new Result(true,StatusCode.OK,"查询成功",informationService.findByDeleteId(id));
    }

    /**
     * 管理员设置推荐
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "管理员设置推荐")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "文件id", paramType = "path", dataType = "String", required = true)
    })
    @RequestMapping(value = "/recommend/{id}", method = RequestMethod.PUT)
    public Result recommend(@PathVariable String id) {
        Information information = informationService.findById(id).get();
        information.setRecommend(true);
        informationService.update(information);
        return new Result(true, StatusCode.OK, "设置成功");
    }

    /**
     * 管理员取消推荐
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "管理员取消推荐")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "文件id", paramType = "path", dataType = "String", required = true)
    })
    @RequestMapping(value = "/cancelRecommend/{id}", method = RequestMethod.PUT)
    public Result cancelRecommend(@PathVariable String id) {
        Information information = informationService.findById(id).get();
        information.setRecommend(false);
        informationService.update(information);
        return new Result(true, StatusCode.OK, "设置成功");
    }

}
