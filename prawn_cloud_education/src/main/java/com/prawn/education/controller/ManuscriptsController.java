package com.prawn.education.controller;

import com.prawn.education.pojo.Information;
import com.prawn.education.service.InformationService;
import com.prawn.education.util.FastDFSUtil;
import entity.PageResult;
import entity.Result;
import entity.StatusCode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

/**
 * 控制器层
 *
 * @author Administrator
 */
@RestController
@CrossOrigin
@RequestMapping("/manuscripts")
@Api(value = "演示文稿接口", tags = "演示文稿接口")
public class ManuscriptsController {

    @Autowired
    private InformationService informationService;

    @Autowired
    private FastDFSUtil fastDFSUtil;

    @Value("${fdfs.web-server-url}")
    private String fastdfsUrl;

    /**
     * 增加
     *
     * @param information
     */
    @ApiOperation(value = "增加演示文稿", notes = " \"pic\" 和 \"contentUrl\" 的数据: 调用公共类的upload接口上传图片/文件，返回的url填入此字段。")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "information",value = "文件对象",dataType = "Course")
    })
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result add(@RequestBody Information information) {
        information.setModule(2);
        informationService.add(information);
        return new Result(true, StatusCode.OK, "增加成功");
    }

    /**
     * 修改
     *
     * @param information
     */
    @ApiOperation(value = "修改演示文稿")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "information",value = "文件对象",dataType = "Course"),
            @ApiImplicitParam(name = "id",value = "文件id",dataType = "String", paramType = "path")
    })
    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public Result update(@RequestBody Information information, @PathVariable String id) {
        Information oldInformation = informationService.findById(id).get();
        //更新图片
        try {
            if(!oldInformation.getPic().equals(information.getPic())){
                fastDFSUtil.deleteFile(oldInformation.getPic());
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        //更新视频
        try {
            if(!oldInformation.getContentUrl().equals(information.getContentUrl())){
                fastDFSUtil.deleteFile(oldInformation.getContentUrl());
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        information.setId(id);
        information.setModule(2);
        informationService.update(information);
        return new Result(true, StatusCode.OK, "修改成功");
    }

    /**
     * 查询全部数据
     * @return
     */
    @ApiOperation(value = "查询所有演示文稿")
    @RequestMapping(method= RequestMethod.GET)
    public Result findAll(){
        return new Result(true,StatusCode.OK,"查询成功",informationService.findAll(2));
    }

    /**
     * 查询全部演示文稿-分页
     *
     * @return
     */
    @ApiOperation(value = "查询全部演示文稿-分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findAll/{page}/{size}", method = RequestMethod.GET)
    public Result findAll(@PathVariable int page, @PathVariable int size) {
        Page<Information> pageList = informationService.findAll(2, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Information>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 按类型编号搜索演示文稿
     * @param typeId 类型id
     * @param page 页码
     * @param size 页大小
     * @return 分页结果
     */
    @ApiOperation(value = "按类型编号搜索演示文稿")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "typeId", value = "类型编号", paramType = "path", required = true),
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value="/search/searchByTypeId/{typeId}/{page}/{size}",method=RequestMethod.GET)
    public Result findSearch(@PathVariable String typeId , @PathVariable int page, @PathVariable int size){
        Page<Information> pageList = informationService.findByTypeId(2, typeId, page, size);
        return  new Result(true,StatusCode.OK,"查询成功",  new PageResult<Information>(pageList.getTotalElements(), pageList.getContent()) );
    }

    /**
     * 按时间查询演示文稿 - 分页
     *
     * @param page
     * @param size
     * @return
     */
    @ApiOperation(value = "按时间查询演示文稿 - 分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findByTime/{page}/{size}", method = RequestMethod.GET)
    public Result findByTime(@PathVariable int page, @PathVariable int size) {
        Page<Information> pageList = informationService.findByTime(2, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Information>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 查询推荐演示文稿 - 分页
     *
     * @param page
     * @param size
     * @return
     */
    @ApiOperation(value = "查询推荐演示文稿 - 分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findByRecommend/{page}/{size}", method = RequestMethod.GET)
    public Result findByRecommend(@PathVariable int page, @PathVariable int size) {
        Page<Information> pageList = informationService.findByRecommend(2, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Information>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 按点击量查询演示文稿 - 分页
     *
     * @param page
     * @param size
     * @return
     */
    @ApiOperation(value = "按点击量查询演示文稿 - 分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findByClickNum/{page}/{size}", method = RequestMethod.GET)
    public Result findByClickNum(@PathVariable int page, @PathVariable int size) {
        Page<Information> pageList = informationService.findByClickNum(2, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Information>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 按时间查询演示文稿
     * @return
     */
    @ApiOperation(value = "按时间查询演示文稿")
    @RequestMapping(value = "/findByTime", method = RequestMethod.GET)
    public Result findByTime() {
        return new Result(true, StatusCode.OK, "查询成功", informationService.findByTime(2));
    }

    /**
     * 查询推荐演示文稿
     * @return
     */
    @ApiOperation(value = "查询推荐演示文稿")
    @RequestMapping(value = "/findByRecommend", method = RequestMethod.GET)
    public Result findByRecommend() {
        return new Result(true, StatusCode.OK, "查询成功", informationService.findByRecommend(2));
    }

    /**
     * 按点击量查询演示文稿
     * @return
     */
    @ApiOperation(value = "按点击量查询演示文稿")
    @RequestMapping(value = "/findByClickNum", method = RequestMethod.GET)
    public Result findByClickNum() {
        return new Result(true, StatusCode.OK, "查询成功", informationService.findByClickNum(2));
    }

    /**
     * 查询已删除-分页
     *
     * @return
     */
    @ApiOperation(value = "查询已删除演示文稿-分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findByDelete/{page}/{size}", method = RequestMethod.GET)
    public Result findByDelete(@PathVariable int page, @PathVariable int size) {
        Page<Information> pageList = informationService.findByDelete(2, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Information>(pageList.getTotalElements(), pageList.getContent()));
    }

}
