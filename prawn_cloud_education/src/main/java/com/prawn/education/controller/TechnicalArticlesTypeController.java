package com.prawn.education.controller;

import com.prawn.education.pojo.Type;
import com.prawn.education.service.TypeService;
import entity.PageResult;
import entity.Result;
import entity.StatusCode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * 控制器层
 * @author Administrator
 *
 */
@RestController
@CrossOrigin
@RequestMapping("/technicalArticlesTypes")
@Api(value = "渔技文章类型接口", tags = "渔技文章类型接口")
public class TechnicalArticlesTypeController {


	@Autowired
	private TypeService typeService;
	
	/**
	 * 增加
	 * @param type
	 */
	@ApiOperation(value = "增加渔技文章类型")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "type",value = "文件类型对象",dataType = "CourseTypes")
	})
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public Result add(@RequestBody Type type  ){
		type.setModule(0);
		typeService.add(type);
		return new Result(true,StatusCode.OK,"增加成功");
	}
	
	/**
	 * 修改
	 * @param type
	 */
	@ApiOperation(value = "修改渔技文章")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "type",value = "文件类型对象",dataType = "CourseTypes"),
			@ApiImplicitParam(name = "id",value = "文件类型id",dataType = "String", paramType = "path")
	})
	@RequestMapping(value="/update/{id}",method= RequestMethod.PUT)
	public Result update(@RequestBody Type type, @PathVariable String id ){
		type.setModule(0);
		type.setId(id);
		typeService.update(type);		
		return new Result(true,StatusCode.OK,"修改成功");
	}

	@ApiOperation(value = "查询所有渔技文章类型")
	@RequestMapping(method = RequestMethod.GET)
	public Result findAll() {
		List<Type> typesList = typeService.findAll(0);
		return new Result(true, StatusCode.OK, "查询成功", typesList);
	}

	/**
	 * 查询全部数据-分页
	 * @param page 页码
	 * @param size 页大小
	 * @return 分页结果
	 */
	@ApiOperation(value = "查询全部渔技文章类型-分页")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
			@ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
	})
	@RequestMapping(value="/{page}/{size}",method=RequestMethod.POST)
	public Result findAll( @PathVariable int page, @PathVariable int size){
		Page<Type> pageList = typeService.findAll(0, page, size);
		return  new Result(true,StatusCode.OK,"查询成功",  new PageResult<Type>(pageList.getTotalElements(), pageList.getContent()) );
	}

	
}
