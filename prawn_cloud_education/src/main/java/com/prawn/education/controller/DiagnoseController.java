package com.prawn.education.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.prawn.education.util.FastDFSUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import com.prawn.education.pojo.Diagnose;
import com.prawn.education.service.DiagnoseService;

import entity.PageResult;
import entity.Result;
import entity.StatusCode;
import org.springframework.web.multipart.MultipartFile;

/**
 * 控制器层
 *
 * @author Administrator
 */
@RestController
@CrossOrigin
@RequestMapping("/diagnose")
public class DiagnoseController {

    @Autowired
    private DiagnoseService diagnoseService;

    @Autowired
    private FastDFSUtil fastDFSUtil;

    /**
     * 查询全部数据
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public Result findAll() {
        return new Result(true, StatusCode.OK, "查询成功", diagnoseService.findAll());
    }

    /**
     * 根据ID查询
     *
     * @param id ID
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Result findById(@PathVariable String id) {
        return new Result(true, StatusCode.OK, "查询成功", diagnoseService.findById(id));
    }


    /**
     * 分页+多条件查询
     *
     * @param searchMap 查询条件封装
     * @param page      页码
     * @param size      页大小
     * @return 分页结果
     */
    @RequestMapping(value = "/search/{page}/{size}", method = RequestMethod.POST)
    public Result findSearch(@RequestBody Map searchMap, @PathVariable int page, @PathVariable int size) {
        Page<Diagnose> pageList = diagnoseService.findSearch(searchMap, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Diagnose>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 根据条件查询
     *
     * @param searchMap
     * @return
     */
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public Result findSearch(@RequestBody Map searchMap) {
        return new Result(true, StatusCode.OK, "查询成功", diagnoseService.findSearch(searchMap));
    }

    /**
     * 增加
     *
     * @param diagnose
     */
    @RequestMapping(method = RequestMethod.POST)
    public Result add(@RequestParam("file") MultipartFile file, Diagnose diagnose) {
        try {
            String url = fastDFSUtil.uploadFile(file);
			diagnose.setPic(url);
        } catch (IOException e) {
            return new Result(false, StatusCode.ERROR, "文件上传失败");
        }
        diagnoseService.add(diagnose);
        return new Result(true, StatusCode.OK, "增加成功");
    }

    /**
     * 修改
     *
     * @param diagnose
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public Result update(@RequestParam("file") MultipartFile file, Diagnose diagnose, @PathVariable String id) {
    	if(file!=null){
			try {
				String url = fastDFSUtil.uploadFile(file);
				diagnose.setPic(url);
			} catch (IOException e) {
				return new Result(false, StatusCode.ERROR, "文件上传失败");
			}
		}
        diagnose.setId(id);
        diagnoseService.update(diagnose);
        return new Result(true, StatusCode.OK, "修改成功");
    }

    /**
     * 删除
     *
     * @param id
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public Result delete(@PathVariable String id) {
        diagnoseService.deleteById(id);
        return new Result(true, StatusCode.OK, "删除成功");
    }

}
