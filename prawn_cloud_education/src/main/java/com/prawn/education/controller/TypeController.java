package com.prawn.education.controller;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.prawn.education.pojo.Type;
import com.prawn.education.service.TypeService;

import entity.PageResult;
import entity.Result;
import entity.StatusCode;
/**
 * 控制器层
 * @author Administrator
 *
 */
@RestController
@CrossOrigin
@RequestMapping("/fileTypes")
@Api(value = "文件类型类公共接口", tags = "文件类型类公共接口")
public class TypeController {


	@Autowired
	private TypeService typeService;

	@ApiOperation(value = "根据id查询文件类型")
	@ApiImplicitParam(name = "id", value = "类型编号", paramType = "path", required = true)
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Result findById(@PathVariable String id) {
		Optional<Type> educationTypes = typeService.findById(id);
		return new Result(true, StatusCode.OK, "查询成功", educationTypes);
	}
	
}
