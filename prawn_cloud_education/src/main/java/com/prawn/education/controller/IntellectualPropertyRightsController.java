package com.prawn.education.controller;

import com.prawn.education.pojo.IntellectualPropertyRights;
import com.prawn.education.service.IntellectualPropertyRightsService;
import com.prawn.education.util.FastDFSUtil;
import entity.PageResult;
import entity.Result;
import entity.StatusCode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * 控制器层
 *
 * @author Administrator
 */
@RestController
@CrossOrigin
@RequestMapping("/intellectualPropertyRights")
@Api(value = "知识产权接口", tags = "知识产权接口")
public class IntellectualPropertyRightsController {
    @Autowired
    private IntellectualPropertyRightsService intellectualPropertyRightsService;

    @Autowired
    private FastDFSUtil fastDFSUtil;

    @Value("${fdfs.web-server-url}")
    private String fastdfsUrl;

    /**
     * 删除
     *
     * @param id
     */
    @ApiOperation(value = "删除知识产权")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id",value = "知识产权id",dataType = "String", paramType = "path")
    })
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
    public Result delete(@PathVariable String id) {
        IntellectualPropertyRights intellectualPropertyRights = intellectualPropertyRightsService.findById(id);
        intellectualPropertyRightsService.deleteById(id); //逻辑删除
        return new Result(true, StatusCode.OK, "删除成功");
    }

    /**
     * 根据ID查询
     * @param id ID
     * @return
     */
    @ApiOperation(value = "根据ID查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "知识产权id", paramType = "path", dataType = "String", required = true)
    })
    @RequestMapping(value="/{id}",method= RequestMethod.GET)
    public Result findById(@PathVariable String id){
        return new Result(true,StatusCode.OK,"查询成功",intellectualPropertyRightsService.findById(id));
    }

    /**
     * 根据已删除ID查询
     * @param id ID
     * @return
     */
    @ApiOperation(value = "根据已删除ID查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "知识产权id", paramType = "path", dataType = "String", required = true)
    })
    @RequestMapping(value="/findByDeleteId/{id}",method= RequestMethod.GET)
    public Result findByDeleteId(@PathVariable String id){
        return new Result(true,StatusCode.OK,"查询成功",intellectualPropertyRightsService.findByDeleteId(id));
    }

    /**
     * 增加
     * @param intellectualPropertyRights
     * @return
     */
    @ApiOperation(value = "增加知识产权", notes = " \"file\" 的数据: 调用公共类的upload接口上传图片/文件，返回的url填入此字段。")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "intellectualPropertyRights",value = "知识产权对象",dataType = "IntellectualPropertyRights")
    })
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result add(@RequestBody IntellectualPropertyRights intellectualPropertyRights) {
        intellectualPropertyRightsService.add(intellectualPropertyRights);
        return new Result(true, StatusCode.OK, "增加成功");
    }

    /**
     * 修改
     * @param intellectualPropertyRights
     * @param id
     * @return
     */
    @ApiOperation(value = "修改知识产权")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "intellectualPropertyRights",value = "知识产权对象",dataType = "IntellectualPropertyRights"),
            @ApiImplicitParam(name = "id",value = "文件id",dataType = "String", paramType = "path")
    })
    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public Result update(@RequestBody IntellectualPropertyRights intellectualPropertyRights, @PathVariable String id) {
        IntellectualPropertyRights oldIntellectualPropertyRights = intellectualPropertyRightsService.findById(id);
        //更新图片
        try {
            if(!oldIntellectualPropertyRights.getFile().equals(intellectualPropertyRights.getFile())){
                fastDFSUtil.deleteFile(oldIntellectualPropertyRights.getFile());
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        intellectualPropertyRights.setId(id);
        intellectualPropertyRights.setState(true);
        intellectualPropertyRightsService.update(intellectualPropertyRights);
        return new Result(true, StatusCode.OK, "修改成功");
    }

    /**
     * 查询全部数据
     * @return
     */
    @ApiOperation(value = "查询所有知识产权")
    @RequestMapping(method= RequestMethod.GET)
    public Result findAll(){
        return new Result(true,StatusCode.OK,"查询成功",intellectualPropertyRightsService.findAll());
    }

    /**
     * 查询全部知识产权-分页
     * @param page
     * @param size
     * @return
     */
    @ApiOperation(value = "查询全部知识产权-分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findAll/{page}/{size}", method = RequestMethod.GET)
    public Result findAll(@PathVariable int page, @PathVariable int size) {
        Page<IntellectualPropertyRights> pageList = intellectualPropertyRightsService.findAll(page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<IntellectualPropertyRights>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 查询已删除知识产权-分页
     * @param page
     * @param size
     * @return
     */
    @ApiOperation(value = "查询已删除知识产权-分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value = "/findByDelete/{page}/{size}", method = RequestMethod.GET)
    public Result findByDelete(@PathVariable int page, @PathVariable int size){
        Page<IntellectualPropertyRights> pageList = intellectualPropertyRightsService.findByDelete(page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<IntellectualPropertyRights>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 按分类查询
     * @param typeId 类型id
     * @param page 页码
     * @param size 页大小
     * @return 分页结果
     */
    @ApiOperation(value = "按分类查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "typeId", value = "类型编号", paramType = "path", required = true),
            @ApiImplicitParam(name = "page", value = "页码", paramType = "path", required = true),
            @ApiImplicitParam(name = "size", value = "每页数据数量", paramType = "path", required = true)
    })
    @RequestMapping(value="/findByType/{typeId}/{page}/{size}",method=RequestMethod.GET)
    public Result findByType(@PathVariable String typeId , @PathVariable int page, @PathVariable int size){
        Page<IntellectualPropertyRights> pageList = intellectualPropertyRightsService.findByTypeId(typeId, page, size);
        return  new Result(true,StatusCode.OK,"查询成功",  new PageResult<IntellectualPropertyRights>(pageList.getTotalElements(), pageList.getContent()) );
    }

}
