package com.prawn.education.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.prawn.education.pojo.Information;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface InformationDao extends JpaRepository<Information,String>,JpaSpecificationExecutor<Information>{

    public Iterable<Information> findByModuleAndStateTrue(int module);

    public Optional<Information> findByIdAndStateTrue(String id);

    public Optional<Information> findByIdAndStateFalse(String id);

    public Page<Information> findByModuleAndTypeIdAndStateTrue(int module, String typeId, Pageable pageable);

    public Page<Information> findByModuleAndStateTrue(int module, Pageable pageable);

    public Page<Information> findByModuleAndStateFalse(int module, Pageable pageable);

    public Page<Information> findByModuleAndStateTrueOrderByCreateDateDesc(int module, Pageable pageable);

    public Page<Information> findByModuleAndStateTrueAndRecommendTrue(int module, Pageable pageable);

    public Page<Information> findByModuleAndStateTrueOrderByClickNumDesc(int module, Pageable pageable);

    public List<Information> findByModuleAndStateTrueOrderByCreateDateDesc(int module);

    public List<Information> findByModuleAndStateTrueAndRecommendTrue(int module);

    public List<Information> findByModuleAndStateTrueOrderByClickNumDesc(int module);

    public Page<Information> findByModuleAndTypeIdAndStateTrueOrderByCreateDateDesc(int module, String typeId, Pageable pageable);

    public Page<Information> findByModuleAndTypeIdAndStateTrueOrderByClickNumDesc(int module, String typeId, Pageable pageable);

    @Transactional
    @Modifying
    @Query(nativeQuery = true, value = "update course_information set clickNum where id=? ")
    public void updateClickNum(int num, String id);
}
