package com.prawn.education.dao;

import com.prawn.education.pojo.IntellectualPropertyRightsTypes;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
import java.util.Optional;

/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface IntellectualPropertyRightsTypesDao extends JpaRepository<IntellectualPropertyRightsTypes,String>,JpaSpecificationExecutor<IntellectualPropertyRightsTypes>{
    public Optional<IntellectualPropertyRightsTypes> findByIdAndStateTrue(String id);

    public List<IntellectualPropertyRightsTypes> findByStateTrue();

    public Page<IntellectualPropertyRightsTypes> findByStateTrue(Pageable pageable);

    Page<IntellectualPropertyRightsTypes> findByLevelAndStateTrue(int level, Pageable pageable);

    Page<IntellectualPropertyRightsTypes> findByParentIdAndStateTrue(String parentId, Pageable pageRequest);
}
