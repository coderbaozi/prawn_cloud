package com.prawn.education.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.prawn.education.pojo.Diagnose;
/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface DiagnoseDao extends JpaRepository<Diagnose,String>,JpaSpecificationExecutor<Diagnose>{
	
}
