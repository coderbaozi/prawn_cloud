package com.prawn.education.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.prawn.education.pojo.Model;
/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface ModelDao extends JpaRepository<Model,String>,JpaSpecificationExecutor<Model>{
	
}
