package com.prawn.education.dao;

import com.prawn.education.pojo.IntellectualPropertyRights;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
import java.util.Optional;

/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface IntellectualPropertyRightsDao extends JpaRepository<IntellectualPropertyRights,String>,JpaSpecificationExecutor<IntellectualPropertyRights>{

    public Page<IntellectualPropertyRights> findAll(Pageable pageable);

    public List<IntellectualPropertyRights> findByStateTrue();

    public Page<IntellectualPropertyRights> findByStateTrue(Pageable pageable);

    public Page<IntellectualPropertyRights> findByStateFalse(Pageable pageable);

    public IntellectualPropertyRights findByIdAndStateTrue(String id);

    public IntellectualPropertyRights findByIdAndStateFalse(String id);

    Page<IntellectualPropertyRights> findByTypeIdAndStateTrue(String typeId, Pageable pageable);
}
