package com.prawn.education.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.prawn.education.pojo.Type;

import java.util.List;
import java.util.Optional;

/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface TypeDao extends JpaRepository<Type,String>,JpaSpecificationExecutor<Type>{
    public Optional<Type> findByIdAndStateTrue(String id);

    public List<Type> findByStateTrue();

    public List<Type> findByModuleAndStateTrue(int module);

    public Page<Type> findByModuleAndStateTrue(int module, Pageable pageable);

}
