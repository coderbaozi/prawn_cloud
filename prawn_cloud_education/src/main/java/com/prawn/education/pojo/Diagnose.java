package com.prawn.education.pojo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="disease_diagnose")
public class Diagnose implements Serializable{

	@Id
	private String id;//


	
	private String diseaseName;//病名
	private String symptom;//症状
	private String solution;//解决方法
	private String status;//删除状态
	private String pic;//图片

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getDiseaseName() {
		return diseaseName;
	}

	public void setDiseaseName(String diseaseName) {
		this.diseaseName = diseaseName;
	}

	public String getSymptom() {
		return symptom;
	}
	public void setSymptom(String symptom) {
		this.symptom = symptom;
	}

	public String getSolution() {		
		return solution;
	}
	public void setSolution(String solution) {
		this.solution = solution;
	}


	
}
