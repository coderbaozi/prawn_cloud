package com.prawn.education.pojo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="production_model")
public class Model implements Serializable{

	@Id
	private String id;//id


	
	private Integer duration;//时长（min）
	private Integer temperature;//温度
	private Integer yield;//出成率（%）
	private String texture;//口感
	private String color;//颜色
	private String appearance;//外观
	private Integer modeltype;//1 蒸煮时间、温度对71/90熟虾仁产品质量的影响  2速冻时间、温度对蝴蝶虾产品质量的影响

	
	public String getId() {		
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public Integer getDuration() {		
		return duration;
	}
	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Integer getTemperature() {		
		return temperature;
	}
	public void setTemperature(Integer temperature) {
		this.temperature = temperature;
	}

	public Integer getYield() {		
		return yield;
	}
	public void setYield(Integer yield) {
		this.yield = yield;
	}

	public String getTexture() {		
		return texture;
	}
	public void setTexture(String texture) {
		this.texture = texture;
	}

	public String getColor() {		
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}

	public String getAppearance() {		
		return appearance;
	}
	public void setAppearance(String appearance) {
		this.appearance = appearance;
	}

	public Integer getModeltype() {
		return modeltype;
	}

	public void setModeltype(Integer modeltype) {
		this.modeltype = modeltype;
	}
}
