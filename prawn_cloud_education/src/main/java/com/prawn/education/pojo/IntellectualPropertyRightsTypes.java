package com.prawn.education.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="intellectual_property_rights_types")
@ApiModel(value="IntellectualPropertyRightsTypes", description = "知识产权类型对象")
public class IntellectualPropertyRightsTypes implements Serializable{

	@Id
	@ApiModelProperty(value = "类型id" , hidden = true)
	private String id;

	@ApiModelProperty(value = "类型名")
	private String name;
	@ApiModelProperty(value = "父级分类")
	private String parentId;//父级分类
	@ApiModelProperty(value = "状态（false表示删除）", hidden = true)
	private boolean state;//状态（false：删除）
	@ApiModelProperty(value = "级别")
	private int level; //级别

	
	public String getId() {		
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getName() {		
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getParentId() {		
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}
}
