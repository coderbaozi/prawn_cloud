package com.prawn.education.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="intellectual_property_rights")
@ApiModel(value="IntellectualPropertyRights", description = "知识产权对象")
public class IntellectualPropertyRights implements Serializable{

	@Id
	@ApiModelProperty(value = "知识产权id", hidden = true)
	private String id;//id

	@ApiModelProperty(value = "标题")
	private String title;//标题
	@ApiModelProperty(value = "作者")
	private String editor;//作者
	@ApiModelProperty(value = "来源")
	private String source;//来源
	@ApiModelProperty(value = "文件url")
	private String file;//文件url
	@ApiModelProperty(value = "发表时间")
	private java.util.Date createDate;//发表时间
	@ApiModelProperty(value = "状态", hidden = true)
	private boolean state;//状态
	@ApiModelProperty(value = "相关专利发明")
	private String invention;
	@ApiModelProperty(value = "相关标准")
	private String standard;
	@ApiModelProperty(value = "分类id")
	private String typeId;//分类id

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getEditor() {
		return editor;
	}

	public void setEditor(String editor) {
		this.editor = editor;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getInvention() {
		return invention;
	}

	public void setInvention(String invention) {
		this.invention = invention;
	}

	public String getStandard() {
		return standard;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}
}
