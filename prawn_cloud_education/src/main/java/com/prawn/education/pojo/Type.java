package com.prawn.education.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="course_type")
@ApiModel(value="CourseTypes", description = "文件类型对象")
public class Type implements Serializable{

	@Id
	@ApiModelProperty(value = "文件类型id" , hidden = true)
	private String id;


	@ApiModelProperty(value = "文件类型")
	private String name;
	@ApiModelProperty(value = "父级分类（扩展需要时使用）", hidden = true)
	private String parentId;//父级分类（扩展需要时使用）
	@ApiModelProperty(value = "状态（false表示删除）", hidden = true)
	private boolean state;//状态（false：删除）
	@ApiModelProperty(value = "所属模块" , hidden = true)
	private int module; //所属模块(0文本1视频2演示)

	
	public String getId() {		
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getName() {		
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getParentId() {		
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public int getModule() {
		return module;
	}

	public void setModule(int module) {
		this.module = module;
	}
}
