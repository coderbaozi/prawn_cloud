package com.prawn.education.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="course_information")
@ApiModel(value="Course", description = "文件对象")
public class Information implements Serializable{

	@Id
	@ApiModelProperty(value = "文件id")
	private String id;//id

	@ApiModelProperty(value = "标题")
	private String title;//标题
	@ApiModelProperty(value = "分类id")
	private String typeId;//分类id
	@ApiModelProperty(value = "简介")
	private String brief;//简介
	@ApiModelProperty(value = "图片url")
	private String pic;//图片url
	@ApiModelProperty(value = "文件url")
	private String contentUrl;//文件url
	@ApiModelProperty(value = "创建者")
	private String createBy;//创建者
	@ApiModelProperty(value = "创建时间")
	private java.util.Date createDate;//创建时间
	@ApiModelProperty(value = "更新者")
	private String updateBy;//更新者
	@ApiModelProperty(value = "更新时间")
	private java.util.Date updateDate;//更新时间
	@ApiModelProperty(value = "状态（false表示删除）", hidden = true)
	private boolean state;
	@ApiModelProperty(value = "所属模块", hidden = true)
	private int module; //所属模块(0文本1视频2演示)
	@ApiModelProperty(value = "点击量", hidden = true)
	private int clickNum; //点击量
	@ApiModelProperty(value = "推荐", hidden = true)
	private boolean recommend; //推荐
	
	public String getId() {		
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {		
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	public String getTypeId() {		
		return typeId;
	}
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	public String getBrief() {		
		return brief;
	}
	public void setBrief(String brief) {
		this.brief = brief;
	}

	public String getPic() {		
		return pic;
	}
	public void setPic(String pic) {
		this.pic = pic;
	}

	public String getContentUrl() {
		return contentUrl;
	}

	public void setContentUrl(String contentUrl) {
		this.contentUrl = contentUrl;
	}

	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public java.util.Date getCreateDate() {		
		return createDate;
	}
	public void setCreateDate(java.util.Date createDate) {
		this.createDate = createDate;
	}

	public String getUpdateBy() {		
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public java.util.Date getUpdateDate() {		
		return updateDate;
	}
	public void setUpdateDate(java.util.Date updateDate) {
		this.updateDate = updateDate;
	}

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public int getModule() {
		return module;
	}

	public void setModule(int module) {
		this.module = module;
	}

	public int getClickNum() {
		return clickNum;
	}

	public void setClickNum(int clickNum) {
		this.clickNum = clickNum;
	}

	public boolean isRecommend() {
		return recommend;
	}

	public void setRecommend(boolean recommend) {
		this.recommend = recommend;
	}
}
