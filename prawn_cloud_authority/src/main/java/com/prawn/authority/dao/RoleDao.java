package com.prawn.authority.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.prawn.authority.pojo.Role;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * 数据访问接口
 *
 * @author Administrator
 */
public interface RoleDao extends JpaRepository<Role, String>, JpaSpecificationExecutor<Role> {

    Role findRoleById(String roleId);

    @Query(value = "SELECT r.`id`, r.`name`, r.`useable`, r.`remarks`, r.`createDate` FROM authority_role AS r, authority_user_role AS ur WHERE r.id = ur.roleId AND ur.userId = ?", nativeQuery = true)
    List<Role> findByUserId(String userId);

    @Modifying
    @Query(value = "DELETE FROM authority_function_role WHERE roleId = ?;", nativeQuery = true)
    void deleteALLById(String roleId);

    //查找可以给该角色赋予的角色
    @Modifying
    @Query(value = "SELECT r.* FROM authority_role r WHERE r.`id` NOT IN ( SELECT r.`id` FROM authority_role r,authority_user u,authority_user_role ur WHERE r.`id`=ur.`roleId` AND ur.`userId` = u.`userId` AND u.`userId`= ?)", nativeQuery = true)
    List<Role> findOthersByUserId(String userId);

    @Modifying
    @Query(value = "DELETE FROM authority_base_role_function  WHERE baseId = ?1 and baseIdentity = ?2", nativeQuery = true)
    void deleteALLBaseFunctionById(String baseId,Integer baseIdentity);
}
