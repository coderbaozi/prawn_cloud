package com.prawn.authority.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.prawn.authority.pojo.User;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.ArrayList;
import java.util.List;

/**
 * 数据访问接口
 *
 * @author Administrator
 */
public interface UserDao extends JpaRepository<User, String>, JpaSpecificationExecutor<User> {

	//根据loginId查询普通用户和管理员
	User findByLoginId(String loginId);

	//根据userId查询普通用户或者管理员
	User findByUserIdAndIsAdmin(String userId, Integer isAdmin);

	//根据loginId查询普通用户或者管理员
	User findByLoginIdAndIsAdmin(String loginId, Integer isAdmin);

	//根据userId查询用户
	User findByUserId(String UserId);


	//查询全部管理员或者全部用户
	List<User> findAllByIsAdmin(Integer isAdmin);

	//删除普通用户或者删除管理员
	void deleteByUserIdAndIsAdmin(String userId, Integer isAdmin);

	//给用户添加角色
	@Modifying
	@Query(value = "INSERT INTO authority_user_role VALUES(?1,?2);", nativeQuery = true)
	void addRoleToUser(String userId, String roleId);

	@Modifying
	@Query(value = "DELETE FROM authority_user_role WHERE userId = ?;", nativeQuery = true)
	void deleteALLById(String userId);

	User findByEmail(String email);

	//上传用户头像
	@Modifying
	@Query(value = "INSERT INTO authority_user_photo VALUES(?1,?2) ", nativeQuery = true)
	void addPhoto(String userId, String photoUrl);

	//删除用户头像(置空)
	@Modifying
	@Query(value = "UPDATE authority_user_photo SET photoUrl = NULL WHERE userId = ?1 ", nativeQuery = true)
	void delPhoto(String userId);

	//删除用户头像
	@Modifying
	@Query(value = "delete from authority_user_photo WHERE userId = ?1 ", nativeQuery = true)
	void delUserAndPhoto(String userId);


	//修改用户头像
	@Modifying
	@Query(value = "UPDATE authority_user_photo set photoUrl = ?2 where userId = ?1  ;", nativeQuery = true)
	void updatePhoto(String userId, String photoUrl);

	//获取头像
	@Query(value = "SELECT photoUrl FROM authority_user_photo WHERE userId =  ?1  ;", nativeQuery = true)
	String getPhoto(String userId);

	//查询基地信息
	@Query(value = "SELECT baseId,baseIdentity FROM authority_user WHERE userId =  ?1  ;", nativeQuery = true)
	String getBase(String userId);

	//查询商店id
	@Query(value = "SELECT merchantId FROM authority_user WHERE userId =  ?1  ;", nativeQuery = true)
	String getMerchant(String userId);

	//修改用户的基地信息
	@Modifying
	@Query(value = "UPDATE authority_user SET baseId = ? , baseIdentity = ? WHERE userId = ? ;", nativeQuery = true)
	void setBase(String baseId, Integer baseIdentity, String userId);

	//当删除基地的时候修改用户的基地信息为空
	@Modifying
	@Query(value = "UPDATE authority_user SET baseId = NULL , baseIdentity = NULL WHERE baseId = ? ", nativeQuery = true)
	void setBaseNull(String baseId);

	//根据基地id查询用户
//	@Modifying
//	@Query(value = "SELECT userId, loginId, name, baseId, baseIdentity, createDate, email, isAdmin, merchantId,mobile,phone,sex FROM authority_user WHERE baseId = ? ",nativeQuery = true)
	ArrayList<User> getUsersByBaseId(String baseId);

	@Modifying
	@Query(value = "UPDATE authority_user SET baseId = NULL , baseIdentity = NULL WHERE userId = ? ", nativeQuery = true)
	void setBaseNullByUserId(String userId);

	// 根据userId、baseId查询用户
	User findByUserIdAndBaseId(String userId, String baseId);

	// 查出所有基地的人员（不包含自身）
	@Modifying
	@Query(value = "SELECT * FROM authority_user WHERE baseId = ?1 AND userId <> ?2 AND baseIdentity >=2 ", nativeQuery = true)
	List<User> findOthersByBaseIdAndUserId(String baseId, String userId);
}
