package com.prawn.authority.Listener;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@RabbitListener(queues = "email")
public class EmailListener {

    @RabbitHandler
    public void sendEmail(Map<String,String> map){
        System.out.println(map.get("email"));
        System.out.println(map.get("checkcode"));
    }
}
