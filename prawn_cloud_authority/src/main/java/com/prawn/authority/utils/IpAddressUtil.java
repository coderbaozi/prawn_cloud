package com.prawn.authority.utils;

import entity.Result;
import entity.StatusCode;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * ip地址工具类
 */
public class IpAddressUtil {
    public static String getIpAdrress(HttpServletRequest request) {
        String ip = request.getRemoteAddr();
//        String ip = request.getHeader("xip");
        if (StringUtils.isEmpty(ip)) {
            throw new GlobalException(Result.error(StatusCode.ERROR, "非法访问"));
        }
        return ip;
    }
}

