package com.prawn.authority.utils;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

/**
 * 响应工具类
 */
public class ResponseUtil {

	/**
	 * 将token放入header中
	 */
	public static void placeToken(HttpServletResponse response, String token) {
		response.setHeader("token", token);
		response.setHeader("Access-Control-Expose-Headers", "token");
	}

	public static void placeToken(HttpServletResponse response, String header, String token) {
		response.setHeader(header, token);
		response.setHeader("Access-Control-Expose-Headers", header);
	}

	/**
	 * 向浏览器输送内容
	 */
	public static void write(HttpServletResponse response, String content) {
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json;charset=utf-8");
		OutputStream outputStream=null;
		try {
			outputStream = response.getOutputStream();
			outputStream.write(content.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (outputStream!=null){
				try {
					outputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
