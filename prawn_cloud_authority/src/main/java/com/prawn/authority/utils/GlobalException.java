package com.prawn.authority.utils;

import entity.Result;
import lombok.Data;

/**
 * 全局异常类
 **/
@Data
public class GlobalException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private Result result;
    public GlobalException(Result result) {
        super(result.getMessage());
        this.result = result;
    }

    public Result getResult() {
        return result;
    }
}
