package com.prawn.authority.utils;

import com.prawn.authority.pojo.UserDetailsImpl;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * 安全环境工具类
 */
public class SecurityContextUtil {

	// 获取安全上下文中用户的信息
	public static UserDetailsImpl getUserDetails(){
		return (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}

	// 将用户认证放入安全上下文中
	public static void setAuthentication(Authentication authentication){
		SecurityContextHolder.getContext().setAuthentication(authentication);
	}

}
