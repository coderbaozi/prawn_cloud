package com.prawn.authority.service;


import com.prawn.authority.dao.UserDao;
import com.prawn.authority.pojo.UserDetailsImpl;
import com.prawn.authority.pojo.User;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

/**
 * SpringSecurity用户的业务实现
 */

@Component
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserDao userDao;

    /**
     * 查询用户信息
     * @Param  loginId 登陆id
     * @Return UserDetails SpringSecurity用户信息
     */
    @Override
    public UserDetailsImpl loadUserByUsername(String loginId) throws UsernameNotFoundException {
        // 查询用户信息
        User user = userDao.findByLoginId(loginId);

        if (user != null){
            // 组装参数
            UserDetailsImpl userDetailsImpl = new UserDetailsImpl();
            BeanUtils.copyProperties(user, userDetailsImpl);
            return userDetailsImpl;
        }
        return null;
    }
}