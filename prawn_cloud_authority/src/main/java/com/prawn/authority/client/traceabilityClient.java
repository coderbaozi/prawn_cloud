package com.prawn.authority.client;

import entity.Base;
import entity.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("prawn-cloud-traceability")
public interface traceabilityClient {

    @RequestMapping(value = "/base",method = RequestMethod.GET)
    public Result getAllBase();

    @RequestMapping(value = "/base/{baseId}",method = RequestMethod.GET)
    public Result getBase(@PathVariable("baseId") String baseId);

    @RequestMapping(value = "/base",method = RequestMethod.POST,consumes = "application/json")
    public Result addBase(Base base);

    @RequestMapping(value = "/base/{baseId}",method = RequestMethod.DELETE)
    public Result delBase(@PathVariable("baseId") String baseId);
}

