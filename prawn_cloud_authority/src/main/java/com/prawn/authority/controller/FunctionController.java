package com.prawn.authority.controller;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.prawn.authority.pojo.Function;
import com.prawn.authority.service.FunctionService;

import entity.PageResult;
import entity.Result;
import entity.StatusCode;
/**
 * 控制器层
 * @author Administrator
 *
 */
@Api(value = "权限Controller",tags = "权限管理模块")
@RestController
@CrossOrigin
@RequestMapping("/function")
public class FunctionController {

	@Autowired
	private FunctionService functionService;

	/**
	 * 查询全部数据
	 * @return
	 */
	@ApiOperation("查找全部权限")
	@PreAuthorize("hasAnyAuthority('authority')")
	@RequestMapping(method= RequestMethod.GET)
	public Result findAll(){
		return new Result(true,StatusCode.OK,"查询成功",functionService.grand(functionService.findAll()));
	}
	
	/**
	 * 根据ID查询
	 * @param id ID
	 * @return
	 */
	@ApiOperation("根据权限Id查找权限")
	@PreAuthorize("hasAnyAuthority('authority')")
	@RequestMapping(value="/{id}",method= RequestMethod.GET)
	public Result findById(@PathVariable String id){
		return new Result(true,StatusCode.OK,"查询成功",functionService.findById(id));
	}


	/**
	 * 分页+多条件查询
	 * @param searchMap 查询条件封装
	 * @param page 页码
	 * @param size 页大小
	 * @return 分页结果
	 */
	@ApiOperation("分页+多条件查询查找权限")
	@PreAuthorize("hasAnyAuthority('authority')")
	@RequestMapping(value="/search/{page}/{size}",method=RequestMethod.POST)
	public Result findSearch(@RequestBody @ApiParam(name = "搜索条件",value = "传入json格式") Map searchMap , @PathVariable int page, @PathVariable int size){
		Page<Function> pageList = functionService.findSearch(searchMap, page, size);
		return  new Result(true,StatusCode.OK,"查询成功",  new PageResult<Function>(pageList.getTotalElements(), pageList.getContent()) );
	}

	/**
     * 根据条件查询
     * @param searchMap
     * @return
     */
	@ApiOperation("多条件查询查找权限")
	@PreAuthorize("hasAnyAuthority('authority')")
    @RequestMapping(value="/search",method = RequestMethod.POST)
    public Result findSearch( @RequestBody @ApiParam(name = "搜索条件",value = "传入json格式") Map searchMap){
        return new Result(true,StatusCode.OK,"查询成功",functionService.findSearch(searchMap));
    }
	
	/**
	 * 更新角色的权限
	 * @param roleId
	 * @param functionIdList
	 * @return
	 */
	@ApiOperation("更新角色的权限")
	@PreAuthorize("hasAnyAuthority('authority_role_updateRoleFunction')")
	@RequestMapping(value="/{roleId}",method = RequestMethod.POST)
	public Result UpdateFunOfRole(@PathVariable String roleId , @RequestBody @ApiParam(name = "权限Id集合Map",value = "传入json格式,{\"functionIdList\": [\"1\", \"3\"]}") Map functionIdList){
		functionService.UpdateFunOfRole(roleId,functionIdList);
		try {
			TimeUnit.MILLISECONDS.sleep(800);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return new Result(true,StatusCode.OK,"更新角色权限成功");
	}

	/**
	 * 查找可授权给角色的权限
	 * @param roleId
	 * @return
	 */
	@ApiOperation("查找可授权给角色的权限")
	@PreAuthorize("hasAnyAuthority('authority')")
	@RequestMapping(value="/findOtherFunction/{roleId}",method = RequestMethod.GET)
	public Result findFunction(@PathVariable String roleId){
		List<Function> functionList = functionService.findOthersByRoleId(roleId);
		return new Result(true,StatusCode.OK,"查找角色权限成功",functionList);
	}

	/**
	 * 根据角色id查询权限
	 * @param roleId
	 * @return
	 */
	@ApiOperation("根据角色id查询权限")
	@PreAuthorize("hasAnyAuthority('authority')")
	@RequestMapping(value="/findFunction/{roleId}",method = RequestMethod.GET)
	public Result findFunctionByRoleId(@PathVariable String roleId){
		List<Function> functionList = functionService.findByRoleId(roleId);
		return new Result(true,StatusCode.OK,"查找角色权限成功",functionService.grand(functionList));
	}

	/**
	 * 根据基地id和基地身份查询基地用户权限
	 * @param baseId
	 * @param baseIdentity
	 * @return
	 */
	@ApiOperation("根据基地id和基地身份查询角色权限")
	@RequestMapping(value="/{baseId}/{baseIdentity}",method = RequestMethod.GET)
	public Result findBaseFunctionByBase(@PathVariable String baseId,@PathVariable Integer baseIdentity){
		List<Function> functionList = functionService.findByBase(baseId,baseIdentity);
		return new Result(true,StatusCode.OK,"查找基地角色权限成功",functionService.grand(functionList));
	}

	/**
	 * 基地老板更新基地角色的权限
	 * @param baseId
	 * @param baseIdentity
	 * @param functionIdList
	 * @return
	 */
	@ApiOperation("基地老板更新基地角色的权限")
	@PreAuthorize("hasAnyAuthority('authority_role_updateRoleFunction')")
	@RequestMapping(value="/base/{baseId}/{baseIdentity}",method = RequestMethod.PUT)
	public Result UpdateFunOfRole(@PathVariable String baseId,@PathVariable Integer baseIdentity, @RequestBody @ApiParam(name = "权限Id集合Map",value = "传入json格式,{\"functionIdList\": [\"1\", \"3\"]}") Map functionIdList){
		functionService.UpdateBaseFunOfRole(baseId,baseIdentity,functionIdList);
		return new Result(true,StatusCode.OK,"更新角色权限成功");
	}
}
