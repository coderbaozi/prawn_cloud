package com.prawn.authority.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.prawn.authority.pojo.vo.BaseRoleVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import com.prawn.authority.pojo.Role;
import com.prawn.authority.service.RoleService;

import entity.PageResult;
import entity.Result;
import entity.StatusCode;
/**
 * 控制器层
 * @author Administrator
 *
 */
@Api(tags = "角色相关接口", description = "提供角色相关的 Rest API")
@RestController
@CrossOrigin
@RequestMapping("/role")
public class RoleController {

	@Autowired
	private RoleService roleService;

	/**
	 * 查询全部数据
	 * @return
	 */
	@ApiOperation("查询所有角色接口")
	@PreAuthorize("hasAnyAuthority('authority','authority_role')")
	@RequestMapping(method= RequestMethod.GET)
	public Result findAll(){
		return new Result(true,StatusCode.OK,"查询成功",roleService.findAll());
	}
	
	/**
	 * 根据ID查询
	 * @param id ID
	 * @return
	 */
	@ApiOperation("根据id查询角色接口")
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "path", name = "id", value = "登录ID", required = true, dataType = "String")
	})
	@PreAuthorize("hasAnyAuthority('authority','authority_role')")
	@RequestMapping(value="/{id}",method= RequestMethod.GET)
	public Result findById(@PathVariable String id){
		return new Result(true,StatusCode.OK,"查询成功", roleService.findById(id));
	}

	/**
	 * 分页+多条件查询
	 * @param searchMap 查询条件封装
	 * @param page 页码
	 * @param size 页大小
	 * @return 分页结果
	 */
	@ApiOperation("分页加条件查询接口")
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "path", name = "page", value = "页面", dataType = "Integer"),
			@ApiImplicitParam(paramType = "path", name = "size", value = "尺寸",  dataType = "Integer"),
			@ApiImplicitParam(paramType = "body", name = "searchMap", value = "查询条件", required = true, dataType = "Role")
	})
	@PreAuthorize("hasAnyAuthority('authority','authority_role')")
	@RequestMapping(value="/search/{page}/{size}",method=RequestMethod.POST)
	public Result findSearch(@RequestBody Map searchMap , @PathVariable int page, @PathVariable int size){
		Page<Role> pageList = roleService.findSearch(searchMap, page, size);
		return  new Result(true,StatusCode.OK,"查询成功",  new PageResult<Role>(pageList.getTotalElements(), pageList.getContent()) );
	}

	/**
     * 根据条件查询
     * @param searchMap
     * @return
     */
	@ApiOperation("条件查询接口")
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "body", name = "searchMap", value = "查询条件", required = true, dataType = "Role")
	})
	@PreAuthorize("hasAnyAuthority('authority','authority_role')")
    @RequestMapping(value="/search",method = RequestMethod.POST)
    public Result findSearch( @RequestBody Map searchMap){
        return new Result(true,StatusCode.OK,"查询成功",roleService.findSearch(searchMap));
    }
	
	/**
	 * 增加
	 * @param role
	 */
	@ApiOperation("新增角色接口")
	@PreAuthorize("hasAnyAuthority('authority_role_add')")
	@RequestMapping(method=RequestMethod.POST)
	public Result add(@RequestBody Role role){
		roleService.add(role);
		return new Result(true,StatusCode.OK,"增加成功");
	}
	
	/**
	 * 修改
	 * @param role
	 */
	@ApiOperation("修改角色接口")
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "path", name = "id", value = "角色ID", required = true, dataType = "String")
	})
	@PreAuthorize("hasAnyAuthority('authority_role_update')")
	@RequestMapping(value="/{id}",method= RequestMethod.PUT)
	public Result update(@RequestBody Role role, @PathVariable String id ){
		role.setId(id);
		roleService.update(role);		
		return new Result(true,StatusCode.OK,"修改成功");
	}
	
	/**
	 * 删除
	 * @param id
	 */
	@ApiOperation("删除角色接口")
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "path", name = "id", value = "角色ID", required = true, dataType = "String")
	})
	@PreAuthorize("hasAnyAuthority('authority_role_delete')")
	@RequestMapping(value="/{id}",method= RequestMethod.DELETE)
	public Result delete(@PathVariable String id ){
		roleService.deleteById(id);
		return new Result(true,StatusCode.OK,"删除成功");
	}

	/**
	 * 返回基地两个角色的数据（写死）
	 * @return
	 */
	@ApiOperation("基地的两个角色数据")
	@PreAuthorize("hasAnyAuthority('authority_role')")
	@GetMapping("/base")
	public Result getBaseRole(){
		List<BaseRoleVo> baseRoleVoList = Arrays.asList(
				new BaseRoleVo(1, "员工", 4),
				new BaseRoleVo(2, "经理", 3));
		return Result.success("查询成功", baseRoleVoList);
	}
}
