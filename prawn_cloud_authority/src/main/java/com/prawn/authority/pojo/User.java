package com.prawn.authority.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@ApiModel(value = "User", description = "用户实体")
@Entity
@Table(name="authority_user")
public class User implements Serializable{

	@Id
	@ApiModelProperty(hidden = true)
	private String userId;//用户编号

	@ApiModelProperty(hidden = true)
	@NotNull(message = "是否管理员字段不能为空,2是超级管理员,1是管理员，0是普通用户")
	private Integer isAdmin;//是否管理员

	@ApiModelProperty(name = "loginId",value = "用户登录账号",required = true,example = "123456")
	@NotNull(message = "登陆账号不能为空")
	private String loginId;//用户登录账号

	@ApiModelProperty(name = "password",value = "用户登录密码",required = true,example = "123456")
	@NotNull(message = "密码不能为空")
	private String password;//用户登录密码

	@ApiModelProperty(name = "name",value = "姓名",required = false,example = "张三")
	private String name;//姓名

	@ApiModelProperty(name = "email",value = "邮箱",required = true,example = "123@163.com")
	@NotNull(message = "邮箱不能为空")
	private String email;//邮箱

	@ApiModelProperty(name = "phone",value = "电话",required = false,example = "12345678")
	private String phone;//电话

	@ApiModelProperty(name = "mobile",value = "手机",required = false,example = "13513513513")
	private String mobile;//手机

	@ApiModelProperty(name = "sex",value = "性别",required = false,example = "男")
	private String sex;//性别

	@ApiModelProperty(name = "merchantId",value = "店铺Id",required = false,example = "123456")
	private String merchantId;//店铺Id

	@ApiModelProperty(name = "baseId",value = "养殖基地Id",required = false,example = "123456")
	private String baseId;//养殖基地Id

	@ApiModelProperty(name = "baseIdentity",value = "用户在养殖基地的身份",required = false, example = "1")
	private Integer baseIdentity;//用户在养殖基地的身份

	@ApiModelProperty(hidden = true)
	@JsonFormat(shape= JsonFormat.Shape.STRING,pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private java.util.Date createDate;//创建时间

	public User() {
	}

	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Integer getIsAdmin() {
		return isAdmin;
	}
	public void setIsAdmin(Integer isAdmin) {
		this.isAdmin = isAdmin;
	}

	public String getLoginId() {		
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPassword() {		
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {		
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {		
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {		
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMobile() {		
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getSex() {		
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getBaseId() {
		return baseId;
	}

	public void setBaseId(String baseId) {
		this.baseId = baseId;
	}

	public Integer getBaseIdentity() {
		return baseIdentity;
	}

	public void setBaseIdentity(Integer baseIdentity) {
		this.baseIdentity = baseIdentity;
	}

	public java.util.Date getCreateDate() {		
		return createDate;
	}

	public void setCreateDate(java.util.Date createDate) {
		this.createDate = createDate;
	}

}
