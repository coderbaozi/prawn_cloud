package com.prawn.authority.pojo.vo;

public class BaseRoleVo {
    private Integer id; // 编号
    private String name; // 角色名称
    private Integer baseIdentity; // 角色身份ID

    public BaseRoleVo() {
    }

    public BaseRoleVo(Integer id, String name, Integer baseIdentity) {
        this.id = id;
        this.name = name;
        this.baseIdentity = baseIdentity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getBaseIdentity() {
        return baseIdentity;
    }

    public void setBaseIdentity(Integer baseIdentity) {
        this.baseIdentity = baseIdentity;
    }
}