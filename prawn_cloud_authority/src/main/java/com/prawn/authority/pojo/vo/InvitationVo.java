package com.prawn.authority.pojo.vo;

import com.prawn.authority.pojo.User;

public class InvitationVo extends User {
	private String Status; //邀请状态类型ID

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}
}
