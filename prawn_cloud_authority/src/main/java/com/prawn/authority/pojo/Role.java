package com.prawn.authority.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@ApiModel(value = "Role", description = "角色实体")
@Entity
@Table(name="authority_role")
public class Role implements Serializable{

	@Id
	@ApiModelProperty(hidden = true)
	private String id;//id


	@ApiModelProperty(name = "name",value = "角色名字",required = true, example = "user")
	@NotNull(message = "名字不能为空")
	private String name;//角色名字
	@ApiModelProperty(name = "useable",value = "是否可用",required = true, example = "1")
	@NotNull(message = "状态不能为空")
	private Integer useable;//是否可用
	@ApiModelProperty(name = "remarks",value = "备注信息",required = false, example = "备注")
	private String remarks;//备注信息
	@ApiModelProperty(hidden = true)
	@JsonFormat(shape= JsonFormat.Shape.STRING,pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private java.util.Date createDate;//创建时间

	
	public String getId() {		
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public Integer getUseable() {
		return useable;
	}
	public void setUseable(Integer useable) {
		this.useable = useable;
	}

	public String getRemarks() {		
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public java.util.Date getCreateDate() {		
		return createDate;
	}
	public void setCreateDate(java.util.Date createDate) {
		this.createDate = createDate;
	}
	
}
