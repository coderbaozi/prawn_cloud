package com.prawn.authority.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FunctionParent extends Function{
    List<FunctionParent> children = null;

    public FunctionParent(String id,
                          String name,
                          Date createDate,
                          String parentId,
                          Integer sort,
                          String href,
                          String remarks,
                          List<FunctionParent> children) {
        super(id, name, createDate, parentId, sort, href, remarks);
        this.children = children;
    }

    public FunctionParent(){
    }

    public List<FunctionParent> getChildren(){
        return children;
    }

    public void setChildren(List<FunctionParent> children){
        this.children = children;
    }
}
