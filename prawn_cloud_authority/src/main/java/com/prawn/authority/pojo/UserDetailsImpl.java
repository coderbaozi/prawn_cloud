package com.prawn.authority.pojo;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.util.Collection;


/**
 * SpringSecurity用户的实体
 */
@Data
public class UserDetailsImpl extends User implements Serializable, UserDetails {

    private static final long serialVersionUID = 1L;

    /**
     * 用户ID
     */
    private String userId;
    /**
     * 用户名
     */
    private String name;
    /**
     * 密码
     */
    private String password;

    private Integer isAdmin;//是否管理员
    private String loginId;//用户登录账号
    private String email;//邮箱
    private String phone;//电话
    private String mobile;//手机
    private String sex;//性别
    private String merchantId;//店铺Id
    private String baseId;//养殖基地Id
    private Integer baseIdentity;//用户在养殖基地的身份

    /**
     * 用户角色
     */
    private Collection<GrantedAuthority> authorities;
    /**
     * 账户是否过期
     */
    private boolean isAccountNonExpired = true;
    /**
     * 账户是否被锁定
     */
    private boolean isAccountNonLocked = true;
    /**
     * 证书是否过期
     */
    private boolean isCredentialsNonExpired = true;
    /**
     * 账户是否有效
     */
    private boolean isEnabled = true;

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public String getUserId() {
        return userId;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return name;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public String getBaseId() {
        return baseId;
    }

    public Integer getBaseIdentity() {
        return baseIdentity;
    }

    @Override
    public boolean isAccountNonExpired() {
        return isAccountNonExpired;
    }
    @Override
    public boolean isAccountNonLocked() {
        return isAccountNonLocked;
    }
    @Override
    public boolean isCredentialsNonExpired() {
        return isCredentialsNonExpired;
    }
    @Override
    public boolean isEnabled() {
        return isEnabled;
    }

    public void setAuthorities(Collection<GrantedAuthority> authorities) {
        this.authorities = authorities;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setUsername(String name) {
        this.name = name;
    }

    public Integer getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(Integer isAdmin) {
        this.isAdmin = isAdmin;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public void setBaseId(String baseId) {
        this.baseId = baseId;
    }

    public void setBaseIdentity(Integer baseIdentity) {
        this.baseIdentity = baseIdentity;
    }
}