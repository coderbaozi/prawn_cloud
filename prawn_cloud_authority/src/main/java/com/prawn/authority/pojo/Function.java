package com.prawn.authority.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */

@ApiModel(value = "function对象",description = "权限对象")
@Entity
@Table(name="authority_function")
public class Function implements Serializable{

	@ApiModelProperty(value = "权限Id",name = "id")
	@Id
	private String id;//id

	@ApiModelProperty(value = "权限名字",name = "name")
	@NotNull(message = "名字不能为空")
	private String name;//名称

	@JsonFormat(shape= JsonFormat.Shape.STRING,pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private java.util.Date createDate;//创建时间

	@ApiModelProperty(value = "父级编号",name = "parentId")
	private String parentId;//父级编号

	@ApiModelProperty(value = "排序",name = "sort")
	@NotNull(message = "排序不能为空")
	private Integer sort;//排序

	@ApiModelProperty(value = "链接",name = "href")
	private String href;//链接

	@ApiModelProperty(value = "备注信息",name = "remarks")
	private String remarks;//备注信息

	public Function() {
	}

	public Function(String id,
					String name,
					java.util.Date createDate,
					String parentId,
					Integer sort,
					String href,
					String remarks) {
		this.id = id;
		this.name = name;
		this.createDate = createDate;
		this.parentId = parentId;
		this.sort = sort;
		this.href = href;
		this.remarks = remarks;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getName() {		
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public java.util.Date getCreateDate() {		
		return createDate;
	}
	public void setCreateDate(java.util.Date createDate) {
		this.createDate = createDate;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
}
