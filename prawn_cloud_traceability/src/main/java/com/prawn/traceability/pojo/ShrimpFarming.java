package com.prawn.traceability.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 实体类
 *
 * @author Administrator
 */
@Entity
@Table(name = "trac_shrimp_farming")
public class ShrimpFarming implements Serializable {

    @Id
    private String id;//id


    private String shrimpId;//虾苗编号
    private String suppliesName;//农资名称
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private java.util.Date applyTime;//使用时间
    private String pondId;//池塘编号
    private String applicator;//使用人
    private String applyType;//操作类别
    private Double deliveryVolume;//投放量
    private String suppliesInfoId;//农资来源信息Id
    private String remark;//备注

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSuppliesInfoId() {
        return suppliesInfoId;
    }

    public void setSuppliesInfoId(String suppliesInfoId) {
        this.suppliesInfoId = suppliesInfoId;
    }

    public Double getDeliveryVolume() {
        return deliveryVolume;
    }

    public void setDeliveryVolume(Double deliveryVolume) {
        this.deliveryVolume = deliveryVolume;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShrimpId() {
        return shrimpId;
    }

    public void setShrimpId(String shrimpId) {
        this.shrimpId = shrimpId;
    }

    public String getSuppliesName() {
        return suppliesName;
    }

    public void setSuppliesName(String suppliesName) {
        this.suppliesName = suppliesName;
    }

    public java.util.Date getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(java.util.Date applyTime) {
        this.applyTime = applyTime;
    }

    public String getPondId() {
        return pondId;
    }

    public void setPondId(String pondId) {
        this.pondId = pondId;
    }

    public String getApplicator() {
        return applicator;
    }

    public void setApplicator(String applicator) {
        this.applicator = applicator;
    }

    public String getApplyType() {
        return applyType;
    }

    public void setApplyType(String applyType) {
        this.applyType = applyType;
    }


}
