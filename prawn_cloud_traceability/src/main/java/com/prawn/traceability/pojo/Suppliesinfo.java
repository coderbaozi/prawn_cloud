package com.prawn.traceability.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="trac_suppliesInfo")
public class Suppliesinfo implements Serializable{

	@Id
	private String id;//id


	
	private String suppliesName;//农资名称
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date productDate;//生产日期
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date shelfDate;//保质期
	private String inspector;//检验人
	private String supplierName;//供应商名称
	private String supplierAddr;//供应商地址
	private String supplierPhone;//供应商电话
	private String supplierLicense;//供应商生产许可证
	private String suppliesPic;//农资照片
	private String baseId;//基地id
	private String type;//投入品二级类型id
	private String ingredient;//农资成分
	private String specification;//规格(mm)

	public String getSpecification() {
		return specification;
	}

	public void setSpecification(String specification) {
		this.specification = specification;
	}

	public String getIngredient() {
		return ingredient;
	}

	public void setIngredient(String ingredient) {
		this.ingredient = ingredient;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getSuppliesName() {		
		return suppliesName;
	}
	public void setSuppliesName(String suppliesName) {
		this.suppliesName = suppliesName;
	}

	public java.util.Date getProductDate() {		
		return productDate;
	}
	public void setProductDate(java.util.Date productDate) {
		this.productDate = productDate;
	}

	public java.util.Date getShelfDate() {		
		return shelfDate;
	}
	public void setShelfDate(java.util.Date shelfDate) {
		this.shelfDate = shelfDate;
	}

	public String getInspector() {		
		return inspector;
	}
	public void setInspector(String inspector) {
		this.inspector = inspector;
	}

	public String getSupplierName() {		
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getSupplierAddr() {		
		return supplierAddr;
	}
	public void setSupplierAddr(String supplierAddr) {
		this.supplierAddr = supplierAddr;
	}

	public String getSupplierPhone() {		
		return supplierPhone;
	}
	public void setSupplierPhone(String supplierPhone) {
		this.supplierPhone = supplierPhone;
	}

	public String getSupplierLicense() {		
		return supplierLicense;
	}
	public void setSupplierLicense(String supplierLicense) {
		this.supplierLicense = supplierLicense;
	}

	public String getSuppliesPic() {		
		return suppliesPic;
	}
	public void setSuppliesPic(String suppliesPic) {
		this.suppliesPic = suppliesPic;
	}

	public String getBaseId() {		
		return baseId;
	}
	public void setBaseId(String baseId) {
		this.baseId = baseId;
	}


	
}
