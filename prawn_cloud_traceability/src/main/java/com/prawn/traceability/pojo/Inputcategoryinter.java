package com.prawn.traceability.pojo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="trac_inputCategoryInter")
public class Inputcategoryinter implements Serializable{

	@Id
	private String id;//ID


	
	private String typeTwo;//二级类型
	private String firstLevelId;//一级分类id

	
	public String getId() {		
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getTypeTwo() {		
		return typeTwo;
	}
	public void setTypeTwo(String typeTwo) {
		this.typeTwo = typeTwo;
	}

	public String getFirstLevelId() {		
		return firstLevelId;
	}
	public void setFirstLevelId(String firstLevelId) {
		this.firstLevelId = firstLevelId;
	}


	
}
