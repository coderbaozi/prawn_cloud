package com.prawn.traceability.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * 实体类
 *
 * @author Administrator
 */
@Entity
@Table(name = "trac_adult_shrimp")
public class AdultShrimp implements Serializable {

    @Id
    private String id;//成虾编号


    private double adultShrimpWeight;//成虾重量
    private String shrimpId;//虾苗编号
    private String baseId;//基地编号
    private String plantId;//加工厂id
    private String storageId;//冷库id
    private String qualificationId;//资质id

//    @ManyToOne(targetEntity = Shrimp.class,fetch = FetchType.LAZY)
//    @JoinColumn(name = "shrimpId", referencedColumnName = "id", insertable = false, updatable = false)
//    private Shrimp shrimp;
//
//    @OneToMany(mappedBy = "adultShrimp",fetch = FetchType.LAZY)
//    private Set<Logistics> logistics = new HashSet<>();


    public String getQualificationId() {
        return qualificationId;
    }

    public void setQualificationId(String qualificationId) {
        this.qualificationId = qualificationId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getAdultShrimpWeight() {
        return adultShrimpWeight;
    }

    public void setAdultShrimpWeight(double adultShrimpWeight) {
        this.adultShrimpWeight = adultShrimpWeight;
    }

    public String getShrimpId() {
        return shrimpId;
    }

    public void setShrimpId(String shrimpId) {
        this.shrimpId = shrimpId;
    }

    public String getBaseId() {
        return baseId;
    }

    public void setBaseId(String baseId) {
        this.baseId = baseId;
    }

    public String getPlantId() {
        return plantId;
    }

    public void setPlantId(String plantId) {
        this.plantId = plantId;
    }

    public String getStorageId() {
        return storageId;
    }

    public void setStorageId(String storageId) {
        this.storageId = storageId;
    }
}
