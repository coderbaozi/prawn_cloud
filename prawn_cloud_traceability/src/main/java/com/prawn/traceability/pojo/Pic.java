package com.prawn.traceability.pojo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="trac_pic")
public class Pic implements Serializable{

	@Id
	private String id;//ID


	
	private String pictures;//图片
	private String associationId;//关联id
	private String picName;//图片名称

	public String getPicName() {
		return picName;
	}

	public void setPicName(String picName) {
		this.picName = picName;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getPictures() {		
		return pictures;
	}
	public void setPictures(String pictures) {
		this.pictures = pictures;
	}

	public String getAssociationId() {		
		return associationId;
	}
	public void setAssociationId(String associationId) {
		this.associationId = associationId;
	}


	
}
