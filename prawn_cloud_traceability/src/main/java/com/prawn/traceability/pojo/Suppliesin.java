package com.prawn.traceability.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="trac_suppliesIn")
public class Suppliesin implements Serializable{

	@Id
	private String id;//农资编号


	
	private String suppliesName;//农资名称
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date inTime;//入库日期
	private String inOperator;//操作人
	private String warehouse;//仓库号
	private Double inNumber;//入库数量(kg)
	private Double remainNumber;//剩余数量
	private String baseId;//基地编号
	private String type;//农资类型(1渔药 2饲料 3渔具)

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Double getRemainNumber() {
		return remainNumber;
	}

	public void setRemainNumber(Double remainNumber) {
		this.remainNumber = remainNumber;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getSuppliesName() {		
		return suppliesName;
	}
	public void setSuppliesName(String suppliesName) {
		this.suppliesName = suppliesName;
	}

	public java.util.Date getInTime() {		
		return inTime;
	}
	public void setInTime(java.util.Date inTime) {
		this.inTime = inTime;
	}

	public String getInOperator() {		
		return inOperator;
	}
	public void setInOperator(String inOperator) {
		this.inOperator = inOperator;
	}

	public String getWarehouse() {		
		return warehouse;
	}
	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}

	public Double getInNumber() {		
		return inNumber;
	}
	public void setInNumber(Double inNumber) {
		this.inNumber = inNumber;
	}

	public String getBaseId() {		
		return baseId;
	}
	public void setBaseId(String baseId) {
		this.baseId = baseId;
	}


	
}
