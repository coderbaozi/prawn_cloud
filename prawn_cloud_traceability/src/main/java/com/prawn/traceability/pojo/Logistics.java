package com.prawn.traceability.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 实体类
 *
 * @author Administrator
 */
@Entity
@Table(name = "trac_logistics")
public class Logistics implements Serializable {

    @Id
    private String id;//表id


    private String departureAddr;//出发地址
    private String logisticsStartLongitude;//出发地址经度
    private String logisticsStartLatitude;//出发地址纬度
    private String arrivalAddr;//抵达地址
    private String logisticsArrivalLongitude;//抵达地址经度
    private String logisticsArrivalLatitude;//抵达地址纬度
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private java.util.Date departureTime;//出发时间
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private java.util.Date arrivalTime;//抵达时间
    private String adultShrimpId;//成虾编号
    private String createBy;//创建者
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private java.util.Date createDate;//创建时间
    private String logisticsStatus;//物流状态 0（否）1（是）
    private String orderId;//订单id
    private String baseId;//基地id

    public String getBaseId() {
        return baseId;
    }

    public void setBaseId(String baseId) {
        this.baseId = baseId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    //    @ManyToOne(targetEntity = AdultShrimp.class,fetch = FetchType.LAZY)
//    @JoinColumn(name = "adultShrimpId", referencedColumnName = "id", insertable = false, updatable = false)
//    private AdultShrimp adultShrimp;


    public String getLogisticsStatus() {
        return logisticsStatus;
    }

    public void setLogisticsStatus(String logisticsStatus) {
        this.logisticsStatus = logisticsStatus;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDepartureAddr() {
        return departureAddr;
    }

    public void setDepartureAddr(String departureAddr) {
        this.departureAddr = departureAddr;
    }

    public String getLogisticsStartLongitude() {
        return logisticsStartLongitude;
    }

    public void setLogisticsStartLongitude(String logisticsStartLongitude) {
        this.logisticsStartLongitude = logisticsStartLongitude;
    }

    public String getLogisticsStartLatitude() {
        return logisticsStartLatitude;
    }

    public void setLogisticsStartLatitude(String logisticsStartLatitude) {
        this.logisticsStartLatitude = logisticsStartLatitude;
    }

    public String getArrivalAddr() {
        return arrivalAddr;
    }

    public void setArrivalAddr(String arrivalAddr) {
        this.arrivalAddr = arrivalAddr;
    }

    public String getLogisticsArrivalLongitude() {
        return logisticsArrivalLongitude;
    }

    public void setLogisticsArrivalLongitude(String logisticsArrivalLongitude) {
        this.logisticsArrivalLongitude = logisticsArrivalLongitude;
    }

    public String getLogisticsArrivalLatitude() {
        return logisticsArrivalLatitude;
    }

    public void setLogisticsArrivalLatitude(String logisticsArrivalLatitude) {
        this.logisticsArrivalLatitude = logisticsArrivalLatitude;
    }

    public java.util.Date getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(java.util.Date departureTime) {
        this.departureTime = departureTime;
    }

    public java.util.Date getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(java.util.Date arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getAdultShrimpId() {
        return adultShrimpId;
    }

    public void setAdultShrimpId(String adultShrimpId) {
        this.adultShrimpId = adultShrimpId;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public java.util.Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(java.util.Date createDate) {
        this.createDate = createDate;
    }


}
