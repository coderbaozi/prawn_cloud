package com.prawn.traceability.pojo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="trac_qualification")
public class Qualification implements Serializable{

	@Id
	private String id;//ID


	
	private String productName;//产品名称
	private String inspectionReport;//质检部门检验报告
	private String qualityCertificate;//质量管理体系认证书
	private String safetyCertificate;//食品安全管理体系认证书
	private String license;//产品生产许可证
	private String productInfo;//产品信息
	private String standard;//采用标准
	private String baseId;//基地id

	
	public String getId() {		
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getProductName() {		
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getInspectionReport() {		
		return inspectionReport;
	}
	public void setInspectionReport(String inspectionReport) {
		this.inspectionReport = inspectionReport;
	}

	public String getQualityCertificate() {		
		return qualityCertificate;
	}
	public void setQualityCertificate(String qualityCertificate) {
		this.qualityCertificate = qualityCertificate;
	}

	public String getSafetyCertificate() {		
		return safetyCertificate;
	}
	public void setSafetyCertificate(String safetyCertificate) {
		this.safetyCertificate = safetyCertificate;
	}

	public String getLicense() {		
		return license;
	}
	public void setLicense(String license) {
		this.license = license;
	}

	public String getProductInfo() {		
		return productInfo;
	}
	public void setProductInfo(String productInfo) {
		this.productInfo = productInfo;
	}

	public String getStandard() {		
		return standard;
	}
	public void setStandard(String standard) {
		this.standard = standard;
	}

	public String getBaseId() {		
		return baseId;
	}
	public void setBaseId(String baseId) {
		this.baseId = baseId;
	}


	
}
