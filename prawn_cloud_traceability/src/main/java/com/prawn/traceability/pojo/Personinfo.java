package com.prawn.traceability.pojo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="trac_personInfo")
public class Personinfo implements Serializable{

	@Id
	private String id;//人员编号


	
	private String personName;//姓名
	private String sex;//性别
	private String politicalStatus;//政治面貌
	private String workType;//工种类别
	private String mobile;//联系电话
	private String baseId;//基地编号

	
	public String getId() {		
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getPersonName() {		
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getSex() {		
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getPoliticalStatus() {		
		return politicalStatus;
	}
	public void setPoliticalStatus(String politicalStatus) {
		this.politicalStatus = politicalStatus;
	}

	public String getWorkType() {		
		return workType;
	}
	public void setWorkType(String workType) {
		this.workType = workType;
	}

	public String getMobile() {		
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getBaseId() {		
		return baseId;
	}
	public void setBaseId(String baseId) {
		this.baseId = baseId;
	}


	
}
