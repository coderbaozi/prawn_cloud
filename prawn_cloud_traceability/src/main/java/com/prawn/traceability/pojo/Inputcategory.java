package com.prawn.traceability.pojo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="trac_inputCategory")
public class Inputcategory implements Serializable{

	@Id
	private String id;//ID


	
	private String typeOne;//一级类型
	private String baseId;//基地id

	
	public String getId() {		
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getTypeOne() {		
		return typeOne;
	}
	public void setTypeOne(String typeOne) {
		this.typeOne = typeOne;
	}

	public String getBaseId() {		
		return baseId;
	}
	public void setBaseId(String baseId) {
		this.baseId = baseId;
	}


	
}
