package com.prawn.traceability.pojo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="trac_customer")
public class Customer implements Serializable{

	@Id
	private String id;//Id


	
	private String customerName;//客户名
	private String customerType;//客户类型
	private String contactPerson;//联系人
	private String phoneNumber;//联系电话
	private String email;//邮箱
	private String receiptAddress;//收货地址
	private String addressLongitude;//收货地址经度
	private String addressLatitude;//收货地址纬度
	private String baseId;//基地编号

	public String getBaseId() {
		return baseId;
	}

	public void setBaseId(String baseId) {
		this.baseId = baseId;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getCustomerName() {		
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerType() {		
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getContactPerson() {		
		return contactPerson;
	}
	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getPhoneNumber() {		
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {		
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public String getReceiptAddress() {		
		return receiptAddress;
	}
	public void setReceiptAddress(String receiptAddress) {
		this.receiptAddress = receiptAddress;
	}

	public String getAddressLongitude() {		
		return addressLongitude;
	}
	public void setAddressLongitude(String addressLongitude) {
		this.addressLongitude = addressLongitude;
	}

	public String getAddressLatitude() {		
		return addressLatitude;
	}
	public void setAddressLatitude(String addressLatitude) {
		this.addressLatitude = addressLatitude;
	}


	
}
