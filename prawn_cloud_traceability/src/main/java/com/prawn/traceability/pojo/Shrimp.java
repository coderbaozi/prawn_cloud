package com.prawn.traceability.pojo;


import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 实体类
 *
 * @author Administrator
 */
@Entity
@Table(name = "trac_shrimp")
public class Shrimp implements Serializable {

    @Id
    private String id;//虾苗编号


    private String shrimpBatchName;//虾苗批次名称
    private String shrimpSpecies;//虾苗品种
    private String shrimpOrigin;//虾苗产地
    private String shrimpSupplier;//虾苗供应商
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private java.util.Date seedlingTime;//放苗时间
    private String fishingStatus;//捕捞状态
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private java.util.Date fishingTime;//捕捞时间
    private String pondId;//池塘编号
    private String baseId;//基地编号
    private String createBy;//（捕捞）审核人
    private int inputNum;//投放尾数
    private double yield;//产量
    private double remain;//剩余量
    private String specification;//对虾规格（XX 尾/Kg)

//    @OneToMany(mappedBy = "shrimp")
//    private Set<AdultShrimp> adultShrimps = new HashSet<>();
//
//    @OneToMany(mappedBy = "shrimp")
//    private List<ShrimpFarming> shrimpFarmings = new ArrayList<>();
//
//    @ManyToOne(targetEntity = entity.Base.class)
//    @JoinColumn(name = "baseId", referencedColumnName = "id", insertable = false, updatable = false)
//    private entity.Base base;


    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public double getRemain() {
        return remain;
    }

    public void setRemain(double remain) {
        this.remain = remain;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShrimpBatchName() {
        return shrimpBatchName;
    }

    public void setShrimpBatchName(String shrimpBatchName) {
        this.shrimpBatchName = shrimpBatchName;
    }

    public String getShrimpSpecies() {
        return shrimpSpecies;
    }

    public void setShrimpSpecies(String shrimpSpecies) {
        this.shrimpSpecies = shrimpSpecies;
    }

    public String getShrimpOrigin() {
        return shrimpOrigin;
    }

    public void setShrimpOrigin(String shrimpOrigin) {
        this.shrimpOrigin = shrimpOrigin;
    }

    public String getShrimpSupplier() {
        return shrimpSupplier;
    }

    public void setShrimpSupplier(String shrimpSupplier) {
        this.shrimpSupplier = shrimpSupplier;
    }

    public java.util.Date getSeedlingTime() {
        return seedlingTime;
    }

    public void setSeedlingTime(java.util.Date seedlingTime) {
        this.seedlingTime = seedlingTime;
    }

    public String getFishingStatus() {
        return fishingStatus;
    }

    public void setFishingStatus(String fishingStatus) {
        this.fishingStatus = fishingStatus;
    }

    public java.util.Date getFishingTime() {
        return fishingTime;
    }

    public void setFishingTime(java.util.Date fishingTime) {
        this.fishingTime = fishingTime;
    }

    public String getPondId() {
        return pondId;
    }

    public void setPondId(String pondId) {
        this.pondId = pondId;
    }

    public String getBaseId() {
        return baseId;
    }

    public void setBaseId(String baseId) {
        this.baseId = baseId;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public int getInputNum() {
        return inputNum;
    }

    public void setInputNum(int inputNum) {
        this.inputNum = inputNum;
    }

    public double getYield() {
        return yield;
    }

    public void setYield(double yield) {
        this.yield = yield;
    }
}
