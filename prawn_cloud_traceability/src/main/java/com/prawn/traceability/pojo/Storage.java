package com.prawn.traceability.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="trac_storage")
public class Storage implements Serializable{

	@Id
	private String id;//Id


	
	private String storageName;//冷库名称
	private String storageAddr;//冷库地址
	private String storagePositionLongitude;//冷库地址经度
	private String storagePositionLatitude;//冷库地址纬度
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date storageTime;//入库时间
	private String outboundStatus;//出库状态
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date outboundTime;//出库时间
	private String adultShrimpId;//成虾编号
	private Float storageTemperature;//冷库温度
	private String createBy;//创建者
	private String baseId;//基地id

	public String getBaseId() {
		return baseId;
	}

	public void setBaseId(String baseId) {
		this.baseId = baseId;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getStorageName() {		
		return storageName;
	}
	public void setStorageName(String storageName) {
		this.storageName = storageName;
	}

	public String getStorageAddr() {		
		return storageAddr;
	}
	public void setStorageAddr(String storageAddr) {
		this.storageAddr = storageAddr;
	}

	public String getStoragePositionLongitude() {		
		return storagePositionLongitude;
	}
	public void setStoragePositionLongitude(String storagePositionLongitude) {
		this.storagePositionLongitude = storagePositionLongitude;
	}

	public String getStoragePositionLatitude() {		
		return storagePositionLatitude;
	}
	public void setStoragePositionLatitude(String storagePositionLatitude) {
		this.storagePositionLatitude = storagePositionLatitude;
	}

	public java.util.Date getStorageTime() {		
		return storageTime;
	}
	public void setStorageTime(java.util.Date storageTime) {
		this.storageTime = storageTime;
	}

	public String getOutboundStatus() {		
		return outboundStatus;
	}
	public void setOutboundStatus(String outboundStatus) {
		this.outboundStatus = outboundStatus;
	}

	public java.util.Date getOutboundTime() {		
		return outboundTime;
	}
	public void setOutboundTime(java.util.Date outboundTime) {
		this.outboundTime = outboundTime;
	}

	public String getAdultShrimpId() {		
		return adultShrimpId;
	}
	public void setAdultShrimpId(String adultShrimpId) {
		this.adultShrimpId = adultShrimpId;
	}

	public Float getStorageTemperature() {
		return storageTemperature;
	}
	public void setStorageTemperature(Float storageTemperature) {
		this.storageTemperature = storageTemperature;
	}

	public String getCreateBy() {		
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

}
