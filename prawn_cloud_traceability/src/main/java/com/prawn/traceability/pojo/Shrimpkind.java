package com.prawn.traceability.pojo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="trac_shrimp_kind")
public class Shrimpkind implements Serializable{

	@Id
	private String id;//ID


	
	private String kindName;//种类名称
	private Integer minDensity;//最小密度
	private Integer maxDensity;//最大密度
	private String waterTemperature;//水温
	private String ph;//Ph值
	private String salinity;//盐度
	private String ammoniaValue;//氨氮值
	private String dissolvedOxygen;//溶解氧

	public String getWaterTemperature() {
		return waterTemperature;
	}

	public void setWaterTemperature(String waterTemperature) {
		this.waterTemperature = waterTemperature;
	}

	public String getPh() {
		return ph;
	}

	public void setPh(String ph) {
		this.ph = ph;
	}

	public String getSalinity() {
		return salinity;
	}

	public void setSalinity(String salinity) {
		this.salinity = salinity;
	}

	public String getAmmoniaValue() {
		return ammoniaValue;
	}

	public void setAmmoniaValue(String ammoniaValue) {
		this.ammoniaValue = ammoniaValue;
	}

	public String getDissolvedOxygen() {
		return dissolvedOxygen;
	}

	public void setDissolvedOxygen(String dissolvedOxygen) {
		this.dissolvedOxygen = dissolvedOxygen;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getKindName() {		
		return kindName;
	}
	public void setKindName(String kindName) {
		this.kindName = kindName;
	}

	public Integer getMinDensity() {		
		return minDensity;
	}
	public void setMinDensity(Integer minDensity) {
		this.minDensity = minDensity;
	}

	public Integer getMaxDensity() {		
		return maxDensity;
	}
	public void setMaxDensity(Integer maxDensity) {
		this.maxDensity = maxDensity;
	}


	
}
