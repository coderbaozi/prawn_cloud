package com.prawn.traceability.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="trac_plant")
public class Plant implements Serializable{

	@Id
	private String id;//厂家编号



	private String plantName;//厂家名称
	private String plantAddress;//厂家地址
	private String plantPositionLongitude;//厂家地址经度
	private String plantPositionLatitude;//厂家地址纬度
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date productionTime;//生产时间
	private String adultShrimpId;//成虾编号
	private String productName;//产品名称
	private String createBy;//创建者
	private String qualificationId;//资质id
	private String baseId;//基地id

	public String getBaseId() {
		return baseId;
	}

	public void setBaseId(String baseId) {
		this.baseId = baseId;
	}

	public String getQualificationId() {
		return qualificationId;
	}

	public void setQualificationId(String qualificationId) {
		this.qualificationId = qualificationId;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getPlantName() {		
		return plantName;
	}
	public void setPlantName(String plantName) {
		this.plantName = plantName;
	}

	public String getPlantAddress() {
		return plantAddress;
	}

	public void setPlantAddress(String plantAddress) {
		this.plantAddress = plantAddress;
	}

	public String getPlantPositionLongitude() {
		return plantPositionLongitude;
	}
	public void setPlantPositionLongitude(String plantPositionLongitude) {
		this.plantPositionLongitude = plantPositionLongitude;
	}

	public String getPlantPositionLatitude() {		
		return plantPositionLatitude;
	}
	public void setPlantPositionLatitude(String plantPositionLatitude) {
		this.plantPositionLatitude = plantPositionLatitude;
	}

	public java.util.Date getProductionTime() {		
		return productionTime;
	}
	public void setProductionTime(java.util.Date productionTime) {
		this.productionTime = productionTime;
	}

	public String getAdultShrimpId() {		
		return adultShrimpId;
	}
	public void setAdultShrimpId(String adultShrimpId) {
		this.adultShrimpId = adultShrimpId;
	}

	public String getProductName() {		
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}


	public String getCreateBy() {		
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}


}
