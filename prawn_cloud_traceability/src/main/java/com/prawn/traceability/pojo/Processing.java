package com.prawn.traceability.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="trac_processing")
public class Processing implements Serializable{

	@Id
	private String id;//id


	
	private String processName;//工艺名称
	private String responsible;//责任人
	private String step;//工艺步骤/环节
	private String description;//工艺描述
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date onTime;//进行时间
	private String processPic;//工艺图片
	private String plantId;//产品id
	private String suppliesId;//投入品信息id
	private String typeId;//二级类型id

	public String getSuppliesId() {
		return suppliesId;
	}

	public void setSuppliesId(String suppliesId) {
		this.suppliesId = suppliesId;
	}

	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getProcessName() {		
		return processName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}

	public String getResponsible() {		
		return responsible;
	}
	public void setResponsible(String responsible) {
		this.responsible = responsible;
	}

	public String getStep() {		
		return step;
	}
	public void setStep(String step) {
		this.step = step;
	}

	public String getDescription() {		
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public java.util.Date getOnTime() {		
		return onTime;
	}
	public void setOnTime(java.util.Date onTime) {
		this.onTime = onTime;
	}

	public String getProcessPic() {		
		return processPic;
	}
	public void setProcessPic(String processPic) {
		this.processPic = processPic;
	}

	public String getPlantId() {		
		return plantId;
	}
	public void setPlantId(String plantId) {
		this.plantId = plantId;
	}


	
}
