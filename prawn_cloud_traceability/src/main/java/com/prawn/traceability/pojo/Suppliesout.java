package com.prawn.traceability.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="trac_suppliesOut")
public class Suppliesout implements Serializable{

	@Id
	private String id;//农资编号


	
	private String suppliesName;//农资名称
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date outTime;//出库日期
	private String outOperator;//操作人
	private String warehouse;//仓库号
	private Double outNumber;//出库数量
	private String baseId;//基地编号
	private String suppliesInId;//入库编号
	private String type;//农资类型(1渔药 2饲料 3渔具)

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSuppliesInId() {
		return suppliesInId;
	}

	public void setSuppliesInId(String suppliesInId) {
		this.suppliesInId = suppliesInId;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getSuppliesName() {		
		return suppliesName;
	}
	public void setSuppliesName(String suppliesName) {
		this.suppliesName = suppliesName;
	}

	public java.util.Date getOutTime() {		
		return outTime;
	}
	public void setOutTime(java.util.Date outTime) {
		this.outTime = outTime;
	}

	public String getOutOperator() {		
		return outOperator;
	}
	public void setOutOperator(String outOperator) {
		this.outOperator = outOperator;
	}

	public String getWarehouse() {		
		return warehouse;
	}
	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}

	public Double getOutNumber() {		
		return outNumber;
	}
	public void setOutNumber(Double outNumber) {
		this.outNumber = outNumber;
	}

	public String getBaseId() {		
		return baseId;
	}
	public void setBaseId(String baseId) {
		this.baseId = baseId;
	}


	
}
