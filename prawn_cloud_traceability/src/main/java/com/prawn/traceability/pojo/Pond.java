package com.prawn.traceability.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="trac_pond")
public class Pond implements Serializable{

	@Id
	private String id;//池塘编号


	private String pondName;//池塘名称
	private String baseId;//基地编号
	private String shrimpId;//虾苗编号
	private Float pondVolume;//池塘体积
	private Integer inputNum;//投放尾数
	private String createBy;//创建者
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createDate;//创建时间
	private String deliveryStatus;//投放状态
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date catchDate;//捕捞时间
	private String pondType;//池塘类型
	private Double depth;//深度(米)
	private String species;//虾种类

	public String getSpecies() {
		return species;
	}

	public void setSpecies(String species) {
		this.species = species;
	}

	public Double getDepth() {
		return depth;
	}

	public void setDepth(Double depth) {
		this.depth = depth;
	}

	public String getPondType() {
		return pondType;
	}

	public void setPondType(String pondType) {
		this.pondType = pondType;
	}

	//	@ManyToOne(targetEntity = entity.Base.class)
//	@JoinColumn(name = "baseId", referencedColumnName = "id")
//	private entity.Base base;
//
//	public entity.Base getBase() {
//		return base;
//	}
//
//	public void setBase(entity.Base base) {
//		this.base = base;
//	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getPondName() {
		return pondName;
	}
	public void setPondName(String pondName) {
		this.pondName = pondName;
	}

	public String getBaseId() {
		return baseId;
	}
	public void setBaseId(String baseId) {
		this.baseId = baseId;
	}

	public String getShrimpId() {
		return shrimpId;
	}
	public void setShrimpId(String shrimpId) {
		this.shrimpId = shrimpId;
	}

	public Float getPondVolume() {
		return pondVolume;
	}
	public void setPondVolume(Float pondVolume) {
		this.pondVolume = pondVolume;
	}

	public Integer getInputNum() {
		return inputNum;
	}
	public void setInputNum(Integer inputNum) {
		this.inputNum = inputNum;
	}

	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getDeliveryStatus() {
		return deliveryStatus;
	}

	public void setDeliveryStatus(String deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}

	public Date getCatchDate() {
		return catchDate;
	}

	public void setCatchDate(Date catchDate) {
		this.catchDate = catchDate;
	}
}
