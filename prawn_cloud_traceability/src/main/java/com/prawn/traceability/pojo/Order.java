package com.prawn.traceability.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="trac_order")
public class Order implements Serializable{

	@Id
	private String id;//ID


	
	private String logisticsId;//物流编号
	private String customerName;//客户名
	private String customerType;//客户类型
	private Double money;//金额
	private String shrimpId;//虾苗编号
	private Double weight;//重量
	private String createBy;//创建者
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date createDate;//物流出发时间
	private String receiptAddress;//收货地址
	private String addressLongitude;//收货地址经度
	private String addressLatitude;//收货地址纬度
	private String baseId;//基地编号
	private String shrimpBatchName;//虾苗批次名称
	private String adultShrimpId;//成虾编号


	public String getAdultShrimpId() {
		return adultShrimpId;
	}

	public void setAdultShrimpId(String adultShrimpId) {
		this.adultShrimpId = adultShrimpId;
	}

	public String getShrimpBatchName() {
		return shrimpBatchName;
	}

	public void setShrimpBatchName(String shrimpBatchName) {
		this.shrimpBatchName = shrimpBatchName;
	}

	public String getBaseId() {
		return baseId;
	}

	public void setBaseId(String baseId) {
		this.baseId = baseId;
	}

	public String getReceiptAddress() {
		return receiptAddress;
	}

	public void setReceiptAddress(String receiptAddress) {
		this.receiptAddress = receiptAddress;
	}

	public String getAddressLongitude() {
		return addressLongitude;
	}

	public void setAddressLongitude(String addressLongitude) {
		this.addressLongitude = addressLongitude;
	}

	public String getAddressLatitude() {
		return addressLatitude;
	}

	public void setAddressLatitude(String addressLatitude) {
		this.addressLatitude = addressLatitude;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getLogisticsId() {		
		return logisticsId;
	}
	public void setLogisticsId(String logisticsId) {
		this.logisticsId = logisticsId;
	}

	public String getCustomerName() {		
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerType() {		
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public Double getMoney() {		
		return money;
	}
	public void setMoney(Double money) {
		this.money = money;
	}

	public String getShrimpId() {		
		return shrimpId;
	}
	public void setShrimpId(String shrimpId) {
		this.shrimpId = shrimpId;
	}

	public Double getWeight() {		
		return weight;
	}
	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public String getCreateBy() {		
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public java.util.Date getCreateDate() {		
		return createDate;
	}
	public void setCreateDate(java.util.Date createDate) {
		this.createDate = createDate;
	}


	
}
