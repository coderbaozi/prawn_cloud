package com.prawn.traceability.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="trac_shrimp_management")
public class ShrimpManagement implements Serializable{

	@Id
	private String id;//ID


	
	private String shrimpBatchName;//虾苗批次名称
	private String shrimpSpecies;//虾苗品种
	private String shrimpOrigin;//虾苗产地
	private String shrimpSupplier;//虾苗供应商
	private String supplierPhone;//供应商电话
	private int shrimpNumber;//虾苗数量
	private Double yield;//产量
	private String baseId;//基地编号
	private String createBy;//创建者
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date createDate;//创建时间
	private int remainNumber;//剩余数量
	private String qualityInspection;//质检（图片）

	public String getSupplierPhone() {
		return supplierPhone;
	}

	public void setSupplierPhone(String supplierPhone) {
		this.supplierPhone = supplierPhone;
	}

	public String getQualityInspection() {
		return qualityInspection;
	}

	public void setQualityInspection(String qualityInspection) {
		this.qualityInspection = qualityInspection;
	}

	public int getRemainNumber() {
		return remainNumber;
	}

	public void setRemainNumber(int remainNumber) {
		this.remainNumber = remainNumber;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getShrimpBatchName() {		
		return shrimpBatchName;
	}
	public void setShrimpBatchName(String shrimpBatchName) {
		this.shrimpBatchName = shrimpBatchName;
	}

	public String getShrimpSpecies() {		
		return shrimpSpecies;
	}
	public void setShrimpSpecies(String shrimpSpecies) {
		this.shrimpSpecies = shrimpSpecies;
	}

	public String getShrimpOrigin() {		
		return shrimpOrigin;
	}
	public void setShrimpOrigin(String shrimpOrigin) {
		this.shrimpOrigin = shrimpOrigin;
	}

	public String getShrimpSupplier() {		
		return shrimpSupplier;
	}
	public void setShrimpSupplier(String shrimpSupplier) {
		this.shrimpSupplier = shrimpSupplier;
	}

	public int getShrimpNumber() {
		return shrimpNumber;
	}

	public void setShrimpNumber(int shrimpNumber) {
		this.shrimpNumber = shrimpNumber;
	}

	public Double getYield() {
		return yield;
	}
	public void setYield(Double yield) {
		this.yield = yield;
	}

	public String getBaseId() {		
		return baseId;
	}
	public void setBaseId(String baseId) {
		this.baseId = baseId;
	}

	public String getCreateBy() {		
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public java.util.Date getCreateDate() {		
		return createDate;
	}
	public void setCreateDate(java.util.Date createDate) {
		this.createDate = createDate;
	}


	
}
