package com.prawn.traceability.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import entity.Base;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import util.IdWorker;

import com.prawn.traceability.dao.BaseDao;


/**
 * 服务层
 * 
 * @author Administrator
 *
 */
@Service
public class BaseService {

	@Autowired
	private BaseDao BaseDao;
	
	@Autowired
	private IdWorker idWorker;

	/**
	 * 查询1全部列表
	 * @return
	 */
	public List<Base> findAll() {
		return BaseDao.findAll();
	}

	
	/**
	 * 条件查询+分页
	 * @param whereMap
	 * @param page
	 * @param size
	 * @return
	 */
	public Page<Base> findSearch(Map whereMap, int page, int size) {
		Specification<Base> specification = createSpecification(whereMap);
		PageRequest pageRequest =  PageRequest.of(page-1, size);
		return BaseDao.findAll(specification, pageRequest);
	}

	
	/**
	 * 条件查询
	 * @param whereMap
	 * @return
	 */
	public List<Base> findSearch(Map whereMap) {
		Specification<Base> specification = createSpecification(whereMap);
		return BaseDao.findAll(specification);
	}

	/**
	 * 根据ID查询实体
	 * @param id
	 * @return
	 */
	public Base findById(String id) {
		return BaseDao.findById(id).get();
	}

	/**
	 * 增加
	 * @param Base
	 */
	public void add(Base Base) {
		if(Base.getId()==null || "".equals(Base.getId())){
			Base.setId( idWorker.nextId()+"" );
		}
		BaseDao.save(Base);
	}

	/**
	 * 修改
	 * @param Base
	 */
	public void update(Base Base) {
		BaseDao.save(Base);
	}

	/**
	 * 删除
	 * @param id
	 */
	public void deleteById(String id) {
		BaseDao.deleteById(id);
	}

	/**
	 * 动态条件构建
	 * @param searchMap
	 * @return
	 */
	private Specification<Base> createSpecification(Map searchMap) {

		return new Specification<Base>() {

			@Override
			public Predicate toPredicate(Root<Base> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<Predicate>();
                // 基地编号
                if (searchMap.get("id")!=null && !"".equals(searchMap.get("id"))) {
                	predicateList.add(cb.like(root.get("id").as(String.class), "%"+(String)searchMap.get("id")+"%"));
                }
                // 基地名称
                if (searchMap.get("baseName")!=null && !"".equals(searchMap.get("baseName"))) {
                	predicateList.add(cb.like(root.get("baseName").as(String.class), "%"+(String)searchMap.get("baseName")+"%"));
                }
                // 基地简介
                if (searchMap.get("baseIntroduction")!=null && !"".equals(searchMap.get("baseIntroduction"))) {
                	predicateList.add(cb.like(root.get("baseIntroduction").as(String.class), "%"+(String)searchMap.get("baseIntroduction")+"%"));
                }
                // 基地地址
                if (searchMap.get("baseAddr")!=null && !"".equals(searchMap.get("baseAddr"))) {
                	predicateList.add(cb.like(root.get("baseAddr").as(String.class), "%"+(String)searchMap.get("baseAddr")+"%"));
                }
                // 基地地址经度
                if (searchMap.get("basePositionLongitude")!=null && !"".equals(searchMap.get("basePositionLongitude"))) {
                	predicateList.add(cb.like(root.get("basePositionLongitude").as(String.class), "%"+(String)searchMap.get("basePositionLongitude")+"%"));
                }
                // 基地地址纬度
                if (searchMap.get("basePositionLatitude")!=null && !"".equals(searchMap.get("basePositionLatitude"))) {
                	predicateList.add(cb.like(root.get("basePositionLatitude").as(String.class), "%"+(String)searchMap.get("basePositionLatitude")+"%"));
                }
                // 创建者
                if (searchMap.get("createBy")!=null && !"".equals(searchMap.get("createBy"))) {
                	predicateList.add(cb.like(root.get("createBy").as(String.class), "%"+(String)searchMap.get("createBy")+"%"));
                }
                //营业执照注册号
				if (searchMap.get("registNumber")!=null && !"".equals(searchMap.get("registNumber"))) {
					predicateList.add(cb.like(root.get("registNumber").as(String.class), "%"+(String)searchMap.get("registNumber")+"%"));
				}
				//注册资金(万元)
				if (searchMap.get("funds")!=null && !"".equals(searchMap.get("funds"))) {
					predicateList.add(cb.like(root.get("funds").as(String.class), "%"+(String)searchMap.get("funds")+"%"));
				}
				//基地图片
				if (searchMap.get("basePic")!=null && !"".equals(searchMap.get("basePic"))) {
					predicateList.add(cb.like(root.get("basePic").as(String.class), "%"+(String)searchMap.get("basePic")+"%"));
				}

				//营业范围
				if (searchMap.get("scope")!=null && !"".equals(searchMap.get("scope"))) {
					predicateList.add(cb.like(root.get("scope").as(String.class), "%"+(String)searchMap.get("scope")+"%"));
				}
				
				return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

			}
		};

	}

}
