package com.prawn.traceability.service;

import java.util.*;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import com.prawn.traceability.dao.AdultShrimpDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;
import util.IdWorker;

import com.prawn.traceability.dao.StorageDao;
import com.prawn.traceability.pojo.Storage;

/**
 * 服务层
 *
 * @author Administrator
 */
@Transactional
@Service
public class StorageService {

    @Autowired
    private StorageDao storageDao;

    @Autowired
    private IdWorker idWorker;

    @Autowired
    private AdultShrimpDao adultShrimpDao;

    @Resource
    private RedisTemplate redisTemplate;

    /**
     * 查询全部列表
     *
     * @return
     */
    public List<Storage> findAll() {
        return storageDao.findAll();
    }


    /**
     * 条件查询+分页
     *
     * @param whereMap
     * @param page
     * @param size
     * @return
     */
    public Page<Storage> findSearch(Map whereMap, int page, int size) {
        Specification<Storage> specification = createSpecification(whereMap);
        PageRequest pageRequest = PageRequest.of(page - 1, size);
        return storageDao.findAll(specification, pageRequest);
    }


    /**
     * 条件查询
     *
     * @param whereMap
     * @return
     */
    public List<Storage> findSearch(Map whereMap) {
        Specification<Storage> specification = createSpecification(whereMap);
        return storageDao.findAll(specification);
    }

    /**
     * 根据ID查询实体
     *
     * @param id
     * @return
     */
    public Storage findById(String id) {
        return storageDao.findById(id).get();
    }

    /**
     * 增加
     *
     * @param storage
     */
    public void add(Storage storage) {
        String storageId = idWorker.nextId() + "";
        storage.setId(storageId);
        //同步冷库id到成虾表中
        adultShrimpDao.updateStorageId(storageId, storage.getAdultShrimpId());
        storage.setOutboundStatus("0");
        storageDao.save(storage);
    }

	/**
	 * 冷库出库
	 * @param outboundTime
	 * @param adultShrimpId
	 */
    public void outStorage(Date outboundTime, String adultShrimpId) {
        storageDao.outStorage("1", outboundTime, adultShrimpId);
    }

    /**
     * 修改
     *
     * @param storage
     */
    public void update(Storage storage) {
        redisTemplate.delete("adultShrimp_"+storage.getAdultShrimpId());
        storageDao.save(storage);
    }

    /**
     * 删除
     *
     * @param id
     */
    public void deleteById(String id) {
        Storage storage = storageDao.findById(id).get();
        adultShrimpDao.updateStorageId("", storage.getAdultShrimpId());
        storageDao.deleteById(id);
    }

    /**
     * 动态条件构建
     *
     * @param searchMap
     * @return
     */
    private Specification<Storage> createSpecification(Map searchMap) {

        return new Specification<Storage>() {

            @Override
            public Predicate toPredicate(Root<Storage> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                List<Predicate> predicateList = new ArrayList<Predicate>();
                // Id
                if (searchMap.get("id") != null && !"".equals(searchMap.get("id"))) {
                    predicateList.add(cb.like(root.get("id").as(String.class), "%" + (String) searchMap.get("id") + "%"));
                }
                // 冷库名称
                if (searchMap.get("storageName") != null && !"".equals(searchMap.get("storageName"))) {
                    predicateList.add(cb.like(root.get("storageName").as(String.class), "%" + (String) searchMap.get("storageName") + "%"));
                }
                // 冷库地址
                if (searchMap.get("storageAddr") != null && !"".equals(searchMap.get("storageAddr"))) {
                    predicateList.add(cb.like(root.get("storageAddr").as(String.class), "%" + (String) searchMap.get("storageAddr") + "%"));
                }
                // 冷库地址经度
                if (searchMap.get("storagePositionLongitude") != null && !"".equals(searchMap.get("storagePositionLongitude"))) {
                    predicateList.add(cb.like(root.get("storagePositionLongitude").as(String.class), "%" + (String) searchMap.get("storagePositionLongitude") + "%"));
                }
                // 冷库地址纬度
                if (searchMap.get("storagePositionLatitude") != null && !"".equals(searchMap.get("storagePositionLatitude"))) {
                    predicateList.add(cb.like(root.get("storagePositionLatitude").as(String.class), "%" + (String) searchMap.get("storagePositionLatitude") + "%"));
                }
                // 出库状态
                if (searchMap.get("outboundStatus") != null && !"".equals(searchMap.get("outboundStatus"))) {
                    predicateList.add(cb.like(root.get("outboundStatus").as(String.class), "%" + (String) searchMap.get("outboundStatus") + "%"));
                }
                // 成虾编号
                if (searchMap.get("adultShrimpId") != null && !"".equals(searchMap.get("adultShrimpId"))) {
                    predicateList.add(cb.like(root.get("adultShrimpId").as(String.class), "%" + (String) searchMap.get("adultShrimpId") + "%"));
                }
                // 创建者
                if (searchMap.get("createBy") != null && !"".equals(searchMap.get("createBy"))) {
                    predicateList.add(cb.like(root.get("createBy").as(String.class), "%" + (String) searchMap.get("createBy") + "%"));
                }
                // 创建者
                if (searchMap.get("baseId") != null && !"".equals(searchMap.get("baseId"))) {
                    predicateList.add(cb.like(root.get("baseId").as(String.class), "%" + (String) searchMap.get("baseId") + "%"));
                }

                return cb.and(predicateList.toArray(new Predicate[predicateList.size()]));

            }
        };

    }

}
