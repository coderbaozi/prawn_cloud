package com.prawn.traceability.service;

import java.util.*;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import util.IdWorker;

import com.prawn.traceability.dao.LogisticsDao;
import com.prawn.traceability.pojo.Logistics;

/**
 * 服务层
 *
 * @author Administrator
 */
@Service
public class LogisticsService {

    @Autowired
    private LogisticsDao logisticsDao;

    @Autowired
    private IdWorker idWorker;

    @Resource
    private RedisTemplate<String,Object> redisTemplate;

    public Logistics findByShrimpId(String adultShrimpId) {
        return logisticsDao.findByAdultShrimpIdAndLogisticsStatus(adultShrimpId, "0");
    }

    public void updatelogistics(Date arrivalTime,String adultShrimpId){
        logisticsDao.updatelogistics(arrivalTime,"1",adultShrimpId,"0");
    }

    /**
     * 查询全部列表
     *
     * @return
     */
    public List<Logistics> findAll() {
        return logisticsDao.findAll();
    }


    /**
     * 条件查询+分页
     *
     * @param whereMap
     * @param page
     * @param size
     * @return
     */
    public Page<Logistics> findSearch(Map whereMap, int page, int size) {
        Specification<Logistics> specification = createSpecification(whereMap);
        PageRequest pageRequest = PageRequest.of(page - 1, size);
        return logisticsDao.findAll(specification, pageRequest);
    }


    /**
     * 条件查询
     *
     * @param whereMap
     * @return
     */
    public List<Logistics> findSearch(Map whereMap) {
        Specification<Logistics> specification = createSpecification(whereMap);
        return logisticsDao.findAll(specification);
    }

    /**
     * 根据ID查询实体
     *
     * @param id
     * @return
     */
    public Logistics findById(String id) {
        return logisticsDao.findById(id).get();
    }

    /**
     * 增加
     *
     * @param logistics
     */
    public void add(Logistics logistics) {
        if (logistics.getId() == null || "".equals(logistics.getId())) {
            logistics.setId(idWorker.nextId() + "");
        }
        logistics.setLogisticsStatus("0");
        logisticsDao.save(logistics);
    }

    /**
     * 修改
     *
     * @param logistics
     */
    public void update(Logistics logistics) {
        redisTemplate.delete("adultShrimp_"+logistics.getAdultShrimpId());
        logisticsDao.save(logistics);
    }

    /**
     * 删除
     *
     * @param id
     */
    public void deleteById(String id) {
        logisticsDao.deleteById(id);
    }

    /**
     * 动态条件构建
     *
     * @param searchMap
     * @return
     */
    private Specification<Logistics> createSpecification(Map searchMap) {

        return new Specification<Logistics>() {

            @Override
            public Predicate toPredicate(Root<Logistics> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                List<Predicate> predicateList = new ArrayList<Predicate>();
                // 表id
                if (searchMap.get("id") != null && !"".equals(searchMap.get("id"))) {
                    predicateList.add(cb.like(root.get("id").as(String.class), "%" + (String) searchMap.get("id") + "%"));
                }
                // 出发地址
                if (searchMap.get("departureAddr") != null && !"".equals(searchMap.get("departureAddr"))) {
                    predicateList.add(cb.like(root.get("departureAddr").as(String.class), "%" + (String) searchMap.get("departureAddr") + "%"));
                }
                // 出发地址经度
                if (searchMap.get("logisticsStartLongitude") != null && !"".equals(searchMap.get("logisticsStartLongitude"))) {
                    predicateList.add(cb.like(root.get("logisticsStartLongitude").as(String.class), "%" + (String) searchMap.get("logisticsStartLongitude") + "%"));
                }
                // 出发地址纬度
                if (searchMap.get("logisticsStartLatitude") != null && !"".equals(searchMap.get("logisticsStartLatitude"))) {
                    predicateList.add(cb.like(root.get("logisticsStartLatitude").as(String.class), "%" + (String) searchMap.get("logisticsStartLatitude") + "%"));
                }
                // 抵达地址
                if (searchMap.get("arrivalAddr") != null && !"".equals(searchMap.get("arrivalAddr"))) {
                    predicateList.add(cb.like(root.get("arrivalAddr").as(String.class), "%" + (String) searchMap.get("arrivalAddr") + "%"));
                }
                // 抵达地址经度
                if (searchMap.get("logisticsArrivalLongitude") != null && !"".equals(searchMap.get("logisticsArrivalLongitude"))) {
                    predicateList.add(cb.like(root.get("logisticsArrivalLongitude").as(String.class), "%" + (String) searchMap.get("logisticsArrivalLongitude") + "%"));
                }
                // 抵达地址纬度
                if (searchMap.get("logisticsArrivalLatitude") != null && !"".equals(searchMap.get("logisticsArrivalLatitude"))) {
                    predicateList.add(cb.like(root.get("logisticsArrivalLatitude").as(String.class), "%" + (String) searchMap.get("logisticsArrivalLatitude") + "%"));
                }
                // 成虾编号
                if (searchMap.get("adultShrimpId") != null && !"".equals(searchMap.get("adultShrimpId"))) {
                    predicateList.add(cb.like(root.get("adultShrimpId").as(String.class), "%" + (String) searchMap.get("adultShrimpId") + "%"));
                }
                // 创建者
                if (searchMap.get("createBy") != null && !"".equals(searchMap.get("createBy"))) {
                    predicateList.add(cb.like(root.get("createBy").as(String.class), "%" + (String) searchMap.get("createBy") + "%"));
                }
                // 基地id
                if (searchMap.get("baseId") != null && !"".equals(searchMap.get("baseId"))) {
                    predicateList.add(cb.like(root.get("baseId").as(String.class), "%" + (String) searchMap.get("baseId") + "%"));
                }

                return cb.and(predicateList.toArray(new Predicate[predicateList.size()]));

            }
        };

    }

}
