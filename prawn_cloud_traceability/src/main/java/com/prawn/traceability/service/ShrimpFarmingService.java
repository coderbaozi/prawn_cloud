package com.prawn.traceability.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.prawn.traceability.dao.ShrimpDao;
import com.prawn.traceability.pojo.Shrimp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import util.IdWorker;

import com.prawn.traceability.dao.ShrimpFarmingDao;
import com.prawn.traceability.pojo.ShrimpFarming;

/**
 * 服务层
 *
 * @author Administrator
 */
@Service
public class ShrimpFarmingService {

    @Autowired
    private ShrimpFarmingDao shrimpFarmingDao;

    @Autowired
    private ShrimpDao shrimpDao;

    @Autowired
    private IdWorker idWorker;

    /**
     * 查询池塘投料记录
     *
     * @param pondId
     * @return
     */
    public Page<ShrimpFarming> findPondFarming(String pondId, int page, int size) {
        Shrimp shrimp = shrimpDao.findByPondIdAndFishingStatus(pondId, "0");
        Pageable pageable = PageRequest.of(page - 1, size);
        return shrimpFarmingDao.findFarmingByShrimpId(shrimp.getId(), pageable);
    }

    /**
     * 查询全部列表
     *
     * @return
     */
    public List<ShrimpFarming> findAll() {
        return shrimpFarmingDao.findAll();
    }


    /**
     * 条件查询+分页
     *
     * @param whereMap
     * @param page
     * @param size
     * @return
     */
    public Page<ShrimpFarming> findSearch(Map whereMap, int page, int size) {
        Specification<ShrimpFarming> specification = createSpecification(whereMap);
        PageRequest pageRequest = PageRequest.of(page - 1, size);
        return shrimpFarmingDao.findAll(specification, pageRequest);
    }


    /**
     * 条件查询
     *
     * @param whereMap
     * @return
     */
    public List<ShrimpFarming> findSearch(Map whereMap) {
        Specification<ShrimpFarming> specification = createSpecification(whereMap);
        return shrimpFarmingDao.findAll(specification);
    }

    /**
     * 根据ID查询实体
     *
     * @param id
     * @return
     */
    public ShrimpFarming findById(String id) {
        return shrimpFarmingDao.findById(id).get();
    }

    /**
     * 增加
     *
     * @param shrimpFarming
     */
    public void add(ShrimpFarming shrimpFarming) {
        shrimpFarming.setId(idWorker.nextId() + "");
        Shrimp shrimp = shrimpDao.findByPondIdAndFishingStatus(shrimpFarming.getPondId(), "0");
        shrimpFarming.setShrimpId(shrimp.getId());
        shrimpFarmingDao.save(shrimpFarming);
    }

    /**
     * 修改
     *
     * @param shrimpFarming
     */
    public void update(ShrimpFarming shrimpFarming) {
        shrimpFarmingDao.save(shrimpFarming);
    }

    /**
     * 删除
     *
     * @param id
     */
    public void deleteById(String id) {
        shrimpFarmingDao.deleteById(id);
    }

    /**
     * 动态条件构建
     *
     * @param searchMap
     * @return
     */
    private Specification<ShrimpFarming> createSpecification(Map searchMap) {

        return new Specification<ShrimpFarming>() {

            @Override
            public Predicate toPredicate(Root<ShrimpFarming> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                List<Predicate> predicateList = new ArrayList<Predicate>();
                // id
                if (searchMap.get("id") != null && !"".equals(searchMap.get("id"))) {
                    predicateList.add(cb.like(root.get("id").as(String.class), "%" + (String) searchMap.get("id") + "%"));
                }
                // 虾苗编号
                if (searchMap.get("shrimpId") != null && !"".equals(searchMap.get("shrimpId"))) {
                    predicateList.add(cb.like(root.get("shrimpId").as(String.class), "%" + (String) searchMap.get("shrimpId") + "%"));
                }
                // 农资名称
                if (searchMap.get("suppliesName") != null && !"".equals(searchMap.get("suppliesName"))) {
                    predicateList.add(cb.like(root.get("suppliesName").as(String.class), "%" + (String) searchMap.get("suppliesName") + "%"));
                }
                // 池塘编号
                if (searchMap.get("pondId") != null && !"".equals(searchMap.get("pondId"))) {
                    predicateList.add(cb.like(root.get("pondId").as(String.class), "%" + (String) searchMap.get("pondId") + "%"));
                }
                // 使用人
                if (searchMap.get("applicator") != null && !"".equals(searchMap.get("applicator"))) {
                    predicateList.add(cb.like(root.get("applicator").as(String.class), "%" + (String) searchMap.get("applicator") + "%"));
                }
                // 操作类别
                if (searchMap.get("applyType") != null && !"".equals(searchMap.get("applyType"))) {
                    predicateList.add(cb.like(root.get("applyType").as(String.class), "%" + (String) searchMap.get("applyType") + "%"));
                }
                // 创建者
                if (searchMap.get("createBy") != null && !"".equals(searchMap.get("createBy"))) {
                    predicateList.add(cb.like(root.get("createBy").as(String.class), "%" + (String) searchMap.get("createBy") + "%"));
                }

                return cb.and(predicateList.toArray(new Predicate[predicateList.size()]));

            }
        };

    }

}
