package com.prawn.traceability.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
;
import org.springframework.data.jpa.domain.Specification;

import org.springframework.stereotype.Service;
import util.IdWorker;

import com.prawn.traceability.dao.InputcategoryinterDao;
import com.prawn.traceability.pojo.Inputcategoryinter;

/**
 * 服务层
 * 
 * @author Administrator
 *
 */
@Service
public class InputcategoryinterService {

	@Autowired
	private InputcategoryinterDao inputcategoryinterDao;
	
	@Autowired
	private IdWorker idWorker;

	/**
	 * 查询全部列表
	 * @return
	 */
	public List<Inputcategoryinter> findAll() {
		return inputcategoryinterDao.findAll();
	}

	
	/**
	 * 条件查询+分页
	 * @param whereMap
	 * @param page
	 * @param size
	 * @return
	 */
	public Page<Inputcategoryinter> findSearch(Map whereMap, int page, int size) {
		Specification<Inputcategoryinter> specification = createSpecification(whereMap);
		PageRequest pageRequest =  PageRequest.of(page-1, size);
		return inputcategoryinterDao.findAll(specification, pageRequest);
	}

	
	/**
	 * 条件查询
	 * @param whereMap
	 * @return
	 */
	public List<Inputcategoryinter> findSearch(Map whereMap) {
		Specification<Inputcategoryinter> specification = createSpecification(whereMap);
		return inputcategoryinterDao.findAll(specification);
	}

	/**
	 * 根据ID查询实体
	 * @param id
	 * @return
	 */
	public Inputcategoryinter findById(String id) {
		return inputcategoryinterDao.findById(id).get();
	}

	/**
	 * 增加
	 * @param inputcategoryinter
	 */
	public void add(Inputcategoryinter inputcategoryinter) {
		inputcategoryinter.setId( idWorker.nextId()+"" );
		inputcategoryinterDao.save(inputcategoryinter);
	}

	/**
	 * 修改
	 * @param inputcategoryinter
	 */
	public void update(Inputcategoryinter inputcategoryinter) {
		inputcategoryinterDao.save(inputcategoryinter);
	}

	/**
	 * 删除
	 * @param id
	 */
	public void deleteById(String id) {
		inputcategoryinterDao.deleteById(id);
	}

	/**
	 * 动态条件构建
	 * @param searchMap
	 * @return
	 */
	private Specification<Inputcategoryinter> createSpecification(Map searchMap) {

		return new Specification<Inputcategoryinter>() {

			@Override
			public Predicate toPredicate(Root<Inputcategoryinter> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<Predicate>();
                // ID
                if (searchMap.get("id")!=null && !"".equals(searchMap.get("id"))) {
                	predicateList.add(cb.like(root.get("id").as(String.class), "%"+(String)searchMap.get("id")+"%"));
                }
                // 二级类型
                if (searchMap.get("typeTwo")!=null && !"".equals(searchMap.get("typeTwo"))) {
                	predicateList.add(cb.like(root.get("typeTwo").as(String.class), "%"+(String)searchMap.get("typeTwo")+"%"));
                }
                // 一级分类id
                if (searchMap.get("firstLevelId")!=null && !"".equals(searchMap.get("firstLevelId"))) {
                	predicateList.add(cb.like(root.get("firstLevelId").as(String.class), "%"+(String)searchMap.get("firstLevelId")+"%"));
                }
				
				return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

			}
		};

	}

}
