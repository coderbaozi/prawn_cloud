package com.prawn.traceability.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import util.IdWorker;

import com.prawn.traceability.dao.ShrimpkindDao;
import com.prawn.traceability.pojo.Shrimpkind;

/**
 * 服务层
 * 
 * @author Administrator
 *
 */
@Service
public class ShrimpkindService {

	@Autowired
	private ShrimpkindDao shrimpkindDao;
	
	@Autowired
	private IdWorker idWorker;

	/**
	 * 查询全部列表
	 * @return
	 */
	public List<Shrimpkind> findAll() {
		return shrimpkindDao.findAll();
	}

	
	/**
	 * 条件查询+分页
	 * @param whereMap
	 * @param page
	 * @param size
	 * @return
	 */
	public Page<Shrimpkind> findSearch(Map whereMap, int page, int size) {
		Specification<Shrimpkind> specification = createSpecification(whereMap);
		PageRequest pageRequest =  PageRequest.of(page-1, size);
		return shrimpkindDao.findAll(specification, pageRequest);
	}

	
	/**
	 * 条件查询
	 * @param whereMap
	 * @return
	 */
	public List<Shrimpkind> findSearch(Map whereMap) {
		Specification<Shrimpkind> specification = createSpecification(whereMap);
		return shrimpkindDao.findAll(specification);
	}

	/**
	 * 根据ID查询实体
	 * @param id
	 * @return
	 */
	public Shrimpkind findById(String id) {
		return shrimpkindDao.findById(id).get();
	}

	/**
	 * 增加
	 * @param shrimpkind
	 */
	public void add(Shrimpkind shrimpkind) {
		shrimpkind.setId( idWorker.nextId()+"" );
		shrimpkindDao.save(shrimpkind);
	}

	/**
	 * 修改
	 * @param shrimpkind
	 */
	public void update(Shrimpkind shrimpkind) {
		shrimpkindDao.save(shrimpkind);
	}

	/**
	 * 删除
	 * @param id
	 */
	public void deleteById(String id) {
		shrimpkindDao.deleteById(id);
	}

	/**
	 * 动态条件构建
	 * @param searchMap
	 * @return
	 */
	private Specification<Shrimpkind> createSpecification(Map searchMap) {

		return new Specification<Shrimpkind>() {

			@Override
			public Predicate toPredicate(Root<Shrimpkind> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<Predicate>();
                // ID
                if (searchMap.get("id")!=null && !"".equals(searchMap.get("id"))) {
                	predicateList.add(cb.like(root.get("id").as(String.class), "%"+(String)searchMap.get("id")+"%"));
                }
                // 种类名称
                if (searchMap.get("kindName")!=null && !"".equals(searchMap.get("kindName"))) {
                	predicateList.add(cb.like(root.get("kindName").as(String.class), "%"+(String)searchMap.get("kindName")+"%"));
                }
				
				return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

			}
		};

	}

}
