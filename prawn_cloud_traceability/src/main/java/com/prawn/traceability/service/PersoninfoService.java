package com.prawn.traceability.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import util.IdWorker;

import com.prawn.traceability.dao.PersoninfoDao;
import com.prawn.traceability.pojo.Personinfo;

/**
 * 服务层
 * 
 * @author Administrator
 *
 */
@Service
public class PersoninfoService {

	@Autowired
	private PersoninfoDao personinfoDao;
	
	@Autowired
	private IdWorker idWorker;

	/**
	 * 查询全部列表
	 * @return
	 */
	public List<Personinfo> findAll() {
		return personinfoDao.findAll();
	}

	
	/**
	 * 条件查询+分页
	 * @param whereMap
	 * @param page
	 * @param size
	 * @return
	 */
	public Page<Personinfo> findSearch(Map whereMap, int page, int size) {
		Specification<Personinfo> specification = createSpecification(whereMap);
		PageRequest pageRequest =  PageRequest.of(page-1, size);
		return personinfoDao.findAll(specification, pageRequest);
	}

	
	/**
	 * 条件查询
	 * @param whereMap
	 * @return
	 */
	public List<Personinfo> findSearch(Map whereMap) {
		Specification<Personinfo> specification = createSpecification(whereMap);
		return personinfoDao.findAll(specification);
	}

	/**
	 * 根据ID查询实体
	 * @param id
	 * @return
	 */
	public Personinfo findById(String id) {
		return personinfoDao.findById(id).get();
	}

	/**
	 * 增加
	 * @param personinfo
	 */
	public void add(Personinfo personinfo) {
		personinfo.setId( idWorker.nextId()+"" );
		personinfoDao.save(personinfo);
	}

	/**
	 * 修改
	 * @param personinfo
	 */
	public void update(Personinfo personinfo) {
		personinfoDao.save(personinfo);
	}

	/**
	 * 删除
	 * @param id
	 */
	public void deleteById(String id) {
		personinfoDao.deleteById(id);
	}

	/**
	 * 动态条件构建
	 * @param searchMap
	 * @return
	 */
	private Specification<Personinfo> createSpecification(Map searchMap) {

		return new Specification<Personinfo>() {

			@Override
			public Predicate toPredicate(Root<Personinfo> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<Predicate>();
                // 人员编号
                if (searchMap.get("id")!=null && !"".equals(searchMap.get("id"))) {
                	predicateList.add(cb.like(root.get("id").as(String.class), "%"+(String)searchMap.get("id")+"%"));
                }
                // 姓名
                if (searchMap.get("personName")!=null && !"".equals(searchMap.get("personName"))) {
                	predicateList.add(cb.like(root.get("personName").as(String.class), "%"+(String)searchMap.get("personName")+"%"));
                }
                // 性别
                if (searchMap.get("sex")!=null && !"".equals(searchMap.get("sex"))) {
                	predicateList.add(cb.like(root.get("sex").as(String.class), "%"+(String)searchMap.get("sex")+"%"));
                }
                // 政治面貌
                if (searchMap.get("politicalStatus")!=null && !"".equals(searchMap.get("politicalStatus"))) {
                	predicateList.add(cb.like(root.get("politicalStatus").as(String.class), "%"+(String)searchMap.get("politicalStatus")+"%"));
                }
                // 工种类别
                if (searchMap.get("workType")!=null && !"".equals(searchMap.get("workType"))) {
                	predicateList.add(cb.like(root.get("workType").as(String.class), "%"+(String)searchMap.get("workType")+"%"));
                }
                // 联系电话
                if (searchMap.get("mobile")!=null && !"".equals(searchMap.get("mobile"))) {
                	predicateList.add(cb.like(root.get("mobile").as(String.class), "%"+(String)searchMap.get("mobile")+"%"));
                }
                // 基地编号
                if (searchMap.get("baseId")!=null && !"".equals(searchMap.get("baseId"))) {
                	predicateList.add(cb.like(root.get("baseId").as(String.class), "%"+(String)searchMap.get("baseId")+"%"));
                }
				
				return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

			}
		};

	}

}
