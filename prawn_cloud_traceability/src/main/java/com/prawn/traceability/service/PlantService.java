package com.prawn.traceability.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import com.prawn.traceability.dao.AdultShrimpDao;
import com.prawn.traceability.pojo.AdultShrimp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;
import util.IdWorker;

import com.prawn.traceability.dao.PlantDao;
import com.prawn.traceability.pojo.Plant;

/**
 * 服务层
 *
 * @author Administrator
 */
@Service
public class PlantService {

    @Autowired
    private PlantDao plantDao;

    @Autowired
    private IdWorker idWorker;

    @Autowired
    private AdultShrimpDao adultShrimpDao;

    @Resource
    private RedisTemplate redisTemplate;

    /**
     * 查询全部列表
     *
     * @return
     */
    public List<Plant> findAll() {
        return plantDao.findAll();
    }


    /**
     * 条件查询+分页
     *
     * @param whereMap
     * @param page
     * @param size
     * @return
     */
    public Page<Plant> findSearch(Map whereMap, int page, int size) {
        Specification<Plant> specification = createSpecification(whereMap);
        PageRequest pageRequest = PageRequest.of(page - 1, size);
        return plantDao.findAll(specification, pageRequest);
    }


    /**
     * 条件查询
     *
     * @param whereMap
     * @return
     */
    public List<Plant> findSearch(Map whereMap) {
        Specification<Plant> specification = createSpecification(whereMap);
        return plantDao.findAll(specification);
    }

    /**
     * 根据ID查询实体
     *
     * @param id
     * @return
     */
    public Plant findById(String id) {
        return plantDao.findById(id).get();
    }

    /**
     * 增加
     *
     * @param plant
     */
    @Transactional
    public void add(Plant plant) {
        String plantId = idWorker.nextId() + "";
        plant.setId(plantId);
        //把工厂id同步到成虾表中
        adultShrimpDao.updatePlantId(plantId, plant.getAdultShrimpId());
        plantDao.save(plant);
    }

    /**
     * 修改
     *
     * @param plant
     */
    public void update(Plant plant) {
        redisTemplate.delete("adultShrimp_" + plant.getAdultShrimpId());
        plantDao.save(plant);
    }

    /**
     * 删除
     *
     * @param id
     */
    @Transactional
    public void deleteById(String id) {
        Plant plant = plantDao.findById(id).get();
        adultShrimpDao.updatePlantId("", plant.getAdultShrimpId());
        plantDao.deleteById(id);
    }

    /**
     * 动态条件构建
     *
     * @param searchMap
     * @return
     */
    private Specification<Plant> createSpecification(Map searchMap) {

        return new Specification<Plant>() {

            @Override
            public Predicate toPredicate(Root<Plant> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                List<Predicate> predicateList = new ArrayList<Predicate>();
                // 厂家编号
                if (searchMap.get("id") != null && !"".equals(searchMap.get("id"))) {
                    predicateList.add(cb.like(root.get("id").as(String.class), "%" + (String) searchMap.get("id") + "%"));
                }
                // 厂家名称
                if (searchMap.get("plantName") != null && !"".equals(searchMap.get("plantName"))) {
                    predicateList.add(cb.like(root.get("plantName").as(String.class), "%" + (String) searchMap.get("plantName") + "%"));
                }
                // 厂家地址经度
                if (searchMap.get("plantPositionLongitude") != null && !"".equals(searchMap.get("plantPositionLongitude"))) {
                    predicateList.add(cb.like(root.get("plantPositionLongitude").as(String.class), "%" + (String) searchMap.get("plantPositionLongitude") + "%"));
                }
                // 厂家地址纬度
                if (searchMap.get("plantPositionLatitude") != null && !"".equals(searchMap.get("plantPositionLatitude"))) {
                    predicateList.add(cb.like(root.get("plantPositionLatitude").as(String.class), "%" + (String) searchMap.get("plantPositionLatitude") + "%"));
                }
                // 成虾编号
                if (searchMap.get("adultShrimpId") != null && !"".equals(searchMap.get("adultShrimpId"))) {
                    predicateList.add(cb.like(root.get("adultShrimpId").as(String.class), "%" + (String) searchMap.get("adultShrimpId") + "%"));
                }
                // 产品名称
                if (searchMap.get("productName") != null && !"".equals(searchMap.get("productName"))) {
                    predicateList.add(cb.like(root.get("productName").as(String.class), "%" + (String) searchMap.get("productName") + "%"));
                }
                // 创建者
                if (searchMap.get("createBy") != null && !"".equals(searchMap.get("createBy"))) {
                    predicateList.add(cb.like(root.get("createBy").as(String.class), "%" + (String) searchMap.get("createBy") + "%"));
                }

                //基地id
                if (searchMap.get("baseId") != null && !"".equals(searchMap.get("baseId"))) {
                    predicateList.add(cb.like(root.get("baseId").as(String.class), "%" + (String) searchMap.get("baseId") + "%"));
                }

                return cb.and(predicateList.toArray(new Predicate[predicateList.size()]));

            }
        };

    }

}
