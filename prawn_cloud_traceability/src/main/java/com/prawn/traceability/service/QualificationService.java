package com.prawn.traceability.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import util.IdWorker;

import com.prawn.traceability.dao.QualificationDao;
import com.prawn.traceability.pojo.Qualification;

/**
 * 服务层
 *
 * @author Administrator
 */
@Service
public class QualificationService {

    @Autowired
    private QualificationDao qualificationDao;

    @Autowired
    private IdWorker idWorker;

    /**
     * 查询全部列表
     *
     * @return
     */
    public List<Qualification> findAll() {
        return qualificationDao.findAll();
    }


    /**
     * 条件查询+分页
     *
     * @param whereMap
     * @param page
     * @param size
     * @return
     */
    public Page<Qualification> findSearch(Map whereMap, int page, int size) {
        Specification<Qualification> specification = createSpecification(whereMap);
        PageRequest pageRequest = PageRequest.of(page - 1, size);
        return qualificationDao.findAll(specification, pageRequest);
    }


    /**
     * 条件查询
     *
     * @param whereMap
     * @return
     */
    public List<Qualification> findSearch(Map whereMap) {
        Specification<Qualification> specification = createSpecification(whereMap);
        return qualificationDao.findAll(specification);
    }

    /**
     * 根据ID查询实体
     *
     * @param id
     * @return
     */
    public Qualification findById(String id) {
        return qualificationDao.findById(id).get();
    }

    /**
     * 增加
     *
     * @param qualification
     */
    public void add(Qualification qualification) {
        if (qualification.getId() == null || "".equals(qualification.getId())) {
            qualification.setId(idWorker.nextId() + "");
        }
        qualificationDao.save(qualification);
    }

    /**
     * 修改
     *
     * @param qualification
     */
    public void update(Qualification qualification) {
        qualificationDao.save(qualification);
    }

    /**
     * 删除
     *
     * @param id
     */
    public void deleteById(String id) {
        qualificationDao.deleteById(id);
    }

    /**
     * 动态条件构建
     *
     * @param searchMap
     * @return
     */
    private Specification<Qualification> createSpecification(Map searchMap) {

        return new Specification<Qualification>() {

            @Override
            public Predicate toPredicate(Root<Qualification> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                List<Predicate> predicateList = new ArrayList<Predicate>();
                // ID
                if (searchMap.get("id") != null && !"".equals(searchMap.get("id"))) {
                    predicateList.add(cb.like(root.get("id").as(String.class), "%" + (String) searchMap.get("id") + "%"));
                }
                // 产品名称
                if (searchMap.get("productName") != null && !"".equals(searchMap.get("productName"))) {
                    predicateList.add(cb.like(root.get("productName").as(String.class), "%" + (String) searchMap.get("productName") + "%"));
                }
                // 质检部门检验报告
                if (searchMap.get("inspectionReport") != null && !"".equals(searchMap.get("inspectionReport"))) {
                    predicateList.add(cb.like(root.get("inspectionReport").as(String.class), "%" + (String) searchMap.get("inspectionReport") + "%"));
                }
                // 质量管理体系认证书
                if (searchMap.get("qualityCertificate") != null && !"".equals(searchMap.get("qualityCertificate"))) {
                    predicateList.add(cb.like(root.get("qualityCertificate").as(String.class), "%" + (String) searchMap.get("qualityCertificate") + "%"));
                }
                // 食品安全管理体系认证书
                if (searchMap.get("safetyCertificate") != null && !"".equals(searchMap.get("safetyCertificate"))) {
                    predicateList.add(cb.like(root.get("safetyCertificate").as(String.class), "%" + (String) searchMap.get("safetyCertificate") + "%"));
                }
                // 产品生产许可证
                if (searchMap.get("license") != null && !"".equals(searchMap.get("license"))) {
                    predicateList.add(cb.like(root.get("license").as(String.class), "%" + (String) searchMap.get("license") + "%"));
                }
                // 产品信息
                if (searchMap.get("productInfo") != null && !"".equals(searchMap.get("productInfo"))) {
                    predicateList.add(cb.like(root.get("productInfo").as(String.class), "%" + (String) searchMap.get("productInfo") + "%"));
                }
                // 采用标准
                if (searchMap.get("standard") != null && !"".equals(searchMap.get("standard"))) {
                    predicateList.add(cb.like(root.get("standard").as(String.class), "%" + (String) searchMap.get("standard") + "%"));
                }
                // 基地id
                if (searchMap.get("baseId") != null && !"".equals(searchMap.get("baseId"))) {
                    predicateList.add(cb.like(root.get("baseId").as(String.class), "%" + (String) searchMap.get("baseId") + "%"));
                }

                return cb.and(predicateList.toArray(new Predicate[predicateList.size()]));

            }
        };

    }

}
