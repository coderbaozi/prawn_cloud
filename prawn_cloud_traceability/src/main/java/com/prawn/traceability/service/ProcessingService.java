package com.prawn.traceability.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import util.IdWorker;

import com.prawn.traceability.dao.ProcessingDao;
import com.prawn.traceability.pojo.Processing;

/**
 * 服务层
 * 
 * @author Administrator
 *
 */
@Service
public class ProcessingService {

	@Autowired
	private ProcessingDao processingDao;
	
	@Autowired
	private IdWorker idWorker;

	/**
	 * 查询全部列表
	 * @return
	 */
	public List<Processing> findAll() {
		return processingDao.findAll();
	}

	
	/**
	 * 条件查询+分页
	 * @param whereMap
	 * @param page
	 * @param size
	 * @return
	 */
	public Page<Processing> findSearch(Map whereMap, int page, int size) {
		Specification<Processing> specification = createSpecification(whereMap);
		PageRequest pageRequest =  PageRequest.of(page-1, size);
		return processingDao.findAll(specification, pageRequest);
	}

	
	/**
	 * 条件查询
	 * @param whereMap
	 * @return
	 */
	public List<Processing> findSearch(Map whereMap) {
		Specification<Processing> specification = createSpecification(whereMap);
		return processingDao.findAll(specification);
	}

	/**
	 * 根据ID查询实体
	 * @param id
	 * @return
	 */
	public Processing findById(String id) {
		return processingDao.findById(id).get();
	}

	/**
	 * 增加
	 * @param processing
	 */
	public void add(Processing processing) {
		processing.setId( idWorker.nextId()+"" );
		processingDao.save(processing);
	}

	/**
	 * 修改
	 * @param processing
	 */
	public void update(Processing processing) {
		processingDao.save(processing);
	}

	/**
	 * 删除
	 * @param id
	 */
	public void deleteById(String id) {
		processingDao.deleteById(id);
	}

	/**
	 * 动态条件构建
	 * @param searchMap
	 * @return
	 */
	private Specification<Processing> createSpecification(Map searchMap) {

		return new Specification<Processing>() {

			@Override
			public Predicate toPredicate(Root<Processing> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<Predicate>();
                // id
                if (searchMap.get("id")!=null && !"".equals(searchMap.get("id"))) {
                	predicateList.add(cb.like(root.get("id").as(String.class), "%"+(String)searchMap.get("id")+"%"));
                }
                // 工艺名称
                if (searchMap.get("processName")!=null && !"".equals(searchMap.get("processName"))) {
                	predicateList.add(cb.like(root.get("processName").as(String.class), "%"+(String)searchMap.get("processName")+"%"));
                }
                // 责任人
                if (searchMap.get("responsible")!=null && !"".equals(searchMap.get("responsible"))) {
                	predicateList.add(cb.like(root.get("responsible").as(String.class), "%"+(String)searchMap.get("responsible")+"%"));
                }
                // 工艺步骤/环节
                if (searchMap.get("step")!=null && !"".equals(searchMap.get("step"))) {
                	predicateList.add(cb.like(root.get("step").as(String.class), "%"+(String)searchMap.get("step")+"%"));
                }
                // 工艺描述
                if (searchMap.get("description")!=null && !"".equals(searchMap.get("description"))) {
                	predicateList.add(cb.like(root.get("description").as(String.class), "%"+(String)searchMap.get("description")+"%"));
                }
                // 工艺图片
                if (searchMap.get("processPic")!=null && !"".equals(searchMap.get("processPic"))) {
                	predicateList.add(cb.like(root.get("processPic").as(String.class), "%"+(String)searchMap.get("processPic")+"%"));
                }
                // 产品id
                if (searchMap.get("plantId")!=null && !"".equals(searchMap.get("plantId"))) {
                	predicateList.add(cb.like(root.get("plantId").as(String.class), "%"+(String)searchMap.get("plantId")+"%"));
                }
				// 投入品信息id
				if (searchMap.get("suppliesId")!=null && !"".equals(searchMap.get("suppliesId"))) {
					predicateList.add(cb.like(root.get("suppliesId").as(String.class), "%"+(String)searchMap.get("suppliesId")+"%"));
				}
				// 二级类型id
				if (searchMap.get("typeId")!=null && !"".equals(searchMap.get("typeId"))) {
					predicateList.add(cb.like(root.get("typeId").as(String.class), "%"+(String)searchMap.get("typeId")+"%"));
				}
				
				return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

			}
		};

	}

}
