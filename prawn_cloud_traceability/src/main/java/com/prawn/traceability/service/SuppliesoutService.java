package com.prawn.traceability.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import com.prawn.traceability.dao.SuppliesinDao;
import com.prawn.traceability.pojo.Suppliesin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;
import util.IdWorker;

import com.prawn.traceability.dao.SuppliesoutDao;
import com.prawn.traceability.pojo.Suppliesout;

/**
 * 服务层
 *
 * @author Administrator
 */
@Service
@Transactional
public class SuppliesoutService {
    @Autowired
    private SuppliesoutDao suppliesoutDao;

    @Autowired
    private SuppliesinDao suppliesinDao;

    @Autowired
    private IdWorker idWorker;

    /**
     * 查询全部列表
     *
     * @return
     */
    public List<Suppliesout> findAll() {
        return suppliesoutDao.findAll();
    }


    /**
     * 条件查询+分页
     *
     * @param whereMap
     * @param page
     * @param size
     * @return
     */
    public Page<Suppliesout> findSearch(Map whereMap, int page, int size) {
        Specification<Suppliesout> specification = createSpecification(whereMap);
        PageRequest pageRequest = PageRequest.of(page - 1, size);
        return suppliesoutDao.findAll(specification, pageRequest);
    }


    /**
     * 条件查询
     *
     * @param whereMap
     * @return
     */
    public List<Suppliesout> findSearch(Map whereMap) {
        Specification<Suppliesout> specification = createSpecification(whereMap);
        return suppliesoutDao.findAll(specification);
    }

    /**
     * 根据ID查询实体
     *
     * @param id
     * @return
     */
    public Suppliesout findById(String id) {
        return suppliesoutDao.findById(id).get();
    }

    /**
     * 增加
     *
     * @param suppliesout
     */
    public void add(Suppliesout suppliesout) {
        Suppliesin suppliesin = suppliesinDao.findById(suppliesout.getSuppliesInId()).get();
        //修改库存剩余数量
        suppliesin.setRemainNumber(suppliesin.getRemainNumber() - suppliesout.getOutNumber());

        //信息同步到出库信息数据库
        suppliesout.setSuppliesName(suppliesin.getSuppliesName());
        suppliesout.setWarehouse(suppliesin.getWarehouse());

        suppliesout.setId(idWorker.nextId() + "");
        suppliesoutDao.save(suppliesout);
        suppliesinDao.save(suppliesin);
    }

    /**
     * 修改
     * 仅修改出库数量、出库日期、操作人
     * @param suppliesout
     */
    public void update(Suppliesout suppliesout) {
        Suppliesin suppliesin = suppliesinDao.findById(suppliesout.getSuppliesInId()).get();
        Double oldOutNumber = suppliesoutDao.getOutNumberById(suppliesout.getId());
        //同步库存剩余量
        suppliesin.setRemainNumber(suppliesin.getRemainNumber() + oldOutNumber - suppliesout.getOutNumber());

        suppliesoutDao.save(suppliesout);
        suppliesinDao.save(suppliesin);
    }

    /**
     * 删除
     *
     * @param id
     */
    public void deleteById(String id) {
        suppliesoutDao.deleteById(id);
    }

    /**
     * 动态条件构建
     *
     * @param searchMap
     * @return
     */
    private Specification<Suppliesout> createSpecification(Map searchMap) {

        return new Specification<Suppliesout>() {

            @Override
            public Predicate toPredicate(Root<Suppliesout> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                List<Predicate> predicateList = new ArrayList<Predicate>();
                // 农资编号
                if (searchMap.get("id") != null && !"".equals(searchMap.get("id"))) {
                    predicateList.add(cb.like(root.get("id").as(String.class), "%" + (String) searchMap.get("id") + "%"));
                }
                // 农资名称
                if (searchMap.get("suppliesName") != null && !"".equals(searchMap.get("suppliesName"))) {
                    predicateList.add(cb.like(root.get("suppliesName").as(String.class), "%" + (String) searchMap.get("suppliesName") + "%"));
                }
                // 操作人
                if (searchMap.get("outOperator") != null && !"".equals(searchMap.get("outOperator"))) {
                    predicateList.add(cb.like(root.get("outOperator").as(String.class), "%" + (String) searchMap.get("outOperator") + "%"));
                }
                // 仓库号
                if (searchMap.get("warehouse") != null && !"".equals(searchMap.get("warehouse"))) {
                    predicateList.add(cb.like(root.get("warehouse").as(String.class), "%" + (String) searchMap.get("warehouse") + "%"));
                }
                // 基地编号
                if (searchMap.get("baseId") != null && !"".equals(searchMap.get("baseId"))) {
                    predicateList.add(cb.like(root.get("baseId").as(String.class), "%" + (String) searchMap.get("baseId") + "%"));
                }
                // 基地编号
                if (searchMap.get("type") != null && !"".equals(searchMap.get("type"))) {
                    predicateList.add(cb.like(root.get("type").as(String.class), "%" + (String) searchMap.get("type") + "%"));
                }

                return cb.and(predicateList.toArray(new Predicate[predicateList.size()]));

            }
        };

    }

}
