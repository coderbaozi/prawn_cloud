package com.prawn.traceability.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import util.IdWorker;

import com.prawn.traceability.dao.ShrimpManagementDao;
import com.prawn.traceability.pojo.ShrimpManagement;

/**
 * 服务层
 * 
 * @author Administrator
 *
 */
@Service
public class ShrimpManagementService {

	@Autowired
	private ShrimpManagementDao shrimpManagementDao;
	
	@Autowired
	private IdWorker idWorker;

	/**
	 * 查询全部列表
	 * @return
	 */
	public List<ShrimpManagement> findAll() {
		return shrimpManagementDao.findAll();
	}

	
	/**
	 * 条件查询+分页
	 * @param whereMap
	 * @param page
	 * @param size
	 * @return
	 */
	public Page<ShrimpManagement> findSearch(Map whereMap, int page, int size) {
		Specification<ShrimpManagement> specification = createSpecification(whereMap);
		PageRequest pageRequest =  PageRequest.of(page-1, size);
		return shrimpManagementDao.findAll(specification, pageRequest);
	}

	
	/**
	 * 条件查询
	 * @param whereMap
	 * @return
	 */
	public List<ShrimpManagement> findSearch(Map whereMap) {
		Specification<ShrimpManagement> specification = createSpecification(whereMap);
		return shrimpManagementDao.findAll(specification);
	}

	/**
	 * 根据ID查询实体
	 * @param id
	 * @return
	 */
	public ShrimpManagement findById(String id) {
		return shrimpManagementDao.findById(id).get();
	}

	/**
	 * 增加
	 * @param shrimpManagement
	 */
	public void add(ShrimpManagement shrimpManagement) {
		shrimpManagement.setId( idWorker.nextId()+"" );
		shrimpManagement.setYield(0.0);
		shrimpManagementDao.save(shrimpManagement);
	}

	/**
	 * 修改
	 * @param shrimpManagement
	 */
	public void update(ShrimpManagement shrimpManagement) {
		shrimpManagementDao.save(shrimpManagement);
	}

	/**
	 * 删除
	 * @param id
	 */
	public void deleteById(String id) {
		shrimpManagementDao.deleteById(id);
	}

	/**
	 * 动态条件构建
	 * @param searchMap
	 * @return
	 */
	private Specification<ShrimpManagement> createSpecification(Map searchMap) {

		return new Specification<ShrimpManagement>() {

			@Override
			public Predicate toPredicate(Root<ShrimpManagement> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<Predicate>();
                // ID
                if (searchMap.get("id")!=null && !"".equals(searchMap.get("id"))) {
                	predicateList.add(cb.like(root.get("id").as(String.class), "%"+(String)searchMap.get("id")+"%"));
                }
                // 虾苗批次名称
                if (searchMap.get("shrimpBatchName")!=null && !"".equals(searchMap.get("shrimpBatchName"))) {
                	predicateList.add(cb.like(root.get("shrimpBatchName").as(String.class), "%"+(String)searchMap.get("shrimpBatchName")+"%"));
                }
                // 虾苗品种
                if (searchMap.get("shrimpSpecies")!=null && !"".equals(searchMap.get("shrimpSpecies"))) {
                	predicateList.add(cb.like(root.get("shrimpSpecies").as(String.class), "%"+(String)searchMap.get("shrimpSpecies")+"%"));
                }
                // 虾苗产地
                if (searchMap.get("shrimpOrigin")!=null && !"".equals(searchMap.get("shrimpOrigin"))) {
                	predicateList.add(cb.like(root.get("shrimpOrigin").as(String.class), "%"+(String)searchMap.get("shrimpOrigin")+"%"));
                }
                // 虾苗供应商
                if (searchMap.get("shrimpSupplier")!=null && !"".equals(searchMap.get("shrimpSupplier"))) {
                	predicateList.add(cb.like(root.get("shrimpSupplier").as(String.class), "%"+(String)searchMap.get("shrimpSupplier")+"%"));
                }
                // 基地编号
                if (searchMap.get("baseId")!=null && !"".equals(searchMap.get("baseId"))) {
                	predicateList.add(cb.like(root.get("baseId").as(String.class), "%"+(String)searchMap.get("baseId")+"%"));
                }
                // 创建者
                if (searchMap.get("createBy")!=null && !"".equals(searchMap.get("createBy"))) {
                	predicateList.add(cb.like(root.get("createBy").as(String.class), "%"+(String)searchMap.get("createBy")+"%"));
                }
				
				return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

			}
		};

	}

}
