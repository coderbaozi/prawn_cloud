package com.prawn.traceability.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import util.IdWorker;

import com.prawn.traceability.dao.PicDao;
import com.prawn.traceability.pojo.Pic;

/**
 * 服务层
 * 
 * @author Administrator
 *
 */
@Service
public class PicService {

	@Autowired
	private PicDao picDao;
	
	@Autowired
	private IdWorker idWorker;

	/**
	 * 查询全部列表
	 * @return
	 */
	public List<Pic> findAll() {
		return picDao.findAll();
	}

	
	/**
	 * 条件查询+分页
	 * @param whereMap
	 * @param page
	 * @param size
	 * @return
	 */
	public Page<Pic> findSearch(Map whereMap, int page, int size) {
		Specification<Pic> specification = createSpecification(whereMap);
		PageRequest pageRequest =  PageRequest.of(page-1, size);
		return picDao.findAll(specification, pageRequest);
	}

	
	/**
	 * 条件查询
	 * @param whereMap
	 * @return
	 */
	public List<Pic> findSearch(Map whereMap) {
		Specification<Pic> specification = createSpecification(whereMap);
		return picDao.findAll(specification);
	}

	/**
	 * 根据ID查询实体
	 * @param id
	 * @return
	 */
	public Pic findById(String id) {
		return picDao.findById(id).get();
	}

	/**
	 * 增加
	 * @param pic
	 */
	public void add(Pic pic) {
		pic.setId( idWorker.nextId()+"" );
		picDao.save(pic);
	}

	/**
	 * 修改
	 * @param pic
	 */
	public void update(Pic pic) {
		picDao.save(pic);
	}

	/**
	 * 删除
	 * @param id
	 */
	public void deleteById(String id) {
		picDao.deleteById(id);
	}

	/**
	 * 动态条件构建
	 * @param searchMap
	 * @return
	 */
	private Specification<Pic> createSpecification(Map searchMap) {

		return new Specification<Pic>() {

			@Override
			public Predicate toPredicate(Root<Pic> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<Predicate>();
                // 
                if (searchMap.get("id")!=null && !"".equals(searchMap.get("id"))) {
                	predicateList.add(cb.like(root.get("id").as(String.class), "%"+(String)searchMap.get("id")+"%"));
                }
                // 
                if (searchMap.get("pictures")!=null && !"".equals(searchMap.get("pictures"))) {
                	predicateList.add(cb.like(root.get("pictures").as(String.class), "%"+(String)searchMap.get("pictures")+"%"));
                }
                // 
                if (searchMap.get("associationId")!=null && !"".equals(searchMap.get("associationId"))) {
                	predicateList.add(cb.like(root.get("associationId").as(String.class), "%"+(String)searchMap.get("associationId")+"%"));
                }
				if (searchMap.get("picName")!=null && !"".equals(searchMap.get("picName"))) {
					predicateList.add(cb.like(root.get("picName").as(String.class), "%"+(String)searchMap.get("picName")+"%"));
				}
				
				return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

			}
		};

	}

}
