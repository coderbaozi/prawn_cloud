package com.prawn.traceability.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import util.IdWorker;

import com.prawn.traceability.dao.CustomerDao;
import com.prawn.traceability.pojo.Customer;

/**
 * 服务层
 * 
 * @author Administrator
 *
 */
@Service
public class CustomerService {

	@Autowired
	private CustomerDao customerDao;
	
	@Autowired
	private IdWorker idWorker;

	/**
	 * 查询全部列表
	 * @return
	 */
	public List<Customer> findAll() {
		return customerDao.findAll();
	}

	
	/**
	 * 条件查询+分页
	 * @param whereMap
	 * @param page
	 * @param size
	 * @return
	 */
	public Page<Customer> findSearch(Map whereMap, int page, int size) {
		Specification<Customer> specification = createSpecification(whereMap);
		PageRequest pageRequest =  PageRequest.of(page-1, size);
		return customerDao.findAll(specification, pageRequest);
	}

	
	/**
	 * 条件查询
	 * @param whereMap
	 * @return
	 */
	public List<Customer> findSearch(Map whereMap) {
		Specification<Customer> specification = createSpecification(whereMap);
		return customerDao.findAll(specification);
	}

	/**
	 * 根据ID查询实体
	 * @param id
	 * @return
	 */
	public Customer findById(String id) {
		return customerDao.findById(id).get();
	}

	/**
	 * 增加
	 * @param customer
	 */
	public void add(Customer customer) {
		customer.setId( idWorker.nextId()+"" );
		customerDao.save(customer);
	}

	/**
	 * 修改
	 * @param customer
	 */
	public void update(Customer customer) {
		customerDao.save(customer);
	}

	/**
	 * 删除
	 * @param id
	 */
	public void deleteById(String id) {
		customerDao.deleteById(id);
	}

	/**
	 * 动态条件构建
	 * @param searchMap
	 * @return
	 */
	private Specification<Customer> createSpecification(Map searchMap) {

		return new Specification<Customer>() {

			@Override
			public Predicate toPredicate(Root<Customer> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<Predicate>();
                // Id
                if (searchMap.get("id")!=null && !"".equals(searchMap.get("id"))) {
                	predicateList.add(cb.like(root.get("id").as(String.class), "%"+(String)searchMap.get("id")+"%"));
                }
                // 客户名
                if (searchMap.get("customerName")!=null && !"".equals(searchMap.get("customerName"))) {
                	predicateList.add(cb.like(root.get("customerName").as(String.class), "%"+(String)searchMap.get("customerName")+"%"));
                }
                // 客户类型
                if (searchMap.get("customerType")!=null && !"".equals(searchMap.get("customerType"))) {
                	predicateList.add(cb.like(root.get("customerType").as(String.class), "%"+(String)searchMap.get("customerType")+"%"));
                }
                // 联系人
                if (searchMap.get("contactPerson")!=null && !"".equals(searchMap.get("contactPerson"))) {
                	predicateList.add(cb.like(root.get("contactPerson").as(String.class), "%"+(String)searchMap.get("contactPerson")+"%"));
                }
                // 联系电话
                if (searchMap.get("phoneNumber")!=null && !"".equals(searchMap.get("phoneNumber"))) {
                	predicateList.add(cb.like(root.get("phoneNumber").as(String.class), "%"+(String)searchMap.get("phoneNumber")+"%"));
                }
                // 邮箱
                if (searchMap.get("email")!=null && !"".equals(searchMap.get("email"))) {
                	predicateList.add(cb.like(root.get("email").as(String.class), "%"+(String)searchMap.get("email")+"%"));
                }
                // 收货地址
                if (searchMap.get("receiptAddress")!=null && !"".equals(searchMap.get("receiptAddress"))) {
                	predicateList.add(cb.like(root.get("receiptAddress").as(String.class), "%"+(String)searchMap.get("receiptAddress")+"%"));
                }
                // 收货地址经度
                if (searchMap.get("addressLongitude")!=null && !"".equals(searchMap.get("addressLongitude"))) {
                	predicateList.add(cb.like(root.get("addressLongitude").as(String.class), "%"+(String)searchMap.get("addressLongitude")+"%"));
                }
                // 收货地址纬度
                if (searchMap.get("addressLatitude")!=null && !"".equals(searchMap.get("addressLatitude"))) {
                	predicateList.add(cb.like(root.get("addressLatitude").as(String.class), "%"+(String)searchMap.get("addressLatitude")+"%"));
                }
				// 基地id
				if (searchMap.get("baseId")!=null && !"".equals(searchMap.get("baseId"))) {
					predicateList.add(cb.like(root.get("baseId").as(String.class), "%"+(String)searchMap.get("baseId")+"%"));
				}
				return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

			}
		};

	}

}
