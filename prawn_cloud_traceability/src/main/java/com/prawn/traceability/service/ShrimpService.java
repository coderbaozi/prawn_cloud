package com.prawn.traceability.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.prawn.traceability.dao.SpeciesDao;
import com.prawn.traceability.pojo.Shrimp;
import entity.Species;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import util.IdWorker;

import com.prawn.traceability.dao.ShrimpDao;


/**
 * 服务层
 *
 * @author Administrator
 */
@Service
public class ShrimpService {

    @Autowired
    private ShrimpDao shrimpDao;

    @Autowired
    private IdWorker idWorker;

    @Autowired
    private SpeciesDao speciesDao;

    /**
     * 查询大数据模块所需数据
     *
     * @param baseId
     * @return
     */
    public List<Species> findSpeciesForBD(String baseId) {
        return speciesDao.findSpeciesForBD(baseId);
    }

    /**
     * 查询全部列表
     *
     * @return
     */
    public List<Shrimp> findAll() {
        return shrimpDao.findAll();
    }


    /**
     * 条件查询+分页
     *
     * @param whereMap
     * @param page
     * @param size
     * @return
     */
    public Page<Shrimp> findSearch(Map whereMap, int page, int size) {
        Specification<Shrimp> specification = createSpecification(whereMap);
        Sort sort = new Sort(Sort.Direction.DESC, "fishingTime", "fishingStatus");
        PageRequest pageRequest = PageRequest.of(page - 1, size, sort);
        return shrimpDao.findAll(specification, pageRequest);
    }


    /**
     * 条件查询
     *
     * @param whereMap
     * @return
     */
    public List<Shrimp> findSearch(Map whereMap) {
        Specification<Shrimp> specification = createSpecification(whereMap);
        return shrimpDao.findAll(specification);
    }

    /**
     * 根据ID查询实体
     *
     * @param id
     * @return
     */
    public Shrimp findById(String id) {
        return shrimpDao.findById(id).get();
    }

    /**
     * 增加
     *
     * @param shrimp
     */
    public void add(Shrimp shrimp) {
        shrimp.setId(idWorker.nextId() + "");
        shrimpDao.save(shrimp);
    }

    /**
     * 修改
     *
     * @param shrimp
     */
    public void update(Shrimp shrimp) {
        shrimpDao.save(shrimp);
    }

    /**
     * 删除
     *
     * @param id
     */
    public void deleteById(String id) {
        shrimpDao.deleteById(id);
    }

    /**
     * 动态条件构建
     *
     * @param searchMap
     * @return
     */
    private Specification<Shrimp> createSpecification(Map searchMap) {

        return new Specification<Shrimp>() {

            @Override
            public Predicate toPredicate(Root<Shrimp> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                List<Predicate> predicateList = new ArrayList<Predicate>();
                // 虾苗编号
                if (searchMap.get("id") != null && !"".equals(searchMap.get("id"))) {
                    predicateList.add(cb.like(root.get("id").as(String.class), "%" + (String) searchMap.get("id") + "%"));
                }
                // 虾苗批次名称
                if (searchMap.get("shrimpBatchName") != null && !"".equals(searchMap.get("shrimpBatchName"))) {
                    predicateList.add(cb.like(root.get("shrimpBatchName").as(String.class), "%" + (String) searchMap.get("shrimpBatchName") + "%"));
                }
                // 虾苗品种
                if (searchMap.get("shrimpSpecies") != null && !"".equals(searchMap.get("shrimpSpecies"))) {
                    predicateList.add(cb.like(root.get("shrimpSpecies").as(String.class), "%" + (String) searchMap.get("shrimpSpecies") + "%"));
                }
                // 虾苗产地
                if (searchMap.get("shrimpOrigin") != null && !"".equals(searchMap.get("shrimpOrigin"))) {
                    predicateList.add(cb.like(root.get("shrimpOrigin").as(String.class), "%" + (String) searchMap.get("shrimpOrigin") + "%"));
                }
                // 虾苗供应商
                if (searchMap.get("shrimpSupplier") != null && !"".equals(searchMap.get("shrimpSupplier"))) {
                    predicateList.add(cb.like(root.get("shrimpSupplier").as(String.class), "%" + (String) searchMap.get("shrimpSupplier") + "%"));
                }
                // 捕捞状态
                if (searchMap.get("fishingStatus") != null && !"".equals(searchMap.get("fishingStatus"))) {
                    predicateList.add(cb.like(root.get("fishingStatus").as(String.class), "%" + (String) searchMap.get("fishingStatus") + "%"));
                }
                // 池塘编号
                if (searchMap.get("pondId") != null && !"".equals(searchMap.get("pondId"))) {
                    predicateList.add(cb.like(root.get("pondId").as(String.class), "%" + (String) searchMap.get("pondId") + "%"));
                }
                // 饲料名称
                if (searchMap.get("feedName") != null && !"".equals(searchMap.get("feedName"))) {
                    predicateList.add(cb.like(root.get("feedName").as(String.class), "%" + (String) searchMap.get("feedName") + "%"));
                }
                // 基地编号
                if (searchMap.get("baseId") != null && !"".equals(searchMap.get("baseId"))) {
                    predicateList.add(cb.like(root.get("baseId").as(String.class), "%" + (String) searchMap.get("baseId") + "%"));
                }
                // 创建者
                if (searchMap.get("createBy") != null && !"".equals(searchMap.get("createBy"))) {
                    predicateList.add(cb.like(root.get("createBy").as(String.class), "%" + (String) searchMap.get("createBy") + "%"));
                }

                return cb.and(predicateList.toArray(new Predicate[predicateList.size()]));

            }
        };

    }

}
