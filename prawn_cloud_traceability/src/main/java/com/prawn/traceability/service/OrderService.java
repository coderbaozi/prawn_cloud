package com.prawn.traceability.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import util.IdWorker;

import com.prawn.traceability.dao.OrderDao;
import com.prawn.traceability.pojo.Order;

/**
 * 服务层
 *
 * @author Administrator
 */
@Service
public class OrderService {

    @Autowired
    private OrderDao orderDao;

    @Autowired
    private IdWorker idWorker;


    /**
     * 查询全部列表
     *
     * @return
     */
    public List<Order> findAll() {
        return orderDao.findAll();
    }


    /**
     * 条件查询+分页
     *
     * @param whereMap
     * @param page
     * @param size
     * @return
     */
    public Page<Order> findSearch(Map whereMap, int page, int size) {
        Specification<Order> specification = createSpecification(whereMap);
        PageRequest pageRequest = PageRequest.of(page - 1, size);
        return orderDao.findAll(specification, pageRequest);
    }


    /**
     * 条件查询
     *
     * @param whereMap
     * @return
     */
    public List<Order> findSearch(Map whereMap) {
        Specification<Order> specification = createSpecification(whereMap);
        return orderDao.findAll(specification);
    }

    /**
     * 根据ID查询实体
     *
     * @param id
     * @return
     */
    public Order findById(String id) {
        return orderDao.findById(id).get();
    }

    /**
     * 增加
     *
     * @param order
     */
    public void add(Order order) {

        orderDao.save(order);
    }

    /**
     * 修改
     *
     * @param order
     */
    public void update(Order order) {
        orderDao.save(order);
    }

    /**
     * 删除
     *
     * @param id
     */
    public void deleteById(String id) {
        orderDao.deleteById(id);
    }

    /**
     * 动态条件构建
     *
     * @param searchMap
     * @return
     */
    private Specification<Order> createSpecification(Map searchMap) {

        return new Specification<Order>() {

            @Override
            public Predicate toPredicate(Root<Order> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                List<Predicate> predicateList = new ArrayList<Predicate>();
                // ID
                if (searchMap.get("id") != null && !"".equals(searchMap.get("id"))) {
                    predicateList.add(cb.like(root.get("id").as(String.class), "%" + (String) searchMap.get("id") + "%"));
                }
                // 物流编号
                if (searchMap.get("logisticsId") != null && !"".equals(searchMap.get("logisticsId"))) {
                    predicateList.add(cb.like(root.get("logisticsId").as(String.class), "%" + (String) searchMap.get("logisticsId") + "%"));
                }
                // 客户名
                if (searchMap.get("customerName") != null && !"".equals(searchMap.get("customerName"))) {
                    predicateList.add(cb.like(root.get("customerName").as(String.class), "%" + (String) searchMap.get("customerName") + "%"));
                }
                // 客户类型
                if (searchMap.get("customerType") != null && !"".equals(searchMap.get("customerType"))) {
                    predicateList.add(cb.like(root.get("customerType").as(String.class), "%" + (String) searchMap.get("customerType") + "%"));
                }
                // 虾苗编号
                if (searchMap.get("shrimpId") != null && !"".equals(searchMap.get("shrimpId"))) {
                    predicateList.add(cb.like(root.get("shrimpId").as(String.class), "%" + (String) searchMap.get("shrimpId") + "%"));
                }
                // 创建者
                if (searchMap.get("createBy") != null && !"".equals(searchMap.get("createBy"))) {
                    predicateList.add(cb.like(root.get("createBy").as(String.class), "%" + (String) searchMap.get("createBy") + "%"));
                }
                // 基地id
                if (searchMap.get("baseId") != null && !"".equals(searchMap.get("baseId"))) {
                    predicateList.add(cb.like(root.get("baseId").as(String.class), "%" + (String) searchMap.get("baseId") + "%"));
                }

                return cb.and(predicateList.toArray(new Predicate[predicateList.size()]));

            }
        };

    }

}
