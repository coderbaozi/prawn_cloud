package com.prawn.traceability.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import util.IdWorker;

import com.prawn.traceability.dao.InputcategoryDao;
import com.prawn.traceability.pojo.Inputcategory;

/**
 * 服务层
 * 
 * @author Administrator
 *
 */
@Service
public class InputcategoryService {

	@Autowired
	private InputcategoryDao inputcategoryDao;
	
	@Autowired
	private IdWorker idWorker;

	/**
	 * 查询全部列表
	 * @return
	 */
	public List<Inputcategory> findAll() {
		return inputcategoryDao.findAll();
	}

	
	/**
	 * 条件查询+分页
	 * @param whereMap
	 * @param page
	 * @param size
	 * @return
	 */
	public Page<Inputcategory> findSearch(Map whereMap, int page, int size) {
		Specification<Inputcategory> specification = createSpecification(whereMap);
		PageRequest pageRequest =  PageRequest.of(page-1, size);
		return inputcategoryDao.findAll(specification, pageRequest);
	}

	
	/**
	 * 条件查询
	 * @param whereMap
	 * @return
	 */
	public List<Inputcategory> findSearch(Map whereMap) {
		Specification<Inputcategory> specification = createSpecification(whereMap);
		return inputcategoryDao.findAll(specification);
	}

	/**
	 * 根据ID查询实体
	 * @param id
	 * @return
	 */
	public Inputcategory findById(String id) {
		return inputcategoryDao.findById(id).get();
	}

	/**
	 * 增加
	 * @param inputcategory
	 */
	public void add(Inputcategory inputcategory) {
		inputcategory.setId( idWorker.nextId()+"" );
		inputcategoryDao.save(inputcategory);
	}

	/**
	 * 修改
	 * @param inputcategory
	 */
	public void update(Inputcategory inputcategory) {
		inputcategoryDao.save(inputcategory);
	}

	/**
	 * 删除
	 * @param id
	 */
	public void deleteById(String id) {
		inputcategoryDao.deleteById(id);
	}

	/**
	 * 动态条件构建
	 * @param searchMap
	 * @return
	 */
	private Specification<Inputcategory> createSpecification(Map searchMap) {

		return new Specification<Inputcategory>() {

			@Override
			public Predicate toPredicate(Root<Inputcategory> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<Predicate>();
                // ID
                if (searchMap.get("id")!=null && !"".equals(searchMap.get("id"))) {
                	predicateList.add(cb.like(root.get("id").as(String.class), "%"+(String)searchMap.get("id")+"%"));
                }
                // 一级类型
                if (searchMap.get("typeOne")!=null && !"".equals(searchMap.get("typeOne"))) {
                	predicateList.add(cb.like(root.get("typeOne").as(String.class), "%"+(String)searchMap.get("typeOne")+"%"));
                }
                // 基地id
                if (searchMap.get("baseId")!=null && !"".equals(searchMap.get("baseId"))) {
                	predicateList.add(cb.like(root.get("baseId").as(String.class), "%"+(String)searchMap.get("baseId")+"%"));
                }
				
				return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

			}
		};

	}

}
