package com.prawn.traceability.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import util.IdWorker;

import com.prawn.traceability.dao.SuppliesinfoDao;
import com.prawn.traceability.pojo.Suppliesinfo;

/**
 * 服务层
 * 
 * @author Administrator
 *
 */
@Service
public class SuppliesinfoService {

	@Autowired
	private SuppliesinfoDao suppliesinfoDao;
	
	@Autowired
	private IdWorker idWorker;

	/**
	 * 查询全部列表
	 * @return
	 */
	public List<Suppliesinfo> findAll() {
		return suppliesinfoDao.findAll();
	}

	
	/**
	 * 条件查询+分页
	 * @param whereMap
	 * @param page
	 * @param size
	 * @return
	 */
	public Page<Suppliesinfo> findSearch(Map whereMap, int page, int size) {
		Specification<Suppliesinfo> specification = createSpecification(whereMap);
		PageRequest pageRequest =  PageRequest.of(page-1, size);
		return suppliesinfoDao.findAll(specification, pageRequest);
	}

	
	/**
	 * 条件查询
	 * @param whereMap
	 * @return
	 */
	public List<Suppliesinfo> findSearch(Map whereMap) {
		Specification<Suppliesinfo> specification = createSpecification(whereMap);
		return suppliesinfoDao.findAll(specification);
	}

	/**
	 * 根据ID查询实体
	 * @param id
	 * @return
	 */
	public Suppliesinfo findById(String id) {
		return suppliesinfoDao.findById(id).get();
	}

	/**
	 * 增加
	 * @param suppliesinfo
	 */
	public void add(Suppliesinfo suppliesinfo) {
		suppliesinfo.setId( idWorker.nextId()+"" );
		suppliesinfoDao.save(suppliesinfo);
	}

	/**
	 * 修改
	 * @param suppliesinfo
	 */
	public void update(Suppliesinfo suppliesinfo) {
		suppliesinfoDao.save(suppliesinfo);
	}

	/**
	 * 删除
	 * @param id
	 */
	public void deleteById(String id) {
		suppliesinfoDao.deleteById(id);
	}

	/**
	 * 动态条件构建
	 * @param searchMap
	 * @return
	 */
	private Specification<Suppliesinfo> createSpecification(Map searchMap) {

		return new Specification<Suppliesinfo>() {

			@Override
			public Predicate toPredicate(Root<Suppliesinfo> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<Predicate>();
                // id
                if (searchMap.get("id")!=null && !"".equals(searchMap.get("id"))) {
                	predicateList.add(cb.like(root.get("id").as(String.class), "%"+(String)searchMap.get("id")+"%"));
                }
                // 农资名称
                if (searchMap.get("suppliesName")!=null && !"".equals(searchMap.get("suppliesName"))) {
                	predicateList.add(cb.like(root.get("suppliesName").as(String.class), "%"+(String)searchMap.get("suppliesName")+"%"));
                }
                // 检验人
                if (searchMap.get("inspector")!=null && !"".equals(searchMap.get("inspector"))) {
                	predicateList.add(cb.like(root.get("inspector").as(String.class), "%"+(String)searchMap.get("inspector")+"%"));
                }
                // 供应商名称
                if (searchMap.get("supplierName")!=null && !"".equals(searchMap.get("supplierName"))) {
                	predicateList.add(cb.like(root.get("supplierName").as(String.class), "%"+(String)searchMap.get("supplierName")+"%"));
                }
                // 供应商地址
                if (searchMap.get("supplierAddr")!=null && !"".equals(searchMap.get("supplierAddr"))) {
                	predicateList.add(cb.like(root.get("supplierAddr").as(String.class), "%"+(String)searchMap.get("supplierAddr")+"%"));
                }
                // 供应商电话
                if (searchMap.get("supplierPhone")!=null && !"".equals(searchMap.get("supplierPhone"))) {
                	predicateList.add(cb.like(root.get("supplierPhone").as(String.class), "%"+(String)searchMap.get("supplierPhone")+"%"));
                }
                // 供应商生产许可证
                if (searchMap.get("supplierLicense")!=null && !"".equals(searchMap.get("supplierLicense"))) {
                	predicateList.add(cb.like(root.get("supplierLicense").as(String.class), "%"+(String)searchMap.get("supplierLicense")+"%"));
                }
                // 农资照片
                if (searchMap.get("suppliesPic")!=null && !"".equals(searchMap.get("suppliesPic"))) {
                	predicateList.add(cb.like(root.get("suppliesPic").as(String.class), "%"+(String)searchMap.get("suppliesPic")+"%"));
                }
                // 基地id
				if (searchMap.get("baseId")!=null && !"".equals(searchMap.get("baseId"))) {
					predicateList.add(cb.like(root.get("baseId").as(String.class), "%"+(String)searchMap.get("baseId")+"%"));
				}
				// 农资类型
				if (searchMap.get("type")!=null && !"".equals(searchMap.get("type"))) {
					predicateList.add(cb.like(root.get("type").as(String.class), "%"+(String)searchMap.get("type")+"%"));
				}
				// 规格
				if (searchMap.get("specification")!=null && !"".equals(searchMap.get("specification"))) {
					predicateList.add(cb.like(root.get("specification").as(String.class), "%"+(String)searchMap.get("specification")+"%"));
				}
				
				return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

			}
		};

	}

}
