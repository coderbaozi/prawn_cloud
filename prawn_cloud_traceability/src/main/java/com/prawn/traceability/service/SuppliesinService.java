package com.prawn.traceability.service;

import java.util.*;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import com.prawn.traceability.dao.SuppliesoutDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;
import util.IdWorker;

import com.prawn.traceability.dao.SuppliesinDao;
import com.prawn.traceability.pojo.Suppliesin;

/**
 * 服务层
 * 
 * @author Administrator
 *
 */
@Service
@Transactional
public class SuppliesinService {

	@Autowired
	private SuppliesinDao suppliesinDao;

	@Autowired
	private SuppliesoutDao suppliesoutDao;
	
	@Autowired
	private IdWorker idWorker;

	/**
	 * 查询全部列表
	 * @return
	 */
	public List<Suppliesin> findAll() {
		// 库存量为0时不展示但不删除数据
		return suppliesinDao.findAll();
//		Iterator<Suppliesin> iterator = list.iterator();
//		while (iterator.hasNext()){
//			Suppliesin suppliesin = iterator.next();
//			if(suppliesin.getInNumber()==0){
//				iterator.remove();
//			}
//		}
//		return list;
	}

	
	/**
	 * 条件查询+分页
	 * @param whereMap
	 * @param page
	 * @param size
	 * @return
	 */
	public Page<Suppliesin> findSearch(Map whereMap, int page, int size) {
		Specification<Suppliesin> specification = createSpecification(whereMap);
		PageRequest pageRequest =  PageRequest.of(page-1, size);
		return suppliesinDao.findAll(specification, pageRequest);
	}

	
	/**
	 * 条件查询
	 * @param whereMap
	 * @return
	 */
	public List<Suppliesin> findSearch(Map whereMap) {
		Specification<Suppliesin> specification = createSpecification(whereMap);
		return suppliesinDao.findAll(specification);
	}

	/**
	 * 根据ID查询实体
	 * @param id
	 * @return
	 */
	public Suppliesin findById(String id) {
		return suppliesinDao.findById(id).get();
	}

	/**
	 * 增加
	 * @param suppliesin
	 */
	public void add(Suppliesin suppliesin) {
		suppliesin.setId( idWorker.nextId()+"" );
		suppliesin.setRemainNumber(suppliesin.getInNumber());
		suppliesinDao.save(suppliesin);
	}

	/**
	 * 修改
	 * @param suppliesin
	 */
	public void update(Suppliesin suppliesin) {
		suppliesinDao.save(suppliesin);
	}

	/**
	 * 删除
	 * @param id
	 */
	public void deleteById(String id) {
		//同时删除所以对应的入库id的出库信息
		suppliesoutDao.deleteAllBySuppliesInId(id);
		suppliesinDao.deleteById(id);
	}

	/**
	 * 动态条件构建
	 * @param searchMap
	 * @return
	 */
	private Specification<Suppliesin> createSpecification(Map searchMap) {

		return new Specification<Suppliesin>() {

			@Override
			public Predicate toPredicate(Root<Suppliesin> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicateList = new ArrayList<Predicate>();
                // 农资编号
                if (searchMap.get("id")!=null && !"".equals(searchMap.get("id"))) {
                	predicateList.add(cb.like(root.get("id").as(String.class), "%"+(String)searchMap.get("id")+"%"));
                }
                // 农资名称
                if (searchMap.get("suppliesName")!=null && !"".equals(searchMap.get("suppliesName"))) {
                	predicateList.add(cb.like(root.get("suppliesName").as(String.class), "%"+(String)searchMap.get("suppliesName")+"%"));
                }
                // 操作人
                if (searchMap.get("inOperator")!=null && !"".equals(searchMap.get("inOperator"))) {
                	predicateList.add(cb.like(root.get("inOperator").as(String.class), "%"+(String)searchMap.get("inOperator")+"%"));
                }
                // 仓库号
                if (searchMap.get("warehouse")!=null && !"".equals(searchMap.get("warehouse"))) {
                	predicateList.add(cb.like(root.get("warehouse").as(String.class), "%"+(String)searchMap.get("warehouse")+"%"));
                }
                // 基地编号
                if (searchMap.get("baseId")!=null && !"".equals(searchMap.get("baseId"))) {
                	predicateList.add(cb.like(root.get("baseId").as(String.class), "%"+(String)searchMap.get("baseId")+"%"));
                }
				// 基地编号
				if (searchMap.get("type") != null && !"".equals(searchMap.get("type"))) {
					predicateList.add(cb.like(root.get("type").as(String.class), "%" + (String) searchMap.get("type") + "%"));
				}
				
				return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

			}
		};

	}

}
