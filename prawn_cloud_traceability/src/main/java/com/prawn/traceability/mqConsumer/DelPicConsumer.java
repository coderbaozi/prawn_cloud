package com.prawn.traceability.mqConsumer;

import com.prawn.traceability.pojo.Qualification;
import com.prawn.traceability.util.FastDFSUtil;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "del_pic")
public class DelPicConsumer {

    @Autowired
    private FastDFSUtil fastDFSUtil;

    @RabbitHandler
    public void delPic(Qualification oldQual) {
        String inspectionReport = oldQual.getInspectionReport();
        String qualityCertificate = oldQual.getQualityCertificate();
        String safetyCertificate = oldQual.getSafetyCertificate();
        String license = oldQual.getLicense();
        String standard = oldQual.getStandard();
        try {

            if ("".equals(inspectionReport) || inspectionReport != null) {
                fastDFSUtil.deleteFile(inspectionReport);
            }
            if ("".equals(qualityCertificate) || qualityCertificate != null) {
                fastDFSUtil.deleteFile(qualityCertificate);
            }
            if ("".equals(safetyCertificate) || safetyCertificate != null) {
                fastDFSUtil.deleteFile(safetyCertificate);
            }
            if ("".equals(license) || license != null) {
                fastDFSUtil.deleteFile(license);
            }
            if ("".equals(standard) || standard != null) {
                fastDFSUtil.deleteFile(standard);
            }
        }catch (Exception e){
            return;
        }
    }
}
