package com.prawn.traceability.mqConsumer;

import com.prawn.traceability.dao.ProcessingDao;
import com.prawn.traceability.pojo.Processing;
import com.prawn.traceability.util.FastDFSUtil;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RabbitListener(queues = "del_process")
public class DelProcessConsumer {
    @Autowired
    private FastDFSUtil fastDFSUtil;

    @Autowired
    private ProcessingDao processingDao;

    @RabbitHandler
    public void delProcess(List<Processing> processings){
        if(processings.size()>0){
            for(Processing processing:processings){
                fastDFSUtil.deleteFile(processing.getProcessPic());
                processingDao.delete(processing);
            }
        }
    }
}
