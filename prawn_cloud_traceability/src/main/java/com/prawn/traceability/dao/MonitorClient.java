package com.prawn.traceability.dao;

import entity.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


@FeignClient("prawn-cloud-monitor")
public interface MonitorClient {

    @RequestMapping(value = "/monitor/trace", method = RequestMethod.GET)
    public Result getBaseMonitor(@RequestParam("baseId") String baseId);

}
