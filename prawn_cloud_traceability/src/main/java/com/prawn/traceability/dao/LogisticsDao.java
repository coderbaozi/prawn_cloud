package com.prawn.traceability.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.prawn.traceability.pojo.Logistics;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface LogisticsDao extends JpaRepository<Logistics,String>,JpaSpecificationExecutor<Logistics>{
	public Logistics findByAdultShrimpIdAndLogisticsStatus(String adultShrimpId,String logisticsStatus);

	@Transactional
	@Modifying
	@Query(value = "update trac_logistics set arrivalTime =? , logisticsStatus=? where adultShrimpId=? and logisticsStatus= ? ",nativeQuery = true)
	public void updatelogistics(Date arrivalTime,String arrivalStatus, String adultShrimpId,String logisticsStatus);

	public List<Logistics> findByAdultShrimpId(String adultShrimpId);
}
