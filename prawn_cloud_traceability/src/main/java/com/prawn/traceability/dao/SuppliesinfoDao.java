package com.prawn.traceability.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.prawn.traceability.pojo.Suppliesinfo;
/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface SuppliesinfoDao extends JpaRepository<Suppliesinfo,String>,JpaSpecificationExecutor<Suppliesinfo>{
	
}
