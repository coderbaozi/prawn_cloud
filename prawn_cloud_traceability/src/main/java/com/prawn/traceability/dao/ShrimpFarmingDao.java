package com.prawn.traceability.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.prawn.traceability.pojo.ShrimpFarming;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * 数据访问接口
 *
 * @author Administrator
 */
public interface ShrimpFarmingDao extends JpaRepository<ShrimpFarming, String>, JpaSpecificationExecutor<ShrimpFarming> {
    @Query(value = "select * from trac_shrimp_farming where shrimpId = ? ", nativeQuery = true)
    public Page<ShrimpFarming> findFarmingByShrimpId(String shrimpId, Pageable pageable);

    public List<ShrimpFarming> findByShrimpId(String shrimpId);
}
