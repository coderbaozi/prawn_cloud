package com.prawn.traceability.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.prawn.traceability.pojo.Shrimpkind;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface ShrimpkindDao extends JpaRepository<Shrimpkind,String>,JpaSpecificationExecutor<Shrimpkind>{
//    @Query(value = "select kindName,minDensity,maxDensity from trac_shrimp_kind ", nativeQuery = true)
//    public List<Shrimpkind> findAllKind();
}
