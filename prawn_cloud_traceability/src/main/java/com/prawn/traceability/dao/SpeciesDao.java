package com.prawn.traceability.dao;

import entity.Species;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface SpeciesDao extends JpaRepository<Species,String>,JpaSpecificationExecutor<Species>{

    @Query(value = "select trac_shrimp.id,trac_shrimp.shrimpSpecies,trac_shrimp.inputNum, trac_shrimp.yield from trac_shrimp where trac_shrimp.baseId=?",nativeQuery = true)
    public List<Species> findSpeciesForBD(String baseId);

}
