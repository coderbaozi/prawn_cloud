package com.prawn.traceability.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.prawn.traceability.pojo.ShrimpManagement;
/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface ShrimpManagementDao extends JpaRepository<ShrimpManagement,String>,JpaSpecificationExecutor<ShrimpManagement>{
	
}
