package com.prawn.traceability.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.prawn.traceability.pojo.Storage;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;

/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface StorageDao extends JpaRepository<Storage,String>,JpaSpecificationExecutor<Storage>{
    @Modifying
    @Query(value = "update trac_storage set outboundStatus = ? , outboundTime= ? where adultShrimpId =? ", nativeQuery = true)
	public void outStorage(String outboundStatus,Date outboundTime, String adultShrimpId);
}
