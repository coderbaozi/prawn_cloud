package com.prawn.traceability.dao;



import com.prawn.traceability.pojo.Shrimp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface ShrimpDao extends JpaRepository<Shrimp,String>,JpaSpecificationExecutor<Shrimp>{
	public Shrimp findByPondIdAndFishingStatus(String pondId, String finishingStatus);

	@Query(value = "SELECT * FROM trac_shrimp WHERE baseId = ? ", nativeQuery = true)
	public List<Shrimp> findShrimpForBD(String baseId);

	@Modifying
	@Query(value = "update trac_shrimp set remain = ? where id=?",nativeQuery = true)
	public void updateShrimpYield(double down,String shrimpId);

}
