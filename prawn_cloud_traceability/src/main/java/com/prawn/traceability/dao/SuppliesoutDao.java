package com.prawn.traceability.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.prawn.traceability.pojo.Suppliesout;
import org.springframework.data.jpa.repository.Query;

/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface SuppliesoutDao extends JpaRepository<Suppliesout,String>,JpaSpecificationExecutor<Suppliesout>{
    @Query(value = "select outNumber from trac_suppliesOut where id = ?", nativeQuery = true)
	public Double getOutNumberById(String id);

    public void deleteAllBySuppliesInId(String suppliesInId);

}
