package com.prawn.traceability.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.prawn.traceability.pojo.Personinfo;
/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface PersoninfoDao extends JpaRepository<Personinfo,String>,JpaSpecificationExecutor<Personinfo>{
	
}
