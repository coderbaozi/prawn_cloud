package com.prawn.traceability.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.prawn.traceability.pojo.AdultShrimp;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.QueryByExampleExecutor;

/**
 * 数据访问接口
 *
 * @author Administrator
 */
public interface AdultShrimpDao extends JpaRepository<AdultShrimp, String>, JpaSpecificationExecutor<AdultShrimp> {

    @Modifying
    @Query(value = "update trac_adult_shrimp set plantId =? where id=? ", nativeQuery = true)
    public void updatePlantId(String plantId, String id);

    @Modifying
    @Query(value = "update trac_adult_shrimp set storageId =? where id=?  ", nativeQuery = true)
    public void updateStorageId(String storageId, String id);
}
