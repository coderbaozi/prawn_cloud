package com.prawn.traceability.dao;

import entity.Base;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface BaseDao extends JpaRepository<Base,String>,JpaSpecificationExecutor<Base>{
	
}
