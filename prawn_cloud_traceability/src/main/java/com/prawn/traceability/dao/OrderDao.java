package com.prawn.traceability.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.prawn.traceability.pojo.Order;
/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface OrderDao extends JpaRepository<Order,String>,JpaSpecificationExecutor<Order>{
	
}
