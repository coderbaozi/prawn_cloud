package com.prawn.traceability.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.prawn.traceability.pojo.Plant;
/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface PlantDao extends JpaRepository<Plant,String>,JpaSpecificationExecutor<Plant>{
	
}
