package com.prawn.traceability.dao;


import com.prawn.traceability.pojo.Pond;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;



/**
 * 数据访问接口
 *
 * @author Administrator
 */
public interface PondDao extends JpaRepository<Pond, String>, JpaSpecificationExecutor<Pond> {
    @Modifying
    @Query(value = "update trac_pond set shrimpId =? , inputNum=?, createDate= ? ,deliveryStatus = ?,species=? where id=?  ", nativeQuery = true)
    public void drop(String shrimpId, Integer inputNum, Date createDate, String deliveryStatus, String species,String id);

    @Modifying
    @Query(value = "update trac_pond set deliveryStatus=? ,catchDate= ? where id=?  ", nativeQuery = true)
    public void pondCatch(String deliveryStatus, Date catchDate, String id);

    @Query(value = "SELECT SUM(pondVolume) FROM trac_pond WHERE baseId = ?", nativeQuery = true)
    public int getVolumeSumByBaseId(String baseId);

    @Query(value = "SELECT COUNT(id) FROM trac_pond WHERE baseId = ?", nativeQuery = true)
    public int getPondNumbersByBaseId(String baseId);

//    @Query(value = "SELECT t1.*,t2.shrimpSpecies,t2.shrimpBatchName FROM trac_pond AS t1 LEFT OUTER JOIN trac_shrimp_management AS t2 ON t1.shrimpId=t2.id WHERE t1.baseId=? ",nativeQuery = true)
//    public List<PondShrimpManagementDTO> getPonds(String baseId);
}
