package com.prawn.traceability.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.prawn.traceability.pojo.Processing;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface ProcessingDao extends JpaRepository<Processing,String>,JpaSpecificationExecutor<Processing>{
	public List<Processing> findProcessingsByPlantId(String plantId);

}
