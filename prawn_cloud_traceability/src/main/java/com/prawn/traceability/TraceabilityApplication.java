package com.prawn.traceability;

import com.github.tobato.fastdfs.FdfsClientConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableMBeanExport;
import org.springframework.context.annotation.Import;
import org.springframework.integration.annotation.Filter;
import org.springframework.jmx.support.RegistrationPolicy;
import org.springframework.web.filter.HttpPutFormContentFilter;
import util.IdWorker;

@EnableDiscoveryClient
@EnableFeignClients
@EnableMBeanExport(registration = RegistrationPolicy.IGNORE_EXISTING)
@Import(FdfsClientConfig.class)
@EntityScan(value = {"entity","com.prawn.traceability.pojo"})
@SpringBootApplication
public class TraceabilityApplication {

	public static void main(String[] args) {
		SpringApplication.run(TraceabilityApplication.class, args);
	}

	@Bean
	public IdWorker idWorkker(){
		return new IdWorker(1, 1);
	}

}
