package com.prawn.traceability.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.prawn.traceability.pojo.AdultShrimp;
import com.prawn.traceability.service.AdultShrimpService;

import entity.PageResult;
import entity.Result;
import entity.StatusCode;

/**
 * 控制器层
 *
 * @author Administrator
 */
@RestController
@CrossOrigin
@RequestMapping("/adultShrimp")
public class AdultShrimpController {

    @Autowired
    private AdultShrimpService adultShrimpService;

    /**
     * 溯源
     *
     * @param adultShrimpId 成虾id
     * @return
     */
    @RequestMapping(value = "/traceability/{adultShrimpId}", method = RequestMethod.GET)
    public Result traceability(@PathVariable String adultShrimpId) {
        return new Result(true, StatusCode.OK, "查询成功", adultShrimpService.traceability(adultShrimpId));
    }

    /**
     * 查询全部数据
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public Result findAll() {
        return new Result(true, StatusCode.OK, "查询成功", adultShrimpService.findAll());
    }

    /**
     * 根据ID查询
     *
     * @param id ID
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Result findById(@PathVariable String id) {
        return new Result(true, StatusCode.OK, "查询成功", adultShrimpService.findById(id));
    }


    /**
     * 分页+多条件查询
     *
     * @param searchMap 查询条件封装
     * @param page      页码
     * @param size      页大小
     * @return 分页结果
     */
    @RequestMapping(value = "/search/{page}/{size}", method = RequestMethod.POST)
    public Result findSearch(@RequestBody Map searchMap, @PathVariable int page, @PathVariable int size) {
        Page<AdultShrimp> pageList = adultShrimpService.findSearch(searchMap, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<AdultShrimp>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 根据条件查询
     *
     * @param searchMap
     * @return
     */
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public Result findSearch(@RequestBody Map searchMap) {
        return new Result(true, StatusCode.OK, "查询成功", adultShrimpService.findSearch(searchMap));
    }

    /**
     * 增加
     *
     * @param adultShrimp
     */
    @RequestMapping(method = RequestMethod.POST)
    public Result add(@RequestBody AdultShrimp adultShrimp) {
        adultShrimpService.add(adultShrimp);
        return new Result(true, StatusCode.OK, "增加成功");
    }

    /**
     * 修改
     *
     * @param adultShrimp
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public Result update(@RequestBody AdultShrimp adultShrimp, @PathVariable String id) {
        adultShrimp.setId(id);
        adultShrimpService.update(adultShrimp);
        return new Result(true, StatusCode.OK, "修改成功");
    }

    /**
     * 删除
     *
     * @param id
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public Result delete(@PathVariable String id) {
        adultShrimpService.deleteById(id);
        return new Result(true, StatusCode.OK, "删除成功");
    }

}
