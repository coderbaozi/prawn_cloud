package com.prawn.traceability.controller;

import java.util.Map;

import com.prawn.traceability.pojo.ShrimpFarming;
import com.prawn.traceability.service.ShrimpFarmingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import entity.PageResult;
import entity.Result;
import entity.StatusCode;

/**
 * 控制器层
 *
 * @author Administrator
 */
@RestController
@CrossOrigin
@RequestMapping("/shrimpFarming")
public class ShrimpFarmingController {

    @Autowired
    private ShrimpFarmingService shrimpFarmingService;

    /**
     * 查询池塘投料记录
     *
     * @param pondId
     * @return
     */
    @RequestMapping(value = "/pondF/{pondId}/{page}/{size}", method = RequestMethod.POST)
    public Result findPondFarming(@PathVariable String pondId, @PathVariable int page, @PathVariable int size) {
        return new Result(true, StatusCode.OK, "查询成功", shrimpFarmingService.findPondFarming(pondId, page, size));
    }

    /**
     * 查询全部数据
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public Result findAll() {
        return new Result(true, StatusCode.OK, "查询成功", shrimpFarmingService.findAll());
    }

    /**
     * 根据ID查询
     *
     * @param id ID
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Result findById(@PathVariable String id) {
        return new Result(true, StatusCode.OK, "查询成功", shrimpFarmingService.findById(id));
    }


    /**
     * 分页+多条件查询
     *
     * @param searchMap 查询条件封装
     * @param page      页码
     * @param size      页大小
     * @return 分页结果
     */
    @RequestMapping(value = "/search/{page}/{size}", method = RequestMethod.POST)
    public Result findSearch(@RequestBody Map searchMap, @PathVariable int page, @PathVariable int size) {
        Page<ShrimpFarming> pageList = shrimpFarmingService.findSearch(searchMap, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<ShrimpFarming>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 根据条件查询
     *
     * @param searchMap
     * @return
     */
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public Result findSearch(@RequestBody Map searchMap) {
        return new Result(true, StatusCode.OK, "查询成功", shrimpFarmingService.findSearch(searchMap));
    }

    /**
     * 增加
     *
     * @param shrimpFarming
     */
    @RequestMapping(method = RequestMethod.POST)
    public Result add(@RequestBody ShrimpFarming shrimpFarming) {
        shrimpFarmingService.add(shrimpFarming);
        return new Result(true, StatusCode.OK, "增加成功");
    }

    /**
     * 修改
     *
     * @param shrimpFarming
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public Result update(@RequestBody ShrimpFarming shrimpFarming, @PathVariable String id) {
        shrimpFarming.setId(id);
        shrimpFarmingService.update(shrimpFarming);
        return new Result(true, StatusCode.OK, "修改成功");
    }

    /**
     * 删除
     *
     * @param id
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public Result delete(@PathVariable String id) {
        shrimpFarmingService.deleteById(id);
        return new Result(true, StatusCode.OK, "删除成功");
    }

}
