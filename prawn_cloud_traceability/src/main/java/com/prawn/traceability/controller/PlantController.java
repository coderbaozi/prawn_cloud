package com.prawn.traceability.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.prawn.traceability.dao.AdultShrimpDao;
import com.prawn.traceability.dao.ProcessingDao;
import com.prawn.traceability.pojo.Processing;
import com.prawn.traceability.util.FastDFSUtil;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import com.prawn.traceability.pojo.Plant;
import com.prawn.traceability.service.PlantService;

import entity.PageResult;
import entity.Result;
import entity.StatusCode;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MultipartFile;

/**
 * 控制器层
 *
 * @author Administrator
 */
@RestController
@CrossOrigin
@RequestMapping("/plant")
public class PlantController {

    @Autowired
    private PlantService plantService;

    @Autowired
    private AdultShrimpDao adultShrimpDao;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private ProcessingDao processingDao;

    /**
     * 查询全部数据
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public Result findAll() {
        return new Result(true, StatusCode.OK, "查询成功", plantService.findAll());
    }

    /**
     * 根据ID查询
     *
     * @param id ID
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Result findById(@PathVariable String id) {
        return new Result(true, StatusCode.OK, "查询成功", plantService.findById(id));
    }


    /**
     * 分页+多条件查询
     *
     * @param searchMap 查询条件封装
     * @param page      页码
     * @param size      页大小
     * @return 分页结果
     */
    @RequestMapping(value = "/search/{page}/{size}", method = RequestMethod.POST)
    public Result findSearch(@RequestBody Map searchMap, @PathVariable int page, @PathVariable int size) {
        Page<Plant> pageList = plantService.findSearch(searchMap, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Plant>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 根据条件查询
     *
     * @param searchMap
     * @return
     */
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public Result findSearch(@RequestBody Map searchMap) {
        return new Result(true, StatusCode.OK, "查询成功", plantService.findSearch(searchMap));
    }

    @InitBinder
    public void initBinder(WebDataBinder binder, WebRequest request) {

        //转换日期
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));// CustomDateEditor为自定义日期编辑器
    }

    /**
     * 从物流添加
     *
     * @param plant
     */
    @RequestMapping(method = RequestMethod.POST)
    public Result add(@RequestBody Plant plant) {
        plantService.add(plant);
        return new Result(true, StatusCode.OK, "增加成功");
    }

    /**
     * 修改
     *
     * @param plant
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public Result update(@RequestBody Plant plant, @PathVariable String id){
        plant.setId(id);
        plantService.update(plant);
        return new Result(true, StatusCode.OK, "修改成功");
    }

    /**
     * 删除
     *
     * @param id
     */
    @Transactional
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public Result delete(@PathVariable String id) {
        Plant plant = plantService.findById(id);
        //删除成虾表的加工厂id
        adultShrimpDao.updatePlantId("", plant.getAdultShrimpId());

        List<Processing> processings = processingDao.findProcessingsByPlantId(id);
        rabbitTemplate.convertAndSend(processings);

        plantService.deleteById(id);
        return new Result(true, StatusCode.OK, "删除成功");
    }

}
