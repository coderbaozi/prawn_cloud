package com.prawn.traceability.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.prawn.traceability.pojo.Logistics;
import com.prawn.traceability.service.LogisticsService;

import entity.PageResult;
import entity.Result;
import entity.StatusCode;

/**
 * 控制器层
 *
 * @author Administrator
 */
@RestController
@CrossOrigin
@RequestMapping("/logistics")
public class LogisticsController {

    @Autowired
    private LogisticsService logisticsService;

    //根据成虾id获得物流信息
    @RequestMapping(value = "/findForQR/{adultShrimpId}", method = RequestMethod.GET)
    public Result findForQR(@PathVariable String adultShrimpId) {
        return new Result(true, StatusCode.OK, "查询成功", logisticsService.findByShrimpId(adultShrimpId));
    }

    //物流到达
    @RequestMapping(value = "/arrive/{adultShrimpId}", method = RequestMethod.PUT)
    public Result arrive(@PathVariable String adultShrimpId) {
//        Logistics logistics = logisticsService.findByShrimId(adultShrimId);
//        logistics.setArrivalTime(new Date());
//        logistics.setLogisticsStatus("1");
//        logisticsService.update(logistics);
        logisticsService.updatelogistics(new Date(), adultShrimpId);
        return new Result(true, StatusCode.OK, "修改成功");
    }

    /**
     * 查询全部数据
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public Result findAll() {
        return new Result(true, StatusCode.OK, "查询成功", logisticsService.findAll());
    }

    /**
     * 根据ID查询
     *
     * @param id ID
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Result findById(@PathVariable String id) {
        return new Result(true, StatusCode.OK, "查询成功", logisticsService.findById(id));
    }


    /**
     * 分页+多条件查询
     *
     * @param searchMap 查询条件封装
     * @param page      页码
     * @param size      页大小
     * @return 分页结果
     */
    @RequestMapping(value = "/search/{page}/{size}", method = RequestMethod.POST)
    public Result findSearch(@RequestBody Map searchMap, @PathVariable int page, @PathVariable int size) {
        Page<Logistics> pageList = logisticsService.findSearch(searchMap, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Logistics>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 根据条件查询
     *
     * @param searchMap
     * @return
     */
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public Result findSearch(@RequestBody Map searchMap) {
        return new Result(true, StatusCode.OK, "查询成功", logisticsService.findSearch(searchMap));
    }

    /**
     * 增加
     *
     * @param logistics
     */
    @RequestMapping(method = RequestMethod.POST)
    public Result add(@RequestBody Logistics logistics) {
        logisticsService.add(logistics);
        return new Result(true, StatusCode.OK, "增加成功");
    }

    /**
     * 修改
     *
     * @param logistics
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public Result update(@RequestBody Logistics logistics, @PathVariable String id) {

        logistics.setId(id);
        logisticsService.update(logistics);
        return new Result(true, StatusCode.OK, "修改成功");
    }

    /**
     * 删除
     *
     * @param id
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public Result delete(@PathVariable String id) {
        logisticsService.deleteById(id);
        return new Result(true, StatusCode.OK, "删除成功");
    }

}
