package com.prawn.traceability.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;


import com.prawn.traceability.util.FastDFSUtil;
import entity.Base;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import com.prawn.traceability.service.BaseService;

import entity.PageResult;
import entity.Result;
import entity.StatusCode;
import org.springframework.web.multipart.MultipartFile;

/**
 * 控制器层
 *
 * @author Administrator
 */
@RestController
@CrossOrigin
@RequestMapping("/base")
public class BaseController {

    @Autowired
    private BaseService BaseService;

    @Autowired
    private FastDFSUtil fastDFSUtil;

    /**
     * 上传图片
     *
     * @param file
     * @return
     */
    @RequestMapping(value = "/addPic", method = RequestMethod.POST)
    public Result addPic(@RequestParam("file") MultipartFile file) {
        try {
            String url = fastDFSUtil.uploadFile(file);
            return new Result(true, StatusCode.OK, "上传成功", url);
        } catch (IOException e) {
            return new Result(false, StatusCode.UPLOADERROT, "上传失败");
        }
    }

	/**
	 * 删除图片
	 * @param delUrl
	 * @return
	 */
	@RequestMapping(value = "/delPic", method = RequestMethod.DELETE)
    public Result delPic(@RequestParam("delUrl") String delUrl) {
		fastDFSUtil.deleteFile(delUrl);
		return new Result(true,StatusCode.OK,"删除成功");
    }


    /**
     * 大数据接口
     *
     * @return
     */
    @RequestMapping(value = "/BD", method = RequestMethod.GET)
    public List<Base> findAllForBD() {
        return BaseService.findAll();
    }

    /**
     * 查询全部数据
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public Result findAll() {
        return new Result(true, StatusCode.OK, "查询成功", BaseService.findAll());
    }

    /**
     * 根据ID查询
     *
     * @param id ID
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Result findById(@PathVariable String id) {
        return new Result(true, StatusCode.OK, "查询成功", BaseService.findById(id));
    }


    /**
     * 分页+多条件查询
     *
     * @param searchMap 查询条件封装
     * @param page      页码
     * @param size      页大小
     * @return 分页结果
     */
    @RequestMapping(value = "/search/{page}/{size}", method = RequestMethod.POST)
    public Result findSearch(@RequestBody Map searchMap, @PathVariable int page, @PathVariable int size) {
        Page<Base> pageList = BaseService.findSearch(searchMap, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Base>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 根据条件查询
     *
     * @param searchMap
     * @return
     */
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public Result findSearch(@RequestBody Map searchMap) {
        return new Result(true, StatusCode.OK, "查询成功", BaseService.findSearch(searchMap));
    }

    /**
     * 增加
     *
     * @param Base
     */
    @RequestMapping(method = RequestMethod.POST)
    public Result add(@RequestBody Base Base) {
        BaseService.add(Base);
        return new Result(true, StatusCode.OK, "增加成功");
    }

    /**
     * 修改
     *
     * @param Base
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public Result update(@RequestBody Base Base, @PathVariable String id) {
        Base.setId(id);
        BaseService.update(Base);
        return new Result(true, StatusCode.OK, "修改成功");
    }

    /**
     * 删除
     *
     * @param id
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public Result delete(@PathVariable String id) {
        BaseService.deleteById(id);
        return new Result(true, StatusCode.OK, "删除成功");
    }

}
