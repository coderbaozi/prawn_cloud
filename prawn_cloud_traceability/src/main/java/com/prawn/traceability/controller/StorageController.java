package com.prawn.traceability.controller;

import java.util.List;
import java.util.Map;

import com.prawn.traceability.dao.AdultShrimpDao;
import com.prawn.traceability.dao.ShrimpDao;
import com.prawn.traceability.pojo.AdultShrimp;
import com.prawn.traceability.pojo.Shrimp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.prawn.traceability.pojo.Storage;
import com.prawn.traceability.service.StorageService;

import entity.PageResult;
import entity.Result;
import entity.StatusCode;
import util.IdWorker;

/**
 * 控制器层
 *
 * @author Administrator
 */

@RestController
@CrossOrigin
@RequestMapping("/storage")
public class StorageController {

    @Autowired
    private StorageService storageService;

    @Autowired
    private AdultShrimpDao adultShrimpDao;

    @Autowired
    private IdWorker idWorker;

    @Autowired
    private ShrimpDao shrimpDao;

    /**
     * 从基地入冷库
     *
     * @param storage
     * @param shrimpId
     * @param weight
     * @return
     */
    @Transactional
    @RequestMapping(value = "/baseIn", method = RequestMethod.POST)
    public Result inStorage(@RequestBody Storage storage, @RequestParam String shrimpId, @RequestParam double weight) {
        AdultShrimp adultShrimp = new AdultShrimp();
        String adultShrimpId = idWorker.nextId() + "";
        adultShrimp.setId(adultShrimpId);
        adultShrimp.setShrimpId(shrimpId);
        adultShrimp.setBaseId(storage.getBaseId());
        adultShrimp.setAdultShrimpWeight(weight);
        adultShrimpDao.save(adultShrimp);

        Shrimp shrimp = shrimpDao.findById(shrimpId).get();
        shrimpDao.updateShrimpYield(shrimp.getRemain() - weight, shrimpId);

        storage.setAdultShrimpId(adultShrimpId);
        storageService.add(storage);
        return new Result(true, StatusCode.OK, "入库成功");
    }

    /**
     * 查询全部数据
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public Result findAll() {
        return new Result(true, StatusCode.OK, "查询成功", storageService.findAll());
    }

    /**
     * 根据ID查询
     *
     * @param id ID
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Result findById(@PathVariable String id) {
        return new Result(true, StatusCode.OK, "查询成功", storageService.findById(id));
    }


    /**
     * 分页+多条件查询
     *
     * @param searchMap 查询条件封装
     * @param page      页码
     * @param size      页大小
     * @return 分页结果
     */
    @RequestMapping(value = "/search/{page}/{size}", method = RequestMethod.POST)
    public Result findSearch(@RequestBody Map searchMap, @PathVariable int page, @PathVariable int size) {
        Page<Storage> pageList = storageService.findSearch(searchMap, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Storage>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 根据条件查询
     *
     * @param searchMap
     * @return
     */
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public Result findSearch(@RequestBody Map searchMap) {
        return new Result(true, StatusCode.OK, "查询成功", storageService.findSearch(searchMap));
    }

    /**
     * 从物流入库
     *
     * @param storage
     */
    @RequestMapping(method = RequestMethod.POST)
    public Result add(@RequestBody Storage storage) {
        storageService.add(storage);
        return new Result(true, StatusCode.OK, "增加成功");
    }

    /**
     * 出库
     *
     * @param storage
     * @return
     */
    @RequestMapping(value = "/outStorage", method = RequestMethod.PUT)
    public Result outStorage(@RequestBody Storage storage) {
        storageService.outStorage(storage.getOutboundTime(), storage.getAdultShrimpId());
        return new Result(true, StatusCode.OK, "出库成功");
    }

    /**
     * 修改
     *
     * @param storage
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public Result update(@RequestBody Storage storage, @PathVariable String id) {
        storage.setId(id);
        storageService.update(storage);
        return new Result(true, StatusCode.OK, "修改成功");
    }

    /**
     * 删除
     *
     * @param id
     */
    @Transactional
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public Result delete(@PathVariable String id) {
        Storage storage = storageService.findById(id);
        //删除成虾表冷库id
        adultShrimpDao.updateStorageId("", storage.getAdultShrimpId());
        storageService.deleteById(id);
        return new Result(true, StatusCode.OK, "删除成功");
    }

}
