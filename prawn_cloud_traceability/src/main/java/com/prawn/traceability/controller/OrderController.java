package com.prawn.traceability.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.prawn.traceability.pojo.*;
import com.prawn.traceability.service.*;
import com.prawn.traceability.util.FastDFSUtil;
import entity.Base;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import entity.PageResult;
import entity.Result;
import entity.StatusCode;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MultipartFile;
import util.IdWorker;

/**
 * 控制器层
 *
 * @author Administrator
 */
@RestController
@CrossOrigin
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private AdultShrimpService adultShrimpService;

    @Autowired
    private IdWorker idWorker;

    @Autowired
    private BaseService baseService;

    @Autowired
    private LogisticsService logisticsService;

    @Autowired
    private ShrimpService shrimpService;

    /**
     * 查询全部数据
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public Result findAll() {
        return new Result(true, StatusCode.OK, "查询成功", orderService.findAll());
    }

    /**
     * 根据ID查询
     *
     * @param id ID
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Result findById(@PathVariable String id) {
        return new Result(true, StatusCode.OK, "查询成功", orderService.findById(id));
    }


    /**
     * 分页+多条件查询
     *
     * @param searchMap 查询条件封装
     * @param page      页码
     * @param size      页大小
     * @return 分页结果
     */
    @RequestMapping(value = "/search/{page}/{size}", method = RequestMethod.POST)
    public Result findSearch(@RequestBody Map searchMap, @PathVariable int page, @PathVariable int size) {
        Page<Order> pageList = orderService.findSearch(searchMap, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Order>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 根据条件查询
     *
     * @param searchMap
     * @return
     */
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public Result findSearch(@RequestBody Map searchMap) {
        return new Result(true, StatusCode.OK, "查询成功", orderService.findSearch(searchMap));
    }

    @InitBinder
    public void initBinder(WebDataBinder binder, WebRequest request) {

        //转换日期
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));// CustomDateEditor为自定义日期编辑器
    }


    /**
     * 增加
     *
     * @param order
     */
    @RequestMapping(method = RequestMethod.POST)
    public Result add(@RequestBody Order order) {
        //成虾表
        AdultShrimp adultShrimp = new AdultShrimp();
        adultShrimp.setBaseId(order.getBaseId());
        adultShrimp.setShrimpId(order.getShrimpId());
        adultShrimp.setAdultShrimpWeight(order.getWeight());
        //成虾编号生成
        String id = idWorker.nextId() + "";
        adultShrimp.setId(id);
        //将信息添加至物流信息表
        Logistics logistics = new Logistics();
        Base base = baseService.findById(order.getBaseId());
        order.setAdultShrimpId(id);
        logistics.setAdultShrimpId(id);
        logistics.setDepartureAddr(base.getBaseAddr());
        logistics.setLogisticsStartLatitude(base.getBasePositionLatitude());
        logistics.setLogisticsStartLongitude(base.getBasePositionLongitude());
        logistics.setArrivalAddr(order.getReceiptAddress());
        logistics.setLogisticsArrivalLongitude(order.getAddressLongitude());
        logistics.setLogisticsArrivalLatitude(order.getAddressLatitude());
        logistics.setDepartureTime(order.getCreateDate());
        logistics.setLogisticsStatus("0");
        String logisticsId = idWorker.nextId() + "";
        logistics.setId(logisticsId);
        logistics.setBaseId(order.getBaseId());

        order.setLogisticsId(logisticsId);

        //修改虾苗剩余量
        Shrimp shrimp = shrimpService.findById(order.getShrimpId());
        shrimp.setRemain(shrimp.getRemain() - order.getWeight());

        String orderId = idWorker.nextId() + "";
        order.setId(orderId);
        logistics.setOrderId(orderId);

        shrimpService.update(shrimp);
        logisticsService.add(logistics);
        adultShrimpService.add(adultShrimp);
        orderService.add(order);
        return new Result(true, StatusCode.OK, "增加成功");
    }

    /**
     * 修改
     *
     * @param order
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public Result update(@RequestBody Order order, @PathVariable String id, @RequestParam(value = "oldNum") double oldNum) {
        AdultShrimp adultShrimp = adultShrimpService.findById(order.getAdultShrimpId());
        Logistics logistics = logisticsService.findById(order.getLogisticsId());
        Shrimp shrimp = shrimpService.findById(order.getShrimpId());

        shrimp.setRemain(shrimp.getRemain() + oldNum - order.getWeight());

        adultShrimp.setBaseId(order.getBaseId());
        adultShrimp.setShrimpId(order.getShrimpId());
        adultShrimp.setAdultShrimpWeight(order.getWeight());

        logistics.setArrivalAddr(order.getReceiptAddress());
        logistics.setLogisticsArrivalLongitude(order.getAddressLongitude());
        logistics.setLogisticsArrivalLatitude(order.getAddressLatitude());
        logistics.setDepartureTime(order.getCreateDate());

        order.setId(id);
        shrimpService.update(shrimp);
        adultShrimpService.update(adultShrimp);
        logisticsService.update(logistics);
        orderService.update(order);

        return new Result(true, StatusCode.OK, "修改成功");
    }

    /**
     * 删除
     *
     * @param id
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public Result delete(@PathVariable String id) {
        orderService.deleteById(id);
        return new Result(true, StatusCode.OK, "删除成功");
    }

}
