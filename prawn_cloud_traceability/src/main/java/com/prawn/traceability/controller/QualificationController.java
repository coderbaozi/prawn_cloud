package com.prawn.traceability.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.prawn.traceability.pojo.Plant;
import com.prawn.traceability.service.PlantService;
import com.prawn.traceability.util.FastDFSUtil;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import com.prawn.traceability.pojo.Qualification;
import com.prawn.traceability.service.QualificationService;

import entity.PageResult;
import entity.Result;
import entity.StatusCode;
import org.springframework.web.multipart.MultipartFile;
import util.IdWorker;

/**
 * 控制器层
 *
 * @author Administrator
 */
@RestController
@CrossOrigin
@RequestMapping("/qualification")
public class QualificationController {

    private final QualificationService qualificationService;

    private final FastDFSUtil fastDFSUtil;

    private final RabbitTemplate rabbitTemplate;

    private final IdWorker idWorker;

    private final PlantService plantService;

    @Autowired
    public QualificationController(IdWorker idWorker, PlantService plantService, QualificationService qualificationService, FastDFSUtil fastDFSUtil, RabbitTemplate rabbitTemplate) {
        this.idWorker = idWorker;
        this.plantService = plantService;
        this.qualificationService = qualificationService;
        this.fastDFSUtil = fastDFSUtil;
        this.rabbitTemplate = rabbitTemplate;
    }

    /**
     * 手机端添加产品资质
     * @param qualification
     * @param inspectionReport
     * @param qualityCertificate
     * @param safetyCertificate
     * @param license
     * @param standard
     * @param plantId
     * @return
     */
    @RequestMapping(value = "/mobileAdd",method = RequestMethod.POST)
    public Result mobileAdd(Qualification qualification,
                            @RequestParam(value = "inspection") MultipartFile inspectionReport,
                            @RequestParam(value = "quality") MultipartFile qualityCertificate,
                            @RequestParam(value = "safety") MultipartFile safetyCertificate,
                            @RequestParam(value = "licensePic") MultipartFile license,
                            @RequestParam(value = "standardPic") MultipartFile standard,
                            @RequestParam(value = "plantId") String plantId){
        Plant plant = plantService.findById(plantId);
        String qualificationId = idWorker.nextId()+"";
        plant.setQualificationId(qualificationId);
        qualification.setId(qualificationId);

        Qualification qualification1 = new Qualification();
        try {
            if (inspectionReport != null) {
                String inspectionUrl = fastDFSUtil.uploadFile(inspectionReport);
                qualification.setInspectionReport(inspectionUrl);
                qualification1.setInspectionReport(inspectionUrl);
            }
            if (qualityCertificate != null) {
                String qualityCertificateUrl = fastDFSUtil.uploadFile(qualityCertificate);
                qualification.setQualityCertificate(qualityCertificateUrl);
                qualification1.setQualityCertificate(qualityCertificateUrl);
            }
            if (safetyCertificate != null) {
                String safetyCertificateUrl = fastDFSUtil.uploadFile(safetyCertificate);
                qualification.setSafetyCertificate(safetyCertificateUrl);
                qualification1.setSafetyCertificate(safetyCertificateUrl);
            }
            if (license != null) {
                String licenseUrl = fastDFSUtil.uploadFile(license);
                qualification.setLicense(licenseUrl);
                qualification1.setLicense(licenseUrl);
            }
            if (standard != null) {
                String standardUrl = fastDFSUtil.uploadFile(standard);
                qualification.setStandard(standardUrl);
                qualification1.setStandard(standardUrl);
            }
        } catch (IOException e) {
            rabbitTemplate.convertAndSend("del_pic", qualification1);
            return new Result(false, StatusCode.UPLOADERROT, "上传图片失败");
        }
        qualification1 = null;
        qualificationService.add(qualification);
        plantService.update(plant);
        return new Result(true, StatusCode.OK, "增加成功");
    }

    /**
     * 查询全部数据
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public Result findAll() {
        return new Result(true, StatusCode.OK, "查询成功", qualificationService.findAll());
    }

    /**
     * 根据ID查询
     *
     * @param id ID
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Result findById(@PathVariable String id) {
        return new Result(true, StatusCode.OK, "查询成功", qualificationService.findById(id));
    }


    /**
     * 分页+多条件查询
     *
     * @param searchMap 查询条件封装
     * @param page      页码
     * @param size      页大小
     * @return 分页结果
     */
    @RequestMapping(value = "/search/{page}/{size}", method = RequestMethod.POST)
    public Result findSearch(@RequestBody Map searchMap, @PathVariable int page, @PathVariable int size) {
        Page<Qualification> pageList = qualificationService.findSearch(searchMap, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Qualification>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 根据条件查询
     *
     * @param searchMap
     * @return
     */
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public Result findSearch(@RequestBody Map searchMap) {
        return new Result(true, StatusCode.OK, "查询成功", qualificationService.findSearch(searchMap));
    }

    /**
     * 增加
     *
     * @param qualification
     */
    @RequestMapping(method = RequestMethod.POST)
    public Result add(Qualification qualification,
                      @RequestParam(value = "inspection") MultipartFile inspectionReport,
                      @RequestParam(value = "quality") MultipartFile qualityCertificate,
                      @RequestParam(value = "safety") MultipartFile safetyCertificate,
                      @RequestParam(value = "licensePic") MultipartFile license,
                      @RequestParam(value = "standardPic") MultipartFile standard) {
        Qualification qualification1 = new Qualification();
        try {
            if (inspectionReport != null) {
                String inspectionUrl = fastDFSUtil.uploadFile(inspectionReport);
                qualification.setInspectionReport(inspectionUrl);
                qualification1.setInspectionReport(inspectionUrl);
            }
            if (qualityCertificate != null) {
                String qualityCertificateUrl = fastDFSUtil.uploadFile(qualityCertificate);
                qualification.setQualityCertificate(qualityCertificateUrl);
                qualification1.setQualityCertificate(qualityCertificateUrl);
            }
            if (safetyCertificate != null) {
                String safetyCertificateUrl = fastDFSUtil.uploadFile(safetyCertificate);
                qualification.setSafetyCertificate(safetyCertificateUrl);
                qualification1.setSafetyCertificate(safetyCertificateUrl);
            }
            if (license != null) {
                String licenseUrl = fastDFSUtil.uploadFile(license);
                qualification.setLicense(licenseUrl);
                qualification1.setLicense(licenseUrl);
            }
            if (standard != null) {
                String standardUrl = fastDFSUtil.uploadFile(standard);
                qualification.setStandard(standardUrl);
                qualification1.setStandard(standardUrl);
            }
        } catch (IOException e) {
            rabbitTemplate.convertAndSend("del_pic", qualification1);
            return new Result(false, StatusCode.UPLOADERROT, "上传图片失败");
        }
        qualification1 = null;
        qualificationService.add(qualification);
        return new Result(true, StatusCode.OK, "增加成功");
    }

    /**
     * 修改
     *
     * @param qualification
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public Result update(Qualification qualification, @PathVariable String id,
                         @RequestParam(value = "inspection") MultipartFile inspectionReport,
                         @RequestParam(value = "quality") MultipartFile qualityCertificate,
                         @RequestParam(value = "safety") MultipartFile safetyCertificate,
                         @RequestParam(value = "licensePic") MultipartFile license,
                         @RequestParam(value = "standardPic") MultipartFile standard) {
        qualification.setId(id);
        Qualification oldQual = qualificationService.findById(id);
        rabbitTemplate.convertAndSend("del_pic", oldQual);
        try {
            if (inspectionReport != null) {
                String inspectionUrl = fastDFSUtil.uploadFile(inspectionReport);
                qualification.setInspectionReport(inspectionUrl);
            }
            if (qualityCertificate != null) {
                String qualityCertificateUrl = fastDFSUtil.uploadFile(qualityCertificate);
                qualification.setQualityCertificate(qualityCertificateUrl);
            }
            if (safetyCertificate != null) {
                String safetyCertificateUrl = fastDFSUtil.uploadFile(safetyCertificate);
                qualification.setSafetyCertificate(safetyCertificateUrl);
            }
            if (license != null) {
                String licenseUrl = fastDFSUtil.uploadFile(license);
                qualification.setLicense(licenseUrl);
            }
            if (standard != null) {
                String standardUrl = fastDFSUtil.uploadFile(standard);
                qualification.setStandard(standardUrl);
            }
        } catch (IOException e) {
            return new Result(false, StatusCode.ERROR, "上传图片失败");
        }
        qualificationService.update(qualification);
        return new Result(true, StatusCode.OK, "修改成功");
    }

    /**
     * 删除
     *
     * @param id
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public Result delete(@PathVariable String id) {
        Qualification oldQual = qualificationService.findById(id);
        rabbitTemplate.convertAndSend("del_pic", oldQual);
        qualificationService.deleteById(id);
        return new Result(true, StatusCode.OK, "删除成功");
    }

}
