package com.prawn.traceability.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.prawn.traceability.pojo.Shrimpkind;
import com.prawn.traceability.service.ShrimpkindService;
import com.prawn.traceability.pojo.Pond;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import com.prawn.traceability.service.PondService;

import entity.PageResult;
import entity.Result;
import entity.StatusCode;

import javax.websocket.server.PathParam;

/**
 * 控制器层
 *
 * @author Administrator
 */
@RestController
@CrossOrigin
@RequestMapping("/pond")
public class PondController {

    @Autowired
    private PondService pondService;

    @Autowired
    private ShrimpkindService shrimpkindService;

    /**
     * 获得养殖场池塘面积和池塘数量
     *
     * @param baseId
     * @return
     */
    @RequestMapping(value = "/volume/{baseId}", method = RequestMethod.GET)
    public Map<String, Integer> getVolumeSumByBaseId(@PathVariable String baseId) {
        return pondService.getVolumeAndPondNum(baseId);
    }

    //捕捞
    @RequestMapping(value = "/catch/{yield}/{people}", method = RequestMethod.PUT)
    public Result catchShrimp(@RequestBody Pond pond, @PathVariable Double yield, @PathVariable String people, @PathParam(value = "specification") String specification) {
        pondService.catchShrimp(pond, yield, people, specification);
        return new Result(true, StatusCode.OK, "捕捞成功");
    }

    //投放虾苗
    @RequestMapping(value = "/drop", method = RequestMethod.PUT)
    public Result dropShrimp(@RequestBody Pond pond) {
        pondService.dropShrimp(pond);
        return new Result(true, StatusCode.OK, "投放成功");
    }

    //密度推荐
    @RequestMapping(value = "/density/{area}", method = RequestMethod.GET)
    public Result densityRecommend(@PathVariable Float area) {
        List<Shrimpkind> shrimpkinds = shrimpkindService.findAll();
        for (Shrimpkind kind : shrimpkinds) {
            kind.setMinDensity((int) ((float) kind.getMinDensity() * area));
            kind.setMaxDensity((int) ((float) kind.getMaxDensity() * area));
        }
        return new Result(true, StatusCode.OK, "查询成功", shrimpkinds);
    }

//    /**
//     * 获取基地池塘和池塘所养种类
//     * @param searchMap
//     * @param page
//     * @param size
//     * @return
//     */
//    @RequestMapping(value = "/pondAndSpecies/{page}/{size}", method = RequestMethod.GET)
//    public Result getPonds(@RequestBody Map searchMap,@PathVariable int page,@PathVariable int size) {
//        Page<Pond> ponds = pondService.findSearch(searchMap, page, size);
//        List<Pond> content = ponds.getContent();
//        for(Pond curPond:content){
//            shrimpkindService.findById()
//        }
//        return new Result(true, StatusCode.OK, "查询成功");
//    }


    /**
     * 查询全部数据
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public Result findAll() {
        return new Result(true, StatusCode.OK, "查询成功", pondService.findAll());
    }

    /**
     * 根据ID查询
     *
     * @param id ID
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Result findById(@PathVariable String id) {
        return new Result(true, StatusCode.OK, "查询成功", pondService.findById(id));
    }


    /**
     * 分页+多条件查询
     *
     * @param searchMap 查询条件封装
     * @param page      页码
     * @param size      页大小
     * @return 分页结果
     */
    @RequestMapping(value = "/search/{page}/{size}", method = RequestMethod.POST)
    public Result findSearch(@RequestBody Map searchMap, @PathVariable int page, @PathVariable int size) {
        Page<Pond> pageList = pondService.findSearch(searchMap, page, size);
        return new Result(true, StatusCode.OK, "查询成功", new PageResult<Pond>(pageList.getTotalElements(), pageList.getContent()));
    }

    /**
     * 根据条件查询
     *
     * @param searchMap
     * @return
     */
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public Result findSearch(@RequestBody Map searchMap) {
        return new Result(true, StatusCode.OK, "查询成功", pondService.findSearch(searchMap));
    }

    /**
     * 增加
     *
     * @param pond
     */
    @RequestMapping(method = RequestMethod.POST)
    public Result add(@RequestBody Pond pond) {
        pondService.add(pond);
        return new Result(true, StatusCode.OK, "增加成功");
    }

    /**
     * 修改
     *
     * @param pond
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public Result update(@RequestBody Pond pond, @PathVariable String id) {
        pond.setId(id);
        pondService.update(pond);
        return new Result(true, StatusCode.OK, "修改成功");
    }

    /**
     * 删除
     *
     * @param id
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public Result delete(@PathVariable String id) {
        pondService.deleteById(id);
        return new Result(true, StatusCode.OK, "删除成功");
    }

}
