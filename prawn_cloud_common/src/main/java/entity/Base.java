package entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="trac_base")
public class Base implements Serializable{

	@Id
	private String id;//基地编号


	
	private String baseName;//基地名称
	private String baseIntroduction;//基地简介
	private String baseAddr;//基地地址
	private String basePositionLongitude;//基地地址经度
	private String basePositionLatitude;//基地地址纬度
	private String createBy;//创建者
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date createDate;//创建时间
	private String registNumber;//营业执照注册号
	private double funds;//注册资金(万元)
	private String scope;//营业范围
	private String basePic;//基地图片

	public String getRegistNumber() {
		return registNumber;
	}

	public void setRegistNumber(String registNumber) {
		this.registNumber = registNumber;
	}

	public double getFunds() {
		return funds;
	}

	public void setFunds(double funds) {
		this.funds = funds;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getBasePic() {
		return basePic;
	}

	public void setBasePic(String basePic) {
		this.basePic = basePic;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getBaseName() {		
		return baseName;
	}
	public void setBaseName(String baseName) {
		this.baseName = baseName;
	}

	public String getBaseIntroduction() {		
		return baseIntroduction;
	}
	public void setBaseIntroduction(String baseIntroduction) {
		this.baseIntroduction = baseIntroduction;
	}

	public String getBaseAddr() {		
		return baseAddr;
	}
	public void setBaseAddr(String baseAddr) {
		this.baseAddr = baseAddr;
	}

	public String getBasePositionLongitude() {		
		return basePositionLongitude;
	}
	public void setBasePositionLongitude(String basePositionLongitude) {
		this.basePositionLongitude = basePositionLongitude;
	}

	public String getBasePositionLatitude() {		
		return basePositionLatitude;
	}
	public void setBasePositionLatitude(String basePositionLatitude) {
		this.basePositionLatitude = basePositionLatitude;
	}

	public String getCreateBy() {		
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public java.util.Date getCreateDate() {		
		return createDate;
	}
	public void setCreateDate(java.util.Date createDate) {
		this.createDate = createDate;
	}


	
}
