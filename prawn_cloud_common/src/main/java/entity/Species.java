package entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * 实体类
 * @author Administrator
 *
 */
@Entity
public class Species implements Serializable{

	@Id
	private String id;
	private String shrimpSpecies;//对虾种类
	private String inputNum;
	private String yield;



	public String getYield() {
		return yield;
	}

	public void setYield(String yield) {
		this.yield = yield;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getShrimpSpecies() {
		return shrimpSpecies;
	}

	public void setShrimpSpecies(String shrimpSpecies) {
		this.shrimpSpecies = shrimpSpecies;
	}

	public String getInputNum() {
		return inputNum;
	}

	public void setInputNum(String inputNum) {
		this.inputNum = inputNum;
	}



}
